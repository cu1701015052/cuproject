import Cocoa


/*
 編集距離 Levenshtein Distance
 を求める
 ↓
 標準化する。
 
 */

func levenshtein(_ str1: String, _ str2: String) -> Double {

  let n = str1.count
  let m = str2.count
  
  var d: [[Int]] = [[Int]](repeating: [Int](repeating: 0, count: m + 1), count: n + 1)
  
  for i in 0 ... n {
   d[i][0] = i
  }

  for j in 0 ... m {
    d[0][j] = j
  }
  
  for i in 1 ... n {
    for j in 1 ... m {
      let cost = str1[str1.index(str1.startIndex, offsetBy: i - 1)] == str2[str2.index(str2.startIndex, offsetBy: j - 1)] ? 0 : 1
      d[i][j] = min(d[i - 1][j] + 1,
      d[i][j - 1] + 1,
      d[i - 1][j - 1] + cost)
    }
  }

  return Double(d[n][m]) / Double(max(n,m))
}

levenshtein("kitten", "sitting")
levenshtein("あいうえお", "あいうえお")
levenshtein("あいうえお", "かきくけこ")


levenshtein("損害が生じたことが認められる場合において、損害の性質上その額を立証することが極めて困難であるときは、裁判所は、口頭弁論の全趣旨及び証拠調べの結果に基づき、相当な損害額を認定することができる。", "著作権、出版権又は著作隣接権の侵害に係る訴訟において、損害が生じたことが認められる場合において、損害額を立証するために必要な事実を立証することが当該事実の性質上極めて困難であるときは、裁判所は、口頭弁論の全趣旨及び証拠調べの結果に基づき、相当な損害額を認定することができる。")
