//
//  DetailViewControllerMac.swift
//  eLawsMac
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Cocoa

class DetailViewControllerMac: NSViewController {
  
  // VIEW ROUTER
  weak var document: DetailDocument? = nil
  
  // SOURCE
  var viewModel: DetailViewModel! {
    didSet {
      tagViewModel = TagViewModel(viewModel: viewModel)

      if isViewLoaded {
        self.reload()
      }
    }
  }
  var tagViewModel: TagViewModel!

  
  var findDataSource: FindDataSource? {
    didSet {
      viewModel?.source?.clearAllCellHeights()
      tableView.reloadData()
    }
  }
  
  var uuid = shortUUIDString()
  
  // SUB VIEW CONTROLLERS
  
  lazy var headerFunctionViewController = HeaderFunctionViewController()
  lazy var addingBookmarkViewController = AddingBookmarkViewController()

  lazy var notesWindowController = NotesWindowController(windowNibName: "NotesWindowController")
  
  var highlightPopover_: NSPopover? = nil
  
  @IBOutlet var findViewController: FindViewController!
  
  /*  SCROLL LAYER or TABLE VIEW  */
  weak var tableView: (UIView & LawScrollableTableView)! = nil
  var framesetterCache: Dictionary<Int, MNFramesetter>  =  [:]

  /* -------------- */
  var findHighlight: FindHighlightView? = nil
  
  // GESTURES
  weak var pinchGesture_: NSMagnificationGestureRecognizer! = nil
  
  var originalFontSize = StyleSheet.fontSize
  var touchedLocationInView: CGPoint? = nil
  var touchedIndexPath: MyIndexPath? = nil
  var touchedCharacterIndex: Int? = nil
  
  // SELECTION
  weak var topKnobLayer_: SelectionKnobLayer? = nil
  weak var bottomKnobLayer_: SelectionKnobLayer? = nil
  
  var selectionKnobTop_: CGRect? = nil
  var selectionKnobEnclosureTop_: CGRect? = nil
  
  var selectionKnobBottom_: CGRect? = nil
  var selectionKnobEnclosureBottom_: CGRect? = nil
  
  var unmaskingHighlightUuid: String? = nil
  var startedAsLawDescriptor = false

  // CONST
  
  //  let verticalHeightMargin: CGFloat = 100
  let verticalHeightMargin: CGFloat = 0
  
  
  //MARK:- BODY
  convenience init(viewModel: DetailViewModel) {
    self.init()
    self.viewModel = viewModel
    if let law = viewModel.selectedLaw, ArticleCacheManager.shared.realmExists(for: law) != nil {
    }else {
      startedAsLawDescriptor = true
    }
    viewModel.delegate = self
    tagViewModel = TagViewModel(viewModel: viewModel)


    //    self.displayName = viewModel.selectedLaw?.lawTitle
    
    
    //    self.toolbarController = DetailToolbarController()
    //    toolbarController.findClearAction = { [weak self] in self?.findClear() }
    //    toolbarController.findNextAction = { [weak self] in self?.findNext() }
    //    toolbarController.findPreviousAction = { [weak self] in self?.findPrevious() }
    //    toolbarController.goBackAction = { [weak self] in self?.goBack() }
    //    toolbarController.goFowardAction = { [weak self] in self?.goFoward() }
    //    toolbarController.searchAction = { [weak self] sender in self?.search() }
    //    toolbarController.jumpAction = { [weak self] sender in self?.jump(sender) }
    //    toolbarController.mokujiAction = { [weak self] sender in self?.mokuji(sender) }
    //    toolbarController.goForwardIsEnabledAction = { [weak self] in (self?.viewModel?.contentOffsetForwardStack.count ?? 0) > 0  }
    
    
//    CloudKitRepository.shared.hasPrivateAccount { hasAccount in self.viewModel.hasCloudKitAccount = hasAccount }
  }
  //
  
  
  
  deinit {
    tableView?.removeObserver(self, forKeyPath: "contentOffset")
  }
  
  override func viewDidLoad() {
    let frame = self.view.bounds
    
    let tableView: LawScrollView
    tableView = LawScrollView(frame: frame)
    
    tableView.insetsContentViewsToSafeArea = true
    
    tableView.autoresizingMask = [.width, .height]
    tableView.dataSource = self
    tableView.delegate = self
    self.view.addSubview(tableView)
    
    self.tableView = tableView
    
    
    // RESPONDER CHAIN
    
    self.view.window?.nextResponder = self
    self.nextResponder = nil
    
    // GESTURES
    
    let gesture = NSMagnificationGestureRecognizer(target: self, action: #selector(pinch(_:)))
    tableView.addGestureRecognizer(gesture)
    pinchGesture_ = gesture
    
    // TAP PRESS
    //    let tapGesture = NSClickGestureRecognizer(target: self, action: #selector(tapGesture(_:)))
    //    tapGesture.delegate = self
    //    tableView.addGestureRecognizer(tapGesture)
    
    //    let doubleTapGesture = NSClickGestureRecognizer(target: self, action: #selector(doubleTapGesture(_:)))
    //    doubleTapGesture.delegate = self
    //    doubleTapGesture.numberOfClicksRequired = 2
    //    tableView.addGestureRecognizer(doubleTapGesture)
    
    
    // LONG PRESS
    //    let longPressGesture = NSPressGestureRecognizer(target: self, action: #selector(longPress(gesture:)))
    //    longPressGesture.minimumPressDuration = 1.0
    //    longPressGesture.delegate = self
    //    tableView.addGestureRecognizer(longPressGesture)
    
    // DRAGGING KNOB
    let panGesture = MeganeLineGestureRecognizer(target: self, action: #selector(pan(gesture:)))
    panGesture.delegate = self
    tableView.addGestureRecognizer(panGesture)
    
    
    self.tableView.addObserver(self, forKeyPath: "contentOffset", options: .new, context: nil)
    
    let center = NotificationCenter.default
    center.addObserver(self, selector: #selector(realmDidUpdate), name: LawNotification.realmDidUpdateFromExternNotification, object: nil)
    center.addObserver(self, selector: #selector(reload), name: LawNotification.highlightDidChangeExternally, object: nil)
    center.addObserver(self, selector: #selector(reload), name: LawNotification.bookmarkDidChangeExternally, object: nil)
    center.addObserver(self, selector: #selector(reload), name: LawNotification.settingsDidChange, object: nil)
    center.addObserver(self, selector: #selector(didBecomeKey), name: NSWindow.didBecomeKeyNotification, object: nil)

    tableView.postsFrameChangedNotifications = true
    center.addObserver(self, selector: #selector(frameUpdated), name: NSView.frameDidChangeNotification, object: tableView)
    viewWidth = tableView.bounds.size.width
    
//    CloudKitRepository.shared.hasPrivateAccount { hasAccount in self.viewModel?.hasCloudKitAccount = hasAccount }
  }
  
  var viewWidth: CGFloat = 0
  @objc func frameUpdated() {
    
    viewWidth = tableView.bounds.size.width
    self.viewModel?.source?.clearAllCellHeights()
  }
  
  override func encodeRestorableState(with coder: NSCoder) {
    coder.encode(plist(), forKey: "plist")
    super.encodeRestorableState(with: coder)
  }
  
  override func restoreState(with coder: NSCoder) {
    super.restoreState(with: coder)
    guard let plist = coder.decodeObject(forKey: "plist") as? [String: Any] else { return }
    guard let viewModel = DetailViewModel.restoreViewModel(from: plist) else { return }
    self.viewModel = viewModel
  }
  
  func plist() -> [String: Any] {
    guard let viewModel = viewModel else { return [:] }
    let plist: [String: Any] = viewModel.restorationPlist()
    return plist
  }
  
  override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    
    if keyPath != "contentOffset" {
      super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
      return
    }
    
    guard let tableView = tableView else { return }
    
    let contentHeight = tableView.contentSize.height
    let frameHeight = tableView.frame.size.height
    
    if contentHeight == frameHeight { return }
    //if accessoryView!.alpha == 0 { return }
    var attributedText: NSAttributedString? = nil
    
    let cells = tableView.visibleCells
    
    
    for cell in cells {
      if cell.frame.maxY > 20  {
        if let row = tableView.indexPath(for: cell)?.row {
          if row == 0 {
            
          }else {
            attributedText = viewModel.sectionHeaderTitle(forRow: row)
          }
        }
        break
      }
    }
    
    func getOffset() -> DetailViewModel.Offset? {
      guard let cell = tableView.visibleCells.first else { return nil }
      guard let indexPath = tableView.indexPath(for: cell) else { return nil }
      let contentOffsetY = tableView.contentOffset.y
      let dif = contentOffsetY - cell.frame.origin.y
      let offset = DetailViewModel.Offset(indexPath: indexPath, localOffset: dif, destinationLink: nil)
      return offset
    }
    
    let indexPath = MyIndexPath(row: (tableView as! LawScrollView).row, section: 1)
    let offset = DetailViewModel.Offset(indexPath: indexPath, localOffset: (tableView as! LawScrollView).cellOffset, destinationLink: nil)
    viewModel.contentOffsetCache = offset
    
  }
  
  var hasAppeared = false
  
  @objc func didBecomeKey() {
    SingleCellTextView.clearUnmaskedUuids()
    tableView.reloadData()
  }
  
  override func viewWillAppear() {
    hasAppeared = false
    setupViews()
  }
  
  func setupViews() {
    guard viewModel != nil else { return }
    
    if let title = viewModel.selectedLaw?.lawTitle {
      self.document?.displayName = title
      self.document?.windowForSheet?.title = title
    }
    
    _ = viewModel.updateStyleSheet()
    tableView.backgroundColor = viewModel.styleSheet.backgroundColorScheme.backgroundColor
    tableView.contentInset = NSEdgeInsetsMake(viewModel.sectionHeaderViewHeight, 0, 20, 0)
    
    
    if viewModel.source == nil {
      isDownloading = true
      viewModel.progressHandler = { progressMessage in
        
        DispatchQueue.main.async {
          Swift.print(progressMessage)
        }
      }
      
      viewModel.load  { [weak self] error in
        guard let strongSelf = self else { return }
        
        if error != nil {
          displayErrorAlertIfNecessary(error: error)
          
        }else {
          self?.document?.displayName = self?.viewModel.selectedLaw?.lawTitle
        }
        
        if let anchor = strongSelf.viewModel.startupAnchor {
          strongSelf.hasAppearedBlock = {
            
            strongSelf.isDownloading = false
            strongSelf.tableView.reloadData()
            strongSelf.jump(to:  LawXMLTypes.internalLinkPrefix + "/" + anchor, addingHistory: false)
            strongSelf.viewModel.startupAnchor = nil
          }
          
        }else {
          strongSelf.isDownloading = false
          strongSelf.tableView.reloadData()
          strongSelf.contentOffsetDidChange()
        }
        
        if let offset = strongSelf.viewModel.startupContentOffset, let indexPath = offset.indexPath {
          
          if indexPath.row < strongSelf.numberOfRows(in: strongSelf.tableView as! LawScrollView) {
            //            strongSelf.tableView.scrollToRow(at: offset.indexPath!, at: .top, animated: false)
            strongSelf.viewModel.contentOffset = offset
          }
          strongSelf.viewModel.startupContentOffset = nil
          strongSelf.viewModel.hideTitleOnOpening = true
        }
        
        if strongSelf.hasAppeared {
          strongSelf.hasAppearedBlock?()
          strongSelf.hasAppearedBlock = nil
        }
        
        
        if let title = self?.viewModel.selectedLaw?.lawTitle {
          self?.document?.displayName = title
          self?.document?.windowForSheet?.title = title
        }
        
        self?.document?.setupTabs()
      }
    }
    
    if hasAppearedBlock != nil {
      
      hasAppearedBlock?()
      hasAppearedBlock = nil
      
    }else {
      
      
    }
  }
  
  override func viewDidAppear() {
    super.viewDidAppear()
    hasAppeared = true
    hasAppearedBlock?()
    hasAppearedBlock = nil
    
    tableView.window?.makeFirstResponder(self)
    tableView.window?.initialFirstResponder = tableView
  }
  
  var hasAppearedBlock: (()->Void)? = nil
  
  @objc func pinch(_ gesture: NSMagnificationGestureRecognizer) {
    
    let scale = gesture.magnification + 1.0
    
    if gesture.state == .began {
      originalFontSize = StyleSheet.fontSize
      
      let touchedPoint = gesture.location(in: self.view)
      touchedLocationInView = touchedPoint
      let cells = self.tableView.visibleCells
      for cell in cells {
        guard cell is TextViewCell else { continue }
        let cellFrame = self.view.convert(cell.frame, from: self.tableView)
        if cellFrame.origin.y <= touchedPoint.y && touchedPoint.y <= cellFrame.maxY {
          touchedIndexPath = self.tableView.indexPath(for: cell)
          let touchedPointInCell = self.view.convert(touchedPoint, to: cell)
          touchedCharacterIndex = (cell as! TextViewCell).characterIndexAtPointInCell(touchedPointInCell)
          break
        }
      }
    }
    
    if gesture.state == .changed || gesture.state == .ended {
      StyleSheet.fontSize = max( 9, round(originalFontSize * scale))
      _ = viewModel.updateStyleSheet()
      
      if gesture.state == .changed {
        (self.tableView as! LawScrollView).reloadData(recalcEndLimitFlag: false)
        
      }else {
        self.tableView.reloadData()
      }
      
    }
    
    if gesture.state == .changed {
      
      if touchedIndexPath != nil && touchedCharacterIndex != nil && touchedLocationInView != nil, tableView.indexPathsForVisibleRows?.contains(touchedIndexPath!) == true {
        
        if let lastRowIndex = tableView.indexPathsForVisibleRows?.last?.row, viewModel.source != nil, lastRowIndex >= viewModel.source!.count - 10 {
          return // WORKING AROUND FORCE CALCULATION OF ALL THE CELLS
        }
        
        let cells = tableView.visibleCells
        if let cell = cells.filter({ (aCell) -> Bool in
          return (aCell as? TextViewCell)?.indexPath == touchedIndexPath }).first as? TextViewCell{
          
          let offsetY = cell.pointForCharacterIndex(touchedCharacterIndex!)?.y ?? 0
          let targetY = cell.frame.origin.y + offsetY
          let targetOnView = self.tableView.convert(CGPoint(x: 0, y:targetY), to: self.view)
          let difY = targetOnView.y - touchedLocationInView!.y
          
          (self.tableView as! LawScrollView).move(difY)
          
        }
      }
    }
    
    if gesture.state == .ended || gesture.state == .cancelled {
      if gesture.state == .cancelled {
        StyleSheet.fontSize = originalFontSize
      }
      
      //      self.tableView.setNeedsLayout()
      touchedCharacterIndex = nil
      touchedIndexPath = nil
      touchedLocationInView = nil
      self.selectionDidChange()
    }
  }
  
  @objc func reload() {
    _ = viewModel.updateStyleSheet()
    tableView.backgroundColor = viewModel.styleSheet.backgroundColorScheme.backgroundColor
    tableView.contentInset = NSEdgeInsetsMake(viewModel.sectionHeaderViewHeight, 0, 20, 0)
    
    tableView.reloadData()
  }
  
  @objc func realmDidUpdate() {
    Swift.print("realmDidUpdate in detail")
    guard let law = self.viewModel.selectedLaw else { return }
    if startedAsLawDescriptor == false {

      viewModel?.source?.clearAllCellHeights()
      viewModel?.source?.clearAttributedStringCache()
      
      reload()
    }else {
      startedAsLawDescriptor = false
      let nextDownloadedLaw = LawDescriptor(lawTitle: law.lawTitle, lawNum: law.lawNum, lawEdition: "", filename: "", filepath: "", downloadOption: .regular)
      //let currentEdition = self.viewModel.selectedLaw!.lawEdition
      let nextViewModel = DetailViewModel(law: nextDownloadedLaw)
      self.viewModel = nextViewModel
      nextViewModel.delegate = self
      nextViewModel.progressHandler = { progressMessage in
        
        DispatchQueue.main.async {
          //        self.downloadingMessage?.text = progressMessage
          Swift.print(progressMessage)
        }
      }
      
      nextViewModel.load { error in
        
        self.tableView?.reloadData()
        if error != nil {
          displayErrorAlertIfNecessary(error: error)
          return
        }
      }
    }
  }
  
  
  weak var downloadingView: UIView? = nil
  
  var isDownloading = false {
    didSet {
      if isDownloading == false {
        downloadingView?.removeFromSuperview()
        downloadingView = nil
        //        downloadingMessage?.removeFromSuperview()
        //        downloadingMessage = nil
      }else {
        
        if downloadingView == nil {
          let frame = self.tableView.bounds
          let width: CGFloat = 80
          let circular = NSProgressIndicator(frame: CGRect(x:(frame.size.width - width )/2, y:(frame.size.height - width )/2, width: width, height: width))
          circular.isIndeterminate = true
          circular.autoresizingMask = [.minXMargin, .maxXMargin, .minYMargin, .maxYMargin]
          downloadingView = circular
          self.view.addSubview(circular)
          circular.startAnimation(self)
          
        }
      }
    }
  }
  
  
  //MARK:- LINK ACTION
  func jump(to linkString: String, addingHistory: Bool = true) {
    
    guard let trimmedAnchor = viewModel.trimmedAnchor(linkString) else { return }
    
    if trimmedAnchor.hasPrefix("法") {
      
      Swift.print("SHOW \(trimmedAnchor)") // 法昭和三十四年法律第百二十一号/条4/項1/文
      
      if let law = trimmedAnchor.components(separatedBy: "/").first {
        let lawNum = String(law[law.index(law.startIndex, offsetBy: 1)...])
        let mainAnchor = trimmedAnchor.components(separatedBy: "/").dropFirst().joined(separator: "/")
        
        if lawNum == viewModel.selectedLaw?.lawNum {
          if addingHistory {
            viewModel.addHistory()
          }
          focusAnchor(mainAnchor)
          //          updateUI()
          return
        }
        
        if addingHistory {
          viewModel.addHistory(linkString)
        }
        
        
        if let law = LawList().lawsWithNum(lawNum)?.last {
        let nextViewModel = DetailViewModel(law: law)
        nextViewModel.startupAnchor = mainAnchor
        let controller = DetailViewControllerMac(viewModel: nextViewModel)
        self.hasAppearedBlock = {/*dummy not to display jump*/}
        //          self.navigationController?.pushViewController(controller, animated: true)
        self.document?.pushViewController(controller)
        }
      }
      
      ////
      
    }else {
      if addingHistory {
        viewModel.addHistory()
      }
      
      focusAnchor(trimmedAnchor)
      //      updateUI()
    }
  }
  
  enum ScrollPosition {
    case none, center, nearTop
  }
  
  func focusSelectedObject(_ selectedObject: SelectionObject) {
    let anchor = selectedObject.startAnchor
    guard let row = viewModel.source?.findAnchor(anchor) else { return }
    
    
    detectCell(at: row) { [weak self] targetCell in
      guard let targetCell = targetCell else { return }
      guard let strongSelf = self else { return }
      
      if let attributedString = strongSelf.viewModel.source?.attributedString(at: row) {
        targetCell.textView?.setTextStorage(attributedString)
      }
      var keyToFind = NSAttributedString.Key.highlightSerialIdName
      if selectedObject.highlightColor == .mask {
        keyToFind = .mask
      }
      guard let frame = targetCell.rectForAttribute(key: keyToFind, value: selectedObject.uuid) else { return }
      strongSelf.target(visibleCellAt: row, frame: frame, scrollToCenter: .nearTop)
    }
  }
  
  
  func focusAnchor(_ anchor: String) {
    if viewModel.source == nil {
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
        self.focusAnchor(anchor)
      }
      return
    }
    guard let row = viewModel.source?.findAnchor(anchor) else { return }
    
    detectCell(at: row) { [weak self] targetCell in
      guard let targetCell = targetCell else { return }
      guard let strongSelf = self else { return }
      
      if let attributedString = strongSelf.viewModel.source?.attributedString(at: row) {
        targetCell.textView?.setTextStorage(attributedString)
      }
      
      guard let frame = targetCell.rectForAnchor(anchor) else { return }
      strongSelf.target(visibleCellAt: row, frame: frame, scrollToCenter: .nearTop)
    }
  }
  
  @IBAction func copy(_ sender: Any) {
    let attributedString = viewModel.getSelectedString()
    Swift.print(attributedString.string)
    let pasteboard = NSPasteboard.general
    pasteboard.setString(attributedString.string, forType: .string)
    
  }
  
  func adjustedSelectionRangeFor(_ range: SelectionRange, oldRange: SelectionRange) -> SelectionRange {
    var selectedRange_ = range
    let visibleRows = tableView.indexPathsForVisibleRows
    if selectedRange_.isEmpty == false {
      visibleRows?.forEach {
        if $0.row == selectedRange_.start.index {
          
          if let cell = tableView.cellForRow(at: $0) as? TextViewCell  {
            let loc = self.textViewCell(cell, adjustSelectionAt: selectedRange_.start.characterLoc, adjustForward: false)
            selectedRange_.start.characterLoc = loc
          }
        }
        
        if $0.row == selectedRange_.end.index {
          if let cell = tableView.cellForRow(at: $0) as? TextViewCell {
            let loc = self.textViewCell(cell, adjustSelectionAt: selectedRange_.end.characterLoc, adjustForward: true)
            selectedRange_.end.characterLoc = loc
          }
        }
      }
    }
    return selectedRange_
  }
  
  
  
  @IBAction func toggleSourceList(_ sender: Any) {
    document?.toggleSourceList(sender)
  }
  
  @IBAction func toggleContentsList(_ sender: Any) {
    document?.toggleContentsList(sender)
  }
  

  @IBAction func performFindPanelAction(_ sender: Any) {
    guard let source = viewModel?.source else { return }
    
//    findDataSource = FindDataSource(fetchResults: source)
    findViewController.fetchResults = source
    findViewController.findClearAction = {
      
      self.findDataSource = nil
      self.findViewController.view.removeFromSuperview()
      self.tableView.window?.makeFirstResponder(self)
      self.viewModel.source?.clearAllCellHeights()
      self.viewModel.source?.clearAttributedStringCache()
      self.tableView.reloadData()
    }
    findViewController.findNextAction = { self.findNext() }
    findViewController.findPreviousAction = { self.findPrevious() }
    findViewController.focusEnterAction = { findDataSource in
      self.findDataSource = findDataSource
      self.viewModel.source?.clearAllCellHeights()
      self.viewModel.source?.clearAttributedStringCache()
      self.tableView.reloadData()
      self.focusFind()
    }
    
    let view = findViewController.view
    findViewController.findDataSource = self.findDataSource
    
    var frame = view.frame
    frame.size.width = tableView.bounds.size.width
    frame.origin.y = tableView.bounds.maxY - frame.size.height
    view.frame = frame
    self.view.addSubview(view, positioned: .above, relativeTo: self.tableView)
    findViewController.open()
  }
  
  func focusArticle(at row: Int) {
    
    //    if let snapshotView = self.view.snapshotView(afterScreenUpdates: false) {
    //      self.view.addSubview(snapshotView)
    //      self.snapshotView = snapshotView
    //    }
    
    detectCell(at: row) { [weak self] targetCell in
      //      self?.snapshotView?.removeFromSuperview()
      guard let targetCell = targetCell else { return }
      guard let strongSelf = self else { return }
      
      if let attributedString = strongSelf.viewModel.source?.attributedString(at: row) {
        targetCell.textView?.setTextStorage(attributedString)
      }
      strongSelf.target(visibleCellAt: row, frame: targetCell.bounds, scrollToCenter: .nearTop)
    }
  }
  
  
  func focusFind() {
    //guard preciseCellHeights != nil else { return }
    guard findDataSource != nil else { return }
    guard findDataSource!.count > 0 else {
      //      updateUI(false)
      return
    }
    guard findDataSource!.findIndex >= 0 && findDataSource!.findIndex < findDataSource!.count else { return }
    
    //    var snapshotView: UIView? = nil
    //    if let view = self.view.snapshotView(afterScreenUpdates: false) {
    //      self.view.addSubview(view)
    //      snapshotView = view
    //    }
    
    let match: Match = findDataSource!.match
    
    detectCell(at: match.row) { [weak self] targetCell in
      //      snapshotView?.removeFromSuperview()
      guard let targetCell = targetCell else { return }
      guard let strongSelf = self else { return }
      if let attributedString = strongSelf.viewModel.source?.attributedString(at: match.row) {
        targetCell.textView?.setTextStorage(attributedString)
      }
      targetCell.findString = strongSelf.findDataSource!.findString
      
      guard let frame = targetCell.findStringRect(for: match) else { return }
      strongSelf.target(visibleCellAt: match.row, frame: frame)
    }
  }
  
  func findPrevious() {
    guard let findDataSource = findDataSource, findDataSource.count > 0 else { return }
    guard findDataSource.findIndex > 0 else { return }
    findDataSource.findIndex -= 1
    if let visibleIndexPaths = self.tableView.indexPathsForVisibleRows, visibleIndexPaths.count > 0 {
      
      while true {
        let match: Match = findDataSource.match
        if visibleIndexPaths.last != nil && match.row > visibleIndexPaths.last!.row {
          if findDataSource.findIndex == 0 {
            findDataSource.findIndex = findDataSource.count - 1
            focusFind()
            break
          }
          findDataSource.findIndex -= 1
          
        }else {
          focusFind()
          break
        }
      }
    }
  }
  
  func findNext() {
    guard let findDataSource = findDataSource, findDataSource.count > 0 else { return }
    guard findDataSource.findIndex + 1 < findDataSource.count else { return }
    
    findDataSource.findIndex += 1
    if let visibleIndexPaths = self.tableView.indexPathsForVisibleRows, visibleIndexPaths.count > 0 {
      
      while true {
        let match: Match = findDataSource.match
        if match.row < visibleIndexPaths[0].row {
          findDataSource.findIndex += 1
          if findDataSource.findIndex >= findDataSource.count {
            findDataSource.findIndex = 0
            focusFind()
            break
          }
        }else {
          focusFind()
          break
        }
      }
    }
  }
  
  
  @IBAction func performJumpPanelAction(_ sender: Any) {
    
    JumpWindowControllerMac.shared.showWindow(nil)
    
    //    if highlightPopover_?.isShown == true {
    //      self.highlightPopover_?.close()
    //      self.highlightPopover_ = nil
    //    }
    //
    //    let contentViewController = self.jumpViewController
    //    contentViewController.backgroundColorScheme = viewModel.styleSheet.backgroundColorScheme
    //    contentViewController.jumpAction = { text, tabIdentifierOrDocItem, excerptModeDummy in
    //      self.highlightPopover_?.close()
    //      self.highlightPopover_ = nil
    //
    //      guard let text = text else { return }
    //      let processXML = ProcessSentenceXML()
    //      let regularise = processXML.encodeForTOC2(text as NSString)
    //      let anchor = processXML.encodedStringToAnchor(regularise)
    //      self.jump(to: LawXMLTypes.internalLinkPrefix + anchor)
    //    }
    //
    //    contentViewController.cancelBlock = { (swtichToFind: Bool) -> () in
    //
    //      self.highlightPopover_?.close()
    //      self.highlightPopover_ = nil
    //    }
    //
    //    contentViewController.pickerBlock = {}
    //
    //    highlightPopover_ = NSPopover()
    //    highlightPopover_?.appearance = NSAppearance(named: viewModel.styleSheet.backgroundColorScheme.inverted ? .vibrantDark : .vibrantLight)
    //    highlightPopover_?.animates = false
    //    highlightPopover_?.behavior = .transient
    //    highlightPopover_?.contentViewController = contentViewController
    //    highlightPopover_?.contentSize = contentViewController.preferredContentSize
    //    highlightPopover_?.show(relativeTo: self.view.bounds, of: self.view, preferredEdge: NSRectEdge.maxX)
  }
  
  
  
  func target(visibleCellAt row: Int, frame: CGRect, scrollToCenter: ScrollPosition = .center) {
    tableView.reloadData()
    
    let indexPath = MyIndexPath(row: row, section: 1)
    guard let targetCell = self.tableView.cellForRow(at: indexPath) else { return }
    
    func move() {
      
      let indexPath = MyIndexPath(row: row, section: 1)
      guard let targetCell = self.tableView.cellForRow(at: indexPath) else { return }
      
      var highglightFrame = frame
      highglightFrame.origin.y += ceil(targetCell.frame.origin.y) + 1
      highglightFrame = highglightFrame.insetBy(dx: -5.0, dy: -5.0)
      if self.findHighlight == nil {
      }else {
        self.findHighlight?.removeFromSuperview()
      }
      
      let highlight = FindHighlightView.view(with: highglightFrame)
      self.findHighlight = highlight
      self.tableView.addSubview(self.findHighlight!, positioned: .above, relativeTo: tableView)
      self.findHighlight?.highlight(with: highglightFrame)
    }
    
    if scrollToCenter != .none {
      if scrollToCenter == .center || UserDefaults.standard.bool(forKey: "JumpAlignmentOnTop") == false {
        (tableView as! LawScrollView).move(-frame.origin.y-50)
      }
        
      else if scrollToCenter == .nearTop {
        var alignYPosition: CGFloat = 100
        
        if viewModel.isVerticalFullScreen || viewModel.autoHideToolbars {
          alignYPosition = 30
        }else if viewModel.isVertical  {
          alignYPosition = 60
        }
        
        (tableView as! LawScrollView).move(frame.origin.y-(self.tableView.bounds.size.height ) / 2 + alignYPosition)
        
      }
      move()
      
    }else {
      move()
      
    }
  }
  
  func showHeaderFunction(for linkString: String, from targetRect: CGRect, row: Int) {
    Swift.print("showHeaderFunction \(linkString) \(row)")
    
    if highlightPopover_?.isShown == true {
      highlightPopover_?.close()
    }
    
    headerFunctionViewController.calcCosineAction = {[weak self] in
      (self?.viewModel.source as? TFIDFFetchResults)?.calcCosine(row: self!.viewModel.source![row].1)
    }
    
    headerFunctionViewController.calcDoc2vecAction = {[weak self] in
      (self?.viewModel.source as? TFIDFFetchResults)?.calcDoc2vec(row: self!.viewModel.source![row].1)

    }
    
    
    
    //    var menuItem1 = NSMenuItem(title: WLoc("Bookmark"), action: #selector(performBlock(_:)), keyEquivalent: "")
    //    menuItem1.representedObject = {
    //      self.addingBookmarkViewController.bookmarkTitle = bookmarkTitle
    //      self.addingBookmarkViewController.addBookmarkAction = { title in
    //
    //        self.viewModel.addBookmark(for: linkString, title: title, row: row)
    //        self.tableView.reloadData()
    //
    //        self.highlightPopover_?.close()
    //        self.highlightPopover_ = nil
    //      }
    //      self.highlightPopover_?.contentViewController = self.addingBookmarkViewController
    //    }
    //
    //    var menuItem2 = NSMenuItem(title: WLoc("Take Notes"), action: #selector(performBlock(_:)), keyEquivalent: "")
    //    menuItem2.representedObject = {
    //      guard let bookmark = self.viewModel.noteBookmark(for: linkString, row: row) else {
    //        displayErrorAlertIfNecessary(error: LawXMLError.fileError)
    //        return
    //      }
    //
    //      self.openNotes(bookmark)
    //    }
    //
    //    let baseMenu = NSMenu(title: "")
    //    baseMenu.insertItem(menuItem1, at: 0)
    //    baseMenu.insertItem(menuItem2, at: 1)
    //baseMenu.popUp(positioning: nil, at: targetRect.origin, in: tableView)
    highlightPopover_ = NSPopover()
    highlightPopover_?.appearance = NSAppearance(named: viewModel.styleSheet.backgroundColorScheme.inverted ? .vibrantDark : .vibrantLight)
    highlightPopover_?.animates = true
    highlightPopover_?.behavior = .transient
    highlightPopover_?.contentViewController = headerFunctionViewController
    //    highlightPopover_?.contentSize = contentViewController.preferredContentSize
    highlightPopover_?.show(relativeTo: targetRect, of: tableView, preferredEdge: NSRectEdge.minY)
    
  }
  
  
  @objc func performBlock(_ sender: NSMenuItem) {
    guard let block = sender.representedObject as? (()->Void) else { return }
    block()
  }
  
  func headerTapped() {
  }
  
  func openNotes(_ uuid: String) {
    guard let bookmark = self.document?.bookmarkViewModel.bookmark(for: uuid) else { return }
    openNotes(bookmark)
  }
  
  func openNotes(_ bookmark: Bookmark) {
    
    notesWindowController.originalNotes = ""
    notesWindowController.deleteNotes = {
      _ = self.document?.bookmarkViewModel.delete(bookmark: bookmark)
    }
    notesWindowController.setNotes = { text in
      self.document?.bookmarkViewModel.updateBookmark(bookmark, notes: text)
    }
    notesWindowController.originalNotes =  bookmark.notes
    notesWindowController.willCloseWithChange = { withChange in  }
    
    highlightPopover_?.close()
    highlightPopover_ = nil
    
    self.view.window?.beginSheet(notesWindowController.window!, completionHandler: nil)
    
  }
  
  func tappedOnAttribute(key: NSAttributedString.Key, value: Any, at point: CGPoint) {
    let targetRect = CGRect(origin: point, size: CGSize(width:1, height:1)).insetBy(dx: -10, dy: -10)
    
    if key == .tempLink {
      
      if (value as? String)?.hasPrefix(LawXMLTypes.internalLinkPrefix) == true || (value as? String)?.hasPrefix(LawXMLTypes.fetchedLinkPrefix) == true {
        
        // JUMP
        self.jump(to: value as! String)
      }else {
        var row = 0
        for cell in self.tableView.visibleCells {
          if cell.frame.contains(point) && cell is TextViewCell {
            row = (cell as! TextViewCell).indexPath.row
            break
          }
        }
        self.showHeaderFunction(for: value as! String, from: targetRect, row: row)
      }
    }
    else if key == .embeddedAttachement {
      
      if let tableHTML = value as? String {
        
        if tableHTML.hasPrefix("<table") {
          
          let source = viewModel.tableHTML(with: tableHTML)
          
          let tempUrl = URL(fileURLWithPath: NSTemporaryDirectory().appending("table.html"))
          if FileManager.default.fileExists(atPath: tempUrl.path) {
          try? FileManager.default.removeItem(at: tempUrl)
          }
          try? source.write(to: tempUrl, atomically: false, encoding: String.Encoding.utf8)
          NSWorkspace.shared.open(tempUrl)
          
          hasAppearedBlock = {}
          
        }else if tableHTML.isEmpty == false {
          
          if let itemUrl = viewModel.figureUrl(with: tableHTML) {
            
            NSWorkspace.shared.open(itemUrl)
            
            hasAppearedBlock = {}
          }else {
            displayErrorAlertIfNecessary(error: LawXMLError.attachmentDoesNotExists)
          }
        }else {
          displayErrorAlertIfNecessary(error: LawXMLError.attachmentDoesNotExists)
        }
      }
    }
      
    else if key == .highlightSerialIdName, let uuid = value as? String {
      // DO NOT SHOW HIGHLIGHT FOR MASK
      let highlight = RealmManager.shared.selection(with: uuid)
      if let colorStyle = highlight?.style {
        let selectedColor = HighlightColor(rawValue: Int(colorStyle))
        if selectedColor == .mask && unmaskingHighlightUuid != nil { return }
      }
      showHighlightPopoverFor(uuid: uuid, fromRect: targetRect)
    }
      
    else if key == .mask, let uuid = value as? String, SingleCellTextView.unmaskedUuids()?.contains(uuid) == true {
      
      // DO NOT SHOW HIGHLIGHT FOR MASK
      let highlight = RealmManager.shared.selection(with: uuid)
      if let colorStyle = highlight?.style {
        let selectedColor = HighlightColor(rawValue: Int(colorStyle))
        if selectedColor == .mask && unmaskingHighlightUuid != nil { return }
        
      }
      
      showHighlightPopoverFor(uuid: uuid, fromRect: targetRect)
    }
      
    else if key == .controlName && (value as? String)?.hasPrefix(NSAttributedString.Key.ControlType.blockquote) == true {
      let uuid = (value as! NSString).substring(from: (NSAttributedString.Key.ControlType.blockquote as NSString).length)
      openNotes(uuid)
    }
  }
  
  func showHighlightPopoverFor(uuid: String, fromRect targetRect: CGRect) {
    return
  }
  
  func longPressedOnAttribute(key: NSAttributedString.Key, value: String, at point: CGPoint) {
    Swift.print("Long Presed: \(key) - \(value)")
    
    if key == .tempLink && ( value.hasPrefix(LawXMLTypes.internalLinkPrefix) || value.hasPrefix(LawXMLTypes.fetchedLinkPrefix)) {
      
      //      let humanReadable = ProcessSentenceXML.convertLinkToHumanReadable(value)
      //
      //      let alertController = UIAlertController(title: humanReadable, message: nil, preferredStyle: .actionSheet)
      //
      //      if let popover = alertController.popoverPresentationController {
      //        popover.delegate = self
      //        popover.sourceRect = targetRect
      //        popover.sourceView = tableView
      //      }
      //
      //      let action1 = UIAlertAction(title: "リンク先が間違ってる？", style: .default) { (action) -> Void in
      //        self.edit(anchor: value)
      //      }
      //
      //      let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in
      //      }
      //      alertController.addAction(action1)
      //      alertController.addAction(cancel)
      //      present(alertController, animated: true, completion: nil)
    }
      
    else if key == .highlightSerialIdName {
      
    }
  }
  
  var canShowHighlightPopover: Bool {
      return true
  }

  @IBAction func goBack(_ sender: Any) {
    
    // GO BACK
    if false == viewModel.goBack() {
      document?.popViewController()
    }else {
      //updateUI(false)
    }
  }
  
  @IBAction func goForward(_ sender: Any) {
    if let destinationLink = viewModel.goFoward() {
      
      self.jump(to: destinationLink, addingHistory: true)
      //      document?.navigateForward()
      
    }
    //updateUI(false)
  }
  
  @IBAction func jumpTappedFromPanel(_ sender: Any?) {
    let text = JumpWindowControllerMac.shared.inputtedString
    print(text)
    let processXML = ProcessSentenceXML()
    let regularise = processXML.encodeForTOC2(text as NSString)
    let anchor = processXML.encodedStringToAnchor(regularise)
    jump(to: LawXMLTypes.internalLinkPrefix + anchor, addingHistory: true)
  }
  
  @IBAction func zoomIn(_ sender: Any?) {
    var scale: CGFloat = 1.0
    var fontSize: CGFloat = StyleSheet.fontSize
    
    if fontSize < 20 {
      scale = 1
      
    }else {
      scale = 2
    }
    
    
    fontSize = fontSize + scale
    print("fontSize \(fontSize)")
    StyleSheet.fontSize = min(50, max(8, fontSize))
    FontUtils.clearCache()
    //  updateUI()
    NotificationCenter.default.post(name: LawNotification.settingsDidChange, object: self)
    
  }
  
  @IBAction func zoomOut(_ sender: Any?) {
    var scale: CGFloat = 1.0
    var fontSize: CGFloat = StyleSheet.fontSize
    
    
    if fontSize < 17 {
      scale = -1
    }else {
      scale = -2
    }
    
    fontSize = fontSize + scale
    print("fontSize \(fontSize)")
    StyleSheet.fontSize = min(50, max(8, fontSize))
    FontUtils.clearCache()
    //    updateUI()
    NotificationCenter.default.post(name: LawNotification.settingsDidChange, object: self)
    
  }
  
  

  

  override var acceptsFirstResponder: Bool {
    return true
  }
  
  override func becomeFirstResponder() -> Bool {
    return true
  }
  
  override func resignFirstResponder() -> Bool {
    return true
  }
  
  
  override func keyDown(with event: NSEvent)
  {
    if JumpWindowControllerMac.shared.isVisible {
      _ = JumpWindowControllerMac.shared.performKeyEquivalent(with: event)
    }
  }
  
}

extension DetailViewControllerMac: BrowserViewModelDelegate {

  func selectionDidChange() {
    selectionDidChange(asHighlight: false)
  }
  
  func fetchingDidChange(fetching: Bool, hasUpdate: Bool) {
    if fetching == false && hasUpdate {
      reload()
    }
  }
  
  func contentOffsetDidChange() {
    guard let tableView = tableView as? LawScrollView else { return }
    if tableView.row != viewModel.contentOffset.indexPath?.row && tableView.cellOffset != viewModel.contentOffset.localOffset {
      if let row = viewModel.contentOffset.indexPath?.row {
        tableView.row = row
        tableView.cellOffset = viewModel.contentOffset.localOffset!
      }else {
        tableView.row = LawScrollView.headerRow
        tableView.cellOffset = 0
      }
      tableView.move(0)
    }
  }
  
  func unreadDidChange() {
  }
}

extension DetailViewControllerMac: LawScrollViewDelegate {
  func scrollViewWillBeginDragging(_ scrollView: LawScrollView) {

    setKnobsHidden(true)
    topKnobLayer_?.removeAllAnimations()
    bottomKnobLayer_?.removeAllAnimations()
  }
  
  func scrollViewDidEndDragging(_ scrollView: LawScrollView) {
    selectionDidChange()

  }
  
}

extension DetailViewControllerMac: LawScrollViewDataSource {
  
  func setupCell(_ cell: TextViewCell, forRowAt indexPath: MyIndexPath, framesetter: MNFramesetter? = nil) {
    guard let source = viewModel.source else { return }
    cell.indexPath = indexPath
    cell.isVertical = viewModel.isVertical
    
    if framesetter != nil {
      cell.setFramesetter(framesetter)
      let tags = tagViewModel.tags(from: framesetter!.textStorage)
      cell.setTags(tags)
      //      print("----- REUSE FRAMESETTER")
    }else {
      let attributedString = source.attributedString(at: indexPath.row)
      cell.textView?.setTextStorage(attributedString)
      let tags = tagViewModel.tags(from: attributedString)
      cell.setTags(tags)
    }
    cell.styleSheet = viewModel.styleSheet
    cell.findString = findDataSource?.findString

    cell.autoresizingMask = [.maxYMargin, .width]
    _ = updateCellSelection(cell, for: indexPath)
  }
  
  func updateCellSelection(_ cell: TextViewCell, for indexPath: MyIndexPath) -> (start: CGRect?, end: CGRect?) {
    
    var start: CGRect?, end: CGRect?
    
    if viewModel.selectedRange.indexHasSelection(indexPath.row) == false {
      cell.selectedRange = nil
      return (start, end)
    }
    
    if viewModel.selectedRange.start.index == viewModel.selectedRange.end.index {
      
      let range = NSMakeRange(viewModel.selectedRange.start.characterLoc, viewModel.selectedRange.end.characterLoc - viewModel.selectedRange.start.characterLoc)
      cell.selectedRange = range
      
      var charIndex = NSMaxRange(range)
      if charIndex > 0 { charIndex -= 1 }
      
      start = cell.rectForCharacterIndex(range.location)
      end = cell.rectForCharacterIndex(charIndex)
      
      start = tableView.convert(start!, from: cell)
      end = tableView.convert(end!, from: cell)
      
    }else {
      if viewModel.selectedRange.start.index == indexPath.row {
        let range = NSMakeRange(viewModel.selectedRange.start.characterLoc, cell.stringLength - viewModel.selectedRange.start.characterLoc)
        cell.selectedRange = range
        
        start = cell.rectForCharacterIndex(range.location)
        start = tableView.convert(start!, from: cell)
        
        
      }else if viewModel.selectedRange.end.index == indexPath.row {
        
        let range = NSMakeRange(0, viewModel.selectedRange.end.characterLoc)
        
        cell.selectedRange = range
        var charIndex = viewModel.selectedRange.end.characterLoc
        if charIndex > 0 { charIndex -= 1 }
        end = cell.rectForCharacterIndex(charIndex)
        end = tableView.convert(end!, from: cell)
        
      }else {
        cell.selectedRange = NSMakeRange(0, cell.stringLength)
      }
    }
    
    return (start, end)
  }
  
  func updateCellSelectionAsHighlight(_ cell: TextViewCell, for indexPath: MyIndexPath, attributes: [NSAttributedString.Key: Any]) -> (start: CGRect?, end: CGRect?) {
    
    var start: CGRect?, end: CGRect?
    
    if viewModel.selectedRange.indexHasSelection(indexPath.row) == false {
      cell.setTemporaryHighlightRange(nil, attributes: [:])
      return (start, end)
    }
    
    if viewModel.selectedRange.start.index == viewModel.selectedRange.end.index {
      
      let range = NSMakeRange(viewModel.selectedRange.start.characterLoc, viewModel.selectedRange.end.characterLoc - viewModel.selectedRange.start.characterLoc)
      cell.setTemporaryHighlightRange(range, attributes: attributes)
      
      var charIndex = NSMaxRange(range)
      if charIndex > 0 { charIndex -= 1 }
      
      start = cell.rectForCharacterIndex(range.location)
      end = cell.rectForCharacterIndex(charIndex)
      if start == nil || end == nil { return (nil,nil) }
      
      start = tableView.convert(start!, from: cell)
      end = tableView.convert(end!, from: cell)
      
    }else {
      if viewModel.selectedRange.start.index == indexPath.row {
        let range = NSMakeRange(viewModel.selectedRange.start.characterLoc, cell.stringLength - viewModel.selectedRange.start.characterLoc)
        cell.setTemporaryHighlightRange(range, attributes: attributes)
        
        start = cell.rectForCharacterIndex(range.location)
        if start == nil { return (nil,nil) }
        
        start = tableView.convert(start!, from: cell)
        
        
      }else if viewModel.selectedRange.end.index == indexPath.row {
        
        let range = NSMakeRange(0, viewModel.selectedRange.end.characterLoc)
        
        cell.setTemporaryHighlightRange(range, attributes: attributes)
        
        var charIndex = viewModel.selectedRange.end.characterLoc
        if charIndex > 0 { charIndex -= 1 }
        end = cell.rectForCharacterIndex(charIndex)
        
        if end == nil { return (nil,nil) }
        end = tableView.convert(end!, from: cell)
        
      }else {
        cell.setTemporaryHighlightRange(NSMakeRange(0, cell.stringLength), attributes: attributes)
      }
    }
    
    return (start, end)
  }

  
  func numberOfRows(in tableView: LawScrollView) -> Int {
    guard let source = viewModel?.source else { return 0 }
    if isDownloading { return 0 }
    return source.count
  }
  
  func tableView(_ tableView: LawScrollView, cellForRowAt row: Int) -> NSView {
    let height = self.tableView(tableView, heightForRowAt: row)
    
    let framesetter = framesetterCache[row]

    let cell = TextViewCell(frame: CGRect(x: 0, y: 0, width: viewWidth, height: height), styleSheet: viewModel.styleSheet)
    setupCell(cell, forRowAt: MyIndexPath(row: row, section: 1), framesetter: framesetter)
    return cell
  }
  
  func tableView(_ tableView: LawScrollView, estimatedHeightForRowAt row: Int) -> CGFloat {
    guard let source = viewModel.source else { return 0 }
    
    if source.hasPreciseCellHeight(at: row, forWidth: viewWidth) == true {
      let (framesetter, height) = source.preciseCellHeight(at: row, forWidth: viewWidth)
      framesetterCache.removeAll()
      framesetterCache[row] = framesetter

      return height
    }
    
    return source.estimatedCellHeight(at: row, forWidth: viewWidth)
  }
  
  func tableView(_ tableView: LawScrollView, heightForRowAt row: Int) -> CGFloat {
    guard let source = viewModel.source else { return 0 }
    
    let (framesetter, height) = source.preciseCellHeight(at: row, forWidth: viewWidth)
    framesetterCache.removeAll()
    framesetterCache[row] = framesetter

    return height
  }
  
  func headerViewFor(_ tableView: LawScrollView) -> NSView? {
    return nil
  }
  
  func headerHeightFor(_ tableView: LawScrollView) -> CGFloat {
    return 140.0
  }
  
  func textViewCell(_ cell: TextViewCell, adjustSelectionAt charIndex: Int, adjustForward: Bool) -> Int {
    let attrString = cell.textView!.textStorage
    
    if attrString.length <= charIndex { return attrString.length }
    
    var range = NSMakeRange(0,0)
    let value = attrString.attribute(.blockSelectionName, at: charIndex, longestEffectiveRange: &range, in: NSMakeRange(0, attrString.length))
    
    if value == nil { return charIndex }
    
    if adjustForward {
      return NSMaxRange(range)
    }else {
      return range.location
    }
  }
}



extension DetailViewControllerMac: NSGestureRecognizerDelegate {
  
  // SELECTION
  
  func selectionDidChange(asHighlight: Bool = false, attributes: [NSAttributedString.Key: Any]? = nil) {
    var start: CGRect?, end: CGRect?
    let visibleRows = tableView.indexPathsForVisibleRows
    
    visibleRows?.forEach {
      if let cell = tableView.cellForRow(at: $0) as? TextViewCell {
        if asHighlight {
          
          let (s, e) = updateCellSelectionAsHighlight(cell, for: $0, attributes: attributes ?? [:])
          if s != nil { start = s }
          if e != nil { end = e }
          
        }else {
          let (s, e) = updateCellSelection(cell, for: $0)
          if s != nil { start = s }
          if e != nil { end = e }
        }
      }
    }
    
    start?.size.width = 0
    if let max = end?.maxX {
      end?.origin.x = max
    }
    end?.size.width = 0
    
    if asHighlight {
    }else {
      setupKnobs(start: start, end: end )
    }
  }
  
  func setKnobsHidden(_ flag: Bool) {
    if flag == true {
      topKnobLayer_?.opacity = 0
      bottomKnobLayer_?.opacity = 0
      selectionKnobTop_ = nil
      selectionKnobBottom_ = nil
      selectionKnobEnclosureTop_ = nil
      selectionKnobEnclosureBottom_ = nil
      
    }else if viewModel.selectedRange.isEmpty == false {
      
      topKnobLayer_?.opacity = 1
      bottomKnobLayer_?.opacity = 1
    }
    
  }
  
  func setupKnobs(start: CGRect?, end: CGRect?) {
    var start = start
    var end = end
    
    let safetyAreaInsetLeft: CGFloat = 0
    //    if #available(iOS 11.0, *) {
    //      safetyAreaInsetLeft = self.view.safeAreaInsets.left
    //    }
    
    if start != nil {
      start!.origin.x += safetyAreaInsetLeft
    }
    if end != nil {
      end!.origin.x += safetyAreaInsetLeft
    }
    
    let SELECTION_KNOB_INSET_X: CGFloat = -20
    let SELECTION_KNOB_INSET_Y: CGFloat = -20
    let SELECTION_OFFSET: CGFloat = 10
    
    for gesture in tableView.gestureRecognizers {
      if gesture is MeganeLineGestureRecognizer { continue }
      if gesture.state == .began || gesture.state == .changed {
        setKnobsHidden(true)
        return
      }
    }
    
    if start == nil && end == nil {
      setKnobsHidden(true)
      return
    }
    
    if start != nil {
      if topKnobLayer_ == nil {
        let layer = SelectionKnobLayer()
        topKnobLayer_ = layer
        //topKnobLayer_?.backgroundColor = UIColor.red.cgColor
        topKnobLayer_!.contentsScale = tableView.layer!.contentsScale
        topKnobLayer_!.contentsGravity = CALayerContentsGravity.top
        tableView.layer?.addSublayer( topKnobLayer_!)
      }
      topKnobLayer_?.isVertical = viewModel.isVertical
      selectionKnobTop_ = start
      selectionKnobEnclosureTop_ = selectionKnobTop_!.insetBy(dx: SELECTION_KNOB_INSET_X, dy: SELECTION_KNOB_INSET_Y)
      selectionKnobEnclosureTop_!.origin.y -= SELECTION_OFFSET
    }
    
    if end != nil {
      if bottomKnobLayer_ == nil {
        let layer = SelectionKnobLayer()
        bottomKnobLayer_ = layer
        //bottomKnobLayer_?.backgroundColor = UIColor.green.cgColor
        bottomKnobLayer_!.bottom = true
        bottomKnobLayer_!.contentsGravity = CALayerContentsGravity.top
        bottomKnobLayer_!.contentsScale = tableView.layer!.contentsScale
        tableView.layer?.addSublayer( bottomKnobLayer_!)
      }
      bottomKnobLayer_?.isVertical = viewModel.isVertical
      selectionKnobBottom_ = end
      selectionKnobEnclosureBottom_ = selectionKnobBottom_!.insetBy(dx: SELECTION_KNOB_INSET_X, dy: SELECTION_KNOB_INSET_Y)
      selectionKnobEnclosureBottom_!.origin.y += SELECTION_OFFSET
    }
    
    if selectionKnobEnclosureBottom_ != nil && selectionKnobEnclosureTop_?.intersects(selectionKnobEnclosureBottom_!) == true
    {
      let intersection = selectionKnobEnclosureTop_!.intersection(selectionKnobEnclosureBottom_!)
      
      // if knob rect is on the same line, cut width half.  If in different lines, cut height half
      if selectionKnobEnclosureTop_!.origin.y == selectionKnobEnclosureBottom_!.origin.y {
        selectionKnobEnclosureTop_!.size.width -= intersection.size.width/2
        selectionKnobEnclosureBottom_!.size.width -= intersection.size.width/2
        selectionKnobEnclosureBottom_!.origin.x += intersection.size.width/2
        
      }else {
        
        selectionKnobEnclosureBottom_!.origin.y += intersection.size.height/2
        selectionKnobEnclosureBottom_!.size.height -= intersection.size.height/2
        selectionKnobEnclosureTop_!.size.height -= intersection.size.height/2
      }
    }
    
    // Setup layer
    if selectionKnobTop_?.equalTo(CGRect.zero) == false {
      topKnobLayer_!.frame = CGRect( x: selectionKnobTop_!.midX - 8, y: selectionKnobTop_!.minY - 16, width: 16, height: selectionKnobTop_!.size.height + 16).integral
      topKnobLayer_!.setNeedsDisplay()
      topKnobLayer_!.removeAllAnimations()
    }
    
    if selectionKnobBottom_?.equalTo(CGRect.zero) == false {
      bottomKnobLayer_!.frame = CGRect( x: selectionKnobBottom_!.midX - 8, y: selectionKnobBottom_!.minY, width: 16, height: selectionKnobBottom_!.size.height + 16).integral
      bottomKnobLayer_!.setNeedsDisplay()
      bottomKnobLayer_!.removeAllAnimations()
    }
    
    setKnobsHidden(false)
  }
  
  var selectedRect: CGRect? {
    guard let selectionKnobTop_ = selectionKnobTop_ else { return nil }
    return selectionKnobBottom_?.union(selectionKnobTop_)
  }
  
  func doubleTap(_ point: CGPoint) {
    tap(nil)
    for cell in tableView.visibleCells {
      guard let textViewCell = cell as? TextViewCell else { continue }
      guard let indexPath = tableView.indexPath(for: textViewCell) else { continue }
      let frame = textViewCell.frame
      if frame.origin.y <= point.y && frame.maxY >= point.y {
        let pointInCell = tableView.convert(point, to: textViewCell)
        let charIndex = textViewCell.characterIndexAtPointInCell(pointInCell)
        if let p = textViewCell.pointForCharacterIndex(charIndex) {
          self.viewModel.selectWordRange(textViewCell.wordRange(at: p), inRow:  indexPath.row)
        }
        
        break
      }
    }
  }
  
  @objc func tapGesture(_ gesture: NSClickGestureRecognizer) {
    tap( gesture.location(in: self.tableView), tapped: true )
  }
  
  @objc func doubleTapGesture(_ gesture: NSClickGestureRecognizer) {
    doubleTap( gesture.location(in: self.tableView) )
  }
  
  func tap(_ point: CGPoint?, tapped: Bool = false) {
    
    (self.tableView as! LawScrollView).stopScrolling()
    
    if point != nil {
      if selectionKnobEnclosureTop_?.contains(point!) == true || selectionKnobEnclosureBottom_?.contains(point!) == true {
        return
      }
    }
    
    //
    
    var key: NSAttributedString.Key? = nil
    var value: NSObject? = nil
    var tappedCharIndex: Int? = nil
    var tappedTextStorage: NSAttributedString? = nil

    defer {
      
      if tappedCharIndex != nil && tappedTextStorage != nil {
        
        let obj = tappedTextStorage!.attribute(.mask, at: tappedCharIndex!, longestEffectiveRange: nil, in: NSMakeRange(0,tappedTextStorage!.length) )
        
        if let obj = obj as? String, (SingleCellTextView.unmaskedUuids()?.contains(obj) == false || unmaskingHighlightUuid == obj) {
          key = .highlightSerialIdName
          value = obj as NSString
          unmaskingHighlightUuid = obj
        }
      }
      
      if tapped {
        viewModel.selectedRange = SelectionRange()
        if key != nil && value != nil {
          tappedOnAttribute(key: key!, value: value!, at: point!)
        }
        key = nil
        value = nil
      }else {
        if key == .highlightSerialIdName, let value = value as? String {
          if SingleCellTextView.unmaskedUuids()?.contains(value) == false {
            unmaskingHighlightUuid = value
          }else {
            unmaskingHighlightUuid = nil
          }
        }
      }
      for cell in tableView.visibleCells {
        (cell as? TextViewCell)?.textView?.setTouchHighlightKey(key?.rawValue, andValue: value)
      }
    }
    
    for cell in tableView.visibleCells {
      
      let frame = cell.frame
      
      guard let point = point else { continue }
      guard frame.origin.y <= point.y && frame.maxY >= point.y else { continue }
      
      if tableView.indexPath(for: cell)?.section == 0 {
        headerTapped()
        return
      }
      
      let pointInCell = tableView.convert(point, to: cell)
      guard let charIndex = (cell as? TextViewCell)?.characterIndexAtPointInCell(pointInCell) else { return }
      guard let textStorage = (cell as? TextViewCell)?.textView?.textStorage else { return }
      guard charIndex < textStorage.length else { return }
      
      var effectiveRange: NSRange = NSMakeRange(0,0)
      let clickable = viewModel.clickableAttributes
      for attributeName in clickable {
        if let obj = textStorage.attribute(attributeName,
                                           at: charIndex,
                                           longestEffectiveRange: &effectiveRange,
                                           in: NSMakeRange(0,textStorage.length))  {
          key = attributeName
          value = obj as? NSObject
          tappedCharIndex = charIndex
          tappedTextStorage = textStorage
          break
        }
        
        if charIndex > 0, let obj = textStorage.attribute(attributeName,
                                                          at: charIndex - 1,
                                                          longestEffectiveRange: &effectiveRange,
                                                          in: NSMakeRange(0,textStorage.length)), effectiveRange.length < 3  {
          key = attributeName
          value = obj as? NSObject
          tappedCharIndex = charIndex
          tappedTextStorage = textStorage
          break
        }
      }
      
      #if DEBUG
        let anchor = textStorage.attribute(.anchor,
                                           at: charIndex,
                                           longestEffectiveRange: nil,
                                           in: NSMakeRange(0,textStorage.length))
        Swift.print("+ Clicked on \(String(describing: anchor))")
      #endif
    }
    
    // DEFER
  }
  
  //  @objc func longPress(gesture: NSPressGestureRecognizer) {
  //    let megane = viewModel.isVertical ? MyMeganeLineView.shared() : MeganeLineView.shared()
  //
  //    switch gesture.state {
  //    case .cancelled, .failed, .ended:
  //      megane.isHidden = true
  //
  //      self.selectionDidChange()
  //      return
  //    case .began:
  //      tap(nil)
  //      setupKnobs(start: nil, end: nil)
  //      fallthrough
  //    default:
  //
  //      if megane.isHidden == true, let (key, value) = canHandleLongPress(gesture.location(in: tableView)) {
  //        longPressedOnAttribute(key: key, value: value, at: gesture.location(in: tableView))
  //        return
  //      }
  //
  //      let targetView: UIView = tableView.window!
  //
  //      let point = gesture.location(in: tableView)
  //      for cell in tableView.visibleCells {
  //        guard let textViewCell = cell as? TextViewCell else { continue }
  //        guard let indexPath = tableView.indexPath(for: textViewCell) else { continue }
  //        let frame = textViewCell.frame
  //        if frame.origin.y <= point.y && frame.maxY >= point.y {
  //          let pointInCell = tableView.convert(point, to: textViewCell)
  //          var adjustedPoint = gesture.location(in: targetView)
  //          let charIndex = textViewCell.characterIndexAtPointInCell(pointInCell)
  //          if let p = textViewCell.pointForCharacterIndex(charIndex) {
  //            adjustedPoint = textViewCell.convert(p, to: self.view.window)
  //
  //            self.viewModel.selectWordRange(textViewCell.wordRange(at: p), inRow:  indexPath.row)
  //          }
  //
  //          let pointInWindow = gesture.location(in: targetView)
  //          adjustedPoint.x = pointInWindow.x
  //
  //          if viewModel.isVertical {
  //            (megane as! MyMeganeLineView).interfaceOrientation = self.view.window!.rootViewController!.interfaceOrientation
  //
  //            (megane as! MyMeganeLineView).window = self.view.window
  //            (megane as! MyMeganeLineView).focusCenter = adjustedPoint
  //          }else {
  //            (megane as! MeganeLineView).interfaceOrientation = self.view.window!.rootViewController!.interfaceOrientation
  //
  //            (megane as! MeganeLineView).window = self.view.window
  //            (megane as! MeganeLineView).focusCenter = adjustedPoint
  //          }
  //          megane.setNeedsDisplay()
  //          megane.isHidden = false
  //
  //          break
  //        }
  //      }
  //    }
  //  }
  
  @objc func pan(gesture: MeganeLineGestureRecognizer) {
    //    guard let megane = MyMeganeLineView.shared() else { return }
    //    megane.isVertical = viewModel.isVertical
    
    var touchedPoint = gesture.location(in: tableView)
    
    if gesture.state == .cancelled || gesture.state == .failed || gesture.state == .ended {
      //
      //      // Estend to word rect
      //
      //      /*
      //       if false == gesture.staticEnd {
      //       if gesture.bottom {
      //       let lastIndex = selectedRange.end.characterLoc + 1
      //
      //       IndexedPosition * lastPosition = [IndexedPosition positionWithIndex:lastIndex inAttributedString:attributedString_];
      //
      //       IndexedRange *range = (IndexedRange*)[tokenizer_ rangeEnclosingPosition: lastPosition withGranularity:UITextGranularityWord  inDirection:UITextStorageDirectionBackward];
      //
      //       if( range )
      //       selectedTextNSRange_ = NSUnionRange(selectedTextNSRange_, range.range);
      //
      //       }else  {
      //       IndexedPosition * position = [IndexedPosition positionWithIndex:selectedTextNSRange_.location inAttributedString:attributedString_];
      //
      //       IndexedRange *range = (IndexedRange*)[tokenizer_ rangeEnclosingPosition: position withGranularity:UITextGranularityWord  inDirection:UITextStorageDirectionBackward];
      //
      //       if range {
      //       selectedTextNSRange_ = NSUnionRange(selectedTextNSRange_, range.range)
      //       }
      //       }
      //       }
      //       */
      //
      //      megane.isHidden = true
      setKnobsHidden(false)
      
      tap( touchedPoint, tapped: true )
      return
    }
    
    
    if gesture.state == .began {
      gesture.offset = CGSize.zero
      
      if selectionKnobEnclosureTop_?.contains(touchedPoint) == true {
        gesture.draggingType = .top
        gesture.offset = CGSize(width: 0, height: touchedPoint.y - selectionKnobTop_!.midY)
      }
      else if selectionKnobEnclosureBottom_?.contains(touchedPoint) == true {
        gesture.draggingType = .bottom
        gesture.offset = CGSize(width: 0, height: touchedPoint.y - selectionKnobBottom_!.midY)
      }else {
        gesture.draggingType = .free
        gesture.offset = .zero
        
        for cell in tableView.visibleCells {
          guard let textViewCell = cell as? TextViewCell else { continue }
          guard let _ = tableView.indexPath(for: textViewCell) else { continue }
          let frame = textViewCell.frame
          if frame.origin.y <= touchedPoint.y && frame.maxY >= touchedPoint.y {
            let pointInCell = tableView.convert(touchedPoint, to: textViewCell)
            let charIndex = textViewCell.characterIndexAtPointInCell(pointInCell)
            if let _ = textViewCell.pointForCharacterIndex(charIndex) {
              
              let newStartLocation = SelectionRange.Location(index: textViewCell.indexPath.row, characterLoc: charIndex)
              
              viewModel.selectedRange = SelectionRange(start: newStartLocation, end: newStartLocation)
              
              gesture.draggingType = .bottom
              gesture.offset = .zero
              
            }
            
            break
          }
        }
        
        setKnobsHidden(viewModel.selectedRange.isEmpty)
        tap( touchedPoint, tapped: false )
      }
      
      Swift.print("gesture.offset \(gesture.offset)")
      
      
    }
    
    touchedPoint.y -= gesture.offset.height
    
    for cell in tableView.visibleCells {
      guard let cell = cell as? TextViewCell else { continue }
      let frame = cell.frame
      if frame.origin.y <= touchedPoint.y && frame.maxY >= touchedPoint.y {
        let pointInCell = tableView.convert(touchedPoint, to: cell)
        let charIndex = cell.characterIndexAtPointInCell(pointInCell)
        
        if let row = tableView.indexPath(for: cell)?.row {
          
          if gesture.draggingType == .top {
            let newStartLocation = SelectionRange.Location(index: row, characterLoc: charIndex)
            if viewModel.selectedRange.end.isGreaterThan(newStartLocation) {
              viewModel.selectedRange = SelectionRange(start: newStartLocation, end: viewModel.selectedRange.end)
            }
          }else if gesture.draggingType == .bottom {
            let newEndLocation = SelectionRange.Location(index: row, characterLoc: charIndex)
            if newEndLocation.isGreaterThan(viewModel.selectedRange.start) {
              viewModel.selectedRange = SelectionRange(start: viewModel.selectedRange.start, end: newEndLocation)
            }
          }else {
            
          }
          break
        }
      }
    }
    
    
    
    
    //    if selectionKnobTop_ != nil && selectionKnobBottom_ != nil {
    //      let origin = gesture.bottom ? CGPoint(x: selectionKnobBottom_!.origin.x, y: selectionKnobBottom_!.midY) :  CGPoint(x: selectionKnobTop_!.origin.x, y: selectionKnobTop_!.midY)
    //        megane.window = self.view.window
    //        megane.focusCenter = tableView.convert(origin, to: self.view)
    ////        (megane as! MyMeganeLineView).setNeedsDisplay(<#T##invalidRect: NSRect##NSRect#>)
    //        megane.isHidden = false
    //
    //    }
  }
  
  
  //MARK:- CELL
  
  
  func estimatedCellOriginAtRow(_ row: Int) -> CGFloat? {
    //guard preciseCellHeights != nil else { return nil }
    //guard row < preciseCellHeights.count else { return nil }
    guard let source = viewModel.source else { return nil }
    let safetyAreaWidth = tableView.bounds.size.width
    
    var originY: CGFloat = 0
    for n in 0 ..< row {
      if source.hasPreciseCellHeight(at: n, forWidth: safetyAreaWidth) == true {
        originY += source.preciseCellHeight(at: n, forWidth: safetyAreaWidth).1
      }else {
        originY += source.estimatedCellHeight(at: n, forWidth: safetyAreaWidth)
      }
    }
    return originY
  }
  
  func detectCell(at row: Int, targetOffsetY: CGFloat? = nil, completion: @escaping (_ cell: TextViewCell?)->Void) {
    
    
    (tableView as! LawScrollView).find(row: row)
    let cell = (tableView as! LawScrollView).cellForRow(at: MyIndexPath(row: row, section: 1))
    
    completion(cell as? TextViewCell)
  }
}

//MARK:- TF-IDF

extension DetailViewControllerMac {


}

