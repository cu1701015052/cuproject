//
//  AppDelegate.swift
//  eLawsMac
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Cocoa
import RealmSwift
//import DiffMatchPatch


extension FileManager {
  var appSupportUrl: URL {
    let url = FileManager.default.urls(for: .applicationSupportDirectory,  in: .userDomainMask)[0]
    return url.appendingPathComponent("jp.ac.cyber-u.1701015052")
  }
  
}

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
  
  @IBOutlet weak var window: NSWindow!
  
  func applicationWillFinishLaunching(_ notification: Notification) {
  }
  
  func applicationDidFinishLaunching(_ aNotification: Notification) {
    // Insert code here to initialize your application
    NSPasteboard.general.declareTypes([.string], owner: self)
  }
  
  func applicationWillTerminate(_ aNotification: Notification) {
    // Insert code here to tear down your application
  }
  
  func applicationWillResignActive(_ notification: Notification) {

  }
  
  @IBAction func openXMLDocument(_ sender: Any) {
    //    newDocument(sender)
    //    return
    let openPanel = NSOpenPanel()
    openPanel.message = "イーローズXMLを開きます"
    NSDocumentController.shared.beginOpenPanel(openPanel, forTypes: ["public.xml"]) { [weak openPanel] (results) in
      if results == NSApplication.ModalResponse.OK.rawValue, let url = openPanel?.url {
        NSDocumentController.shared.openDocument(withContentsOf: url, display: true, completionHandler: { (doc, flag, error) in
          
        })
      }
    }
  }
  
  @IBAction func newDocument(_ sender: Any) {
    do {
      let doc = try NSDocumentController.shared.makeUntitledDocument(ofType: "jp.ac.cyber-u.1701015052.cuproject")
      NSDocumentController.shared.addDocument(doc as! DetailDocument)
      doc.makeWindowControllers()
      //    (doc as! DetailDocument).viewModel = viewModel
      //    (doc as! DetailDocument).documentUuid = organizable.uuid
      
      doc.showWindows()
    }catch let error {
      displayErrorAlertIfNecessary(error: error)
    }
  }
  
  @IBAction func parseXml(_ sender: Any) {
    parse()
  }
  
  func calcParagraphBasedTfidf() {
    let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
    queue.async {
      
      let dbUrl = self.outputUrl.appendingPathComponent("paragraph_tfidf.realm")
      let config = Realm.Configuration(fileURL: dbUrl, readOnly: false, objectTypes:[RowEGovLaw.self])
      let realm = try! Realm(configuration: config)
      
      let allObj = realm.objects(RowEGovLaw.self)
      let tfidf = Tfidf()
      try! realm.write {
        let count = allObj.count
        for n in 0..<count {
          autoreleasepool {
            let law = allObj[n]
            let name = law.lawNum + (law.anchors.first ?? "")
            tfidf.add(document: law.text, with: name, doc: law)
            if n/100 * 100 == n {
              DispatchQueue.main.async {
                self.buildLinkLabel.stringValue = "\(n)/\(count)"
              }
            }
          }
        }
      }
      
      try! realm.write {
        tfidf.calcTfidfValues(regularize: true)
      }
      DispatchQueue.main.async {
        self.buildLinkLabel.stringValue = "** Finished"
      }
    }
  }
  
  @IBAction func calcDocumentBasedTfidf(_ sender: Any) {
    
    DispatchQueue.concurrentPerform(iterations: 2)  { (i:size_t) in
      if i == 1 {
        calcDocumentBasedTfidf()
      }else {
        calcParagraphBasedTfidf()
      }
      
    }
    
  }
  
  func calcDocumentBasedTfidf() {
    
    // PARSE XML
    let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
    queue.async {
      
      let dbUrl = self.outputUrl.appendingPathComponent("index.realm")
      let config = Realm.Configuration(fileURL: dbUrl, readOnly: false, objectTypes:[EGovLaw.self])
      let realm = try! Realm(configuration: config)
      
      let allObj = realm.objects(EGovLaw.self)
      let tfidf = Tfidf()
      try! realm.write {
        let count = allObj.count
        for n in 0..<count {
          autoreleasepool {
            let law = allObj[n]
            tfidf.add(document: law.text, with: law.lawNum, doc: law)
            if n/100 * 100 == n {
            DispatchQueue.main.async {
             self.buildLinkLabel.stringValue = "\(n)/\(count)"
            }
            }
          }
        }
      }
      
      try! realm.write {
        tfidf.calcTfidfValues(regularize: true)
      }
      DispatchQueue.main.async {
        self.buildLinkLabel.stringValue = "** Finished"
      }
    }
  }
  
  var xmlFolder: URL? = nil
  @IBAction func setXMLFolder(_ sender: Any) {
    
    let panel = NSOpenPanel()
    panel.canChooseFiles = false
    panel.canChooseDirectories = true
    panel.allowsMultipleSelection = false
    panel.message = "Choose a folder that contains xml files"
    panel.beginSheetModal(for: window) { (response) in
      guard let url = panel.url else { return }
      self.xmlFolder = url
    }
    
  }
  
  
  var outputUrl: URL {
    get {
      let url = FileManager.default.urls(for: .desktopDirectory,  in: .userDomainMask)[0].appendingPathComponent("CU Project", isDirectory: true)
      if FileManager.default.fileExists(atPath: url.path) == false {
        try! FileManager.default.createDirectory(at: url, withIntermediateDirectories: false, attributes: nil)
      }
      return url
    }
  }
  
  var queue: DispatchQueue = {
    let queue = DispatchQueue(label: "jp.ac.cyber-u.1701015052.BuildingQueue", qos: .background)
    return queue
  }()
  
  @IBAction func test(_ sender: Any) {
    let tfidf = Tfidf()
    let dbUrl = self.outputUrl.appendingPathComponent("index.realm")
    let config = Realm.Configuration(fileURL: dbUrl, readOnly: false, objectTypes:[EGovLaw.self])
    let realm = try! Realm(configuration: config)
    let allObj = realm.objects(EGovLaw.self)

    if let obj = allObj.first {
      
      for n in 1..<10 {
      let cosine = tfidf.cosine(obj, with: allObj[n])
        print(cosine)
      }
    }
    
  }
  
  
  @IBOutlet weak var buildLinkLabel: NSTextField!
  var startDate: Date!
  var lapDate: Date!
  
  func parse() {

    guard let xmlFolder = xmlFolder else {
      let alert = NSAlert()
      alert.messageText = "Oops"
      alert.informativeText = "Set XML doc folder first"
      alert.alertStyle = .critical
      alert.addButton(withTitle: WLoc("OK"))
      alert.runModal()
      return
    }

    let lawListUrl = Bundle.main.url(forResource: "LawList", withExtension: "xml")!
    let lawListXml = try! String(contentsOf: lawListUrl, encoding: .utf8)
    
    NSLog("** STEP 2: Build Index File **")
    
    startDate = Date()
    
    let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
    queue.async {
      
      let fm = FileManager.default
      
      let contents = try! fm.contentsOfDirectory(at: xmlFolder, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
      
      var count = contents.count
      let dbUrl = self.outputUrl.appendingPathComponent("index.realm")
      let config = Realm.Configuration(fileURL: dbUrl, readOnly: false, objectTypes:[EGovLaw.self])
      
      let paragraphTfidfUrl = self.outputUrl.appendingPathComponent("paragraph_tfidf.realm")
      let paragraphConfig = Realm.Configuration(fileURL: paragraphTfidfUrl, readOnly: false, objectTypes:[RowEGovLaw.self])

      
      DispatchQueue.concurrentPerform(iterations: contents.count)  { (i:size_t) in
        autoreleasepool {
          let url = contents[i]
          let fileName = url.lastPathComponent
          let xmlUrl = url.appendingPathComponent(fileName).appendingPathExtension("xml")
          if fm.fileExists(atPath: xmlUrl.path) == false { count -= 1; return }
          let rows: [Row] = self.extractTexts(at: xmlUrl)

          var string = ""
          for row in rows {
            autoreleasepool {
              let text = row.attributedString().string
              if text.isEmpty == false {
                string += "\n"
                string += text
              }
            }
          }

          
          let downloader = Downloader()
          downloader.parse(url: xmlUrl) { [weak self] (dict, error) in
            // MAIN THREAD
            if error != nil {
              NSAlert(error: error!).runModal()
              
            }else {
              
              if let lawNum = dict?.lawNum,  let lawTitle = dict?.lawTitle, let lawEdition = dict?.lawEdition   {
                
                let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)

                queue.async {
                  autoreleasepool {
                    let law = EGovLaw()
                    law.lawNum = lawNum
                    law.lawTitle = lawTitle
                    law.filename = fileName
                    law.lawEdition = lawEdition
                    law.text = string
                    law.eLawsCompatible =
                      (lawListXml.range(of: " <LawNo>\(lawNum)</LawNo>") != nil)
                    
                    let pictUrl = url.appendingPathComponent("pict")
                    let pictContents = try! fm.contentsOfDirectory(at: pictUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                    
                    law.hasAttachments = pictContents.count > 0
                    
                    let realm = try! Realm(configuration: config)
                    try! realm.write() {
                      realm.add(law)
                    }
                    
                    
                    let paragraphRealm = try! Realm(configuration: paragraphConfig)
                    
                    let laws: [RowEGovLaw] = rows.map {
                      let law = RowEGovLaw()
                      law.lawNum = lawNum
                      //                    law.lawTitle = lawTitle
                      //                    law.filename = fileName
                      //                    law.lawEdition = lawEdition
                      law.text = $0.attributedString().string
                      law.multipleAnchors = $0.multipleAnchors
                      return law
                    }
                    
                    try! paragraphRealm.write() {
                      paragraphRealm.add(laws)
                    }
                    
                    
                    // DUMP LAWS NOT IN THE LIST
                    //                    if let results = LawList().lawsWithNum(lawNum), results.count > 0 {
                    //
                    //                    }else {
                    //                      print("\(lawTitle) is not in list")
                    //                    }
                  }
                }
                
                if let strongSelf = self {
                let elapsed = Int(Date().timeIntervalSinceReferenceDate - strongSelf.startDate.timeIntervalSinceReferenceDate)
                strongSelf.buildLinkLabel.stringValue = "-- Index Done \(i)/\(contents.count) - \(elapsed) sec"
                }
                count -= 1
      
              }
            }
            
          }
        }
      }
    }

  }
  
  func extractTexts(at xmlUrl: URL) -> [Row] {
    
    var returnValue: [Row]  = []
    autoreleasepool {
      let data = try! Data(contentsOf: xmlUrl)
      let process = ProcessSentenceXML()
      
      process.divide(xml: data, addLinks: false, progress: { str in }) { (_, rows: [Row], error) in
        autoreleasepool {
          
          if error != nil {
            DispatchQueue.main.async {
              NSAlert(error: error!).runModal()
            }
          }else {
            
            returnValue = rows
          }
        }
      }
    }
    
    return returnValue
  }
}

