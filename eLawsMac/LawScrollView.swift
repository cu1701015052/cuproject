//
//  MyView.swift
//  ScrollLayer
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

#if os(iOS) || os(watchOS)
  import UIKit
  typealias NSView = UIView
#else
  import Cocoa
#endif

struct MyIndexPath: Equatable {
  static func ==(lhs: MyIndexPath, rhs: MyIndexPath) -> Bool {
    return lhs.row == rhs.row && lhs.section == rhs.section
  }
  
  var row: Int
  var section: Int
}



protocol LawScrollViewDataSource: class {
  func numberOfRows(in tableView: LawScrollView) -> Int
  func tableView(_ tableView: LawScrollView, cellForRowAt row: Int) -> NSView
  
  func tableView(_ tableView: LawScrollView, estimatedHeightForRowAt row: Int) -> CGFloat
  func tableView(_ tableView: LawScrollView, heightForRowAt row: Int) -> CGFloat
  func headerViewFor(_ tableView: LawScrollView) -> NSView?
  func headerHeightFor(_ tableView: LawScrollView) -> CGFloat
}

protocol LawScrollViewDelegate: class {
  func scrollViewWillBeginDragging(_ scrollView: LawScrollView)
  func scrollViewDidEndDragging(_ scrollView: LawScrollView)
  func jump(to linkString: String, addingHistory: Bool)
}


protocol LawScrollableTableView {
  func reloadData()
  func reloadData(recalcEndLimitFlag: Bool)

  var contentSize: CGSize { get set }
  var contentOffset: CGPoint { get set }
  var contentInset: UIEdgeInsets { get set }
  var backgroundColor: UIColor? { get set }
  var visibleCells: [UIView] { get }
  func cellForRow(at: MyIndexPath) -> UIView?
  func tableView(_ tableView: LawScrollableTableView, numberOfRowsInSection section: Int) -> Int
  
  var insetsContentViewsToSafeArea: Bool { get set }
  func indexPath(for cell: UIView) -> MyIndexPath?
  var indexPathsForVisibleRows: [MyIndexPath]? { get }
//  func scrollToRow(at indexPath: MyIndexPath, at scrollPosition: UITableViewScrollPosition, animated: Bool)
//  var indicatorStyle: UIScrollViewIndicatorStyle { get set }
  func setContentOffset(_ offset: CGPoint, animated: Bool)

}


class LawScrollView: NSView, LawScrollableTableView {
  
  #if os(iOS)
  let panGestureRecognizer = UIPanGestureRecognizer()
  #elseif os(OSX)
  let panGestureRecognizer = NSPanGestureRecognizer()
  var backgroundColor: UIColor? {
    didSet {
      self.layer?.backgroundColor = backgroundColor?.cgColor
    }
  }
  #endif
  
  static let calcContentOffsetQueue = DispatchQueue(label: "jp.ac.cyber-u.1701015052.CUProject.ScrollLayer.MyView")
  
  //
  var viewWidth: CGFloat = 500
  var contentInset: UIEdgeInsets { get { return UIEdgeInsets.zero} set { } }
  var insetsContentViewsToSafeArea: Bool = true
  @objc dynamic var contentSize: CGSize {
    get {
      guard let dataSource = dataSource else { return CGSize(width: 1.0, height: 1.0) }
      var totalHeight: CGFloat = self.headerHeight
      let numberOfRows = dataSource.numberOfRows(in: self)
      
      for n in 0 ..< numberOfRows {
        let height = dataSource.tableView(self, heightForRowAt: n)
        totalHeight += height
        
        if totalHeight > self.bounds.size.height { break }
      }
      if totalHeight > self.bounds.size.height { return CGSize(width: 1.0, height: 10000.0 + self.bounds.size.height * 1.5) }
      
      return CGSize(width: 1.0, height: 1.0)
    }
    set { }
  }
  
  var internalContentOffset: CGPoint = .zero {
    willSet {
      //   self.willChangeValue(forKey: "contentOffset")
      self.willChangeValue(for: \LawScrollView.contentOffset)
      
    }
    didSet {
      
      // didChangeValue(forKey: "contentOffset")
      self.didChangeValue(for: \LawScrollView.contentOffset)
      
    }
  }
  
  @objc dynamic var contentOffset: CGPoint {
    get {
      return internalContentOffset
    }
    set {
      print(newValue.y)
      internalContentOffset.y = newValue.y
      if let (row, cellOffset) = getRow(for: internalContentOffset.y/10000.0) {
        self.row = row
        self.cellOffset = cellOffset
        #if os(iOS)
        mDisplayLink?.invalidate()
        mDisplayLink = nil
        #endif
        
        #if os(macOS)
        inertiaTimer?.invalidate()
        inertiaTimer = nil
        #endif
        scrollSpeed = 0
        displayCells()
      }
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.viewWidth = frame.size.width
    
    #if os(iOS)
    #elseif os(OSX)
      self.wantsLayer = true
    #endif

  }
  
  override var frame: CGRect {
    didSet {
      self.viewWidth = frame.size.width
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setShowsVerticalScrollIndicator(_ flag: Bool) {
  }
  
  func setContentOffset(_ offset: CGPoint, animated: Bool) {
  }
  
  func cellForRow(at indexPath: MyIndexPath) -> UIView? {
    if indexPath.section == 0 {
      return visibleRowsAndCells.filter({ $0.0 == LawScrollView.headerRow }).first?.1

    }else {
      return visibleRowsAndCells.filter({ $0.0 == indexPath.row }).first?.1
    }
  }
  
  var indexPathsForVisibleRows: [MyIndexPath]? {
    return visibleRowsAndCells.sorted(by: { $0.0 < $1.0 })
      .map {
      if $0.0 == LawScrollView.headerRow { return MyIndexPath(row: 0, section: 0) }
      return MyIndexPath(row: $0.0, section: 1)
    }
  }
  
  
//  func scrollToRow(at indexPath: MyIndexPath, at scrollPosition: UITableViewScrollPosition, animated: Bool) {
//
//  }
//
//  var indicatorStyle: UIScrollViewIndicatorStyle {
//    get { return UIScrollViewIndicatorStyle.default }
//    set {}
//
//  }

  typealias RowDescriptor = (Int, CGFloat, CGFloat)

  func getRow(for percentage: CGFloat) -> (/*row*/ Int, /*cellOffset*/ CGFloat)! {
    guard let dataSource = dataSource else { return nil }

    var heightArray: [RowDescriptor] = [ (LawScrollView.headerRow, self.headerHeight, 0) ]
    var totalHeight: CGFloat = self.headerHeight
    
    let numberOfRows = dataSource.numberOfRows(in: self)
    
    for n in 0 ..< numberOfRows {
      let height = dataSource.tableView(self, estimatedHeightForRowAt: n)
      heightArray.append((n, height, totalHeight))
      totalHeight += height
    }
    
    
    func find(location: CGFloat, inArray: [RowDescriptor]) -> (Int, CGFloat) {
      if inArray.count == 0 { return (self.endRow, endCellOffset) }
      
      //print("LOOK FOR \(inArray.first!.0), \(inArray.first!.2) : \(inArray.last!.0),  \(inArray.last!.2)")
      
      let hoge = (inArray.count - 1) / 2
      let height = inArray[hoge].1
      let row = inArray[hoge].0
      let originY = inArray[hoge].2
      
      if location >= originY && originY + height > location {
        // FOUND
        return (row,  (location - originY))
      }
      
      if location < originY {
        //LOWER HALF
        return find(location: location, inArray: Array(inArray[0..<hoge]) )
      }
      
      if location > originY + height && hoge + 1 < inArray.count {
        //UPPER HALF
        return find(location: location, inArray: Array(inArray[hoge+1..<inArray.count]) )
      }
      
      return (self.endRow, endCellOffset) // UNKNOWN
    }
    
    totalHeight -= self.bounds.size.height
    let (row, cellOffset) = find(location: percentage * totalHeight, inArray: heightArray)
    
    return (row, cellOffset)
  }

  //
  
  static let headerRow: Int = -1 // row: -1 for header
  var row: Int = LawScrollView.headerRow
  var endRow: Int = LawScrollView.headerRow
  
  var headerHeight: CGFloat {
   return dataSource?.headerHeightFor(self) ?? 0
  }

  var cellOffset: CGFloat = 50
  var endCellOffset: CGFloat = 50

  weak var delegate: LawScrollViewDelegate?
  
  weak var dataSource: LawScrollViewDataSource? {
    didSet {
      recalcEndLimit()
      reloadData()
      
      #if os(iOS)
        if self.gestureRecognizers?.contains(panGestureRecognizer) != true {
          panGestureRecognizer.delegate = self
          panGestureRecognizer.addTarget(self, action: #selector(panGesture(_ :)))
          self.addGestureRecognizer(panGestureRecognizer)
        }
      #else
        
//        if self.gestureRecognizers.contains(panGestureRecognizer) != true {
//          panGestureRecognizer.delegate = self
//          panGestureRecognizer.target = self
//          panGestureRecognizer.action = #selector(panGesture(_ :))
//          self.addGestureRecognizer(panGestureRecognizer)
//        }
      #endif
      
    }
  }
  var visibleRowsAndCells: [Int: NSView] = [:]
  var visibleCells: [UIView] {
    return visibleRowsAndCells.sorted(by: { $0.0 < $1.0} ).map { $0.1 }
  }
  
  func indexPath(for cell: UIView) -> MyIndexPath? {
    
    guard let dict = visibleRowsAndCells.filter({ $0.1 === cell }).first else  { return nil }
    
    if dict.0 == LawScrollView.headerRow {
      return MyIndexPath(row: 0, section: 0)
    }
    
    return MyIndexPath(row: dict.0, section: 1)
  }
  
  func tableView(_ tableView: LawScrollableTableView, numberOfRowsInSection section: Int) -> Int {
    guard let dataSource = dataSource else { return 0 }

    if section == 0 { return 1 }
    return dataSource.numberOfRows(in: self)
  }
  
  #if os(OSX)
  override var isFlipped: Bool {
    return true
  }
  #endif
  
  override func awakeFromNib() {
    
    #if os(OSX)
      self.bounds = CGRect(x: 0, y: self.bounds.size.height, width: self.bounds.size.width, height: -self.bounds.height)
      self.layer?.masksToBounds = true
    #else
      self.layer.masksToBounds = true
    #endif
  }
  
  #if os(OSX)
  
  override func setFrameSize(_ newSize: NSSize) {
    super.setFrameSize(newSize)
    reloadData()
  }
  #endif
  
  func recalcEndLimit() {
    guard dataSource != nil else { return }
    if let pos = endPosition() {
      self.endCellOffset = pos.1
      self.endRow = pos.0
    }
  }
//  override func convert(_ rect: CGRect, from view: UIView?) -> CGRect {
//    
//    
  //    guard view != nil else { return rect }
  //    var convertedRect = rect
  //    convertedRect.origin.y += view!.frame.origin.y
  //    print("\(rect) -> \(convertedRect)")
  //    return convertedRect
  //  }
  
  func reloadData() {
    reloadData(recalcEndLimitFlag: true)
  }
  
  func reloadData(recalcEndLimitFlag: Bool = true) {
    
    if recalcEndLimitFlag {
      self.willChangeValue(for: \.contentSize)
      recalcEndLimit()
    }
    visibleRowsAndCells.values.forEach { $0.removeFromSuperview() }
    visibleRowsAndCells = [:]
    displayCells()
    
    if recalcEndLimitFlag {
      self.didChangeValue(for: \.contentSize)
    }
  }
  
  func displayCells() {
    guard let dataSource = dataSource else { return }
    
    // FILL VIEW WITH CELLS
    var row = self.row
    var totalHeight: CGFloat = -cellOffset
    let numberOfRows = dataSource.numberOfRows(in: self)
    let viewHeight = self.bounds.size.height
    while true {

      if row >= numberOfRows { break }
      if totalHeight > viewHeight { break }
      
      if let cell = visibleRowsAndCells[row] {

        #if os(iOS)
          cell.transform = CGAffineTransform.identity
          cell.frame.origin.y = totalHeight
        #else
          cell.frame.origin.y = totalHeight
        #endif
        totalHeight += cell.frame.size.height

      }else if let cell = (row == LawScrollView.headerRow ? dataSource.headerViewFor(self) : dataSource.tableView(self, cellForRowAt: row))  {
        let cellHeight = (row == LawScrollView.headerRow ? self.headerHeight : dataSource.tableView(self, heightForRowAt: row) )
        let cellWidth = cell.frame.size.width
        let cellOriginX = cell.frame.origin.x
        visibleRowsAndCells[row] = cell
        
        #if os(iOS)
          cell.frame = CGRect(x: cellOriginX, y: totalHeight, width: cellWidth, height: cellHeight)

        #else
          cell.frame = CGRect(x: cellOriginX, y: totalHeight, width: cellWidth, height: cellHeight)

        #endif
        
        totalHeight += cellHeight
      }
      
      row += 1
    }
    
    // REMOVE UNUSED CELLS
    
    let visibleCellRange: Range<Int> = self.row ..< row
    
    let unusedCells = visibleRowsAndCells.filter { visibleCellRange.contains($0.key) == false }
    unusedCells.keys.forEach {
      visibleRowsAndCells[$0]?.removeFromSuperview()
      visibleRowsAndCells.removeValue(forKey: $0)
    }
    
    visibleRowsAndCells.forEach {
      if $0.1.superview != self  {
        self.addSubview($0.1)
      }
    }
  }
  
  func move(_ value: CGFloat) {
    guard let dataSource = dataSource else { return }

    if value > 0 { // DRAG DOWN
      var lowerExtention = -value + cellOffset
      
      if lowerExtention > 0 {
        cellOffset = lowerExtention // NO NEW CELL ADDED
      }else {
        while true { // GET TOP CELLSx
          if row == LawScrollView.headerRow {
            cellOffset = 0
            #if os(iOS)
            mDisplayLink?.invalidate()
            mDisplayLink = nil
            #endif
            
            #if os(macOS)
            inertiaTimer?.invalidate()
            inertiaTimer = nil
            #endif
            scrollSpeed = 0
            bounceBack(true)
            _ = calcContentOffset()
            break
          }
          row -= 1
          let height = (row == LawScrollView.headerRow ? self.headerHeight : dataSource.tableView(self, heightForRowAt: row) )
          if lowerExtention < height {
            cellOffset = height - lowerExtention
            break
          }
          
          lowerExtention -= height
        }
      }
      
    }else {
      var upperExtension = -value + cellOffset
      
      while true { // CALC NEW ROW
        if endRow == 0 || row > endRow || (row == endRow && endCellOffset < upperExtension) {
          setEndPosition()
          bounceBack(false)
          break
        }
        let height = (row == LawScrollView.headerRow ? headerHeight : dataSource.tableView(self, heightForRowAt: row) )
        if upperExtension < height  {
          cellOffset = upperExtension
          break
        }
        row += 1
        upperExtension -= height
      }
    }
    
    displayCells()
    
    // CALC CONTENT OFFSET FROM ROW AND CELLOFFSET
    if self.calcContentOffsetSignature == 0 {
      _ = calcContentOffset()
    }
    
  }
  var calcContentOffsetSignature: TimeInterval = 0
  
  func calcContentOffset() -> TimeInterval {
    guard let dataSource = dataSource else { return 0 }

    let signature = Date.timeIntervalSinceReferenceDate
    self.calcContentOffsetSignature = signature
    let numberOfRows = dataSource.numberOfRows(in: self)
    
    var heightArray: [RowDescriptor] = [ (LawScrollView.headerRow, self.headerHeight, 0) ]
    var totalHeight: CGFloat = self.headerHeight
    var thisRow: RowDescriptor? = nil
    
    if self.row == LawScrollView.headerRow {
      thisRow = heightArray.first!
    }
    
    for n in 0 ..< numberOfRows {
      if self.calcContentOffsetSignature != signature { break }
      let height = dataSource.tableView(self, estimatedHeightForRowAt: n)
      heightArray.append((n, height, totalHeight))
      if n == self.row {
        thisRow = (n, height, totalHeight)
      }
      totalHeight += height
    }
    
    totalHeight -= self.bounds.size.height
    if thisRow != nil {
      let location = thisRow!.2 + self.cellOffset
      self.internalContentOffset.y = location * 10000.0 / totalHeight
    }
    
    // RECALC WHILE SCROLLING
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
      self.calcContentOffsetSignature = 0
    }
    
    return signature
  }
  
  func endPosition() -> (Int, CGFloat)! {
    guard let dataSource = dataSource else { return nil }

    let count = dataSource.numberOfRows(in: self)
    var totalHeight = abs(self.bounds.size.height) / 2 // SCROLL UP TO SCREEN MID
    for row in (LawScrollView.headerRow..<count).reversed() {
      let height = (row == LawScrollView.headerRow ? self.headerHeight : dataSource.tableView(self, heightForRowAt: row) )
      if totalHeight - height <= 0 {
        return (row, height - totalHeight)
      }
      
      totalHeight -= height
    }
    return (LawScrollView.headerRow, 0)
  }
  
  func focusPosition(for focusedRow: Int) -> (Int, CGFloat)! {
    guard let dataSource = dataSource else { return nil }
    var totalHeight = abs(self.bounds.size.height)/2
    for row in (0..<focusedRow).reversed() {
      let height = (row == LawScrollView.headerRow ? self.headerHeight : dataSource.tableView(self, heightForRowAt: row) )
      if totalHeight - height <= 0 {
        if row > self.endRow || (row == self.endRow && endCellOffset < height - totalHeight) {
          return (endRow, endCellOffset)
        }

        return (row, height - totalHeight)
      }
      
      totalHeight -= height
    }
    return (0, 0)
  }
  
  func setEndPosition() {
    
    if let pos = endPosition() {
      cellOffset = pos.1
      self.row = pos.0
    }
    #if os(iOS)
    mDisplayLink?.invalidate()
    mDisplayLink = nil
    #endif

    #if os(macOS)
    inertiaTimer?.invalidate()
    inertiaTimer = nil
    #endif

    //    mDisplayLink?.invalidate()
    //    mDisplayLink = nil
    scrollSpeed = 0
    displayCells()
  }
  
  var bouncingTime: TimeInterval? = nil
  func bounceBack(_ bounceOnTop: Bool) {
    
    #if os(iOS)
      if bouncingTime != nil && Date.timeIntervalSinceReferenceDate - bouncingTime! < 0.5 {
        return
      }
      
      if bounceOnTop {
        self.visibleCells.forEach { $0.rejectSwipeUp() }
        print("bounceOnTop")
        
      }else {
        self.visibleCells.forEach { $0.rejectSwipeDown() }
        print("bounce bottom")
        
      }
      bouncingTime = Date.timeIntervalSinceReferenceDate
      
    #else
      return
      
    #endif
  }
  
//  private var inertiaTimer: Timer? = nil
  private var inertiaTimerLastTimeStamp: TimeInterval? = nil
  #if os(iOS)
  private var mDisplayLink: CADisplayLink? = nil
  #endif
  
  #if os(macOS)
  private var inertiaTimer: Timer? = nil
  #endif
  
  private var scrollSpeed: Double = 0 {
    didSet {
      if scrollSpeed == 0 && oldValue != 0 && panGestureRecognizer.state != .changed {
        delegate?.scrollViewDidEndDragging(self)
      }
    }
  }

  func inertia() {
    
    if scrollSpeed != 0 {
      inertiaTimerLastTimeStamp = Date.timeIntervalSinceReferenceDate
//      inertiaTimer = Timer.scheduledTimer(timeInterval: 1.0/60.0, target: self, selector: #selector(displayTimer), userInfo: nil, repeats: false)
      
      #if os(iOS)
      if mDisplayLink == nil {
        mDisplayLink = CADisplayLink(target: self, selector: #selector(self.displayTimer))
        //        mDisplayLink!.preferredFramesPerSecond = 60
        mDisplayLink!.add(to: RunLoop.current, forMode: RunLoop.Mode.common)
      }
      
      #endif
      
      #if os(macOS)
    inertiaTimer = Timer.scheduledTimer(timeInterval: 1.0/60.0, target: self, selector: #selector(displayTimer), userInfo: nil, repeats: false)
      #endif
      

//      if #available(iOS 10.0, *) {
//          mDisplayLink?.invalidate()
//          mDisplayLink = CADisplayLink(target: self, selector: #selector(self.displayAnimation))
//        #if os(iOS)
//          mDisplayLink!.preferredFramesPerSecond = 60
//        #endif

//          mDisplayLink!.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
//      } else {
//        // Fallback on earlier versions
//      }
    }
  }
  
//  #if os(iOS)
//
//  @objc func displayAnimation() {
//    guard mDisplayLink != nil else { return }
//    let actualFramesPerSecond: Double
//    if #available(iOS 10.0, *) {
//      let lap = mDisplayLink!.targetTimestamp - mDisplayLink!.timestamp
//      actualFramesPerSecond = 1 / lap
//    } else {
//      actualFramesPerSecond = 60.0
//    }
//
//    let movement = scrollSpeed*0.025 * (60.0/actualFramesPerSecond)
//    move(CGFloat(movement))
//    if abs(scrollSpeed) > 50 {
//      scrollSpeed *= 0.98
//    }else {
//      scrollSpeed *= 0.9
//    }
//    if abs(scrollSpeed) < 0.005 {
//      mDisplayLink?.invalidate()
//      mDisplayLink = nil
//      scrollSpeed = 0
//      return
//
  //    }
  //  }
  //  #endif
  
  
  @objc func displayTimer() {
    let now = Date.timeIntervalSinceReferenceDate
    if inertiaTimerLastTimeStamp != nil {
      let dif = now - inertiaTimerLastTimeStamp!
      let actualFramesPerSecond = 1 / dif
      move(CGFloat(scrollSpeed*0.08 * (60.0/actualFramesPerSecond)))
    }
    
    if abs(scrollSpeed) > 50 {
      scrollSpeed *= 0.93
    }else {
      scrollSpeed *= 0.92
    }
    if abs(scrollSpeed) < 0.01 { return }
    inertiaTimerLastTimeStamp = now
    
    #if os(iOS)
    if mDisplayLink == nil {
      mDisplayLink = CADisplayLink(target: self, selector: #selector(self.displayTimer))
      //      mDisplayLink!.preferredFramesPerSecond = 60
      mDisplayLink!.add(to: RunLoop.current, forMode: RunLoop.Mode.common)
    }
    #endif
    
    #if os(macOS)
    inertiaTimer = Timer.scheduledTimer(timeInterval: 1.0/60.0, target: self, selector: #selector(displayTimer), userInfo: nil, repeats: false)
    #endif
    

    
//    inertiaTimer = Timer.scheduledTimer(timeInterval: 1.0/60.0, target: self, selector: #selector(displayTimer), userInfo: nil, repeats: false)
  }
  
  //
  
  #if os(OSX)
// override var acceptsFirstResponder: Bool { return true }
//
//  override func resignFirstResponder() -> Bool {
//    return true
//  }
//
//  override func becomeFirstResponder() -> Bool {
//    return true
//  }
  
  override func scrollWheel(with event: NSEvent) {
    if NSEvent.pressedMouseButtons != 0 { return }
    
    delegate?.scrollViewWillBeginDragging(self)
    move(event.scrollingDeltaY)
    delegate?.scrollViewDidEndDragging(self)
    if abs(event.scrollingDeltaY) < abs(event.scrollingDeltaX) {
    super.scrollWheel(with: event)
    }
  }
  var bounceBackDistance: CGFloat = 0
  var mouseEventTime: TimeInterval = 0
  var mouseDownLocation: NSPoint? = nil
  override func mouseDown(with event: NSEvent) {
    mouseDownLocation = event.locationInWindow
    mouseEventTime = Date.timeIntervalSinceReferenceDate
    #if os(iOS)
    if mDisplayLink == nil {
      mDisplayLink?.invalidate()
      mDisplayLink = nil    }
    #endif
    
    #if os(macOS)
    inertiaTimer?.invalidate()
    inertiaTimer = nil
    #endif
    
    scrollSpeed = 0

  }
  
  override func mouseDragged(with event: NSEvent) {
    guard mouseDownLocation != nil else { return }
    if NSEvent.pressedMouseButtons != 0 { return }

    let location = event.locationInWindow

    let dif = mouseDownLocation!.y - location.y
    move(dif)
    
    mouseDownLocation = location
    let time = Date.timeIntervalSinceReferenceDate - mouseEventTime
    scrollSpeed = Double(dif) / time
    mouseEventTime = Date.timeIntervalSinceReferenceDate

  }
  
  override func mouseUp(with event: NSEvent) {
    inertia()
  }
  #else
  
  var bounceBackDistance: CGFloat = 0
  var mouseEventTime: TimeInterval = 0
  var mouseDownLocation: CGPoint? = nil

  
  #endif

  func find(row: Int) {
    if let position = focusPosition(for: row) {
      self.row = position.0
      self.cellOffset = position.1
      displayCells()
    }
  }
  
}

#if os(iOS)
extension LawScrollView: UIGestureRecognizerDelegate {

  override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
    let location = gestureRecognizer.location(in: self)
    if location.x < 10 { return false }
    if location.y > self.bounds.maxY - 20 { return false }
    return true
  }
  
//  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//    print(gestureRecognizer)
//    print(otherGestureRecognizer)
//    if otherGestureRecognizer is UIScreenEdgePanGestureRecognizer { return true }
//    return false
//  }
  
  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return otherGestureRecognizer is UIScreenEdgePanGestureRecognizer
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesBegan(touches, with: event)
    mDisplayLink?.invalidate()
    mDisplayLink = nil
    scrollSpeed = 0

  }
  
  func stopScrolling() {
    mDisplayLink?.invalidate()
    mDisplayLink = nil
    scrollSpeed = 0
//    mDisplayLink?.invalidate()
//    mDisplayLink = nil
  }
  
  @objc func panGesture(_ sender: UIPanGestureRecognizer) {
    
    if panGestureRecognizer.state == .began {
      mouseDownLocation = panGestureRecognizer.location(in: self)
      mouseEventTime = Date.timeIntervalSinceReferenceDate
//      mDisplayLink?.invalidate()
//      mDisplayLink = nil
      scrollSpeed = 0
      delegate?.scrollViewWillBeginDragging(self)
    }
    
    if panGestureRecognizer.state == .changed {
      guard mouseDownLocation != nil else { return }
      let location = panGestureRecognizer.location(in: self)
      
      var dif = -(mouseDownLocation!.y - location.y)

      move(dif)
      
      mouseDownLocation = location
      let time = Date.timeIntervalSinceReferenceDate - mouseEventTime
      scrollSpeed = Double(dif) / time * 0.5
      if scrollSpeed < -1000 {
        scrollSpeed = -1000
      }

      if scrollSpeed > 1000 {
        scrollSpeed = 1000
      }
      
      mouseEventTime = Date.timeIntervalSinceReferenceDate
    }
    
    if panGestureRecognizer.state == .ended || panGestureRecognizer.state == .cancelled {
      if scrollSpeed != 0 {
      inertia()
      }else {
        delegate?.scrollViewDidEndDragging(self)
      }
    }
  }

}
  #else
  
  extension LawScrollView: NSGestureRecognizerDelegate {
    
    override func touchesBegan(with event: NSEvent) {
      super.touchesBegan(with: event)
//      mDisplayLink?.invalidate()
//      mDisplayLink = nil
      scrollSpeed = 0
      
    }
    
    func stopScrolling() {
      inertiaTimer?.invalidate()
      inertiaTimer = nil
      scrollSpeed = 0
//      mDisplayLink?.invalidate()
//      mDisplayLink = nil
    }
    
    @objc func panGesture(_ sender: NSPanGestureRecognizer) {
      
      if panGestureRecognizer.state == .began {
        mouseDownLocation = panGestureRecognizer.location(in: self)
        mouseEventTime = Date.timeIntervalSinceReferenceDate
//        mDisplayLink?.invalidate()
//        mDisplayLink = nil
        scrollSpeed = 0
        delegate?.scrollViewWillBeginDragging(self)
      }
      
      if panGestureRecognizer.state == .changed {
        guard mouseDownLocation != nil else { return }
        let location = panGestureRecognizer.location(in: self)
        
        let dif = -(mouseDownLocation!.y - location.y)
        move(dif)
        
        mouseDownLocation = location
        let time = Date.timeIntervalSinceReferenceDate - mouseEventTime
        scrollSpeed = Double(dif) / time * 0.5
        mouseEventTime = Date.timeIntervalSinceReferenceDate
      }
      
      if panGestureRecognizer.state == .ended || panGestureRecognizer.state == .cancelled {
        if scrollSpeed != 0 {
          inertia()
        }else {
          delegate?.scrollViewDidEndDragging(self)
        }
      }
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: NSGestureRecognizer) -> Bool {
      let location = gestureRecognizer.location(in: self)
      if location.x < 20 { return false }
      if location.y > self.bounds.maxY - 20 { return false }
      return true
    }
  }

#endif
