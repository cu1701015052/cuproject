//
//  FindViewController.swift
//  eLawsMac
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Cocoa

protocol FindViewControllerDelegate {
  
}

class FindViewController: NSViewController {
  
  @objc dynamic var findString: String?
  
  @IBOutlet weak var nextButton: NSButton!
  @IBOutlet weak var previousButton: NSButton!
  @IBOutlet weak var searchField: NSSearchField!
  var findTotalString: Int? = nil
  weak var fetchResults: FetchResults? = nil
  var findDataSource: FindDataSource? = nil
  
  var findClearAction: (()->(Void))? = nil
  var findNextAction: (()->(Void))? = nil
  var findPreviousAction: (()->(Void))? = nil
  var focusEnterAction: ((_ findDataSource: FindDataSource)->(Void))? = nil
  
  override func viewWillAppear() {
    super.viewWillAppear()
    searchField.stringValue = ""
    label.stringValue = ""
    nextButton.isEnabled = false
    previousButton.isEnabled = false
  }
  
  override func viewDidAppear() {
    super.viewDidAppear()
    self.view.window?.makeFirstResponder(searchField)
  }

  func open() {
    self.view.window?.makeFirstResponder(searchField)
  }
  
  @IBAction func findNext(_ sender: Any?)  {
    findNextAction?()
    label.stringValue = findDataSource!.findTotalString ?? ""
    nextButton.isEnabled = findDataSource!.canFindNext
    previousButton.isEnabled = findDataSource!.canFindPrevious
  }
  
  @IBAction func findPrevious(_ sender: Any?) {
    findPreviousAction?()
    label.stringValue = findDataSource!.findTotalString ?? ""
    nextButton.isEnabled = findDataSource!.canFindNext
    previousButton.isEnabled = findDataSource!.canFindPrevious

  }
  
  @IBOutlet weak var label: NSTextField!
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do view setup here.
  }
  @IBAction func findEntered(_ sender: Any) {
    guard let findString = findString else { return }
    guard let findResults = fetchResults?.findString(findString) else { return }
    
    findDataSource = FindDataSource(findString: findString, findArray: findResults.1, findIndex: 0)

//    findDataSource!.find(findString)
    
    label.stringValue = findDataSource!.findTotalString ?? ""
    nextButton.isEnabled = findDataSource!.canFindNext
    previousButton.isEnabled = findDataSource!.canFindPrevious

    focusEnterAction?(findDataSource!)
  }
  
  @IBAction func done(_ sender: Any) {
    findClearAction?()
  }
}
