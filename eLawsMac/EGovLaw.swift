//
//  Law.swift
//  eLawsDownloader
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

extension Data {
  
  /// Create hexadecimal string representation of `Data` object.
  ///
  /// - returns: `String` representation of this `Data` object.
  
  func hexadecimal() -> String {
    return map { String(format: "%02x", $0) }
      .joined(separator: "")
  }
  
  func shortHexadecimal() -> String {
    let base64 = self.base64EncodedString(options: Data.Base64EncodingOptions())
    return base64
  }

}

func dataWithHexString(hex: String) -> Data {
  var hex = hex
  var data = Data()
  while(hex.count > 0) {
    let subIndex = hex.index(hex.startIndex, offsetBy: 2)
    let c = String(hex[..<subIndex])
    hex = String(hex[subIndex...])
    var ch: UInt32 = 0
    Scanner(string: c).scanHexInt32(&ch)
    var char = UInt8(ch)
    data.append(&char, count: 1)
  }
  return data
}

/*

 平成 h
 平成十 x
 平成二十 q
 平成三十 z

昭和 s
 大正 t
 明治 m
 十 d
 百 c
 年法律第 $
 省令第 %
 年 n
 第 #
 号 ;
 
 
 
 32 moji
 
 */


extension EGovLaw: TfidfDoc {
  var allWords: [String] {
    return Array(words.keys)
  }
  
  func count(for key: String) -> Int {
    return words[key] ?? 0
  }
  
  func setCount(_ num: Int, for key: String) {
    words[key] = num
  }
  
  func calcFinished(_ tfidf: Tfidf) {
    let dict = tfidf.tfidfValues(in: self, regularize: true)

    tfidfJson = try! JSONSerialization.data(withJSONObject: dict)
  }
  
  var tempTfidf: [String: Double]? {
    set {
      
    }
    get {
      return try! JSONSerialization.jsonObject(with: tfidfJson) as! [String : Double]
    }
  }
  
  var content: String? {
    get { return nil }
    set { }
  }
  
  var paragraph: String? {
    get { return nil }
    set { }
  }

}


final class RowEGovLaw: EGovLaw {
  
  @objc dynamic var multipleAnchors: String?
  var anchors: [String] {
    get {
      guard let multipleAnchors = multipleAnchors else { return [] }
      return multipleAnchors.components(separatedBy: " ")
    }
    set {
      multipleAnchors = newValue.joined(separator: " ")
    }
  }
}


class EGovLaw: Object, LawSource {

  @objc dynamic var lawTitle: String = ""
  @objc dynamic var lawNum: String = ""
  @objc dynamic var lawEdition: String = ""
  @objc dynamic var filename: String = ""
  @objc dynamic var eLawsCompatible = true
  @objc dynamic var hasAttachments = false
  @objc dynamic var text = ""
  @objc dynamic var label: Int = -1
  @objc dynamic var tfidfJson: Data!

  let filepath: String = ""

  // optional
  var xml: Data? = nil
  var downloadOption: DownloadOption = .regular
  
  // TfidfDoc
  var words: [String: Int] = [:]

  var name: String! {
    get {
      return lawNum
    }
    set {
      lawNum = newValue
    }
  }
  
  var tempCosine: Double = 0.0

  static func dateString(from fileName: String) -> String {
    guard fileName.isEmpty == false else { return "" }
    let comps = fileName.components(separatedBy: "_")
    if comps.count < 2 { return "" }
    let datePattern = comps[1]
    if datePattern.count != 8 { return "" }
    
    var start = datePattern.startIndex
    var end = datePattern.index(start, offsetBy: 4)
    let year = datePattern[start..<end]
    
    start = end
    end = datePattern.index(start, offsetBy: 2)

    let month = datePattern[start..<end]
    
    start = end
    end = datePattern.index(start, offsetBy: 2)

    let day = datePattern[start..<end]
    let gengoYear = convertGengo(Int(year), Int(month), Int(day))

    var dateString = "\(gengoYear)年\(month)月\(day)日"
    dateString = dateString.replacingOccurrences(of: "年0", with: "年")
    dateString = dateString.replacingOccurrences(of: "月0", with: "月")

    return dateString
  }
  
  static func convertGengo(_ seireki: Int?, _  month: Int?, _ day: Int?) -> String {
    guard let seireki = seireki else { return "" }
    guard let month = month else { return "" }
    guard let day = day else { return "" }

    if seireki == 2019 && month >= 5 { return "令和元" }
    if seireki == 1989 && ((month == 1 && day >= 8) || month >= 2 ) { return "平成元" }
    if seireki == 1926 { return "昭和元" }
    if seireki == 1912 { return "大正元" }
    if seireki == 1872 { return "明治元" }

    if seireki > 2019 { return "令和\(seireki-2018)" }
    if seireki > 1989 { return "平成\(seireki-1988)" }
    if seireki > 1926 { return "昭和\(seireki-1925)" }
    if seireki > 1912 { return "大正\(seireki-1911)" }
    if seireki > 1872 { return "明治\(seireki-1871)" }

    return "\(seireki)"
  }
  
  override static func indexedProperties() -> [String] {
    return ["lawName", "lawTitle", "lawNo", "label"]
  }
  
  override static func ignoredProperties() -> [String] {
    return ["xml", "downloadOption", "filepath",  "name", "words", "tempCosine", "tfidf", "tempTfidf"]
  }
  
  override func isEqual(_ object: Any?) -> Bool {
    guard let obj = object as? EGovLaw else { return false }
    
    if obj.lawNum == self.lawNum && obj.lawTitle == self.lawTitle { return true }
    return false
  }
  
}

final class EGovLawLink: Object {
  //圧縮するとデータが一割小さくなる
  
  @objc dynamic var fromLaw: EGovLaw!
  @objc dynamic var toLaw: EGovLaw!
//  @objc dynamic var fromAnchor: String = ""
//  @objc dynamic var toAnchor: String = "" // WITHOUT PREFIX

  @objc dynamic var fromAnchorCompressed: String = ""
  @objc dynamic var toAnchorComplessed: String = "" // WITHOUT PREFIX

  var fromAnchor: String {
    set {
      fromAnchorCompressed = compress(newValue)
    }
    get {
      return decompress(fromAnchorCompressed)
    }
  }
  var toAnchor: String {
    set {
      toAnchorComplessed = compress(newValue)
    }
    get {
      return decompress(toAnchorComplessed)
    }
  }
  override static func ignoredProperties() -> [String] {
    return ["fromAnchor", "toAnchor"]
  }
  
  func compress(_ string: String) -> String {
    var str = string
    
    str = str.replacingOccurrences(of: "/文proviso", with: "p")
    str = str.replacingOccurrences(of: "/文main", with: "m")
    str = str.replacingOccurrences(of: "/文", with: "") // STRIP

    str = str.replacingOccurrences(of: "条1", with: "!")
    str = str.replacingOccurrences(of: "条2", with: "@")
    str = str.replacingOccurrences(of: "条3", with: "#")
    str = str.replacingOccurrences(of: "条4", with: "$")
    str = str.replacingOccurrences(of: "条5", with: "%")
    str = str.replacingOccurrences(of: "条6", with: "^")
    str = str.replacingOccurrences(of: "条7", with: "&")
    str = str.replacingOccurrences(of: "条8", with: "*")
    str = str.replacingOccurrences(of: "条9", with: "(")
    str = str.replacingOccurrences(of: "条", with: "j")
    
    str = str.replacingOccurrences(of: "/イ", with: "i")

    str = str.replacingOccurrences(of: "附0/", with: "'")
    str = str.replacingOccurrences(of: "附1", with: ",")
    str = str.replacingOccurrences(of: "附2", with: ".")
    str = str.replacingOccurrences(of: "附3", with: "<")
    str = str.replacingOccurrences(of: "附4", with: ">")
    str = str.replacingOccurrences(of: "附", with: "h")
    str = str.replacingOccurrences(of: "_2", with: ";")
    str = str.replacingOccurrences(of: "_3", with: ":")

    str = str.replacingOccurrences(of: "/項1", with: ")")
    str = str.replacingOccurrences(of: "/項2", with: "{")
    str = str.replacingOccurrences(of: "/項3", with: "}")
    str = str.replacingOccurrences(of: "/項4", with: "[")
    str = str.replacingOccurrences(of: "/項5", with: "]")
    str = str.replacingOccurrences(of: "/項6", with: "?")
    str = str.replacingOccurrences(of: "/項7", with: "+")
    str = str.replacingOccurrences(of: "/項8", with: "|")
    str = str.replacingOccurrences(of: "/項9", with: "=")
    str = str.replacingOccurrences(of: "/項", with: "k")
    
    str = str.replacingOccurrences(of: "/号", with: "g")
    str = str.replacingOccurrences(of: "/欄", with: "l")

    return str
  }
  
  func decompress(_ string: String) -> String {
    var str = string
    
    str = str.replacingOccurrences(of: "i", with: "/イ")
    
    str = str.replacingOccurrences(of: "!", with: "条1")
    str = str.replacingOccurrences(of: "@", with: "条2")
    str = str.replacingOccurrences(of: "#", with: "条3")
    str = str.replacingOccurrences(of: "$", with: "条4")
    str = str.replacingOccurrences(of: "%", with: "条5")
    str = str.replacingOccurrences(of: "^", with: "条6")
    str = str.replacingOccurrences(of: "&", with: "条7")
    str = str.replacingOccurrences(of: "*", with: "条8")
    str = str.replacingOccurrences(of: "(", with: "条9")

    str = str.replacingOccurrences(of: "j", with: "条")
    str = str.replacingOccurrences(of: ";", with: "_2")
    str = str.replacingOccurrences(of: ":", with: "_3")
    str = str.replacingOccurrences(of: "'", with: "附0/")
    str = str.replacingOccurrences(of: ",", with: "附1")
    str = str.replacingOccurrences(of: ".", with: "附2")
    str = str.replacingOccurrences(of: "<", with: "附3")
    str = str.replacingOccurrences(of: ">", with: "附4")
    str = str.replacingOccurrences(of: "h", with: "附")

    str = str.replacingOccurrences(of: ")", with: "/項1")
    str = str.replacingOccurrences(of: "{", with: "/項2")
    str = str.replacingOccurrences(of: "}", with: "/項3")
    str = str.replacingOccurrences(of: "[", with: "/項4")
    str = str.replacingOccurrences(of: "]", with: "/項5")
    str = str.replacingOccurrences(of: "?", with: "/項6")
    str = str.replacingOccurrences(of: "+", with: "/項7")
    str = str.replacingOccurrences(of: "|", with: "/項8")
    str = str.replacingOccurrences(of: "=", with: "/項9")
    str = str.replacingOccurrences(of: "k", with: "/項")
    
    str = str.replacingOccurrences(of: "g", with: "/号")
    str = str.replacingOccurrences(of: "l", with: "/欄")
    str = str.replacingOccurrences(of: "p", with: "/文proviso")
    str = str.replacingOccurrences(of: "m", with: "/文main")

    return str
  }
  override static func indexedProperties() -> [String] {
    return []
  }
  

}
