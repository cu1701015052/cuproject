//
//  MeganeLongPressGestureRecognizer.h
//  MNCTTextView
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright 2010 Masatoshi Nishikata. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

@interface MeganeLineGestureRecognizer : NSGestureRecognizer {
  
@public
  BOOL staticEnd_;
  BOOL bottom_;
  CGSize offset_;
//  CGPoint initialLocatio÷nInView;
  
@private
  
  NSUInteger touchHash;
  CGPoint lastLocationInView;
	
	NSTimer *startTimer;
	NSTimeInterval lastMoved;
}
- (void)mouseDown:(NSEvent *)event;
- (void)mouseDragged:(NSEvent *)event;

- (void)reset;

- (BOOL)canBePreventedByGestureRecognizer:(NSGestureRecognizer *)preventingGestureRecognizer;
- (BOOL)canPreventGestureRecognizer:(NSGestureRecognizer *)preventedGestureRecognizer;

@property (nonatomic) BOOL bottom;
@property (nonatomic) CGSize offset;
@property (nonatomic) BOOL staticEnd;
@property (nonatomic) CGPoint initialLocationInView;

@end
