//
//  MeganeLineGestureRecognizer.swift
//  eLawsMac
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation

class MeganeLineGestureRecognizer: NSGestureRecognizer {
    enum DraggingType {
        case top, bottom, free
    }
    
    
    var draggingType: DraggingType!
    var offset: CGSize = .zero
    var staticEnd: Bool = false
    
    var initialLocationInView: CGPoint? = nil
    var lastLocationInView: CGPoint? = nil
    var lastMoved: TimeInterval? = nil
    
    let STATIC_END_THRESHOLD: Double = 0.25
    
    override func mouseDown(with event: NSEvent) {
        initialLocationInView = super.location(in: self.view)
        lastLocationInView = initialLocationInView
        lastMoved = Date.timeIntervalSinceReferenceDate
        self.state = .began
    }
    
    override func mouseDragged(with event: NSEvent) {
        if self.state.rawValue <= NSGestureRecognizer.State.changed.rawValue {
            lastLocationInView = super.location(in: self.view)
        }
        self.state = .changed
    }
    
    override func location(in view: NSView?) -> NSPoint {
        guard let view = view else { return .zero }
        guard self.view != nil else { return .zero }
        
        if view == self.view { return lastLocationInView! }
        return self.view!.convert(lastLocationInView!, to: view)
    }
    
    override func mouseUp(with event: NSEvent) {
        staticEnd = Date.timeIntervalSinceReferenceDate - lastMoved! > STATIC_END_THRESHOLD
        
        if self.state.rawValue != NSGestureRecognizer.State.cancelled.rawValue {
            if self.state.rawValue >= NSGestureRecognizer.State.began.rawValue {
                self.state = .ended
            }else {
                self.state = .failed
            }
        }
        
    }
    
    override func reset() {
        super.reset()
        
    }
    
    override func canBePrevented(by preventingGestureRecognizer: NSGestureRecognizer) -> Bool {
        if self.state.rawValue >= NSGestureRecognizer.State.began.rawValue {
            return true
        }
        return false
    }
    
    override func canPrevent(_ preventedGestureRecognizer: NSGestureRecognizer) -> Bool {
        return false
    }
}
