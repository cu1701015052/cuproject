//
//  TFIDFFetchResults.swift
//  CUProject
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Cocoa
import NaturalLanguage

protocol Doc2vecSeedDelegate: class {
  func doc2vecSeed(_ string: String)
}

class TFIDFFetchResults: FetchResults {
  
  var tfidf: Tfidf!
  weak var doc2vecSeedDelegate: Doc2vecSeedDelegate?
  override init?(law: LawSource) {
    super.init(law: law)
    
    buildIndex()
  }
  
  func buildIndex() {
    
    tfidf = Tfidf()
    
    let allRows = Array(realm.objects(Row.self))
    
    let strings = allRows.map {  $0.attributedString().string }
    let names = allRows.map {  ProcessSentenceXML.convertLinkToHumanReadable($0.anchors.first ?? "") }
    
    
    tfidf.add(strings: strings, with: names, type: Doc.self) { success in
      self.tfidf.calcTfidfValues()
      NotificationCenter.default.post(name: LawNotification.highlightDidChangeExternally, object: self, userInfo: nil)
    }
  }
  
  override func attributedStringFromRow(row: Row) -> NSMutableAttributedString {
    
    let mattr = super.attributedStringFromRow(row: row)
    guard self.tfidf.maximumTfidf != nil else { return mattr }
    guard let doc = self.tfidf.doc(with: row.rowNumber) else { return mattr }
    let dict: [String: Double] = tfidf.tfidfValues(in: doc, regularize: false)
    
    let strings = dict.keys
    for string in strings {
      
      var idx = 0
      var aRange: NSRange? = nil
      while idx < mattr.length {
        aRange = (mattr.string as NSString).range(of: string, options: [], range: NSMakeRange(idx, mattr.length - idx))
        if aRange == nil { break }
        if aRange!.location == NSNotFound { break }
        
        let adjustedValue = 1.0 - CGFloat(dict[string]!/tfidf.maximumTfidf!)
        mattr.addAttribute(.highlightColor, value: UIColor(red: adjustedValue, green: adjustedValue, blue: adjustedValue, alpha: 1).cgColor, range: aRange!)
        
        idx = NSMaxRange(aRange!)
      }
    }

    return mattr
  }
  
  func calcCosine(row: Row) {
    let theDoc = self.tfidf.doc(with: row.rowNumber)!
    
    for doc in tfidf.docs {
      (doc as! Doc).tempCosine = tfidf.cosine(theDoc, with:(doc as! Doc))
    }
  }
  
  func calcDoc2vec(row: Row) {
    let attr = super.attributedStringFromRow(row: row)
    //trim header
    var idx = 0
    var range = NSMakeRange(0,0)
    let mattr = NSMutableAttributedString()
    
    idx = 0
    while idx < attr.length {
      if let value = attr.attribute(.tempLink, at: idx, longestEffectiveRange: &range, in: NSMakeRange(0,attr.length)) as? String, value.hasPrefix(LawXMLTypes.headerLinkPrefix) == true {
        
      }else {
        mattr.append(attr.attributedSubstring(from: range))
      }
      
      idx = NSMaxRange(range)
    }
    
    doc2vecSeedDelegate?.doc2vecSeed(mattr.string)
  }
  
  func calcParagraphBasedCosine(row: Row) {
    let attr = super.attributedStringFromRow(row: row)
    //trim header
    var idx = 0
    var range = NSMakeRange(0,0)
    let mattr = NSMutableAttributedString()
    
    idx = 0
    while idx < attr.length {
      if let value = attr.attribute(.tempLink, at: idx, longestEffectiveRange: &range, in: NSMakeRange(0,attr.length)) as? String, value.hasPrefix(LawXMLTypes.headerLinkPrefix) == true {
        
      }else {
        mattr.append(attr.attributedSubstring(from: range))
      }
      
      idx = NSMaxRange(range)
    }
    
    
    doc2vecSeedDelegate?.doc2vecSeed(mattr.string)

  }
}
