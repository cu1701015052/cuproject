//
//  AddingBookmarkViewController.swift
//  eLawsMac
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Cocoa

class AddingBookmarkViewController: NSViewController {
  
  var addBookmarkAction:((_ string: String) -> Void)?

  @IBOutlet weak var textField: NSTextField!
  @objc dynamic var bookmarkTitle: String = "" {
    didSet {
      textField?.stringValue = bookmarkTitle
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do view setup here.
    textField?.stringValue = bookmarkTitle
    
  }
  
  @IBAction func ok(_ sender: Any) {
    addBookmarkAction?(bookmarkTitle)
  }
}
