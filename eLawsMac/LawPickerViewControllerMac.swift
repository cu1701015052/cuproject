//
//  LawPickerViewControllerMac.swift
//  eLawsMac
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Cocoa
import Realm
import RealmSwift

class LawPickerViewControllerMac: NSViewController, NSTableViewDelegate, NSTableViewDataSource {

  var completion: (()->Void)!
  var didPickHandler: ((_ law: LawSource?, _ anchor: String?, _ eLawsCompatible: Bool)->Void)!

 lazy var allDownloadedLaws: RealmSwift.Results<DownloadedLaw> = RealmManager.shared.allDownloadedLaws

  var results: [EGovLaw]?

  @IBOutlet weak var searchBar: NSSearchField!
  
  @IBOutlet weak var tableView: NSTableView!

  @IBAction func searchBarInputted(_ sender: Any) {
    updateSearchResults(for: searchBar.stringValue)
  }
  
  func updateSearchResults(for text: String) {
    if text.isEmpty == false {
      let filtered = LawList().searchLaw(text)
      self.results = filtered
      
      let _ = text.replacingOccurrences(of: "　", with: " ").components(separatedBy: " ")

    }else {
      self.results = nil
    }
    self.tableView.reloadData()
  }

  func numberOfRows(in tableView: NSTableView) -> Int {
    if results == nil { return 0 }
    return results!.count
  }
  
  func tableView(_ tableView: NSTableView, willDisplayCell cell: Any, for tableColumn: NSTableColumn?, row: Int) {
    guard let cell = cell as? NSTextFieldCell else { return }
    let thisItem = results![row]
    var title = thisItem.lawTitle
    if allDownloadedLaws.filter(NSPredicate(format: "lawNum == %@", thisItem.lawNum)).count != 0 {
      title = "✅" + title
    }
    cell.stringValue = title //+ " " + results![indexPath.row].lawNo
    
  }
  
  func tableView(_ tableView: NSTableView, shouldSelectRow row: Int) -> Bool {
    
    let law = results![row]
    self.didPickHandler(law, nil, law.eLawsCompatible)
    tableView.reloadData()

    return true
  }
  


}
