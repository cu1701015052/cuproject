//
//  ELawsViewControllerMac.swift
//  eLawsMac
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Cocoa

extension NSOutlineView {
  open override var canBecomeKeyView: Bool { return false }
  open override func becomeFirstResponder() -> Bool { return false  }
}

class MyOutlineView: NSOutlineView {
  override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
    return .generic
  }
  
  override func prepareForDragOperation(_ sender: NSDraggingInfo) -> Bool {
    return true
  }
  
}

class ELawsViewControllerMac: NSViewController, NSOutlineViewDelegate, NSOutlineViewDataSource {

  var viewModel: ELawsViewModel = ELawsViewModel(filepath: "/", title: "法令")
  @IBOutlet weak var blankMessage: NSTextField!
  @IBOutlet weak var outlineView: NSOutlineView!
  var visualEffectView: NSVisualEffectView? {
   return self.view.superview as? NSVisualEffectView
  }
  weak var hairlineView: NSView? = nil
  override func viewDidLoad() {
    super.viewDidLoad()
    blankMessage.isHidden = true
    outlineView.backgroundColor = .clear
    
    let view = NSView(frame: NSMakeRect(self.view.bounds.maxX - 1, 0, 1, self.view.bounds.size.height))
    view.wantsLayer = true
    view.autoresizingMask = [.minXMargin, .height]
    self.view.addSubview(view)
    self.hairlineView = view
    hairlineView?.layer?.backgroundColor = viewModel.backgroundColorScheme.inverted ? NSColor.darkGray.cgColor : RGBA(220,220,220,1).cgColor

    let center = NotificationCenter.default
    center.addObserver(self, selector: #selector(realmDidUpdate), name: LawNotification.realmDidUpdateFromExternNotification, object: nil)
    center.addObserver(self, selector: #selector(realmWillUpdate), name: LawNotification.realmWillUpdateNotification, object: nil)

    center.addObserver(self, selector: #selector(reload), name: LawNotification.settingsDidChange, object: nil)
    center.addObserver(self, selector: #selector(reload), name: LawNotification.bookmarkDidChangeExternally, object: nil)
    
    
    outlineView.registerForDraggedTypes([NSPasteboard.PasteboardType(rawValue: "jp.ac.cyber-u.1701015052.CUProject.LawMac.outlineReorder")])
    outlineView.setDraggingSourceOperationMask(.every, forLocal: true)
    outlineView.setDraggingSourceOperationMask(.every, forLocal: false)
    outlineView.autoresizesOutlineColumn = false

    outlineView.autosaveExpandedItems = true
  }
  
  var draggingItem: Organizable? = nil
  func outlineView(_ outlineView: NSOutlineView, pasteboardWriterForItem item: Any) -> NSPasteboardWriting? {
    draggingItem = item as? Organizable
    if draggingItem == nil { return nil }
    let pboard = NSPasteboardItem()
    pboard.setString(draggingItem!.uuid, forType: NSPasteboard.PasteboardType(rawValue: "jp.ac.cyber-u.1701015052.CUProject.LawMac.outlineReorder"))
    return pboard
  }
  
  func outlineView(_ outlineView: NSOutlineView, draggingSession session: NSDraggingSession, willBeginAt screenPoint: NSPoint, forItems draggedItems: [Any]) {
    print(draggedItems)
  }
  
  func outlineView(_ outlineView: NSOutlineView, validateDrop info: NSDraggingInfo, proposedItem item: Any?, proposedChildIndex index: Int) -> NSDragOperation {
    return NSDragOperation.move
  }
  
  
  func outlineView(_ outlineView: NSOutlineView, acceptDrop info: NSDraggingInfo, item: Any?, childIndex index: Int) -> Bool {
    
    var index = index
    if index == -1 { index = 0 }
    guard let originalItem = draggingItem else {
      outlineView.reloadData()
      return false
    }
    draggingItem = nil
    if item == nil {
      finalizingHandling  = true
      outlineView.reloadData()
      finalizingHandling  = false

      viewModel.moveItem(originalItem, toItem: nil, at: index)
      outlineView.reloadData()
      return true
    }
    
    if item is Bookmark { return false }
    guard originalItem.isAnscestory(of: item as! Organizable) == false else { return false }
    guard originalItem != item as! Organizable else { return false }
    finalizingHandling  = true
    outlineView.reloadData()
    finalizingHandling  = false

    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
    self.viewModel.moveItem(originalItem, toItem: item, at: index)
    self.viewModel.reload()
    outlineView.reloadData()
    }
    return true
  }

  @objc func reload() {
    hairlineView?.layer?.backgroundColor = viewModel.backgroundColorScheme.inverted ? NSColor.darkGray.cgColor : RGBA(220,220,220,1).cgColor
    visualEffectView?.material = viewModel.backgroundColorScheme.inverted ? .ultraDark : .light
    outlineView.reloadData()
  }

  @objc func realmWillUpdate() {
//    self.finalizingHandling  = true
//    self.outlineView.reloadData()
    
    blankMessage.isHidden = viewModel.outlineView(numberOfChildrenOfItem: nil) != 0

  }
  
  @objc func realmDidUpdate() {
//    self.finalizingHandling  = false

    
    viewModel.reload()
    outlineView.reloadData()
  }
  
  //MARK:-
  
  override func viewWillAppear() {
    super.viewWillAppear()
    reload()
  }
  
  var processingLaws: [DownloadedLaw] = []
  
  func reloadTableRowForLaw(_ law: DownloadedLaw, message: String) {
//    guard let idx = viewModel.source.index(of: law) else { return }
//    DispatchQueue.main.async {
//      self.outlineView.reloadData()
//    }
  }
  
  
  override func prepare(for segue: NSStoryboardSegue, sender: Any?) {
    
    if segue.identifier == "LawPicker", let controller = segue.destinationController as? LawPickerViewControllerMac {
    controller.didPickHandler = { (pickedLaw, _, eLawsCompatible) in
      
      guard let lawNum = pickedLaw?.lawNum else { return }
      // ASSIGN FILE PATH
      
      do {
        let token = shortUUIDString()
        let law = try RealmManager.shared.add(lawNum: lawNum, lawEdition: token, lawTitle: pickedLaw!.lawTitle, under: self.viewModel.filepath)
        self.processingLaws.append(law)
        self.viewModel.reload()
        self.outlineView.reloadData()

        Downloader.shared.downloadLawFromAPIThenCloudKitThenSakuraServerV2(law, progress: { (progress) in

          if progress > 0 {
            let formatter = NumberFormatter()
            formatter.numberStyle = .percent
            if let string = formatter.string(for: progress) {
              self.reloadTableRowForLaw(law, message: string)
            }
          }else {
            let downloadedSize = ByteCountFormatter.string(fromByteCount: Int64(-progress), countStyle: ByteCountFormatter.CountStyle.file)
            self.reloadTableRowForLaw(law, message: downloadedSize)
          }


        }) { (downloadedLaw, error) in
        
          do {
            if error != nil || downloadedLaw == nil {
              displayErrorAlertIfNecessary(error: error)
              if let idx = self.processingLaws.index(of: law) {
                self.processingLaws.remove(at: idx)
              }

            }else {

              let predicate = NSPredicate(format: "lawNum == %@ && lawEdition == %@", downloadedLaw!.lawNum, token)
              if let law = RealmManager.shared.allDownloadedLaws.filter(predicate).first {
                let success = try RealmManager.shared.update(law: law, lawEdition: downloadedLaw!.lawEdition, lawTitle: downloadedLaw!.lawTitle, filename: downloadedLaw!.filename)


                // PROCESS
                if success {
                  let nextModel = DetailViewModel(law: law)
                  nextModel.progressHandler = { progressMessage in
                    self.reloadTableRowForLaw(law, message: progressMessage)
                  }
                  nextModel.load { (error) in

                    if error != nil {
                      displayErrorAlertIfNecessary(error: error)
                    }

                    if let idx = self.processingLaws.index(of: law) {
                      self.processingLaws.remove(at: idx)
                    }
                    self.viewModel.reorder()
                    self.viewModel.reload()
                    self.outlineView.reloadData()
                    self.outlineView.reloadData()

                  }
                }else {
                  if let idx = self.processingLaws.index(of: law) {
                    self.processingLaws.remove(at: idx)
                  }
                }
              }
            }
          }catch let error {
            displayErrorAlertIfNecessary(error: error)
          }
        }

      }catch let error {
        displayErrorAlertIfNecessary(error: error)
      }
      
    }
    }
    
  }
  
  @IBOutlet weak var addButton: NSButton!
  @IBOutlet weak var editButton: NSButton!
  @IBOutlet weak var deleteButton: NSButton!
  
  var isEditing = false {
    didSet {
      deletingOrganizable = []
      deleteButton.isHidden = !isEditing
      addButton.isHidden = isEditing
      editButton.state = isEditing ? .on : .off
      deleteButton.isEnabled = deletingOrganizable.count > 0
      editButton.title = isEditing ? "編集終了" : "編集"

    }
  }
  
  @IBAction func deleteAction(_ sender: Any) {
    guard deletingOrganizable.count > 0 else { return }
    
    let alert = NSAlert()
    alert.messageText = WLoc("Delete Record?")
    alert.addButton(withTitle: WLoc("Cancel"))
    alert.addButton(withTitle: WLoc("Delete"))
    
    alert.beginSheetModal(for: self.view.window!, completionHandler: { result in
      if result == NSApplication.ModalResponse.alertSecondButtonReturn {
        print("delete")
        self.finalizingHandling  = true
        self.outlineView.reloadData()
        self.finalizingHandling  = false
        for organizable in self.deletingOrganizable {
          _ = RealmManager.shared.delete(organizable: organizable)
        }
        self.viewModel.reload()
        NotificationCenter.default.post(name: LawNotification.bookmarkDidChangeExternally, object: self, userInfo: nil)
        
        self.finalizingHandling  = false
        self.outlineView.reloadData()
        self.isEditing = false

      }
      print(result)
      
    })
    
  }
  
  @IBAction func editAction(_ sender: Any) {
    isEditing = editButton.state == .on
    self.outlineView.reloadData()
  }
  
  override func viewDidAppear() {
    if viewModel.outlineView(numberOfChildrenOfItem: nil) == 0 {
      blankMessage.isHidden = false
    }
  }
  
  func openOrganizable(_ organizable: Organizable) {
    if organizable is DownloadedLaw {
      let viewModel = DetailViewModel(law: organizable as! DownloadedLaw, delegate: nil)
      
      do {
        let _ = try NSDocumentController.shared.makeUntitledDocument(ofType: "jp.ac.cyber-u.1701015052.cuproject")
        
        if let doc = self.view.window?.windowController?.document as? DetailDocument {
          let viewController = DetailViewControllerMac(viewModel: viewModel)
          doc.setRootViewController(viewController)
        }
        
      } catch let error {
        displayErrorAlertIfNecessary(error: error)
      }
      
    }else if organizable is Bookmark {
      // WHICH EDITION OF LAW IS THIS POINTING??
      
      let bookmark = organizable as! Bookmark
      let law = LawDescriptor(lawTitle: "", lawNum: bookmark.lawNo, lawEdition: "", filename: "", filepath: bookmark.filepath, downloadOption: .regular)
      let viewModel = DetailViewModel(law: law, delegate: nil)
      viewModel.startupAnchor = (organizable as! Bookmark).anchor

      do {
        let _ = try NSDocumentController.shared.makeUntitledDocument(ofType: "jp.ac.cyber-u.1701015052.cuproject")

        if let doc = self.view.window?.windowController?.document as? DetailDocument {
          let viewController = DetailViewControllerMac(viewModel: viewModel)
          doc.setRootViewController(viewController)
        }
        
      } catch let error {
        displayErrorAlertIfNecessary(error: error)
      }
      
      
      
    }else {
//      openFolder(organizable)
    }
  }
  
  //MARK:-
  var    finalizingHandling  = false
  func outlineView(_ outlineView: NSOutlineView, numberOfChildrenOfItem item: Any?) -> Int {
    if  finalizingHandling  { return 0 }
    return viewModel.outlineView(numberOfChildrenOfItem: item as! Organizable!)
  }
  
  func outlineView(_ outlineView: NSOutlineView, isItemExpandable item: Any) -> Bool {
    return viewModel.outlineView(isItemExpandable: item as! Organizable)
  }
  
  func outlineView(_ outlineView: NSOutlineView, child index: Int, ofItem item: Any?) -> Any {
    return viewModel.outlineView(child: index, ofItem: item as! Organizable!)
  }
  
  func outlineView(_ outlineView: NSOutlineView, viewFor tableColumn: NSTableColumn?, item: Any) -> NSView? {
    let baseView = outlineCell(item: item as! Organizable)
    return baseView
  }
//  func outlineView(_ outlineView: NSOutlineView, objectValueFor tableColumn: NSTableColumn?, byItem item: Any?) -> Any? {
//    <#code#>
//  }
  
  
  func outlineView(_ outlineView: NSOutlineView, heightOfRowByItem item: Any) -> CGFloat {
    return 30;
  }
  
  func outlineCell(item: Organizable) -> NSView {
    let backgroundColorScheme = viewModel.backgroundColorScheme
    
    var baseWidth: CGFloat = 320
    let baseView = NSView(frame: viewModel.baseFrame(baseWidth: baseWidth))
    baseView.autoresizingMask = .width
//    baseView.backgroundColor = UIColor.clear
    
    if item.isInvalidated { return baseView }
    
    var labelWidth = baseWidth
    if self.isEditing {
      labelWidth -= 30
    }
    
    if let item = item as? DownloadedLaw, processingLaws.contains(item), self.isEditing == false {
      labelWidth -= 30

    }
    
    let label = NSTextField(frame: viewModel.labelFrame(baseWidth: labelWidth, forItem: item))
    label.autoresizingMask = .width
    label.isBordered = false
    label.backgroundColor = NSColor.clear
    label.isEditable = false
    label.lineBreakMode = .byTruncatingMiddle
//    label.minimumScaleFactor = 0.8
//    label.adjustsFontSizeToFitWidth = true
//    label.numberOfLines = 0
    
    baseView.addSubview( label )
    
    // Small Font for Bookmark
    
    if let selectionObject = item as? SelectionObject {
      let attributedText = NSAttributedString(string: item.title as String, attributes: viewModel.highlightAttributes(for: selectionObject))
      label.attributedStringValue = attributedText
      
    }else {
      // Setup text
      label.font = viewModel.font(for: item)
      label.stringValue = item.titleToDisplay()
      label.textColor = viewModel.backgroundColorScheme.textColor
    }
    
    // Download button
    let status = (item as? DownloadedLaw)?.status
    let accessoryViewFrame = viewModel.accessoryViewFrame(baseWidth: baseWidth)
    
    if let item = item as? DownloadedLaw, processingLaws.contains(item) {
      
      let imageView = NSProgressIndicator(frame: accessoryViewFrame )
      imageView.style = .spinning
      imageView.controlSize = .mini
//      imageView.tag = 99
//      imageView.autoresizingMask = .flexibleLeftMargin
      baseView.addSubview( imageView )

      // Loading animation
      imageView.isIndeterminate = true
      imageView.autoresizingMask = [.minXMargin, .maxYMargin]
      imageView.startAnimation(nil)
    }
    
    if status == .inCloud {
//      
//      let imageView = NSImageView(frame: accessoryViewFrame )
//
//      imageView.autoresizingMask = .minXMargin
//      baseView.addSubview( imageView )
//
//      // "Download from Cloud" Image
//      imageView.image = viewModel.downloadImage
    }
    
    if self.isEditing {
      
      
      let imageView = NSImageView(frame: accessoryViewFrame)

      if deletingOrganizable.contains(item) || deletingOrganizable.filter ({ $0.isAnscestory(of: item) }).count > 0 {
        imageView.image = colorizedImage(originalImage: NSImage(named:"DBChecked")!, withTintColor: .red, alpha: 1)

      }else {
        imageView.image = NSImage(named: "LawProgress")

      }
      baseView.addSubview( imageView )
      
      // Loading animation
      imageView.autoresizingMask = [.minXMargin, .maxYMargin]

    }

    
    return baseView
  }
  
  var deletingOrganizable: [Organizable] = [] {
    didSet {
      deleteButton.isEnabled = deletingOrganizable.count > 0
    }
  }
  
  func outlineView(_ outlineView: NSOutlineView, shouldSelectItem item: Any) -> Bool {
    if let item = item as? DownloadedLaw {
      if processingLaws.contains(item) { return false }
    }
    
    if isEditing {
      if let idx = deletingOrganizable.index(of: item as! Organizable) {
        deletingOrganizable.remove(at: idx)
      }else {
        if deletingOrganizable.contains(item as! Organizable) || deletingOrganizable.filter ({ $0.isAnscestory(of: item as! Organizable) }).count > 0 {
          // ALREADY CHECKED
        }else {
        deletingOrganizable.append(item as! Organizable)
        }
      }
      self.outlineView.reloadData()
      return false
    }

    return true
  }
  
  func outlineViewSelectionDidChange(_ notification: Notification) {
    
    if let item = outlineView.item(atRow: outlineView.selectedRow) as? Organizable {
      openOrganizable(item)
      outlineView.deselectAll(nil)
    }
  }
  
  func outlineView(_ outlineView: NSOutlineView, itemForPersistentObject object: Any) -> Any? {
    guard let uuid = object as? String else { return nil }
    return viewModel.organizable(with: uuid)
  }
  func outlineView(_ outlineView: NSOutlineView, persistentObjectForItem item: Any?) -> Any? {
    if item is Organizable { return (item as! Organizable).uuid }
    return nil
  }
}
