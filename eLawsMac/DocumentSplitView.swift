//
//  DocumentSplitView.swift
//  eLawsMac
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Cocoa

class DocumentSplitView: NSSplitView {
  
  override var dividerColor: NSColor {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    
    return backgroundColorScheme.backgroundColor
  }
  
  var secondaryDividerColor: NSColor {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    
    return backgroundColorScheme.secondaryBackgroundColor
  }
  
  override func draw(_ dirtyRect: NSRect) {
    
    dividerColor.set()
    dirtyRect.fill()
  }
  
  
}

