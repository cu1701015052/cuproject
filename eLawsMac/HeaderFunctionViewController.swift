//
//  HeaderFunctionViewController.swift
//  eLawsMac
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Cocoa

class HeaderFunctionViewController: NSViewController {
  
  var calcCosineAction:(() -> Void)?
  var calcDoc2vecAction:(() -> Void)?

  @IBAction func calcCosine(_ sender: Any) {
    calcCosineAction?()
  }
  
  @IBAction func calcDoc2vec(_ sender: Any) {
    calcDoc2vecAction?()
  }
  
}
