//
//  MeganeLongPressGestureRecognizer.m
//  MNCTTextView
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright 2010 Masatoshi Nishikata. All rights reserved.
//

#import "MeganeLineGestureRecognizer.h"

@implementation MeganeLineGestureRecognizer

@synthesize bottom = bottom_;
@synthesize offset = offset_;
@synthesize staticEnd = staticEnd_;

#define STATIC_END_THRESHOLD 0.25

- (void)mouseDown:(NSEvent *)event;
{
	
	self.initialLocationInView = [super locationInView:[self view]];
	lastLocationInView = self.initialLocationInView;
  
	//self.state = UIGestureRecognizerStateBegan;

//  startTimer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(startGesture) userInfo:nil repeats:NO];

	lastMoved = [NSDate timeIntervalSinceReferenceDate];

	self.state = NSGestureRecognizerStateBegan;
}



#define POINT_DISTANCE(p1, p2) \
sqrtf( (p1.x - p2.x )*(p1.x - p2.x ) + (p1.y - p2.y )*(p1.y  - p2.y ) )

- (void)mouseDragged:(NSEvent *)event;
{
//  [startTimer invalidate];
//  startTimer = nil;
	

  if( self.state <= NSGestureRecognizerStateChanged )
  {
    
    lastLocationInView = [super locationInView:[self view]];
    
//    if( POINT_DISTANCE( [event previousLocationInView:[self view]] , lastLocationInView ) > 1.5 )
//      lastMoved = [NSDate timeIntervalSinceReferenceDate];
//
  }
  self.state = NSGestureRecognizerStateChanged;

}

-(CGPoint)locationInView:(NSView*)view
{
	if( view == [self view] ) return lastLocationInView;
	
	return [[self view] convertPoint:lastLocationInView toView:view];
}

//-(CGPoint)locationInWindow
//{
//  if( [[self view] isKindOfClass:[UIWindow class]] ) return lastLocationInView;
//
//  return [self locationInView: [self view].window];
//}

- (void)mouseUp:(NSEvent *)event;
{
//  [startTimer invalidate];
//  startTimer = nil;
  
  staticEnd_ = ( [NSDate timeIntervalSinceReferenceDate] - lastMoved > STATIC_END_THRESHOLD );
  
  
  if( self.state !=  NSGestureRecognizerStateCancelled  )
  {
    if( self.state >= NSGestureRecognizerStateBegan )
      self.state = NSGestureRecognizerStateEnded;

    else
      self.state = NSGestureRecognizerStateFailed;
  }
  
  touchHash = 0;
  lastMoved = 0;
}

- (void)reset
{
	[super reset];
	
//  [startTimer invalidate];
//  startTimer = nil;
	
	touchHash = 0;
}

- (BOOL)canBePreventedByGestureRecognizer:(NSGestureRecognizer *)preventingGestureRecognizer
{
	if( self.state >= NSGestureRecognizerStateBegan ) return YES;
	return NO;
}
- (BOOL)canPreventGestureRecognizer:(NSGestureRecognizer *)preventedGestureRecognizer
{
//  NSUInteger numberOfTouches = preventedGestureRecognizer.numberOfTouches;
//  for( NSUInteger hoge = 0; hoge < numberOfTouches; hoge++ )
//  {
//
//    CGPoint theirPoint = [preventedGestureRecognizer locationOfTouch:hoge inView:self.view];
//    CGPoint myPoint = [self locationInView:[self view]];
//
//    if( CGPointEqualToPoint(theirPoint, myPoint) ) return YES;
//  }
//
	return NO;
}
//- (void)ignoreTouch:(UITouch *)touch forEvent:(UIEvent *)event;
//{
//  if( [touch hash] == touchHash )
//  {
//    if( self.state !=  UIGestureRecognizerStateCancelled )
//      self.state = UIGestureRecognizerStateCancelled;
//
//    touchHash = 0;
//    initialLocationInView = CGPointZero;
//  }
//}

@end
