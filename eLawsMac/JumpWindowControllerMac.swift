//
//  JumpViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright (c) 2014 Masatoshi Nishikata. All rights reserved.
//

import Foundation
import Cocoa

/*
extension UITextField {
  func modifyClearButton(with image : UIImage) {
    let clearButton = UIButton(type: .custom)
    clearButton.setImage(image, for: .normal)
    clearButton.frame = CGRect(x: 0, y: 0, width: 15, height: 15)
    clearButton.contentMode = .scaleAspectFit
    clearButton.addTarget(self, action: #selector(UITextField.clear(_:)), for: .touchUpInside)
    rightView = clearButton
    rightViewMode = .whileEditing
  }
  
  func clear(_ sender : AnyObject) {
    self.text = ""
    sendActions(for: .editingChanged)
  }
}
*/

class MyButton: NSButton {
  override var allowsVibrancy: Bool { return true }
  
  override func awakeFromNib() {
    self.wantsLayer = true
    self.appearance = NSAppearance(appearanceNamed: .vibrantDark, bundle: nil)
  }
}

class JumpWindowControllerMac: NSWindowController {
	
  static var shared = JumpWindowControllerMac(windowNibName: "JumpWindowControllerMac")
  
  @IBOutlet weak var visualEffectView: NSVisualEffectView!
  @IBOutlet weak var connectorButton: NSButton!
  @IBOutlet weak var noButton: NSButton!
  
  // User Interface for Keypad

  @IBOutlet weak var textField: NSTextField?
  @IBOutlet weak var jumpButton: NSButton!
  
  var isVisible: Bool { return self.window?.isVisible ?? false }
  
//  var jumpAction2: ((_ text: String?, _ tabIdentifierOrDocItem: AnyObject?, _ excerpt: Bool)->Void)? = nil
//  var cancelBlock: ((_ swtichToFind: Bool)->())? = nil
//  var pickerBlock: (()->())? = nil

  var connector: String? = nil
	var connectorStatus = ConnectorStatus(string: "")
	
	var inputtedString: String = ""
	var showTab_: Bool = true

	var allDocuments: NSArray? = nil

  var backgroundColorScheme: BackgroundColorSchemeType = .Paper

  override func windowDidLoad() {
    super.windowDidLoad()
    visualEffectView.appearance = NSAppearance(appearanceNamed: .vibrantLight, bundle: nil)

  }
  
  override func insertText(_ insertString: Any) {
    guard let string = insertString as? String else { return }
    inputtedString = inputtedString + string
    textField?.stringValue = inputtedString
    connectorStatus.transitionWithString(string)
    updateUI()
  }
	
	func enableConnectorButton() -> Bool {
		return true
	}
  
  override func performKeyEquivalent(with event: NSEvent) -> Bool {
    if event.keyCode == 29 { self.insertText("0") }
    if event.keyCode == 18 { self.insertText("1") }
    if event.keyCode == 19 { self.insertText("2") }
    if event.keyCode == 20 { self.insertText("3") }
    if event.keyCode == 21 { self.insertText("4") }
    if event.keyCode == 23 { self.insertText("5") }
    if event.keyCode == 22 { self.insertText("6") }
    if event.keyCode == 26 { self.insertText("7") }
    if event.keyCode == 28 { self.insertText("8") }
    if event.keyCode == 25 { self.insertText("9") }
    if event.keyCode == 49 { self.inputConnector(nil) }
    if event.keyCode == 27 { self.insertText("の") }
    if event.keyCode == 36 { self.jumpTapped(nil) }
    if event.keyCode == 51 { self.deleteBackward(nil) }

    return true
  }
  
	
	//MARK:- Actions
  
  @IBAction func keyTyped(_ sender: NSButton?) {
    guard let tag = sender?.tag else { return }
    switch tag {
    case 0: insertText("0")
    case 1: insertText("1")
    case 2: insertText("2")
    case 3: insertText("3")
    case 4: insertText("4")
    case 5: insertText("5")
    case 6: insertText("6")
    case 7: insertText("7")
    case 8: insertText("8")
    case 9: insertText("9")
    case -1: insertText("の")
    default: break
    }
  }
	
	@IBAction func inputConnector(_ sender: Any?) {
		
		if connectorStatus.keyTopString == "号" && inputtedString.hasSuffix("項") {
			inputtedString = String( inputtedString.dropLast() )
		}

    insertText(connectorStatus.keyTopString)
  }
  
  @IBAction func jumpTapped(_ sender: Any?) {
    let selector = NSSelectorFromString("jumpTappedFromPanel:")
    NSApp.sendAction(selector, to: nil, from: self)
    
    textField?.stringValue = ""
    inputtedString = ""
	}
  
  @IBAction override func deleteBackward(_ sender: Any?) {
		
		if inputtedString == "" { return }
		inputtedString = String( inputtedString.dropLast() )
		textField?.stringValue = inputtedString
		connectorStatus = ConnectorStatus(string: inputtedString)

		updateUI()
	}
	
//  @IBAction func close(_ sender: AnyObject) {
//    cancelBlock?(false)
//  }
	
//  func dismissKeyboard() { //aka jump
//    cancelBlock?(false)
//  }
	
//  func findTapped() {  //aka jump
//    cancelBlock?(true)
//  }
	
//  @IBAction func pickerTapped(_ sender: AnyObject) {  //aka jump
//    pickerBlock?()
//  }
	
//  func numberPadTouchesBegan() {
//  }
	
	/*
	func hasText() -> Bool {
	
	if inputtedString.isEmpty == false { return true }
	
	
	var identifiers = all
	
	if identifiers.contains(current!) == false {
	identifiers.insert(current!, atIndex: 0)
	}
	
	
	let tabIdentifier = identifiers[tabPickerView.selectedRowInComponent(0)]
	
	if tabIdentifier == current { return false }
	
	return true
	}
	
:
	*/
	
  
  func updateUI() {
    connectorButton.title = connectorStatus.keyTopString
	}
}

