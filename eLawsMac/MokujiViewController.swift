//
//  MokujiViewController.swift
//  eLawsMac
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Cocoa

class MokujiViewController: NSViewController {
  
  @IBOutlet weak var segmentedControl: NSSegmentedControl!
  @IBOutlet weak var messageLabel: NSTextField!
  var selectedIndex: Int = 0 {
    didSet {
      setupView()
    }
  }
  
  @IBOutlet weak var bodyView: NSView!
  weak var hairlineView: NSView? = nil
  var visualEffectView: NSVisualEffectView? {
    return self.view.superview as? NSVisualEffectView
  }
  var backgroundColorScheme: BackgroundColorSchemeType {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
  var viewControllers: [NSViewController] = [] {
    didSet {
      setupView()
    }
  }
  
  @objc func setupView() {
    
    guard viewControllers.count == 3 else {
      bodyView.isHidden = true
      return
    }
    
    visualEffectView?.material = backgroundColorScheme.inverted ? .ultraDark : .light

    bodyView.isHidden = false
    segmentedControl.setLabel("条文間", forSegment: 0)
    segmentedControl.setLabel("法令間", forSegment: 1)
    segmentedControl.setLabel("段落doc2vec", forSegment: 2)
//    segmentedControl.setLabel("段落TF-IDF", forSegment: 3)

    bodyView.subviews.forEach { $0.removeFromSuperview() }
    
    let view = viewControllers[selectedIndex].view
    view.frame = bodyView.bounds
    view.autoresizingMask = [.width, .height]
    bodyView.addSubview(view)

    hairlineView?.layer?.backgroundColor = backgroundColorScheme.inverted ? NSColor.darkGray.cgColor : RGBA(220,220,220,1).cgColor
    
  }


  override func viewDidLoad() {
    super.viewDidLoad()
    let center = NotificationCenter.default

    center.addObserver(self, selector: #selector(setupView), name: LawNotification.settingsDidChange, object: nil)

    let view = NSView(frame: NSMakeRect(self.view.bounds.maxX - 1, 0, 1, self.view.bounds.size.height))
    view.wantsLayer = true
    view.autoresizingMask = [.minXMargin, .height]
    self.view.addSubview(view)
    self.hairlineView = view
    hairlineView?.layer?.backgroundColor = backgroundColorScheme.inverted ? NSColor.darkGray.cgColor : RGBA(220,220,220,1).cgColor

  }
  override func viewWillAppear() {
    super.viewWillAppear()
    setupView()
  }
  
  @IBAction func segmentedControlSelected(_ sender: Any) {
    selectedIndex = segmentedControl.selectedSegment
    
    if selectedIndex == 0 {
     messageLabel.stringValue = "選択された法令における、条文間の TF−IDFコサイン類似度を表示します。右の本文の条文番号をクリックし、「コサイン類似度を計算」を選択します。"
    }
    
    if selectedIndex == 1 {
      messageLabel.stringValue = "選択された法令に類似している他の法令をdoc2vecにより探します。"
    }
    
    if selectedIndex == 2 {
      messageLabel.stringValue = "類似条文を全法令から検索します。"
    }
  }

}
