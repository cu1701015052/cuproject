//
//  DetailViewControllerMac.swift
//  LawXML
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
import Cocoa


//class OnDiskXMLLaw: LawSource {
//  var lawTitle: String = "XML Document"
//  
//  var lawNum: String = ""
//  
//  var lawEdition: String = ""
//  
//  var xml: Data?
//  
//  var downloadOption: DownloadOption = .inMemory
//  
//  var fileURL: URL
//  init(fileURL: URL) {
//    self.fileURL = fileURL
//    self.xml = try! Data(contentsOf: fileURL)
//    Downloader().parse(url: fileURL) { (dict, error) in
//      if let lawNum = dict["lawNum"], let lawEdition = dict["lawEdition"], let title = dict["lawTitle"]  {
//        self.lawTitle = title
//        self.lawNum = lawNum
//        self.lawEdition = lawEdition
//      }
//    }
//  }
//
//}

class MyWindow: NSWindow {
  
  override func keyDown(with event: NSEvent) {
    if JumpWindowControllerMac.shared.isVisible {
      _ = JumpWindowControllerMac.shared.performKeyEquivalent(with: event)
    }
  }
  
  var hasDocument: Bool {
    guard let doc = self.windowController?.document as? DetailDocument else { return false }

    return doc.detailViewControllerMacStack.count > 0
  }
  
  func checkKanjiMenu(_ sender: NSMenuItem) {
    sender.state = .off
    
    if sender.tag - 30 == UserDefaults.standard.integer(forKey: "KanjiType") {
      sender.state = .on
    }
  }
  
  func checkBackgroundMenu(_ sender: NSMenuItem) {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    var scheme: BackgroundColorSchemeType = .Paper
    switch sender.tag {
    case 10: scheme = .White
    case 11: scheme = .Paper
    case 12: scheme = .Sepia
    case 13: scheme = .Grey
    case 14: scheme = .Dark
    default: scheme = .Paper
    }
    
    if scheme == backgroundColorScheme {
      sender.state = .on
    }else {
      sender.state = .off
    }
  }

  func checkFontMenu(_ sender: NSMenuItem) {
    sender.state = .off
    
    if sender.tag - 20 == UserDefaults.standard.integer(forKey: "FontName") {
      sender.state = .on
    }
  }
  
  override func validateMenuItem(_ menuItem: NSMenuItem) -> Bool {
    if menuItem.tag == 1 { return hasDocument }
    if menuItem.tag == 2 { return hasDocument }
    if menuItem.tag == 3 { return hasDocument }
    if menuItem.tag == 4 { return hasDocument }

    if (30...32).contains(menuItem.tag) { checkKanjiMenu(menuItem) }
    if (10...14).contains(menuItem.tag) { checkBackgroundMenu(menuItem) }
    if (20...21).contains(menuItem.tag) { checkFontMenu(menuItem) }
    
    return true
  }
  
  @IBAction func zoomIn(_ sender: Any) {
    guard let doc = self.windowController?.document as? DetailDocument else { return }
    doc.zoomIn(sender)
  }
  
  @IBAction func zoomOut(_ sender: Any) {
    guard let doc = self.windowController?.document as? DetailDocument else { return }
    doc.zoomOut(sender)
  }
  
  @IBAction func performFindPanelAction(_ sender: Any) {
    guard let doc = NSDocumentController.shared.currentDocument as? DetailDocument else { return }
    doc.performFindPanelAction(sender)
  }
  
  @IBAction func performJumpPanelAction(_ sender: Any) {
    guard let doc = NSDocumentController.shared.currentDocument as? DetailDocument else { return }
    doc.performJumpPanelAction(sender)
  }
  
  @IBAction func jumpTappedFromPanel(_ sender: Any?) {
    guard let doc = NSDocumentController.shared.currentDocument as? DetailDocument else { return }
    doc.jumpTappedFromPanel(sender)
  }
  
  @IBAction func toggleSourceList(_ sender: Any) {
    guard let doc = NSDocumentController.shared.currentDocument as? DetailDocument else { return }
    doc.toggleSourceList(sender)
  }
  
  @IBAction func toggleContentsList(_ sender: Any) {
    guard let doc = NSDocumentController.shared.currentDocument as? DetailDocument else { return }
    doc.toggleContentsList(sender)
  }
  
  @IBAction func changeBackgroundScheme(_ sender: NSMenuItem) {
    guard let doc = NSDocumentController.shared.currentDocument as? DetailDocument else { return }
    doc.changeBackgroundScheme(sender)
  }
  
  @IBAction func changeFontType(_ sender: NSMenuItem) {
    guard let doc = NSDocumentController.shared.currentDocument as? DetailDocument else { return }
    doc.changeFontType(sender)
  }
  
  @IBAction func changeKanji(_ sender: NSMenuItem) {
    guard let doc = NSDocumentController.shared.currentDocument as? DetailDocument else { return }
    doc.changeKanji(sender)
  }
  
  
  @IBAction func paragpaphTfidf(_ sender: Any) {
    guard let doc = NSDocumentController.shared.currentDocument as? DetailDocument else { return }
    doc.toggleParagpaphTfidf(sender)
  }
}



//
// ROUTER
//
class DetailDocument: NSDocument, NSPageControllerDelegate {

//  var viewModel: DetailViewModel! {
//    didSet {
//      let controller = DetailViewControllerMac(nibName:NSNib.Name(rawValue: "DetailViewControllerMac"), bundle:nil)
//
//      pushViewController(controller)
//      viewModel.delegate = controller
//
//    }
//  }
  
  var detailViewControllerMacStack: [DetailViewControllerMac] = []

  var titleBarAccessory = DetailDocumentTitleViewController(nibName: "DetailDocumentTitleViewController", bundle: nil)
  
  var bookmarkViewModel: BookmarkViewModel!
  var highlightListTableViewModel: HighlightListTableViewModel!

  var kioskMode = false
  
  func setRootViewController(_ controller: DetailViewControllerMac) {
    detailViewControllerMacStack = []
    pushViewController(controller)
  }
  

  @IBOutlet weak var view: NSView!
  var splitViewController: NSSplitViewController = {
    let storyboard = NSStoryboard(name: "DetailViewControllerStoryboard", bundle: nil)
    return storyboard.instantiateInitialController() as! NSSplitViewController
  }()
  
  
  override var windowNibName: NSNib.Name? {
    return "DetailDocument"
  }

  override init() {
  }
  
  @objc func settingsDidChange() {
    splitViewController.splitView.setNeedsDisplay(splitViewController.view.bounds)
  }
  
  override func data(ofType typeName: String) throws -> Data {
    throw NSError(domain: NSOSStatusErrorDomain, code: unimpErr, userInfo: nil)
  }
  
  override func read( from absoluteURL: URL, ofType typeName: String) throws {
    if typeName == "public.xml" {
      kioskMode = true
    }
    Swift.print("readFromURL \(absoluteURL) \(typeName)")
  }
  
  override func windowControllerDidLoadNib(_ aController: NSWindowController) {
    super.windowControllerDidLoadNib(aController)
    
    splitViewController.splitViewItems[0].collapseBehavior = .preferResizingSplitViewWithFixedSiblings
    splitViewController.splitViewItems[1].collapseBehavior = .preferResizingSplitViewWithFixedSiblings
    
    if kioskMode {
      splitViewController.splitViewItems[0].isCollapsed = true
    }
    // SETUP VIEWS
    let splitView = splitViewController.view
    splitView.frame = self.view.bounds
    splitView.autoresizingMask = [.width, .height]
    self.view.addSubview(splitView)
    
    aController.window?.addTitlebarAccessoryViewController(titleBarAccessory)
    
    let pageController = self.splitViewController.splitViewItems[2].viewController as! NSPageController
    pageController.delegate = self
    
    if kioskMode && fileURL != nil {
      Downloader().parse(url: fileURL!) { (dict, error) in
        var theLaw: DownloadedLaw? = nil
        
        if let lawNum = dict?.lawNum, let lawEdition = dict?.lawEdition, let title = dict?.lawTitle {
          
          if let law = RealmManager.shared.law(with: lawEdition) {
            theLaw = law
          }  else {
            if let xml = try? Data(contentsOf: self.fileURL!) {
//              if let law = try? RealmManager.shared.add(lawNum: lawNum, lawEdition: lawEdition, xml: xml, lawTitle: title, imageDataPath: nil, under: "/") {
              
              if let law = try? RealmManager.shared.add(lawNum: lawNum, lawEdition: lawEdition, lawTitle: title, under: "/") {
                law.xml = xml
                theLaw = law
              }
            }
          }
        }
        if theLaw != nil {
          let viewModel = DetailViewModel(law: theLaw!, delegate: nil)
          
          let viewController = DetailViewControllerMac(viewModel: viewModel)
          self.setRootViewController(viewController)
          self.setupTabs()
        }else {
          let alert = NSAlert()
          alert.messageText = "このXMLはイーローズXMLではないようです"
          alert.informativeText = ""
          alert.alertStyle = .informational
          alert.addButton(withTitle: WLoc("OK"))
          alert.beginSheetModal(for: aController.window!, completionHandler: nil)
        }
      }
    }
    
    NotificationCenter.default.addObserver(self, selector: #selector(settingsDidChange), name: LawNotification.settingsDidChange, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(windowDidBecomeKey), name: NSWindow.didBecomeKeyNotification, object: aController.window)
  }

  @objc func windowDidBecomeKey() {
    guard detailViewControllerMacStack.count > 0 else { return }
    let pageController = self.splitViewController.splitViewItems[2].viewController as! NSPageController
    let detailViewController = detailViewControllerMacStack[pageController.selectedIndex]
    self.windowForSheet?.makeFirstResponder(detailViewController)
  }
  
  func toggleSourceList(_ sender: Any) {
      splitViewController.toggleSidebar(sender)
  }
  
  func toggleContentsList(_ sender: Any) {
    let collapsed = splitViewController.splitViewItems[1].animator().isCollapsed
    splitViewController.splitViewItems[1].animator().isCollapsed = !collapsed
  }
  
  func toggleParagpaphTfidf(_ sender: Any) {


  }
  
  @IBAction func changeBackgroundScheme(_ sender: NSMenuItem) {
    let tag = sender.tag
    var scheme: BackgroundColorSchemeType = .Paper
    switch tag {
    case 10: scheme = .White
    case 11: scheme = .Paper
    case 12: scheme = .Sepia
    case 13: scheme = .Grey
    case 14: scheme = .Dark
    default: scheme = .Paper
    }
    let ud = UserDefaults.standard
    ud.set(scheme.rawValue, forKey: "BackgroundColorScheme")
    
    NotificationCenter.default.post(name: LawNotification.settingsDidChange, object: self)
  }
  
  @IBAction func changeFontType(_ sender: NSMenuItem) {
    if let fontType = FontType(rawValue: sender.tag - 20) {
      self.fontType = fontType
    }
  }
  
  var fontType: FontType {
    get {
      let fontType = UserDefaults.standard.integer(forKey: "FontName")
      return FontType(rawValue: fontType) ?? .mincho
    }
    set {
      UserDefaults.standard.set(newValue.rawValue, forKey: "FontName")
      NotificationCenter.default.post(name: LawNotification.settingsDidChange, object: self)
    }
  }
  
  
  @IBAction func jumpTappedFromPanel(_ sender: Any?) {
    if detailViewControllerMacStack.count == 0 { return }
    let pageController = self.splitViewController.splitViewItems[2].viewController as! NSPageController
    let detailViewController = detailViewControllerMacStack[pageController.selectedIndex]
    detailViewController.jumpTappedFromPanel(sender)
  }
  
  @IBAction func zoomIn(_ sender: Any) {
    if detailViewControllerMacStack.count == 0 { return }
    let pageController = self.splitViewController.splitViewItems[2].viewController as! NSPageController
    let detailViewController = detailViewControllerMacStack[pageController.selectedIndex]
    detailViewController.zoomIn(sender)
  }
  
  @IBAction func zoomOut(_ sender: Any) {
    if detailViewControllerMacStack.count == 0 { return }
    let pageController = self.splitViewController.splitViewItems[2].viewController as! NSPageController
    let detailViewController = detailViewControllerMacStack[pageController.selectedIndex]
    detailViewController.zoomOut(sender)
  }
  
  @IBAction func performFindPanelAction(_ sender: Any) {
    if detailViewControllerMacStack.count == 0 { return }
    let pageController = self.splitViewController.splitViewItems[2].viewController as! NSPageController
    let detailViewController = detailViewControllerMacStack[pageController.selectedIndex]
    detailViewController.performFindPanelAction(sender)
  }
  
  @IBAction func performJumpPanelAction(_ sender: Any) {
    if detailViewControllerMacStack.count == 0 { return }
    let pageController = self.splitViewController.splitViewItems[2].viewController as! NSPageController
    let detailViewController = detailViewControllerMacStack[pageController.selectedIndex]
    detailViewController.performJumpPanelAction(sender)
  }
  
  @IBAction func changeKanji(_ sender: NSMenuItem) {
    if let kanjiType = KanjiType(rawValue: sender.tag - 30) {
      self.kanjiType = kanjiType
    }
  }
  
  var kanjiType: KanjiType {
    get {
      let kanjiType = UserDefaults.standard.integer(forKey: "KanjiType")
      return KanjiType(rawValue: kanjiType) ?? .kanji
    }
    set {
      let ud = UserDefaults.standard
      ud.set( newValue.rawValue, forKey: "KanjiType")
      
      NotificationCenter.default.post(name: LawNotification.settingsDidChange, object: self)
    }
  }
  
  var detailViewControllerIdentifierStack: [DetailViewControllerMac] = []
  
  func pushViewController(_ viewController: DetailViewControllerMac) {
    
    // SETUP RELATIONSHIPS
    let pageController = self.splitViewController.splitViewItems[2].viewController as! NSPageController
    
    // REMOVE FORWARDED VIEW CONTROLLERS
    
    if detailViewControllerMacStack.count == 0 {
      detailViewControllerMacStack.append(viewController)
      pageController.arrangedObjects = [viewController]

      viewController.document = self
      
    }else {
      detailViewControllerMacStack = Array(detailViewControllerMacStack[0...pageController.selectedIndex])
      detailViewControllerMacStack.append(viewController)
      viewController.document = self
      pageController.navigateForward(to: viewController )
    }
  }
  
  func popViewController() {
    let pageController = self.splitViewController.splitViewItems[2].viewController as! NSPageController

    pageController.navigateBack(nil)
  }
  
  func setupTabs() {
    let pageController = self.splitViewController.splitViewItems[2].viewController as! NSPageController
    let mokujiViewController = self.splitViewController.splitViewItems[1].viewController as! MokujiViewController
    

    let detailViewController = detailViewControllerMacStack[pageController.selectedIndex]
    guard let _ = detailViewController.viewModel.source as? TFIDFFetchResults else { return }

    let cosineViewModel = CosineViewModel(source: detailViewController.viewModel.source as! TFIDFFetchResults)
    let viewController = CosineViewController(viewModel: cosineViewModel, viewSize: .zero)

    
    let egovLaw = LawList().lawsWithNum(detailViewController.viewModel.selectedLaw!.lawNum)!.first!
    let docCosineViewModel = DocCosineViewModel(source: egovLaw)
    let docViewController = DocCosineViewController(viewModel: docCosineViewModel, viewSize: .zero)
    
    let doc2VecViewModel = Doc2VecViewModel(source: detailViewController.viewModel.source as! TFIDFFetchResults)
    let doc2vecViewController = Doc2VecViewController(viewModel: doc2VecViewModel, viewSize: .zero)

//    let tfidfViewModel = TfidfViewModel(source: detailViewController.viewModel.source as! TFIDFFetchResults)
//    let tfidfViewController = TfidfViewController(viewModel: tfidfViewModel, viewSize: .zero)

    
    viewController.jumpAction = { item in
      self.windowForSheet?.makeFirstResponder(detailViewController)
    }
  
    mokujiViewController.viewControllers = [viewController, docViewController, doc2vecViewController]
  }
  
  func navigateForward() {
    let pageController = self.splitViewController.splitViewItems[2].viewController as! NSPageController
      pageController.navigateForward(nil)
  }
  
  func pageController(_ pageController: NSPageController, identifierFor object: Any) -> NSPageController.ObjectIdentifier {
    return (object as! DetailViewControllerMac).uuid
  }
  
  func pageController(_ pageController: NSPageController, viewControllerForIdentifier identifier: NSPageController.ObjectIdentifier) -> NSViewController {
    
//    print(identifier.rawValue)
    return detailViewControllerMacStack.filter({ $0.uuid == identifier }).first!
  }
  
  func pageControllerDidEndLiveTransition(_ pageController: NSPageController) {
    pageController.completeTransition()
//    setupTabs()
  }
}




//class MyTapGestureRecognizer: NSGestureRecognizer {
//
//  var lastTouchedPoint: CGPoint? = nil
//
//  var touchedPoint: CGPoint? = nil {
//    didSet {
//      if oldValue == touchedPoint { return }
//      (delegate as? DetailViewControllerMac)?.tap(touchedPoint)
//
//    }
//  }
//  override func touchesBegan(with event: NSEvent) {
//    guard let view = self.view else { return }
//    let touches = event.touches(for: view)
//    if touches.count > 1 { return }
//    touchedPoint = touches.first?.location(in: view)
//    state = .possible
//  }
//
//  override func touchesMoved(with event: NSEvent) {
//    let touches = event.touches(for: self.view!)
//
//    let currentPoint = touches.first?.location(in: self.view)
//    if currentPoint == nil || shouldCancel(currentPoint!) == true {
//      self.state = .cancelled
//      touchedPoint = nil
//    }
//  }
//
//  override func touchesEnded(with event: NSEvent) {
//    self.state = .ended
//    guard let view = self.view else { return }
//
//    let touches = event.touches(for: view)
//    //doubleTap(
//    if touches.first?.tapCount == 2 && touchedPoint != nil && lastTouchedPoint != nil {
//
//      if abs(touchedPoint!.x - lastTouchedPoint!.x) < 10 && abs(touchedPoint!.y - lastTouchedPoint!.y) < 10 {
//        (delegate as? DetailViewControllerMac)?.doubleTap(touchedPoint!)
//      }
//    }else {
//
//      if touchedPoint != nil {
//        (delegate as? DetailViewControllerMac)?.tap(touchedPoint, tapped: true)
//      }
//      lastTouchedPoint = touchedPoint
//      touchedPoint = nil
//    }
//  }
//
//  override func touchesCancelled(with event: NSEvent) {
//    self.state = .cancelled
//    touchedPoint = nil
//
//  }
//
//  func shouldCancel(_ point: CGPoint) -> Bool {
//    if touchedPoint == nil { return true }
//    if abs(touchedPoint!.x - point.x) > 10 { return true }
//    if abs(touchedPoint!.y - point.y) > 10 { return true }
//
//    return false
//  }
//}


