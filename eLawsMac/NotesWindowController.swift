//
//  NotesWindowController.swift
//  eLawsMac
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Cocoa

class NotesWindowController: NSWindowController {
  
  var deleteNotes:(() -> Void)?
  var setNotes:((_ text: String) -> Void)?
  var willCloseWithChange:((_ withChange: Bool) -> Void)?
  
  // Property
  var originalNotes: String? {
    didSet {
      if originalNotes != nil {
        textView?.string = originalNotes!
      }else {
        textView?.string = ""
      }
    }
  }

  @IBOutlet var textView: NSTextView!

  
  override func windowDidLoad() {
    super.windowDidLoad()
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    
    if originalNotes != nil {
      textView?.string = originalNotes!
    }else {
      textView?.string = ""
    }
    
  }
  
  @IBAction func delete(_ sender: Any) {
    deleteNotes?()
    self.window?.sheetParent?.endSheet(self.window!, returnCode: .OK)
}
  
  @IBAction func cancel(_ sender: Any) {
    self.window?.sheetParent?.endSheet(self.window!, returnCode: .cancel)
  }
  @IBAction func save(_ sender: Any) {
    let notes = textView.string
    self.setNotes?(notes)
    self.window?.sheetParent?.endSheet(self.window!, returnCode: .OK)
  }
  
}
