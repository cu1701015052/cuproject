//
//  MNFramesetter.m
//  CoreTextRTF
//
//  Created by Masatoshi Nishikata on 26/12/10.
//  Copyright 2010 Masatoshi Nishikata. All rights reserved.
//

#import "MNFramesetter.h"
#import "MNAttachmentCell.h"
#import "MNCoreTextDefinitions.h"


@implementation MNFramesetter
//@synthesize lines = lines_;
//@synthesize CTFramesetter = framesetter_;
@synthesize frames = frames_;
@synthesize suggestedSize = suggestedSize_;
@synthesize containerSize = containerSize_;
@synthesize suggestedContainerSize = suggestedContainerSize_;
@synthesize documentScaling = documentScaling_;
@synthesize textStorage = textStorage_;

#ifdef DEBUG
#define MNLOG  //NSLog
#else
#define MNLOG( fmt, ... )  //
#endif

#ifndef RANGECONTAINSLOCATION
#define RANGECONTAINSLOCATION(r1, r2) \
((r1.location <= r2) && r2<(r1.location+r1.length) )
#endif

#ifndef MAXRANGE
#define MAXRANGE(r) \
r.location+r.length
#endif

#define CGPOINTINTEGRATE(n) CGPointMake( roundf(n.x), roundf(n.y))


#if TARGET_OS_IPHONE
#define CURRENT_CONTEXT UIGraphicsGetCurrentContext()
#else

// OSX
#define CURRENT_CONTEXT (CGContextRef)[[NSGraphicsContext currentContext] graphicsPort]
#endif

- (id) initWithAttributedString:(NSMutableAttributedString*)attr
{
    self = [super init];
    if (self != nil) {
        _containerOrigin = CGPointMake(10, 10);
        textStorage_ = [attr retain];
        //framesetter_ = CTFramesetterCreateWithAttributedString((CFAttributedStringRef)attr);
        frames_ = [[NSMutableArray alloc] init];
        
        documentScaling_ = 1.0;
        containerSize_ = CGSizeMake(300,MAXFLOAT);
        
        MNLOG(@"%@ %@ %d",NSStringFromClass([self class]), NSStringFromSelector( _cmd ), __LINE__);
        
        //gDocumentScaling = 2.0;
    }
    return self;
}

-(void)dealloc
{
    /// ==================================
    MNLOG(@"%@ %@",NSStringFromClass([self class]), NSStringFromSelector( _cmd ));
    /// ==================================
    
    [textStorage_ release];
    textStorage_ = nil;
    
    //	if( framesetter_)
    //		CFRelease(framesetter_);
    
    //[lines_ release];
    //lines_ = nil;
    
    [topLine_ release];
    topLine_ = nil;
    
    [frames_ release];
    frames_ = nil;
    
    [super dealloc];
}

-(void)layoutUsingTypesetter
{
    MNLOG(@"%@ %@ %d",NSStringFromClass([self class]), NSStringFromSelector( _cmd ), __LINE__);
    
    CTTypesetterRef typesetter = CTTypesetterCreateWithAttributedString( (CFAttributedStringRef)textStorage_ );
    
    NSUInteger lineNumber = 0;
    CFIndex startIndex = 0;
    
    ///
    
    MNFrame *frameObject = [[[MNFrame alloc] init] autorelease];
    frameObject.pathBoundingBox = CGRectMake(_containerOrigin.x, _containerOrigin.y, containerSize_.width, 200);
    frameObject.range = CFRangeMake(0, textStorage_.length);
    frameObject.lines = [NSMutableArray array];
    frameObject.documentScaling = self.documentScaling;
    
    //	frameObject.CTFrame = frame;
    
    [frames_ addObject:frameObject];
    
    ///
    
    /*
     
     行末禁則
     」』）、。
     「ぶら下がり」は基本的に句読点にのみ適用さ
     */
    
    MNLine *prevLine = nil;
    CGPoint origin = CGPointZero;
    NSRange currentParagraphRange = NSMakeRange(NSNotFound, 0);
    
    while( startIndex < textStorage_.length )
    {
        @autoreleasepool {
            
            CGFloat headIndent = 0;
            CGFloat tailIndent = 0;
            
            // Get line spacing
            
            CGFloat lineSpacing = 0;
            CTParagraphStyleRef paragraphStyle = (CTParagraphStyleRef)[textStorage_  attribute: MNCTParagraphStyleAttributeName atIndex:startIndex effectiveRange:nil];
            
            if( paragraphStyle ) {
                CTParagraphStyleGetValueForSpecifier( paragraphStyle, kCTParagraphStyleSpecifierLineSpacingAdjustment, sizeof(CGFloat), &lineSpacing);
            }
            
            CGFloat paragraphSpacingBefore = 0;
            CGFloat paragraphSpacingAfter = 0;
            
            // if startIndex == paragraph start index
            if( !RANGECONTAINSLOCATION(currentParagraphRange, startIndex) )
            {
                if( paragraphStyle ) {
                    CTParagraphStyleGetValueForSpecifier( paragraphStyle, kCTParagraphStyleSpecifierParagraphSpacingBefore, sizeof(CGFloat), &paragraphSpacingBefore);
                    CTParagraphStyleGetValueForSpecifier( paragraphStyle, kCTParagraphStyleSpecifierFirstLineHeadIndent, sizeof(CGFloat), &headIndent);
                    CTParagraphStyleGetValueForSpecifier( paragraphStyle, kCTParagraphStyleSpecifierTailIndent, sizeof(CGFloat), &tailIndent);
                }
                currentParagraphRange = [textStorage_.string paragraphRangeForRange: NSMakeRange(startIndex, 1)];
                
                // New paragraph starts here
                if( startIndex > 0 )
                {
                    CTParagraphStyleRef prevParagraphStyle = (CTParagraphStyleRef)[textStorage_  attribute: MNCTParagraphStyleAttributeName atIndex:startIndex-1 effectiveRange:nil];
                    
                    if( prevParagraphStyle )
                    {
                        CTParagraphStyleGetValueForSpecifier( prevParagraphStyle, kCTParagraphStyleSpecifierParagraphSpacing, sizeof(CGFloat), &paragraphSpacingAfter);
                    }
                }
            } else if( paragraphStyle ) {
                CTParagraphStyleGetValueForSpecifier( paragraphStyle, kCTParagraphStyleSpecifierHeadIndent, sizeof(CGFloat), &headIndent);
                CTParagraphStyleGetValueForSpecifier( paragraphStyle, kCTParagraphStyleSpecifierTailIndent, sizeof(CGFloat), &tailIndent);
            }
            
            lineSpacing *= self.documentScaling;
            
            paragraphSpacingBefore *= self.documentScaling;
            paragraphSpacingAfter *= self.documentScaling;
            
            /////
            origin.x = headIndent;
            double contentWidth = (containerSize_.width - headIndent - tailIndent) / self.documentScaling;
            CFIndex length = CTTypesetterSuggestLineBreak( typesetter, startIndex, contentWidth );
            
            
            CTLineRef ctline = CTTypesetterCreateLine( typesetter, CFRangeMake(startIndex, length) );
            
            CGFloat ascent, descent, leading;
            double width = CTLineGetTypographicBounds(ctline, &ascent, &descent, &leading);
            double cwToScw = (width / length ) * 1.0;
            double safeContentWidth = contentWidth - cwToScw;

            if(0)
            if( width < safeContentWidth ) {
                while(1) {
                    if( startIndex + length >= textStorage_.length ) { break; }
                    NSString* nextLetter = [textStorage_.string substringWithRange: NSMakeRange(startIndex + length, 1)];
                    if( [nextLetter isEqualToString:@"\n"] || [nextLetter isEqualToString:@"\r"] ) { break; }

                    length += 1;
                    ctline = CTTypesetterCreateLine( typesetter, CFRangeMake(startIndex, length) );
                    width = CTLineGetTypographicBounds(ctline, &ascent, &descent, &leading);

                    if( width < safeContentWidth ) { continue; }

                    if( width > contentWidth ) {
                        length -= 1;
                        ctline = CTTypesetterCreateLine( typesetter, CFRangeMake(startIndex, length) );
                        width = CTLineGetTypographicBounds(ctline, &ascent, &descent, &leading);
                        break;
                    }

                    else {
                        if( [nextLetter isEqualToString:@"、"] || [nextLetter isEqualToString:@"。"] || [nextLetter isEqualToString:@"）"] ) {
                            break;
                        }else {
                            length -= 1;
                            ctline = CTTypesetterCreateLine( typesetter, CFRangeMake(startIndex, length) );
                            width = CTLineGetTypographicBounds(ctline, &ascent, &descent, &leading);
                            break;
                        }
                    }
                }
            }else {
                while(1) {
                    if( width > safeContentWidth || length < 3 ) { break; }
                    NSString* lastLetter = [textStorage_.string substringWithRange: NSMakeRange(startIndex + length - 1, 1)];
                    if( [lastLetter isEqualToString:@"、"] || [lastLetter isEqualToString:@"。"] || [lastLetter isEqualToString:@"）"] ) {
                        break;
                    }else {
                        length -= 1;
                        ctline = CTTypesetterCreateLine( typesetter, CFRangeMake(startIndex, length) );
                        width = CTLineGetTypographicBounds(ctline, &ascent, &descent, &leading);
                    }
                }
            }
            

            origin.y += descent + lineSpacing + 1;
            
            ascent *= self.documentScaling;
            descent *= self.documentScaling;
            leading *= self.documentScaling;
            
            ascent = roundf(ascent);
            descent = ceilf(descent);
            leading = roundf(leading);
            
            if( startIndex + length >= textStorage_.length )
            {
                //paragraphSpacingAfter -= 10;
            }
            
            CFRange range = CFRangeMake(startIndex, length);
            
            MNLine* lineObj = [[[MNLine alloc] init] autorelease];
            lineObj.range = range;
            lineObj.baseLocation = 0;
            lineObj.origin = origin;
            lineObj.CTLine = ctline;
            lineObj.parentFrame = frameObject;
            lineObj.previousLine = prevLine;
            lineObj.lineNumber = lineNumber;
            lineObj.ascent = ascent;
            lineObj.descent = descent;
            lineObj.leading = leading;
            lineObj.width = width;
            lineObj.lineSpacing = lineSpacing;
            lineObj.paragraphSpacingBefore = paragraphSpacingBefore;
            lineObj.paragraphSpacingAfter = paragraphSpacingAfter;
            
            
            
            [lineObj adjustTextAlignmentForWidth: self.containerSize.width
                               forParagraphStyle: [self paragraphAttributesOfLine:lineObj] ];
            
            [frameObject.lines addObject: lineObj];
            
            if( topLine_ == nil ) topLine_ = [lineObj retain];
            
            [prevLine release];
            prevLine = [lineObj retain];
            
            origin.y += (ascent + leading  + paragraphSpacingBefore + paragraphSpacingAfter) ;
            
            lineNumber++;
            
            /////
            
            CFRelease(ctline);
            startIndex += length;
        }
    }
    [prevLine release];
    prevLine = nil;
    
    if( typesetter ) {
        CFRelease(typesetter);
        typesetter = nil;
    }
    suggestedContainerSize_ = CGSizeMake( frameObject.pathBoundingBox.size.width, frameObject.pathBoundingBox.origin.y + ceilf(origin.y) );
}

//-(void)createFramesFromPaths:(NSArray*)paths attributes:(CFDictionaryRef) frameAttributes
//{
//
//	[topLine_ release];
//	topLine_ = nil;
//
//
//	[self layoutUsingTypesetter];
//	return;
//
//
//	CFIndex startIndex = 0;
//	int column;
//	NSUInteger lineNumber = 0;
//
//	for (column = 0; column < [paths count]; column++) {
//		CGPathRef path = (CGPathRef)[paths objectAtIndex:column];
//
//
//
//		// Create a frame for this column and draw it.
//
//
//
//		// Create frame
//
//		CFRange range = CFRangeMake(startIndex, 0);
//		CTFrameRef frame = CTFramesetterCreateFrame(framesetter_, range, path, frameAttributes);
//
//
//		MNFrame *frameObject = [[[MNFrame alloc] init] autorelease];
//		frameObject.pathBoundingBox = CGPathGetPathBoundingBox(path);
//		frameObject.range = CTFrameGetStringRange(frame);
//		frameObject.lines = [NSMutableArray array];
//		frameObject.CTFrame = frame;
//
//		[frames_ addObject:frameObject];
//
//
//
//		// Create lines from frame
//
//		CFArrayRef lines = CTFrameGetLines(frame);
//		CGPoint origins[ CFArrayGetCount(lines)];
//		CTFrameGetLineOrigins(frame, range, origins);
//
//
//		MNLine *prevLine = nil;
//
//		for( NSInteger hoge = 0; hoge < CFArrayGetCount(lines); hoge++ )
//		{
//
//			CTLineRef line = CFArrayGetValueAtIndex(lines, hoge);
//
//			CGRect typoRect = CTLineBoundingRect( line, origins[hoge] );
//			CFRange range = CTLineGetStringRange(line);
//
//
//			MNLine* lineObj = [[[MNLine alloc] init] autorelease];
//			lineObj.range = range;
//			lineObj.typographicFrame = typoRect;
//			lineObj.origin = origins[hoge];
//			lineObj.CTLine = line;
//			lineObj.parentFrame = frameObject;
//			lineObj.previousLine = prevLine;
//			lineObj.lineNumber = lineNumber;
//
//			[frameObject.lines addObject: lineObj];
//
//			if( topLine_ == nil ) topLine_ = [lineObj retain];
//
//
//			[prevLine release];
//			prevLine = [lineObj retain];
//
//			lineNumber++;
//		}
//
//
//
//
//		CFRelease(frame);
//
//
//		CFRange frameRange = CTFrameGetVisibleStringRange(frame);
//		startIndex += frameRange.length;
//
//	}
//}

-(void)invalidateLayout
{
    [frames_ release];
    frames_ = nil;
    
    [topLine_ release];
    topLine_ = nil;
    
    frames_ = [[NSMutableArray alloc] init];
    
    [self layoutUsingTypesetter];
}

-(NSRange)invalidateLayoutWithOldRange:(NSRange)oldRange newRange:(NSRange)newRange
{
    if( !textStorage_ ) return NSMakeRange(NSNotFound, 0);
    
    // returns modified line to display, 0 means to the last line
    
    
    
    
    //	if( oldRange.location == 0 )
    //	{
    //		[self invalidateLayout];
    //		return;
    //	}
    
    MNLine *lastLineBeforeEditing = nil;
    NSMutableArray* linesInBetween = [NSMutableArray array];
    MNLine *firstLineBeforeEditing = nil;
    
    MNLOG(@"range %@ -> %@", NSStringFromRange(oldRange), NSStringFromRange(newRange));
    if( oldRange.location > 0 )
    {
        lastLineBeforeEditing = [self lineAtIndex: oldRange.location-1];
    }
    
    //if( NSMaxRange(oldRange) != textStorage_.length )
    {
        firstLineBeforeEditing = [self lineAtIndex: NSMaxRange(oldRange)];
    }
    
    MNLOG(@"%d-%d",lastLineBeforeEditing.lineNumber, firstLineBeforeEditing.lineNumber);
    [self evaluateLines];
    
    MNLine* line = lastLineBeforeEditing.nextLine;
    if( line == nil ) line = topLine_;
    
    while( line != firstLineBeforeEditing)
    {
        MNLOG(@"line %d",line.lineNumber);
        [linesInBetween addObject: line];
        line = line.nextLine;
    }
    
    MNLOG(@"Edit lines %@ ",[linesInBetween description]);
    
    
    // Get edited string
    NSAttributedString *newEditedParagraph = [textStorage_ attributedSubstringFromRange: newRange];
    CTTypesetterRef typesetter;
    @autoreleasepool {
        typesetter = CTTypesetterCreateWithAttributedString( (CFAttributedStringRef)newEditedParagraph );
    }
    
    NSMutableArray *newLines = [NSMutableArray array];
    NSUInteger startIndex = 0;
    NSUInteger lineNumber = 0;
    
    if( lastLineBeforeEditing ) lineNumber = lastLineBeforeEditing.lineNumber +1;
    
    CGPoint origin = CGPointZero;
    NSRange currentParagraphRange = NSMakeRange(NSNotFound, 0);
    MNLine *prevLine = nil;
    
    while( startIndex < newEditedParagraph.length )
    {
        @autoreleasepool {
            // Expensive command
            // calculation speed ... textStorage_.length
            
            // Get line
            CFIndex length = CTTypesetterSuggestLineBreak( typesetter, startIndex, containerSize_.width / self.documentScaling);
            
            CTLineRef ctline = CTTypesetterCreateLine( typesetter, CFRangeMake(startIndex, length) );
            
            
            // Get line spacing
            
            CGFloat lineSpacing = 0;
            CTParagraphStyleRef paragraphStyle = (CTParagraphStyleRef)[newEditedParagraph  attribute: MNCTParagraphStyleAttributeName atIndex:startIndex effectiveRange:nil];
            
            if( paragraphStyle )
                CTParagraphStyleGetValueForSpecifier( paragraphStyle, kCTParagraphStyleSpecifierLineSpacingAdjustment, sizeof(CGFloat), &lineSpacing);
            
            
            CGFloat paragraphSpacingBefore = 0;
            CGFloat paragraphSpacingAfter = 0;
            
            // if startIndex == paragraph start index
            if( !RANGECONTAINSLOCATION(currentParagraphRange, newRange.location+startIndex) )
            {
                if( paragraphStyle )
                    CTParagraphStyleGetValueForSpecifier( paragraphStyle, kCTParagraphStyleSpecifierParagraphSpacingBefore, sizeof(CGFloat), &paragraphSpacingBefore);
                
                currentParagraphRange = [textStorage_.string paragraphRangeForRange: NSMakeRange(newRange.location + startIndex, 1)];
                
                // If still contains current range, find one more previous
                
                if( currentParagraphRange.location > 0 )
                {
                    NSRange previousRange =  [textStorage_.string paragraphRangeForRange: NSMakeRange(currentParagraphRange.location -1, 1)];
                    
                    // New paragraph starts here
                    
                    
                    CTParagraphStyleRef prevParagraphStyle = (CTParagraphStyleRef)[textStorage_  attribute: MNCTParagraphStyleAttributeName atIndex:previousRange.location effectiveRange:nil];
                    
                    if( prevParagraphStyle )
                    {
                        CTParagraphStyleGetValueForSpecifier( prevParagraphStyle, kCTParagraphStyleSpecifierParagraphSpacing, sizeof(CGFloat), &paragraphSpacingAfter);
                    }
                }
            }
            
            lineSpacing *= self.documentScaling;
            
            paragraphSpacingBefore *= self.documentScaling;
            paragraphSpacingAfter *= self.documentScaling;
            
            /////
            CGFloat ascent, descent, leading;
            double width = CTLineGetTypographicBounds(ctline, &ascent, &descent, &leading);
            origin.y += descent  + lineSpacing +1;
            
            ascent *= self.documentScaling;
            descent *= self.documentScaling;
            leading *= self.documentScaling;
            
            
            CFRange range = CFRangeMake(startIndex, length);
            
            MNLine* lineObj = [[[MNLine alloc] init] autorelease];
            lineObj.range = CFRangeMake( newRange.location + range.location, range.length);
            lineObj.baseLocation = newRange.location;
            lineObj.origin = origin;
            lineObj.CTLine = ctline;
            //		lineObj.parentFrame = invalidatedLine.parentFrame;
            lineObj.previousLine = prevLine;
            lineObj.lineNumber = lineNumber;
            lineObj.ascent = ascent;
            lineObj.descent = descent;
            lineObj.leading = leading;
            lineObj.width = width;
            lineObj.lineSpacing = lineSpacing;
            lineObj.paragraphSpacingBefore = paragraphSpacingBefore;
            lineObj.paragraphSpacingAfter = paragraphSpacingAfter;
            
            
            [newLines addObject: lineObj];
            
            origin.y += (ascent + leading  + lineSpacing + paragraphSpacingAfter) ;
            
            lineNumber++;
            
            
            /////
            
            CFRelease(ctline);
            
            startIndex += length;
            prevLine = lineObj;
        }
    }
    
    
    // Evaluate
    BOOL invalidateFollowingLines = NO;
    if( [linesInBetween count] != [newLines count] )
    {
        invalidateFollowingLines = YES;
        MNLOG(@"*** increase / decrease line numbers ***");
    }
    
    
    CGFloat occupiedHeight = 0;
    for( MNLine *line in linesInBetween )
    {
        occupiedHeight += line.occupiedFrame.size.height;
    }
    
    CGFloat newOccupiedHeight = 0;
    for( MNLine *line in newLines )
    {
        newOccupiedHeight += line.occupiedFrame.size.height;
    }
    
    if( newOccupiedHeight == occupiedHeight )
    {
        MNLOG(@"*** Same typo frame %.0f ***",occupiedHeight );
    }else {
        invalidateFollowingLines = YES;
        MNLOG(@"*** changed typo frame (%.0f, %.0f)***", occupiedHeight, newOccupiedHeight);
        
    }
    
    
    [self insertLines:newLines from:lastLineBeforeEditing	to:firstLineBeforeEditing offset:newRange.length - oldRange.length];
    
    
    
    // For debug
    //[self invalidateLayout];
    
    
    
    suggestedContainerSize_ = CGSizeMake( topLine_.parentFrame.pathBoundingBox.size.width, CGRectGetMaxY( [self lastLine].occupiedFrame) );
    
    
    
    
    MNLOG(@"invalidateFollowingLines %d",invalidateFollowingLines);
    
    // FInishing
    
    CFRelease(typesetter);
    
    
    
    // End
    NSUInteger firstIndex = 0;
    if( lastLineBeforeEditing )
        firstIndex = MAXRANGE( lastLineBeforeEditing.range );
    
    if( !invalidateFollowingLines && firstLineBeforeEditing )
        return NSMakeRange( firstIndex, firstLineBeforeEditing.range.location - firstIndex );
    else return NSMakeRange( firstIndex, 0 );
    
    
    
    /*
     // Delete unused line here
     
     
     for( MNLine *line in linesInBetween )
     {
     [line.parentFrame.lines removeObject:line];
     line.parentFrame = nil;
     line.previousLine = nil;
     line.nextLine = nil;
     }
     
     
     // insert newLines between last.. and first...
     
     if( [newLines count] == 0 )
     {
     firstLineBeforeEditing.previousLine = lastLineBeforeEditing;
     }
     
     else {
     
     MNLine *previousLine = lastLineBeforeEditing;
     CGPoint origin = CGPointZero;
     if( previousLine)
     {
     previousLine.origin;
     origin.y += previousLine.ascent + previousLine.leading;
     }
     
     for( NSUInteger hoge = 0; hoge < [newLines count]; hoge++ )
     {
     MNLine *line = [newLines objectAtIndex:hoge];
     origin.y += line.descent;
     
     line.previousLine = previousLine;
     previousLine = line;
     origin.y += previousLine.ascent + previousLine.leading;
     
     line.parentFrame = lastLineBeforeEditing.parentFrame;
     }
     
     firstLineBeforeEditing.previousLine = previousLine;
     }
     
     */
    
    
    
    
    
    
    
    
    
    ////
    
    
    
    /*
     MNLine *newLastLineBeforeEditing = nil;
     MNLine *newFirstLineBeforeEditing = nil;
     
     if( newRange.location > 0 )
     {
     newLastLineBeforeEditing = [self lineAtIndex: newRange.location-1];
     }
     
     if( NSMaxRange(newRange) != textStorage_.length )
     {
     newFirstLineBeforeEditing = [self lineAtIndex: NSMaxRange(newRange)];
     }
     
     MNLOG(@"Results in %d-%d ",newLastLineBeforeEditing.lineNumber,  newFirstLineBeforeEditing.lineNumber);
     */
    
}


//-(BOOL)invalidateLayoutFromIndex:(NSUInteger)location
//{
//
//
//	// Expensive process
//
//
//	if( location == 0 )
//	{
//		/*
//		MNLOG(@"d0 %f",d0);
//		MNLOG(@"d1 %f",d1);
//		MNLOG(@"d2 %f",d2);
//		NSLog(@"d3 %f",d3);
//		NSLog(@"d4 %f",d4);
//		NSLog(@"d5 %f",d5);
//		NSLog(@"d6 %f",d6);
//
//		d0 0.133502
//		d1 1.367667
//		d2 1.823603
//		d3 0.101755
//		d4 0.041564
//
//		*/
//
//		[self invalidateLayout];
//		return NO;
//	}
//
//
//	MNLine *invalidatedLine = nil;
//	MNLine *line;
//	NSEnumerator *enumerator = [self lineEnumerator];
//
//	while( line = [enumerator nextObject] )
//	{
//		if( RANGECONTAINSLOCATION( line.range, location ) )
//		{
//			invalidatedLine = line;
//			[invalidatedLine retain];
//			[invalidatedLine autorelease];
//
//			break;
//		}
//	}
//
//	if( invalidatedLine == topLine_ )
//	{
//		[self invalidateLayout];
//		return NO;
//	}
//
//	NSTimeInterval t0 = [NSDate timeIntervalSinceReferenceDate];
//
//
//	while( line = [enumerator nextObject] )
//	{
//		line.previousLine = nil;
//		line.nextLine = nil;
//		line.parentFrame = nil;
//
//		[invalidatedLine.parentFrame.lines removeObject: line];
//	}
//
//
//
//	NSTimeInterval t1 = [NSDate timeIntervalSinceReferenceDate];
//
//	if( typesetter_ )
//		CFRelease(typesetter_);
//
//	// Expensive command
//	// calculation speed ... textStorage_.length
//	typesetter_ = CTTypesetterCreateWithAttributedString( (CFAttributedStringRef)textStorage_ );
//
//	NSTimeInterval t2 = [NSDate timeIntervalSinceReferenceDate];
//
//
//	MNLine *prevLine = [invalidatedLine.previousLine retain];
//	CGPoint origin = CGPointMake(invalidatedLine.origin.x,  invalidatedLine.typographicFrame.origin.y);
//	NSUInteger lineNumber = invalidatedLine.lineNumber;
//	CFIndex startIndex = invalidatedLine.range.location;
//
//	while( startIndex < textStorage_.length )
//	{
//		NSTimeInterval t3 = [NSDate timeIntervalSinceReferenceDate];
//
//
//		// Expensive command
//		// calculation speed ... textStorage_.length
//		CFIndex length = CTTypesetterSuggestClusterBreak( typesetter_, startIndex, containerSize_.width );
//
//		NSTimeInterval t4 = [NSDate timeIntervalSinceReferenceDate];
//
//		CTLineRef ctline = CTTypesetterCreateLine( typesetter_, CFRangeMake(startIndex, length) );
//
//		NSTimeInterval t5 = [NSDate timeIntervalSinceReferenceDate];
//
//		/////
//		CGFloat ascent, descent, leading;
//		double width = CTLineGetTypographicBounds(ctline, &ascent, &descent, &leading);
//		origin.y += descent;
//
//		NSTimeInterval t6 = [NSDate timeIntervalSinceReferenceDate];
//
//		CGRect typoRect = CGRectIntegral( CGRectMake( origin.x , origin.y  -descent, width, ascent+descent));
//		CFRange range = CTLineGetStringRange(ctline);
//
//
//		MNLine* lineObj = [[[MNLine alloc] init] autorelease];
//		lineObj.range = range;
//		lineObj.origin = origin;
//		lineObj.CTLine = ctline;
//		lineObj.parentFrame = invalidatedLine.parentFrame;
//		lineObj.previousLine = prevLine;
//		lineObj.lineNumber = lineNumber;
//		lineObj.ascent = ascent;
//		lineObj.descent = descent;
//		lineObj.leading = leading;
//		lineObj.width = width;
//
//		[invalidatedLine.parentFrame.lines addObject: lineObj];
//
//		if( topLine_ == nil ) topLine_ = [lineObj retain];
//
//
//		[prevLine release];
//		prevLine = [lineObj retain];
//
//
//		origin.y += (ascent + leading);
//
//		lineNumber++;
//
//		NSTimeInterval t7 = [NSDate timeIntervalSinceReferenceDate];
//
//
//		d2 += (t4-t3);
//		d3 += (t5-t4);
//		d4 += (t7-t6);
//
//		/////
//
//		CFRelease(ctline);
//
//		startIndex += length;
//	}
//
//	NSTimeInterval t8 = [NSDate timeIntervalSinceReferenceDate];
//
//	d0 += t1 - t0;
//	d1 += t2 - t1;
//
//
//
//	suggestedContainerSize_ = CGSizeMake( invalidatedLine.parentFrame.pathBoundingBox.size.width, invalidatedLine.parentFrame.pathBoundingBox.origin.y + origin.y);
//
//
//	invalidatedLine.previousLine = nil;
//	invalidatedLine.nextLine = nil;
//	[invalidatedLine.parentFrame.lines removeObject: invalidatedLine];
//	invalidatedLine.parentFrame = nil;
//
//	return NO;
//}


-(MNLineEnumerator*)lineEnumerator
{
    MNLineEnumerator* enumerator = 	[[MNLineEnumerator alloc] init];
    enumerator.line = topLine_;
    
    return [enumerator autorelease];
}


//-(NSString*)description
//{
//	return [lines_ description];
//}


-(CTRunRef)runAtIndex:(NSUInteger)index
{
    MNLine* line = [self lineAtIndex: index];
    NSArray *runs = (NSArray*) CTLineGetGlyphRuns(line.CTLine);
    for( NSInteger runIndex = 0; runIndex < [runs count]; runIndex++ )
    {
        CTRunRef run = (CTRunRef) [runs objectAtIndex: runIndex];
        if( RANGECONTAINSLOCATION( CTRunGetStringRange(run) , index - line.baseLocation ) )
        {
            return run;
            
        }
    }
    return nil;
    
}

-(MNLine*)lineAtIndex:(NSUInteger)index
{
    //MNLOG(@"Line at index %d",index);
    
    NSEnumerator* enumerator = [self lineEnumerator];
    MNLine* line = nil;
    MNLine* lastLine = nil;
    
    while( (line = [enumerator nextObject]) )
    {
        if( RANGECONTAINSLOCATION( line.range, index )  )
        {
            return line;
        }
        
        lastLine = line;
    }
    
    MNLOG(@"could not fount");
    return lastLine;
}

-(void)insertLines:(NSArray*)array from:(MNLine*)fromLine to:(MNLine*)toLine offset:(NSInteger)offset
{
    //fromLine toLine　の間に arrayを挟み込みます。　range.location をoffsetぶんだけずらします。origin, typo frame，line number を再計算します。
    MNLine *line;
    
    MNLOG(@"Insert %d lines from line number %d to %d, move %d characters in following lines",[array count], fromLine.lineNumber, toLine.lineNumber, offset);
    
    
    NSEnumerator *enumerator = nil;
    //	enumerator = [self lineEnumerator];
    //	while( (line = [enumerator nextObject]) )
    //	{
    //
    //		MNLOG(@"line %d, range (%d,%d)",line.lineNumber, line.range.location, line.range.length);
    //
    //	}
    
    
    ////// Delete unused line here
    
    NSMutableSet *unusedLines = [NSMutableSet set];
    
    // First unused line
    line = fromLine.nextLine;
    if( line == nil ) line = topLine_;
    
    while( line && line != toLine )
    {
        [unusedLines addObject: line];
        line = line.nextLine;
    }
    
    ///////
    
    
    
    
    
    
    /// Connect lines
    
    if( [array count] == 0 || array == nil )
    {
        // When array is empty,
        
        toLine.previousLine = fromLine;
        
        // If toLine is the fisrt line, update top line
        if( toLine.previousLine == nil )
        {
            [topLine_ release];
            topLine_ = [toLine retain];
        }
    }
    
    
    else {
        
        
        MNLine* line0 = [array objectAtIndex:0];
        MNLine* lastLine = [array lastObject];
        toLine.previousLine = lastLine;
        line0.previousLine = fromLine;
        
        
        if( fromLine == nil )
        {
            [topLine_ release];
            topLine_ = [line0 retain];
        }
    }
    
    
    
    
    /// Translate range location using offset
    
    line = toLine;
    while( line )
    {
        CFRange range = line.range;
        range.location += offset;
        line.range = range;
        
        
        CFIndex baseLoc = line.baseLocation;
        baseLoc += offset;
        line.baseLocation = baseLoc;
        
        line = line.nextLine;
        
    }
    
    
    
    
    line = fromLine;
    CGPoint origin = CGPointZero;
    NSUInteger lineNumber = 0;
    
    if( line )
    {
        origin.y = CGRectGetMaxY(line.occupiedFrame);
        lineNumber = line.lineNumber;
    }
    
    // Start from new top line
    
    if( line )
    {
        line = line.nextLine;
        lineNumber++;
    }
    else
    {
        if( [array count]> 0 )
            line = [array objectAtIndex:0];
        else line = toLine;
    }
    
    
    // Update origin and line number to the end
    
    while(line)
    {
        origin.y += line.descent + line.lineSpacing +1;
        origin.x = line.origin.x;
        
        line.origin = origin;
        line.lineNumber = lineNumber++;
        line.parentFrame = [frames_ objectAtIndex:0]; // 決めうち
        
        origin.y += line.ascent + line.leading +  line.paragraphSpacingAfter + line.paragraphSpacingBefore;
        line = line.nextLine;
    }
    
    MNFrame* frame = [frames_ objectAtIndex:0];
    NSArray* oldLines = [frame.lines retain];
    frame.lines = [NSMutableArray array];
    
    
    
    enumerator = [self lineEnumerator];
    while( (line = [enumerator nextObject]) )
    {
        line.parentFrame = frame;
        [frame.lines addObject: line];
        
        [line adjustTextAlignmentForWidth: self.containerSize.width
                        forParagraphStyle: [self paragraphAttributesOfLine:line] ];
        
    }
    
    
    [oldLines release];
    [unusedLines makeObjectsPerformSelector:@selector(setParentFrame:) withObject:nil];
    
    [self evaluateLines];
    
}

-(MNLine*)lastLine
{
    
    NSEnumerator *enumerator = [self lineEnumerator];
    MNLine *line;
    MNLine *lastLine = nil;
    while( (line = [enumerator nextObject]) )
    {
        lastLine = line;
        
    }
    
    return lastLine;
}


-(CTParagraphStyleRef)paragraphAttributesOfLine:(MNLine*)line
{
    NSUInteger location = [line stringRange].location;
    
    if( location == NSNotFound || textStorage_.length <= location )
    {
        NSLog(@"** Illegal location (%ld) **", (unsigned long)location);
        return nil;
    }
    
    NSDictionary* attributes = [textStorage_ attributesAtIndex:location effectiveRange:nil];
    
    return (CTParagraphStyleRef)[attributes objectForKey: MNCTParagraphStyleAttributeName];
}

-(void)evaluateLines
{
    
    return;
    
    /*
    //NSLog(@"%@",textStorage_.string);
    NSEnumerator *enumerator = [self lineEnumerator];
    MNLine* previousLine = nil;
    NSUInteger lineNumber = 0;
    MNLine *line;
    
    while( (line = (MNLine*)[enumerator nextObject]) )
    {
        //MNLOG(@"Line %d %d,%d",line.lineNumber, line.range.location, line.range.length);
        if( previousLine )
        {
            if( previousLine.nextLine  != line ) NSLog(@"*** Connection (next) %ld is not correct", (unsigned long)line.lineNumber);
            if( line.previousLine != previousLine ) NSLog(@"*** Connection (prev) %d is not correct", line.lineNumber);
            if( MAXRANGE( line.previousLine.range ) != line.range.location ) NSLog(@"*** Range is not correct");
        }
        
        if( line.lineNumber !=  lineNumber ) NSLog(@"*** Line Number %d is not correct", line.lineNumber);
        
        
        lineNumber++;
    }
     */
}


@end


@implementation MNLineEnumerator
@synthesize line = line_;

- (NSArray *)allObjects
{
    NSMutableArray* array = [NSMutableArray array];
    
    do	{
        
        [array addObject: line_ ];
    }while( (line_ = [line_ nextLine]) );
    
    return array;
}

- (id)nextObject
{
    MNLine* lineToReturn = line_;
    
    line_ = line_.nextLine;
    
    return lineToReturn;
}


@end




@implementation MNLine

@synthesize range = range_;
@synthesize origin = origin_;
@synthesize parentFrame = parentFrame_;
@synthesize previousLine = previousLine_;
@synthesize nextLine = nextLine_;
@synthesize valid = valid_;
@synthesize lineNumber = lineNumber_;
@synthesize ascent = ascent_;
@synthesize descent = descent_;
@synthesize leading = leading_;
@synthesize width = width_;
@synthesize baseLocation = baseLocation_;
@synthesize lineSpacing = lineSpacing_;
@synthesize paragraphSpacingBefore = paragraphSpacingBefore_;
@synthesize paragraphSpacingAfter = paragraphSpacingAfter_;

static NSUInteger lineCount = 0;

- (id) init
{
    self = [super init];
    if (self != nil) {
        lineCount++;
    }
    return self;
}


-(void)setCTLine:(CTLineRef)aLine
{
    line_ = aLine;
    CFRetain(line_);
}

-(CTLineRef)CTLine
{
    return line_;
}

-(NSString*)description
{
  return [NSString stringWithFormat: @"%lu", (unsigned long)self.lineNumber];
}

-(CGRect)typographicFrame
{
    return  CGRectMake( self.origin.x , self.origin.y - self.descent -self.lineSpacing + (self.paragraphSpacingAfter + self.paragraphSpacingBefore), self.width, self.leading+self.ascent+self.descent);
}

-(CGRect)occupiedFrame
{
    return  CGRectMake( self.origin.x , self.origin.y - self.descent -self.lineSpacing, self.width, self.leading + self.ascent + self.descent +self.paragraphSpacingAfter + self.paragraphSpacingBefore + self.lineSpacing);
}

-(void)dealloc
{
    lineCount--;
    
    
    if( line_ ) {
        CFRelease(line_);
        line_ = nil;
    }
    
    self.parentFrame = nil;
    self.previousLine = nil;
    self.nextLine = nil;
    
    [super dealloc];
}

-(void)setPreviousLine:(MNLine *)line
{
    previousLine_ = line;
    if( line.nextLine != self )
        line.nextLine = self;
}
-(void)setNextLine:(MNLine *)line
{
    nextLine_ = line;
    if( line.previousLine != self )
        line.previousLine = self;
}



// Wrapper for CTLine

-(CFRange)stringRange
{
    CFRange range = CTLineGetStringRange(line_);
    range.location += baseLocation_;
    
    return range;
}

-(CGFloat)offsetForStringIndex:(NSUInteger)location
{
    CGFloat offsetX = CTLineGetOffsetForStringIndex(line_, location - baseLocation_, nil);
    
    offsetX *= self.parentFrame.documentScaling;
    
    return offsetX;
    
}

-(CFIndex)CTLineGetStringIndexForPosition:(CGPoint)positionInLine
{
    CGPoint p = CGPointMake( positionInLine.x / self.parentFrame.documentScaling, positionInLine.y / self.parentFrame.documentScaling );
    
    CFIndex index = CTLineGetStringIndexForPosition(line_, p);
    return index + baseLocation_;
}



-(void)adjustTextAlignmentForWidth:(CGFloat)width forParagraphStyle:(CTParagraphStyleRef)style
{
    // Right to left
    CTTextAlignment alignment = kCTLeftTextAlignment;
    
    
    CTParagraphStyleRef paragraphStyle = style;
    
    CTWritingDirection buffer;
    if( CTParagraphStyleGetValueForSpecifier( paragraphStyle, kCTParagraphStyleSpecifierBaseWritingDirection, sizeof(CTWritingDirection),&buffer) )
    {
        
        if( buffer == kCTWritingDirectionRightToLeft )
            alignment = kCTRightTextAlignment;
        
    }
    //
    
    CTTextAlignment buf2;
    if( CTParagraphStyleGetValueForSpecifier( paragraphStyle, kCTParagraphStyleSpecifierAlignment, sizeof(CTTextAlignment),&buf2) )
    {
        alignment = buf2;
        
        MNLOG(@"width %f",width);
        MNLOG(@"self.width %f",self.width);
        MNLOG(@"self.parentFrame %@",self.parentFrame);
        MNLOG(@"self.parentFrame.documentScaling %f",self.parentFrame.documentScaling);
        
        if( self.parentFrame==nil )
        {
            abort();
        }
        
        if( alignment == kCTRightTextAlignment )
        {
            CGPoint origin = self.origin;
            origin.x = (width - self.width*self.parentFrame.documentScaling) ;
            self.origin = origin;
        }
        
        else if( alignment == kCTCenterTextAlignment )
        {
            CGPoint origin = self.origin;
            origin.x = (width - self.width*self.parentFrame.documentScaling)/2 ;
            self.origin = origin;
        }
        
    }
}

@end


@implementation MNFrame

@synthesize pathBoundingBox = pathBoundingBox_;
@synthesize range = range_;
@synthesize lines = lines_;
@synthesize documentScaling = documentScaling_;

-(void)setCTFrame:(CTFrameRef)aFrame
{
    frame_ = aFrame;
    CFRetain(frame_);
}

-(CTFrameRef)CTFrame
{
    return frame_;
}

-(void)dealloc
{
    if( frame_ )
        CFRelease(frame_);
    
    
    [lines_ release];
    lines_ = nil;
    
    [super dealloc];
}
@end


// OMNI AppKit
#ifdef OAAppKitVersionNumber10_8
extern NSString * const OABackgroundColorAttributeName;
extern NSString * const OALinkAttributeName;
extern NSString * const OAStrikethroughStyleAttributeName;
extern NSString * const OAStrikethroughColorAttributeName;
extern NSUInteger const OAUnderlineByWordMask;
#else
#define OABackgroundColorAttributeName @"OABackgroundColorAttributeName"
#define OALinkAttributeName @"OALinkAttributeName"
#define OAStrikethroughStyleAttributeName @"OAStrikethroughStyleAttributeName"
#define OAStrikethroughColorAttributeName @"OAStrikethroughColorAttributeName"
#define OAUnderlineByWordMask @"OAUnderlineByWordMask"
#endif



void MNRunDrawWithScaling(CTRunRef run,
                          CGContextRef ctx,
                          CFRange range,
                          BOOL printingResolution,
                          BOOL ignoreScaling,
                          CGFloat documentScaling,
                          BOOL vertical)
{
    if( !ctx ){
        
        NSLog(@"*** context is nil ***");
        return;
    }
    
    CGContextSaveGState(ctx);
    
    if( !ignoreScaling )
    {
        CGAffineTransform t = CGAffineTransformMakeScale( documentScaling, documentScaling);
        CGContextConcatCTM(ctx, t);
    }
    
#if TARGET_OS_IPHONE
    
    CFDictionaryRef dict = CTRunGetAttributes(run);
    CTRunDelegateRef runDelegate = CFDictionaryGetValue(dict, kCTRunDelegateAttributeName);
    
    if( runDelegate && [ (id)CTRunDelegateGetRefCon(runDelegate) isKindOfClass:[MNAttachmentCell class]] )
    {
        MNAttachmentCell* cell = CTRunDelegateGetRefCon(runDelegate);
        
        const CGPoint  *point;
        point = CTRunGetPositionsPtr(run);
        
        CGPoint pp = point[0];
        
        
        if( point )
        {
            if( [cell respondsToSelector:@selector(renderAtPoint:  inContext:  printingResolution:  attributes:  documentScaling: vertical: ) ])
            {
                [cell renderAtPoint: CGPOINTINTEGRATE( pp )
                          inContext: ctx
                 printingResolution: printingResolution
                         attributes: dict
                    documentScaling: documentScaling
                           vertical: vertical ];
            }
            else
            {
                [cell renderAtPoint: CGPOINTINTEGRATE( pp )
                          inContext: ctx
                 printingResolution: printingResolution
                    documentScaling: documentScaling
                           vertical: vertical ];
            }
        }
    }
    else
    {
        NSNumber* superscript = CFDictionaryGetValue(dict, kCTSuperscriptAttributeName);
        
        if( superscript )
        {
            CTFontRef font = CFDictionaryGetValue(dict, kCTFontAttributeName);
            
            if( font )
            {
                CGFloat ascent = CTFontGetAscent(font);
                CGAffineTransform t = CGAffineTransformMakeTranslation( 0, - ascent * 0.3 * superscript.integerValue );
                CGContextConcatCTM(ctx, t);
            }
        }
        else
        {
            
            //			if( vertical )
            //			{
            //				CTFontRef font = CFDictionaryGetValue(dict, kCTFontAttributeName);
            //
            //				if( font )
            //				{
            //					CGFloat descent = CTFontGetDescent(font);
            //
            //					CGAffineTransform t =  CGAffineTransformMakeTranslation( + descent , 0 );
            //
            //					CGContextConcatCTM(ctx, t);
            //
            //				}
            //			}
            
        }
        CTRunDraw(run, ctx, range);
    }
    
    //Strikethrough
    
    
    NSNumber* strikethrough = CFDictionaryGetValue(dict, OAStrikethroughStyleAttributeName);
    
    CTUnderlineStyle style = [strikethrough integerValue];
    if( style != kCTUnderlineStyleNone )
    {
        CGFloat ascent, descent, leading;
        double width = CTRunGetTypographicBounds(run, CFRangeMake(0, 0), &ascent, &descent, &leading);
        
        CGColorRef color = (CGColorRef)CFDictionaryGetValue(dict, OAStrikethroughColorAttributeName);
        
        if( color == nil ) color = (CGColorRef)CFDictionaryGetValue(dict, MNCTForegroundColorAttributeName);
        if( color == nil ) color = [UIColor colorWithRed:0 green:0 blue:0 alpha:1].CGColor;
        
        const CGPoint  *point;
        point = CTRunGetPositionsPtr(run);
        
        CGPoint pp = point[0];
        
        
        CGFloat offset1 = pp.x;
        CGFloat offset2 = offset1 + width;
        CGFloat lineY = roundf( pp.y - ascent /2 ) + 2.5;
        
        CGPoint points[2];
        points[0] = CGPointMake( offset1, lineY);
        points[1] = CGPointMake( offset2, lineY);
        
        
        CGContextSetStrokeColorWithColor(ctx, color);
        CGContextSetLineDash (ctx, 0, 0, 0 );
        
        if( style == kCTUnderlineStyleSingle )
        {
            points[0] = CGPointMake( offset1, lineY);
            points[1] = CGPointMake( offset2, lineY);
            
            CGContextSetLineWidth(ctx, 1.0);
            CGContextStrokeLineSegments(ctx, points, 2);
        }
        
        if( style == kCTUnderlineStyleThick )
        {
            points[0] = CGPointMake( offset1, lineY);
            points[1] = CGPointMake( offset2, lineY);
            
            CGContextSetLineWidth(ctx, 3.0);
            CGContextStrokeLineSegments(ctx, points, 2);
        }
        
        if( style == kCTUnderlineStyleDouble )
        {
            CGContextSetLineWidth(ctx, 1.0);
            
            points[0] = CGPointMake( offset1, lineY-1);
            points[1] = CGPointMake( offset2, lineY-1);
            
            CGContextStrokeLineSegments(ctx, points, 2);
            
            points[0] = CGPointMake( offset1, lineY+1);
            points[1] = CGPointMake( offset2, lineY+1);
            
            CGContextStrokeLineSegments(ctx, points, 2);
        }
    }
    
    //
    
#else
    
    CTRunDraw(run, ctx, range);
    
#endif
    
    CGContextRestoreGState(ctx);
}



void MNRunDraw(CTRunRef run,
               CGContextRef ctx,
               CFRange range,
               BOOL printingResolution,
               BOOL ignoreScaling)
{
    MNRunDrawWithScaling( run,
                         ctx,
                         range,
                         printingResolution,
                         ignoreScaling,
                         1.0,
                         NO);
}

/*
 void CTRunDraw (
 CTRunRef run,
 CGContextRef context,
 CFRange range
 );
 
 */
