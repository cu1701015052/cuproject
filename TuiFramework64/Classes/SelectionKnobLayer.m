//
//  KnobLayer.m
//  MNCTTextView
//
//  Created by Masatoshi Nishikata on 29/12/10.
//  Copyright 2010 Masatoshi Nishikata. All rights reserved.
//

#import "SelectionKnobLayer.h"


@implementation SelectionKnobLayer
@synthesize  bottom;

#if TARGET_OS_IPHONE
#define CURRENT_CONTEXT UIGraphicsGetCurrentContext()
#else

// OSX
#define CURRENT_CONTEXT (CGContextRef)[[NSGraphicsContext currentContext] graphicsPort]
#endif

#ifndef RGBA

#if TARGET_OS_IPHONE

#define RGBA(r,g,b,a) [UIColor colorWithRed: r/255.0f green: g/255.0f \
blue: b/255.0f alpha: a]

#else

#define RGBA(r,g,b,a) [NSColor colorWithCalibratedRed: r/255.0f green: g/255.0f \
blue: b/255.0f alpha: a]

#endif
#endif

#define kUpdateFGColor RGBA(255, 131, 48, 1)
#define UIColor NSColor

- (instancetype)init
{
  self = [super init];
  if (self) {
    self.zPosition = 100;
  }
  return self;
}

-(UIColor*)iOS7SelectedColor
{
	return RGBA(41, 132, 254, 1);
}

- (void)drawInContext:(CGContextRef)ctx
{
	BOOL flatDesign = NO;
	
  /*
	if( ![[[UIDevice currentDevice] systemVersion] hasPrefix:@"6"] && ![[[UIDevice currentDevice] systemVersion] hasPrefix:@"5"])
	{
		flatDesign = YES;
	}
  */
	
	CGRect frame = self.frame;
	CGPoint points[2];
	CGFloat RADIUS = 6;

	CGFloat width_2 = floorf(frame.size.width/2);
	CGFloat ALPHA = 0.5;
	
	if( bottom )
	{
		
		// Bar
		
		if( flatDesign )
		{
			CGContextSetLineWidth(ctx, 2.0);
			
			points[0] = CGPointMake(width_2, 0);
			points[1] = CGPointMake(width_2, frame.size.height-2*RADIUS);
			
			CGContextSetStrokeColorWithColor(ctx, [self iOS7SelectedColor].CGColor);
			CGContextStrokeLineSegments(ctx, points, 2);
			

		}else
		{
		CGContextSetLineWidth(ctx, 1.0);

		points[0] = CGPointMake(width_2, 0);
		points[1] = CGPointMake(width_2, frame.size.height-2*RADIUS);

		CGContextSetStrokeColorWithColor(ctx, RGBA(42,77,220,1).CGColor);
		CGContextStrokeLineSegments(ctx, points, 2);
		
		points[0] = CGPointMake(width_2-1, 0);
		points[1] = CGPointMake(width_2-1, frame.size.height-2*RADIUS);
		
		CGContextSetStrokeColorWithColor(ctx, RGBA(42,77,220,ALPHA).CGColor);
		CGContextStrokeLineSegments(ctx, points, 2);
		
		
		points[0] = CGPointMake(width_2+1, 0);
		points[1] = CGPointMake(width_2+1, frame.size.height-2*RADIUS);
		
		CGContextSetStrokeColorWithColor(ctx, RGBA(42,77,220,ALPHA).CGColor);
		CGContextStrokeLineSegments(ctx, points, 2);
		}
		
		// Ballsc
	
		
		
		CGRect ballRect;
		
		
		ballRect = CGRectMake( width_2 -RADIUS, frame.size.height- RADIUS*2 -2, RADIUS*2, RADIUS*2);
		
		if( flatDesign )
		{
			CGContextSetFillColorWithColor(ctx, [self iOS7SelectedColor].CGColor);
			CGContextAddEllipseInRect(ctx, ballRect);
			CGContextFillPath(ctx);
		}else
		{
			
            if( self.isVertical ) {
                CGContextSetShadow(ctx, CGSizeMake(2, 0), 1.0);
                
            }else {
                CGContextSetShadow(ctx, CGSizeMake(0, 2), 1.0);
            }
            CGContextSetFillColorWithColor(ctx, RGBA(255,255,255,1).CGColor );
			CGContextFillEllipseInRect(ctx, ballRect);
			CGContextSetShadow(ctx, CGSizeMake(0, 0), 0.0);
			
			
			ballRect = CGRectInset(ballRect, 1, 1);
			CGContextAddEllipseInRect(ctx, ballRect);
			
			CGContextSaveGState(ctx);
			CGContextClip(ctx);
            if( self.isVertical ) {
                [SelectionKnobLayer drawGradientHorizontallyInRect:ballRect fromColor:RGBA(30,84,234,1) toColor:RGBA(20,54,147,1) inContext:ctx];

            }else {
                [SelectionKnobLayer drawGradientInRect:ballRect fromColor:RGBA(30,84,234,1) toColor:RGBA(20,54,147,1) inContext:ctx];
            }
			CGContextRestoreGState(ctx);
			
		}
		
	}
	
	else 
	{
		// Bar
		
		if( flatDesign )
		{
			CGContextSetLineWidth(ctx, 2.0);
			
			points[0] = CGPointMake(width_2, RADIUS*2+2);
			points[1] = CGPointMake(width_2, frame.size.height);
			
			CGContextSetStrokeColorWithColor(ctx, [self iOS7SelectedColor].CGColor);
			CGContextStrokeLineSegments(ctx, points, 2);

		}else
		{
		CGContextSetLineWidth(ctx, 1.0);

		points[0] = CGPointMake(width_2, RADIUS*2+2);
		points[1] = CGPointMake(width_2, frame.size.height);

		CGContextSetStrokeColorWithColor(ctx, RGBA(42,77,220,1).CGColor);
		CGContextStrokeLineSegments(ctx, points, 2);
	
		
		points[0] = CGPointMake(width_2-1, RADIUS*2+2);
		points[1] = CGPointMake(width_2-1, frame.size.height);
		
		CGContextSetStrokeColorWithColor(ctx, RGBA(42,77,220,ALPHA).CGColor);
		CGContextStrokeLineSegments(ctx, points, 2);
		
		
		points[0] = CGPointMake(width_2+1, RADIUS*2+2);
		points[1] = CGPointMake(width_2+1, frame.size.height);
		
		CGContextSetStrokeColorWithColor(ctx, RGBA(42,77,220,ALPHA).CGColor);
		CGContextStrokeLineSegments(ctx, points, 2);
		
		}
		
		
		// Balls
		

		
		CGRect ballRect = CGRectMake(width_2 - RADIUS, 2, RADIUS*2, RADIUS*2);
		
		if( flatDesign )
		{
            CGContextSetFillColorWithColor(ctx, [self iOS7SelectedColor].CGColor);
            CGContextAddEllipseInRect(ctx, ballRect);
            CGContextFillPath(ctx);
        }else
        {
            if( self.isVertical ) {
                CGContextSetShadow(ctx, CGSizeMake(2, 0), 1.0);
                
            }else {
                CGContextSetShadow(ctx, CGSizeMake(0, 2), 1.0);
            }
            CGContextSetFillColorWithColor(ctx, RGBA(255,255,255,1).CGColor );
            CGContextFillEllipseInRect(ctx, ballRect);
            CGContextSetShadow(ctx, CGSizeMake(0, 0), 0.0);

		ballRect = CGRectInset(ballRect, 1, 1);
		CGContextAddEllipseInRect(ctx, ballRect);
		
		CGContextSaveGState(ctx);
		CGContextClip(ctx);
            if( self.isVertical ) {
                [SelectionKnobLayer drawGradientHorizontallyInRect:ballRect fromColor:RGBA(30,84,234,1) toColor:RGBA(20,54,147,1) inContext:ctx];

            }else {
		[SelectionKnobLayer drawGradientInRect:ballRect fromColor:RGBA(30,84,234,1) toColor:RGBA(20,54,147,1) inContext:ctx];
            }
		CGContextRestoreGState(ctx);
		}
	}
	
}

+(void)drawGradientHorizontallyInRect:(CGRect)rect fromColor:(UIColor*)fromColor  toColor:(UIColor*)toColor inContext:(CGContextRef)ctx
{
  CGContextSaveGState(ctx);
  CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
  
  
  CGPoint startPoint = rect.origin;
  CGPoint endPoint = CGPointMake(CGRectGetMaxX(rect), rect.origin.y);
  
  const CGFloat *fromComponents;
  const CGFloat *toComponents;
  
  fromComponents = CGColorGetComponents(fromColor.CGColor);
  toComponents = CGColorGetComponents(toColor.CGColor);
  
  
  CGColorRef colorRef1 = CGColorCreate( colorSpace, fromComponents);
  CGColorRef colorRef2 = CGColorCreate( colorSpace, toComponents);
  
  CFMutableArrayRef array = CFArrayCreateMutable(kCFAllocatorDefault, 0, &kCFTypeArrayCallBacks);
  CFArrayAppendValue(array, colorRef1);
  CFArrayAppendValue(array, colorRef2);
  
  
  CGFloat locations[3];
  locations[0] = 0;
  locations[1] = 1.0;
  
  CGGradientRef gradient = CGGradientCreateWithColors( colorSpace, array, locations);
  CFRelease(array);
  
  CFRelease(colorRef1);
  CFRelease(colorRef2);
  
  
  //CGContextSetBlendMode(ctx, kCGBlendModeNormal);
  
  
  CGContextDrawLinearGradient(ctx, gradient, startPoint, endPoint, 0 );
  
  
  CGGradientRelease(gradient);
  CGColorSpaceRelease(colorSpace);
  CGContextRestoreGState(ctx);
}

+(void)drawGradientInRect:(CGRect)rect fromColor:(UIColor*)fromColor toColor:(UIColor*)toColor highlightColor:(UIColor*)highlightColor
{
  
  
  
  CGContextRef ctx = CURRENT_CONTEXT;
  CGContextSaveGState(ctx);
  CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
  CGPoint startPoint = rect.origin;
  CGPoint endPoint = CGPointMake(rect.origin.x, CGRectGetMaxY(rect));
  
  const CGFloat *fromComponents;
  const CGFloat *toComponents;
  
  fromComponents = CGColorGetComponents(fromColor.CGColor);
  toComponents = CGColorGetComponents(toColor.CGColor);
  
  
  CGColorRef colorRef1 = CGColorCreate( colorSpace, fromComponents);
  CGColorRef colorRef2 = CGColorCreate( colorSpace, toComponents);
  
  CFMutableArrayRef array = CFArrayCreateMutable(kCFAllocatorDefault, 0, &kCFTypeArrayCallBacks);
  CFArrayAppendValue(array, colorRef1);
  CFArrayAppendValue(array, colorRef2);
  
  
  CGFloat locations[2];
  locations[0] = 0;
  locations[1] = 1.0;
  
  CGGradientRef gradient = CGGradientCreateWithColors( colorSpace, array, locations);
  CFRelease(array);
  
  CFRelease(colorRef1);
  CFRelease(colorRef2);
  
  
  CGContextSetBlendMode(ctx, kCGBlendModeNormal);
  
  CGContextClipToRect(ctx, rect);
  
  
  CGContextDrawLinearGradient(ctx, gradient, startPoint, endPoint, 0 );
  
  if( highlightColor )
  {
    CGPoint points[2];
    
    points[0] = CGPointMake(rect.origin.x, rect.origin.y);
    points[1] = CGPointMake(CGRectGetMaxX(rect), rect.origin.y);
    
    CGContextSetLineWidth(ctx, 2.0);
    CGContextSetStrokeColorWithColor(ctx, highlightColor.CGColor);
    CGContextStrokeLineSegments(ctx, points, 2);
    
  }
  
  CGGradientRelease(gradient);
  CGColorSpaceRelease(colorSpace);
  CGContextRestoreGState(ctx);
}

+(void)drawGradientInRect:(CGRect)rect fromColor:(UIColor*)fromColor  toColor:(UIColor*)toColor inContext:(CGContextRef)ctx
{
  CGContextSaveGState(ctx);
  CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
  
  
  CGPoint startPoint = rect.origin;
  CGPoint endPoint = CGPointMake(rect.origin.x, CGRectGetMaxY(rect));
  
  const CGFloat *fromComponents;
  const CGFloat *toComponents;
  
  fromComponents = CGColorGetComponents(fromColor.CGColor);
  toComponents = CGColorGetComponents(toColor.CGColor);
  
  
  CGColorRef colorRef1 = CGColorCreate( colorSpace, fromComponents);
  CGColorRef colorRef2 = CGColorCreate( colorSpace, toComponents);
  
  CFMutableArrayRef array = CFArrayCreateMutable(kCFAllocatorDefault, 0, &kCFTypeArrayCallBacks);
  CFArrayAppendValue(array, colorRef1);
  CFArrayAppendValue(array, colorRef2);
  
  
  CGFloat locations[3];
  locations[0] = 0;
  locations[1] = 1.0;
  
  CGGradientRef gradient = CGGradientCreateWithColors( colorSpace, array, locations);
  CFRelease(array);
  
  CFRelease(colorRef1);
  CFRelease(colorRef2);
  
  
  //CGContextSetBlendMode(ctx, kCGBlendModeNormal);
  
  
  CGContextDrawLinearGradient(ctx, gradient, startPoint, endPoint, 0 );
  
  
  CGGradientRelease(gradient);
  CGColorSpaceRelease(colorSpace);
  CGContextRestoreGState(ctx);
}


@end
