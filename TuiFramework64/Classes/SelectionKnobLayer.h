//
//  KnobLayer.h
//  MNCTTextView
//
//  Created by Masatoshi Nishikata on 29/12/10.
//  Copyright 2010 Masatoshi Nishikata. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <AppKit/AppKit.h>

@interface SelectionKnobLayer : CALayer {

	BOOL bottom;
}
- (void)drawInContext:(CGContextRef)ctx;
@property (nonatomic) BOOL bottom;
@property (nonatomic, getter=isVertical) BOOL vertical;

@end
