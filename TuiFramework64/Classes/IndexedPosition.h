//
//  IndexedPosition.h
//  CoreTextRTF
//
//  Created by Masatoshi Nishikata on 25/12/10.
//  Copyright 2010 Masatoshi Nishikata. All rights reserved.
//

#import <Foundation/Foundation.h>

#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>

#else
@interface UITextPosition: NSObject
@end

@interface UITextRange: NSObject

@property(nonatomic, readonly) UITextPosition *start;
@property(nonatomic, readonly) UITextPosition *end;
@property(nonatomic, readonly, getter=isEmpty) BOOL empty;

@end

#define UITextStorageDirection NSInteger
#define UITextInputDelegate NSObject
#define UITextStorageDirectionForward 0
#define UITextStorageDirectionBackward 1
#endif

@interface IndexedPosition : UITextPosition {

	NSUInteger index_;
	UITextStorageDirection affinity_;
	
	id <UITextInputDelegate> inputDelegate_;
	
}
+(IndexedPosition*)positionWithIndex:(NSUInteger)index inAttributedString:(NSAttributedString*)attr;
-(void)setAffinity:(UITextStorageDirection)newAffinity;
+(IndexedPosition*)positionWithIndex:(NSUInteger)index inAttributedString:(NSAttributedString*)attr suggestedAffinity:(UITextStorageDirection)affinity;;

@property (nonatomic) NSUInteger textIndex;
@property (nonatomic) UITextStorageDirection affinity;

@end


@interface IndexedRange : UITextRange
{
	NSRange range_;
	NSAttributedString *attributedString_;

}
+(IndexedRange*)rangeWithNSRange:(NSRange)nsrange inAttributedString:(NSAttributedString*)attr;

@property (nonatomic) NSRange range;
@property (nonatomic, retain) NSAttributedString *attributedString;

@end

