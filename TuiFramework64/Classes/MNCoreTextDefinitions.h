//
//  MNCoreTextDefinitions.h
//  FastFinga3
//
//  Created by Masatoshi Nishikata on 12/04/11.
//  Copyright 2011 NISHIKATA Masatoshi. All rights reserved.
//

#define MNCTFontAttributeName							(NSString*)kCTFontAttributeName
#define MNCTForegroundColorFromContextAttributeName	(NSString*)kCTForegroundColorFromContextAttributeName
#define MNCTKernAttributeName							(NSString*)kCTKernAttributeName
#define MNCTLigatureAttributeName						(NSString*)kCTLigatureAttributeName
#define MNCTForegroundColorAttributeName				(NSString*)kCTForegroundColorAttributeName
#define MNCTParagraphStyleAttributeName		(NSString*)kCTParagraphStyleAttributeName
#define MNCTStrokeWidthAttributeName		(NSString*)kCTStrokeWidthAttributeName
#define MNCTStrokeColorAttributeName		(NSString*)kCTStrokeColorAttributeName
#define MNCTUnderlineStyleAttributeName		(NSString*)kCTUnderlineStyleAttributeName
#define MNCTSuperscriptAttributeName		(NSString*)kCTSuperscriptAttributeName
#define MNCTUnderlineColorAttributeName		(NSString*)kCTUnderlineColorAttributeName
#define MNCTVerticalFormsAttributeName		(NSString*)kCTVerticalFormsAttributeName
#define MNCTGlyphInfoAttributeName		(NSString*)kCTGlyphInfoAttributeName
#define MNCTCharacterShapeAttributeName		(NSString*)kCTCharacterShapeAttributeName
#define MNCTRunDelegateAttributeName		(NSString*)kCTRunDelegateAttributeName

#define MNCTVerticalFormsAttributeName		(NSString*)kCTVerticalFormsAttributeName

