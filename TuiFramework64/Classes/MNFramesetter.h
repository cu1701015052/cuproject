//
//  MNFramesetter.h
//  CoreTextRTF
//
//  Created by Masatoshi Nishikata on 26/12/10.
//  Copyright 2010 Masatoshi Nishikata. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

#if TARGET_OS_IPHONE
#import <CoreText/CoreText.h>
#import <UIKit/UIKit.h>

#else
#import <ApplicationServices/ApplicationServices.h>
#endif

extern void MNRunDraw(CTRunRef run,
							 CGContextRef ctx,
							 CFRange range,
							 BOOL printingResolution,
							 BOOL ignoreScaling);


extern void MNRunDrawWithScaling(CTRunRef run,
											CGContextRef ctx,
											CFRange range,
											BOOL printingResolution,
											BOOL ignoreScaling,
											CGFloat documentScaling,
											BOOL vertical );

// MNFramesetter
//   creates frames from attributed string and paths

@class MNLine, MNFrame, MNLineEnumerator;

@interface MNFramesetter : NSObject  {

	NSMutableAttributedString* textStorage_;

	CGFloat documentScaling_;
	CGSize containerSize_;
	CGSize suggestedContainerSize_;

	//CTFramesetterRef framesetter_;
	NSMutableArray* frames_;
	
	MNLine *topLine_;
	
	CGSize suggestedSize_;
	
}

- (id) initWithAttributedString:(NSMutableAttributedString*)attr;
-(void)dealloc;
-(void)layoutUsingTypesetter;
-(void)invalidateLayout;
-(NSRange)invalidateLayoutWithOldRange:(NSRange)oldRange newRange:(NSRange)newRange;
-(MNLineEnumerator*)lineEnumerator;
-(CTRunRef)runAtIndex:(NSUInteger)index;
-(MNLine*)lineAtIndex:(NSUInteger)index;
-(void)insertLines:(NSArray*)array from:(MNLine*)fromLine to:(MNLine*)toLine offset:(NSInteger)offset;
-(MNLine*)lastLine;
-(CTParagraphStyleRef)paragraphAttributesOfLine:(MNLine*)line;
-(void)evaluateLines;

//@property (nonatomic, readonly) NSMutableArray* lines;
//@property (nonatomic, readonly) CTFramesetterRef CTFramesetter;
@property (nonatomic, readonly) NSMutableArray* frames;
@property (nonatomic, readonly) CGSize suggestedSize;
@property (nonatomic) CGSize containerSize;
@property (nonatomic) CGPoint containerOrigin;
@property (nonatomic, readonly) CGSize suggestedContainerSize;
@property (nonatomic) CGFloat documentScaling;
@property (nonatomic, readonly) NSAttributedString* textStorage;


@end

@interface MNLineEnumerator : NSEnumerator
{
	MNLine *line_;
}

@property (nonatomic, assign) MNLine *line;
@end

@interface MNLine : NSObject {

	MNLine *previousLine_;
	MNLine *nextLine_;
	
	CFRange range_;
	CFIndex baseLocation_; // base CTLine's base location in the text.  0 or pagaraph index
	NSUInteger lineNumber_;
	
	BOOL valid_;
	
	MNFrame *parentFrame_;
	CGPoint origin_;
	CGFloat ascent_;
	CGFloat descent_;
	CGFloat leading_;
	CGFloat width_;

	CGFloat lineSpacing_; // This comes below the line
	CGFloat paragraphSpacingBefore_;
	CGFloat paragraphSpacingAfter_;
	
	CTLineRef line_;
}

- (id) init;
-(void)setCTLine:(CTLineRef)aLine;
-(CTLineRef)CTLine;
-(NSString*)description;
-(CGRect)typographicFrame;
-(CGRect)occupiedFrame;
-(void)dealloc;
-(void)setPreviousLine:(MNLine *)line;
-(void)setNextLine:(MNLine *)line;
-(CFRange)stringRange;
-(CGFloat)offsetForStringIndex:(NSUInteger)location;
-(CFIndex)CTLineGetStringIndexForPosition:(CGPoint)positionInLine;
-(void)adjustTextAlignmentForWidth:(CGFloat)width forParagraphStyle:(CTParagraphStyleRef)style;

@property (nonatomic, getter=isValid)	BOOL valid;

@property (nonatomic) CFRange range;
@property (nonatomic) CFIndex baseLocation;
@property (nonatomic) NSUInteger lineNumber;

@property (nonatomic, readonly) CGRect typographicFrame;
@property (nonatomic, readonly) CGRect occupiedFrame;

@property (nonatomic) CGFloat ascent;
@property (nonatomic) CGFloat descent;
@property (nonatomic) CGFloat leading;
@property (nonatomic) CGFloat width;

@property (nonatomic) CGFloat lineSpacing; // This comes below the line
@property (nonatomic) CGFloat paragraphSpacingBefore;
@property (nonatomic) CGFloat paragraphSpacingAfter;

@property (nonatomic) CGPoint origin;
@property (nonatomic) CTLineRef CTLine;
@property (nonatomic, assign) MNFrame *parentFrame;

@property (nonatomic, assign) MNLine *previousLine;
@property (nonatomic, assign) MNLine *nextLine;

@end

@interface MNFrame : NSObject
{
	CTFrameRef frame_;
	
	CGFloat documentScaling_;
	CGRect pathBoundingBox_;
	CFRange range_;

	NSMutableArray *lines_;
}

@property (nonatomic) CGRect pathBoundingBox;
@property (nonatomic) CFRange range;
@property (nonatomic, retain) 	NSMutableArray *lines;
@property (nonatomic) CTFrameRef CTFrame;
@property (nonatomic) CGFloat documentScaling;

@end
