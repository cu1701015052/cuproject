//
//  MNAttachmentCell.h
//  CoreTextRTF
//
//  Created by Masatoshi Nishikata on 25/12/10.
//  Copyright 2010 Masatoshi Nishikata. All rights reserved.
//
//MNAttachmentCell *cell = [[[MNAttachmentCell alloc] init] autorelease];
//[cell embedIn: self.attributedString atIndex:3];

#import <Foundation/Foundation.h>

#if TARGET_OS_IPHONE
	#import <CoreText/CoreText.h>
#else
	#include <ApplicationServices/ApplicationServices.h>
	 
	#import "NSDocument+TuiExtension.h"

#endif

extern NSCache* cellDisplayCache_;

@interface MNAttachmentCell : NSObject <NSCoding, NSCopying>{

	CGFloat ascent_;
	CGFloat descent_;
	CGFloat width_;
	CGFloat pointSize_;
	CFUUIDBytes UUIDBytes_;
}
+ (void)initialize;
-(CGFloat)basePointSize;
- (id) init;
-(void)setUUID:(NSString *)string;
-(void)setNewUUID;
-(void)setPointSize:(CGFloat)pointSize undoManager:(NSUndoManager*)undoManager;
-(void)dealloc;
- (id)copyWithZone:(NSZone *)zone;
-(BOOL)isEqualToCell:(MNAttachmentCell*)aCell;
- (id) initWithCoder: (NSCoder*)coder;
- (void) encodeWithCoder: (NSCoder*)coder;
+(NSString*)controlCharacter;
-(NSString*)stringRepresentation;
-(NSString*)description;
-(NSString*)filename;
-(CGFloat)leading;
-(void)renderAtPoint:(CGPoint)origin inContext:(CGContextRef)ctx printingResolution:(BOOL)printingResolution documentScaling:(CGFloat)documentScaling vertical:(BOOL)vertical;

-(NSData*)imageRepresentation;
-(NSString*)md5Hash;
-(void)updateCache;
+(void)clearCache;

@property (nonatomic, readwrite) CGFloat ascent;
@property (nonatomic, readwrite) CGFloat descent;
@property (nonatomic, readwrite) CGFloat width;

@property (nonatomic) CGFloat pointSize;
@property (nonatomic, readonly) NSData* imageRepresentation;
@property (nonatomic) CFUUIDBytes UUIDBytes;

@end

@protocol MNAttachmentCellSaveToFileProtocol
@property (nonatomic, retain)	UIDocument* document;
-(NSString*)attachmentPathForDocumentPath:(NSString*)documentPath;
-(void)setNewUUIDForDocumentPath:(NSString*)path;
@end

#if TARGET_OS_IPHONE

@interface MNAttachmentCell (iOS)
-(CTRunDelegateRef)createRunDelegate;
-(void)embedIn:(NSMutableAttributedString*)attr atIndex:(NSUInteger)index undoManager:(NSUndoManager*)undoManager;
-(NSAttributedString*)attributedString;
-(NSAttributedString*)attributedStringInheritingAttributes:(NSDictionary*)baseAttributes;
+(NSInteger)textPositionIn:(NSAttributedString*)attr atIndex:(NSUInteger)index;
+(NSUInteger)indexIn:(NSAttributedString*)attr forTextPosition:(NSUInteger)position;

@end


@class MNTextView, MNMenuItem;

@protocol MNAttachmentCellTouchProtocol

-(void)touchedRect:(CGRect)cellFrame inView:(MNTextView*)view atIndex:(NSUInteger)characterIndex;
-(void)willTouchInRect:(CGRect)cellFrame inView:(MNTextView*)view atIndex:(NSUInteger)characterIndex;
@end


@protocol MNAttachmentCellMenuProtocol

-(MNMenuItem*)menuItemForTextView:(MNTextView*)textView;

@end


#else

@interface MNAttachmentCell (Mac) <NSTextAttachmentCell>
- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView;
- (BOOL)wantsToTrackMouse;
- (void)highlight:(BOOL)flag withFrame:(NSRect)cellFrame inView:(NSView *)controlView;
- (BOOL)trackMouse:(NSEvent *)theEvent inRect:(NSRect)cellFrame ofView:(NSView *)controlView untilMouseUp:(BOOL)flag;
- (NSSize)cellSize;
- (NSPoint)cellBaselineOffset;
- (void)setAttachment:(NSTextAttachment *)anObject;
- (NSTextAttachment *)attachment;
- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView characterIndex:(NSUInteger)charIndex;
- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView characterIndex:(NSUInteger)charIndex layoutManager:(NSLayoutManager *)layoutManager;
- (BOOL)wantsToTrackMouseForEvent:(NSEvent *)theEvent inRect:(NSRect)cellFrame ofView:(NSView *)controlView atCharacterIndex:(NSUInteger)charIndex;
- (BOOL)trackMouse:(NSEvent *)theEvent inRect:(NSRect)cellFrame ofView:(NSView *)controlView atCharacterIndex:(NSUInteger)charIndex untilMouseUp:(BOOL)flag;
- (NSRect)cellFrameForTextContainer:(NSTextContainer *)textContainer proposedLineFragment:(NSRect)lineFrag glyphPosition:(NSPoint)position characterIndex:(NSUInteger)charIndex;
@end
	
#endif
