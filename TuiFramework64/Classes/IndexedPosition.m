//
//  IndexedPosition.m
//  CoreTextRTF
//
//  Created by Masatoshi Nishikata on 25/12/10.
//  Copyright 2010 Masatoshi Nishikata. All rights reserved.
//

#import "IndexedPosition.h"
#import "MNAttachmentCell.h"


@implementation IndexedPosition
@synthesize textIndex = index_;
@synthesize affinity = affinity_;


+(IndexedPosition*)positionWithIndex:(NSUInteger)index inAttributedString:(NSAttributedString*)attr suggestedAffinity:(UITextStorageDirection)affinity
{
	IndexedPosition *pos = [[IndexedPosition alloc] init];
	
	// Limit index in content range
	if( index > attr.length -2 ) 
		index = attr.length-2;
	
	//if( index < 0 ) index = 0;
	
	pos.textIndex = index;
	pos.affinity = affinity;
	
	// adjust affinity
	if( index == 0 ) pos.affinity = UITextStorageDirectionForward;
	//if( index == attr.length-2 ) pos.affinity = UITextStorageDirectionForward;


	return [pos autorelease];
}

+(IndexedPosition*)positionWithIndex:(NSUInteger)index inAttributedString:(NSAttributedString*)attr
{
	IndexedPosition *pos = [[IndexedPosition alloc] init];
	
	// Limit index in content range
	if( index > attr.length -2 ) 
		index = attr.length-2;
	
	//if( index < 0 ) index = 0;
	
	pos.textIndex = index;
	
	return [pos autorelease];
}

-(void)setAffinity:(UITextStorageDirection)newAffinity
{
	affinity_ = newAffinity;
	if( index_ == 0 ) affinity_ = UITextStorageDirectionForward;
//	if( index_ == attr.length-2 ) affinity_ = UITextStorageDirectionBackward;

}

@end

@implementation IndexedRange

@synthesize attributedString = attributedString_;
@synthesize range = range_;

+(IndexedRange*)rangeWithNSRange:(NSRange)nsrange inAttributedString:(NSAttributedString*)attr
{
	if( nsrange.location == NSNotFound )
		return nil;
	
	IndexedRange *range = [[IndexedRange alloc] init];
	range.range = nsrange;
	range.attributedString = attr;
	return [range autorelease];
}

-(UITextPosition*)start
{
	return [IndexedPosition positionWithIndex:self.range.location inAttributedString:self.attributedString];	
}

-(UITextPosition*)end
{
	return [IndexedPosition positionWithIndex:self.range.location+self.range.length inAttributedString:self.attributedString];	
}

-(BOOL)isEmpty
{
	if( self.range.length == 0 ) return YES;

	IndexedPosition* start = (IndexedPosition*)[self start];
	IndexedPosition* end = (IndexedPosition*)[self end];
	
	return (end.textIndex == start.textIndex);
}

-(void)dealloc
{
  self.attributedString = nil;
  [super dealloc];
}
@end

#if TARGET_OS_IPHONE
#else
@implementation UITextRange
@end

@implementation UITextPosition
@end

#endif
