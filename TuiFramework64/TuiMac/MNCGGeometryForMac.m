//
//  MNCGGeometryForMac.c
//  FastFinga3
//
//  Created by Masatoshi Nishikata on 27/04/11.
//  Copyright 2011 NISHIKATA Masatoshi. All rights reserved.
//

#include "MNCGGeometryForMac.h"

CGRect CGRectFromString(NSString* string)
{
	NSRect rect = NSRectFromString(string);
	CGRect cgrect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
	return cgrect;
}

NSString* NSStringFromCGPoint(CGPoint point)
{
	NSPoint nspoint = NSMakePoint(point.x, point.y);
	return NSStringFromPoint(nspoint);
}

CGPoint CGPointFromString(NSString* string)
{
	NSPoint nspoint = NSPointFromString(string);
	CGPoint point = CGPointMake(nspoint.x, nspoint.y);
	
	return point;
}

NSString* NSStringFromCGRect(CGRect rect)
{
	NSRect nsrect = NSMakeRect(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
	
	return NSStringFromRect(nsrect);

}

CGSize CGSizeFromString(NSString* string)
{
	NSSize nssize = NSSizeFromString(string);
	CGSize size = CGSizeMake(nssize.width, nssize.height);
	
	return size;
}


NSString* NSStringFromCGSize(CGSize size)
{
	NSSize nssize = NSMakeSize(size.width, size.height);
	
	return NSStringFromSize(nssize);
	
}
