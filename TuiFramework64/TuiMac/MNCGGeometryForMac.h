//
//  MNCGGeometryForMac.h
//  FastFinga3
//
//  Created by Masatoshi Nishikata on 27/04/11.
//  Copyright 2011 NISHIKATA Masatoshi. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <AppKit/AppKit.h>

extern CGRect CGRectFromString(NSString* string);
extern NSString* NSStringFromCGPoint(CGPoint point);
extern CGPoint CGPointFromString(NSString* string);
extern NSString* NSStringFromCGRect(CGRect rect);
extern CGSize CGSizeFromString(NSString* string);
extern NSString* NSStringFromCGSize(CGSize size);
