//
//  KeyboardCandidateBar.h
//  JapaneseKeyboardKit
//
//  Created by kishikawa katsumi on 2014/09/28.
//  Copyright (c) 2014 kishikawa katsumi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>

@class InputCandidate;

@protocol KeyboardCandidateBarDelegate;

@interface KeyboardCandidateBar : UIView
{
	CTFontRef cellFont_;
}
- (id)initWithFrame:(CGRect)frame fullscreen:(BOOL)fullscreen;
-(void)enable;
-(void)clearSelection;

@property (nonatomic, weak) id<KeyboardCandidateBarDelegate> delegate;
@property (nonatomic) NSArray * _Nullable candidates;
@property (nonatomic) BOOL fullscreen;
@property (nonatomic) BOOL darkMode;
@property (nonatomic) BOOL transparent;

@property (nonatomic, readonly) InputCandidate *selectedCandidate;

- (BOOL)acceptCurrentCandidate;
- (void)selectPreviousCandidate;
- (void)selectNextCandidate;

@end

@protocol KeyboardCandidateBarDelegate <NSObject>

- (void)candidateBar:(KeyboardCandidateBar *)candidateBar didAcceptCandidate:(InputCandidate *)segment;

@optional
- (void)candidateBar:(KeyboardCandidateBar *)candidateBar showsAll:(NSArray *)candidates;
- (void)candidateBarDidClose:(KeyboardCandidateBar *)candidateBar;

@end


@interface  NSString (EventTableCellDrawing)
@end


