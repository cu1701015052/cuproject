//
//  KeyboardCandidateBarCell.h
//  JapaneseKeyboardKit
//
//  Created by kishikawa katsumi on 2014/09/13.
//  Copyright (c) 2014 kishikawa katsumi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>

@interface CandidateLabel : UIView
@property (nonatomic) NSString *text;
@property (nonatomic) UIColor *textColor;
+(id)font;
+(void)setFont:(CTFontRef)newFont;

@end



@interface KeyboardCandidateBarCell : UICollectionViewCell

-(void)setDarkMode:(BOOL)flag;
+(id)font;
@property (nonatomic) NSString *text;
@property (nonatomic) CandidateLabel *textLabel;

@property (nonatomic) BOOL showsTopSeparator;
@property (nonatomic) BOOL showsBottomSeparator;

@end
