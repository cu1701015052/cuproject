//
//  UIImage+Rotation.h
//  Tanzaku
//
//  Created by Masatoshi Nishikata on 09/05/09.
//  Copyright 2009 Masatoshi Nishikata. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage (KeyboardButton)
-(UIImage*)rotatedImage:(UIImageOrientation)orient;
-(UIImage*)scaleDownTo:(CGFloat)maxResolution;
+(UIImage*)adjustImageOrientation:(UIImage*)image withScale:(CGFloat)scale;
+ (UIImage *)blurryImage:(UIImage *)image withBlurLevel:(CGFloat)blur ;
+ (UIImage *)colorizedImage:(UIImage *)image withTintColor:(UIColor*)tintColor alpha:(CGFloat)alpha glow:(BOOL)outerGlow;
+ (UIImage *)colorizedImage:(UIImage *)image withTintColor:(UIColor*)tintColor alpha:(CGFloat)alpha outerGlow:(UIColor*)outerGlowColor;
+(UIImage*)colorizedImageWithName:(NSString*)name iPadSuffix:(NSString*)iPadSuffixIfAvailable withTintColor:(UIColor*)tintColor;

@end
