//
//  InputManager.h
//  JapaneseKeyboardKit
//
//  Created by kishikawa katsumi on 2014/09/28.
//  Copyright (c) 2014 kishikawa katsumi. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol InputManagerDelegate;
@class InputCandidate;

@interface InputManager : NSObject
{
//	const mozc::testing::MockDataManager mock_data_manager_;
//	const mozc::POSMatcher *pos_matcher_;
//	scoped_ptr<const mozc::PosGroup> pos_group_;
////	DISALLOW_COPY_AND_ASSIGN(UserSegmentHistoryRewriterTest);

}

- (void)requestCandidatesForInput:(NSString *)input;
-(void)releaseResources;
+(instancetype)sharedInputManager;

@property (nonatomic, readonly) NSArray *candidates;
@property (nonatomic, weak) id delegate;

@property (atomic, readwrite) NSTimeInterval signature;

@end

@protocol InputManagerDelegate <NSObject>

- (void)inputManager:(InputManager *)inputManager didCompleteWithCandidates:(NSArray <InputCandidate*> *)candidates;
- (void)inputManager:(InputManager *)inputManager didFailWithError:(NSError *)error;

@end
