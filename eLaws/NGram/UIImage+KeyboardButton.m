//
//  UIImage+Rotation.m
//  Tanzaku
//
//  Created by Masatoshi Nishikata on 09/05/09.
//  Copyright 2009 Masatoshi Nishikata. All rights reserved.
//

#import "UIImage+KeyboardButton.h"


@implementation UIImage (KeyboardButton)


-(UIImage*)rotatedImage:(UIImageOrientation)orient
{
	
	CGImageRef imgRef = self.CGImage;
	
	CGFloat width = CGImageGetWidth(imgRef);
	CGFloat height = CGImageGetHeight(imgRef);
	
	CGAffineTransform transform;
	CGRect bounds = CGRectMake(0, 0, width, height);
	
	
	
	CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
	CGFloat boundHeight;
//	UIImageOrientation orient = self.imageOrientation;
	switch(orient) {
			
		case UIImageOrientationUp: //EXIF = 1
			transform = CGAffineTransformIdentity;
			break;
			
		case UIImageOrientationUpMirrored: //EXIF = 2
			transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			break;
			
		case UIImageOrientationDown: //EXIF = 3
			transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
			transform = CGAffineTransformRotate(transform, M_PI);
			break;
			
		case UIImageOrientationDownMirrored: //EXIF = 4
			transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
			transform = CGAffineTransformScale(transform, 1.0, -1.0);
			break;
			
		case UIImageOrientationLeftMirrored: //EXIF = 5
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
			break;
			
		case UIImageOrientationLeft: //EXIF = 6
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
			break;
			
		case UIImageOrientationRightMirrored: //EXIF = 7
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeScale(-1.0, 1.0);
			transform = CGAffineTransformRotate(transform, M_PI / 2.0);
			break;
			
		case UIImageOrientationRight: //EXIF = 8
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
			transform = CGAffineTransformRotate(transform, M_PI / 2.0);
			break;
			
		default:
			[NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
			
	}
	
	
	UIGraphicsBeginImageContext(bounds.size);
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
		CGContextScaleCTM(context, -1, 1);
		CGContextTranslateCTM(context, -height, 0);
	}
	else {
		CGContextScaleCTM(context, 1, -1);
		CGContextTranslateCTM(context, 0, -height);
	}
	
	CGContextConcatCTM(context, transform);
	
	CGContextDrawImage(context, CGRectMake(0, 0, width, height), imgRef);
	UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return imageCopy;
}


-(UIImage*)scaleDownTo:(CGFloat)maxResolution
{
	
	int kMaxResolution = maxResolution; // Or whatever
	
	CGImageRef imgRef = self.CGImage;
	
	CGFloat width = CGImageGetWidth(imgRef);
	CGFloat height = CGImageGetHeight(imgRef);
	
	
	CGAffineTransform transform = CGAffineTransformIdentity;
	CGRect bounds = CGRectMake(0, 0, width, height);
	if (width > kMaxResolution || height > kMaxResolution) {
		CGFloat ratio = width/height;
		if (ratio > 1) {
			bounds.size.width = kMaxResolution;
			bounds.size.height = bounds.size.width / ratio;
		}
		else {
			bounds.size.height = kMaxResolution;
			bounds.size.width = bounds.size.height * ratio;
		}
	}
	
	CGFloat scaleRatio = bounds.size.width / width;
	CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
	CGFloat boundHeight;
	UIImageOrientation orient = self.imageOrientation;
	switch(orient) {
			
		case UIImageOrientationUp: //EXIF = 1
			transform = CGAffineTransformIdentity;
			break;
			
		case UIImageOrientationUpMirrored: //EXIF = 2
			transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			break;
			
		case UIImageOrientationDown: //EXIF = 3
			transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
			transform = CGAffineTransformRotate(transform, M_PI);
			break;
			
		case UIImageOrientationDownMirrored: //EXIF = 4
			transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
			transform = CGAffineTransformScale(transform, 1.0, -1.0);
			break;
			
		case UIImageOrientationLeftMirrored: //EXIF = 5
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
			break;
			
		case UIImageOrientationLeft: //EXIF = 6
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
			break;
			
		case UIImageOrientationRightMirrored: //EXIF = 7
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeScale(-1.0, 1.0);
			transform = CGAffineTransformRotate(transform, M_PI / 2.0);
			break;
			
		case UIImageOrientationRight: //EXIF = 8
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
			transform = CGAffineTransformRotate(transform, M_PI / 2.0);
			break;
			
		default:
			break;
			//[NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
			
	}
	
	UIGraphicsBeginImageContext(bounds.size);
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
		CGContextScaleCTM(context, -scaleRatio, scaleRatio);
		CGContextTranslateCTM(context, -height, 0);
	}
	else {
		CGContextScaleCTM(context, scaleRatio, -scaleRatio);
		CGContextTranslateCTM(context, 0, -height);
	}
	
	CGContextConcatCTM(context, transform);
	
	CGContextDrawImage(context, CGRectMake(0, 0, width, height), imgRef);
	UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return imageCopy;
}

+(UIImage*)adjustImageOrientation:(UIImage*)image withScale:(CGFloat)scale
{
	UIImageOrientation orient = image.imageOrientation;

	// orient = 0, 1 yoko
	// orient = 2, 3 tate
	
	if( scale != 1.0 )
	switch(orient) {
			
		case 0: //EXIF = 1
		case 1:
		default:
		{
			CGSize originalSize = [image size];

			CGSize newSize = CGSizeMake( originalSize.width / scale, originalSize.height / scale );
			CGPoint newOrigin = CGPointMake( (originalSize.width - newSize.width ) /2 , (originalSize.height - newSize.height ) /2 );
			
			CGRect zoomedRect = CGRectMake(newOrigin.x, newOrigin.y, newSize.width, newSize.height);
			
			CGImageRef zoomedCGImage = CGImageCreateWithImageInRect( image.CGImage, zoomedRect);
			//if( !zoomedCGImage ) return image;

			

			//NSLog(@"scale %f", scale);
			//NSLog(@"x y %@", NSStringFromCGSize( [image size] ));
			//NSLog(@"crop %@",NSStringFromCGRect(zoomedRect));
			image = [[UIImage alloc] initWithCGImage: zoomedCGImage];
			CFRelease(zoomedCGImage);
		}
			
			break;
			
		case 2: //EXIF = 3
		case 3:
		{
			
			CGSize originalSize = [image size];
			
			
			
			CGSize newSize = CGSizeMake( originalSize.width / scale, originalSize.height / scale );
			

			
			CGPoint newOrigin = CGPointMake( (originalSize.width - newSize.width ) /2 , (originalSize.height - newSize.height ) /2 );
			
			CGRect zoomedRect = CGRectMake(newOrigin.y, newOrigin.x, newSize.height, newSize.width);
			
			CGImageRef zoomedCGImage = CGImageCreateWithImageInRect( image.CGImage, zoomedRect);
			//if( !zoomedCGImage ) return image;


			
			//NSLog(@"scale %f", scale);
			//NSLog(@"x y %@", NSStringFromCGSize( [image size] ));
			//NSLog(@"crop %@",NSStringFromCGRect(zoomedRect));
			image = [[UIImage alloc] initWithCGImage: zoomedCGImage];
			
			if( zoomedCGImage) CFRelease(zoomedCGImage);
			
			// image needs rotation
			
			
		}
			break;
			//[NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
			
	}
	
	
	
	switch(orient) {
			
		case 0: //EXIF = 1
			break;
		case 1:
			image = [image rotatedImage: UIImageOrientationDown];
			break;
			
			
		case 2:
			image = [image rotatedImage: UIImageOrientationLeft];
			break;
		case 3:
			image = [image rotatedImage: UIImageOrientationRight ];
			break;
		default:
		
			break;
			
	}
	
	

	
	return image;
	
}

+ (UIImage *)colorizedImage:(UIImage *)image withTintColor:(UIColor*)tintColor alpha:(CGFloat)alpha glow:(BOOL)outerGlow
{
	UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
	
	CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);

	CGContextRef ctx = UIGraphicsGetCurrentContext();
	
	CGAffineTransform t = CGAffineTransformIdentity;
	t = CGAffineTransformScale(t, 1, -1);
	t = CGAffineTransformTranslate(t, 0, -rect.size.height);
	CGContextConcatCTM(ctx, t);
	
	CGContextSetAlpha(ctx, alpha);
	CGContextSetFillColorWithColor(ctx, tintColor.CGColor);
	
	
	UIRectFill( rect );
	
	
	
	CGContextSetAlpha(ctx, 1.0);
	CGContextSetBlendMode(ctx, kCGBlendModeDestinationIn);
	CGContextDrawImage(ctx, rect, image.CGImage);
	
	
	UIImage* theImage = UIGraphicsGetImageFromCurrentImageContext();
	
	if( outerGlow )
	{
		CGContextSetShadowWithColor(ctx, CGSizeMake(0, 0), 3.0, tintColor.CGColor);
		CGContextSetBlendMode(ctx, kCGBlendModeNormal);
		CGContextSetAlpha(ctx, 0.5);
		CGContextDrawImage(ctx, rect, theImage.CGImage);
		
		theImage = UIGraphicsGetImageFromCurrentImageContext();
	}
	
	UIGraphicsEndImageContext();
	
	
	
	return theImage;
}

+(UIImage*)colorizedImageWithName:(NSString*)name iPadSuffix:(NSString*)iPadSuffixIfAvailable withTintColor:(UIColor*)tintColor
{
#define IS_RUNNING_ON_IPAD ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
	
	Class KeyboardButtonClass = NSClassFromString(@"InputManager");
	NSBundle* bundle = [NSBundle bundleForClass:KeyboardButtonClass];
	
	
	UIImage *image = nil;
	
	if( IS_RUNNING_ON_IPAD )
	{
		UIImage *ipadimage = [UIImage imageNamed:[name stringByAppendingString:iPadSuffixIfAvailable]
												  inBundle:bundle
					  compatibleWithTraitCollection:nil];
		
		if( ipadimage ) image = ipadimage;
	}
	
	if( image == nil )
		image = [UIImage imageNamed:name
								 inBundle:bundle
	 compatibleWithTraitCollection:nil];
	
	
	return [UIImage colorizedImage:image withTintColor:tintColor alpha:1 glow:NO];
	
}


+ (UIImage *)colorizedImage:(UIImage *)image withTintColor:(UIColor*)tintColor alpha:(CGFloat)alpha outerGlow:(UIColor*)outerGlowColor
{
	UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
	
	CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
	
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	
	CGAffineTransform t = CGAffineTransformIdentity;
	t = CGAffineTransformScale(t, 1, -1);
	t = CGAffineTransformTranslate(t, 0, -rect.size.height);
	CGContextConcatCTM(ctx, t);
	
	CGContextSetAlpha(ctx, alpha);
	CGContextSetFillColorWithColor(ctx, tintColor.CGColor);
	
	
	UIRectFill( rect );
	
	
	
	CGContextSetAlpha(ctx, 1.0);
	CGContextSetBlendMode(ctx, kCGBlendModeDestinationIn);
	CGContextDrawImage(ctx, rect, image.CGImage);
	
	
	UIImage* theImage = UIGraphicsGetImageFromCurrentImageContext();
	
	if( outerGlowColor )
	{
		CGContextSetShadowWithColor(ctx, CGSizeMake(0, 0), 2.0, outerGlowColor.CGColor);
		CGContextSetBlendMode(ctx, kCGBlendModeNormal);
		CGContextSetAlpha(ctx, 0.5);
		CGContextDrawImage(ctx, rect, theImage.CGImage);
		
		CGContextSetShadowWithColor(ctx, CGSizeMake(0, 0), 0.0, [UIColor clearColor].CGColor);
		CGContextSetBlendMode(ctx, kCGBlendModeNormal);
		CGContextSetAlpha(ctx, 1);
		CGContextDrawImage(ctx, rect, theImage.CGImage);

		
		theImage = UIGraphicsGetImageFromCurrentImageContext();
	}
	
	UIGraphicsEndImageContext();
	
	
	
	return theImage;
}


@end
