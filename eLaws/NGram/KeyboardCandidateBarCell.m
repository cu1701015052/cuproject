//
//  KeyboardCandidateBarCell.m
//  JapaneseKeyboardKit
//
//  Created by kishikawa katsumi on 2014/09/13.
//  Copyright (c) 2014 kishikawa katsumi. All rights reserved.
//

#import "KeyboardCandidateBarCell.h"
#import <CoreText/CoreText.h>

static CTFontRef candidateLabelFont_ = nil;
CGFloat gContentScaleFactor = 1.75;

@implementation CandidateLabel

+(id)font
{
	if( candidateLabelFont_ == nil ) {
		candidateLabelFont_ = CTFontCreateWithName(CFSTR("HelveticaNeue"), 18, nil);
	}
	
	return (__bridge id)(candidateLabelFont_);
}

+(void)setFont:(CTFontRef)newFont
{
  if( candidateLabelFont_ != nil )
    CFRelease(candidateLabelFont_);
  
  candidateLabelFont_ = newFont;
  CFRetain(newFont);
}

-(void)setNeedsLayout
{
  [super setNeedsLayout];
  self.contentScaleFactor = gContentScaleFactor;
}

-(instancetype)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    self.contentScaleFactor = 0.1;
  }
  
  return self;
}

-(void)setContentScaleFactor:(CGFloat)contentScaleFactor
{
  [super setContentScaleFactor:gContentScaleFactor];
}

-(void)drawRect:(CGRect)rect
{
	if( self.textColor == nil )
	{
		self.textColor = [UIColor blackColor];
	}
	
	NSDictionary* attributes = @{ (NSString*)kCTFontAttributeName : [CandidateLabel font],
											NSForegroundColorAttributeName : self.textColor};
  
  CGSize size;
  
  CFAttributedStringRef attributedString = CFAttributedStringCreate( kCFAllocatorDefault , (CFStringRef)self.text, (CFDictionaryRef)attributes);
  
  CTFramesetterRef frameSetter = CTFramesetterCreateWithAttributedString(attributedString);
  CGFloat widthConstraint = CGFLOAT_MAX; // Your width constraint, using 500 as an example
  size = CTFramesetterSuggestFrameSizeWithConstraints( frameSetter, CFRangeMake(0, self.text.length), nil, CGSizeMake(widthConstraint, CGFLOAT_MAX), nil );
  
  
  CGPoint origin = CGPointMake((self.bounds.size.width - size.width)/2, (self.bounds.size.height - size.height)/2);
  
  CGMutablePathRef path = CGPathCreateMutable();
  CGRect drawRect; drawRect.origin = origin; drawRect.size = size;
  CGPathAddRect(path, nil, drawRect);
  
  CTFrameRef frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, self.text.length), path, nil);
  
  CGContextRef context = UIGraphicsGetCurrentContext();
  CGContextSetTextMatrix(context, CGAffineTransformIdentity);
  
  CGContextTranslateCTM(context, 0, rect.size.height);
  
  CGContextScaleCTM(context, 1.0, -1.0);
  
  CTFrameDraw(frame, context);
  
  if( path != nil ) CFRelease(path);
  if( frame != nil ) CFRelease(frame);
  if( frameSetter != nil ) CFRelease(frameSetter);
  if( attributedString != nil ) CFRelease(attributedString);
}

@end


@interface KeyboardCandidateBarCell ()

//@property (nonatomic) UIView *leftSeparator;
//@property (nonatomic) UIView *rightSeparator;

@end

@implementation KeyboardCandidateBarCell

+(id)font
{
	return [CandidateLabel font];
}


- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {

		CGRect bounds = self.contentView.bounds;
		
		CTFontRef font = (__bridge CTFontRef)([KeyboardCandidateBarCell font]);
		
		CGFloat hh = CTFontGetAscent(font) + CTFontGetLeading(font) + CTFontGetDescent(font);
		
		CandidateLabel *label = [[CandidateLabel alloc] initWithFrame:CGRectMake(0, (bounds.size.height - hh)/2, bounds.size.width, hh)];
		label.backgroundColor = [UIColor clearColor];
		label.opaque = NO;
		label.autoresizingMask = UIViewAutoresizingFlexibleWidth;
		
		[self.contentView addSubview:label];
		
		self.textLabel = label;
    self.contentScaleFactor = 0.1;
    
	}
	
	return self;
}

//- (void)awakeFromNib
//{
//    CGFloat scale = [[UIScreen mainScreen] scale];
//    self.leftSeparator = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 1.0 / scale, CGRectGetHeight(self.bounds))];
//    self.leftSeparator.backgroundColor = [UIColor colorWithRed:0.773 green:0.780 blue:0.820 alpha:1.000];
//    [self.contentView addSubview:self.leftSeparator];
//
//    self.rightSeparator = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.bounds) - 1.0 / scale, 0.0, 1.0 / scale, CGRectGetHeight(self.bounds))];
//    self.rightSeparator.backgroundColor = [UIColor colorWithRed:0.773 green:0.780 blue:0.820 alpha:1.000];
//    [self.contentView addSubview:self.rightSeparator];
//}

- (void)layoutSubviews
{
  [super layoutSubviews];
  self.textLabel.text = self.text;
  [self.textLabel setNeedsDisplay];
  
  //    CGFloat scale = [[UIScreen mainScreen] scale];
  //    self.leftSeparator.frame = CGRectMake(0.0, 0.0, 1.0 / scale, CGRectGetHeight(self.bounds));
  //    self.rightSeparator.frame = CGRectMake(CGRectGetWidth(self.bounds) - 1.0 / scale, 0.0f, 1.0 / scale, CGRectGetHeight(self.bounds));
  //
  //    self.leftSeparator.hidden = !self.showsTopSeparator;
  //    self.rightSeparator.hidden = !self.showsBottomSeparator;
}

- (void)setText:(NSString *)text
{
  _text = text;
  [self setNeedsLayout];
}

-(void)setDarkMode:(BOOL)flag
{
  if( flag )
  {
		self.textLabel.textColor = [UIColor whiteColor];
	}else {
		self.textLabel.textColor = [UIColor blackColor];
	}
}

@end
