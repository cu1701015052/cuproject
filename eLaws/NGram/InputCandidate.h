//
//  InputCandidate.h
//  JapaneseKeyboardKit
//
//  Created by kishikawa katsumi on 2014/09/28.
//  Copyright (c) 2014 kishikawa katsumi. All rights reserved.
//

#import <Foundation/Foundation.h>




@interface InputCandidate : NSObject

@property (nonatomic) NSString * _Nonnull input;
@property (nonatomic) NSString * _Nonnull candidate;

- (instancetype _Nonnull)initWithInput:(NSString * _Nonnull)input candidate:(NSString * _Nonnull)candidate;

@end

