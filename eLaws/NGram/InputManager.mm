//
//  InputManager.m
//  JapaneseKeyboardKit
//
//  Created by kishikawa katsumi on 2014/09/28.
//  Copyright (c) 2014 kishikawa katsumi. All rights reserved.
//

// CF  rewriter_test

#import "InputManager.h"
#import "InputCandidate.h"
#import "RangeObject.h"

#include <string>

using namespace std;

#if TARGET_IPHONE_SIMULATOR
#else
#include "composer/composer.h"
#include "composer/table.h"
#include "converter/conversion_request.h"
#include "converter/converter_interface.h"
#include "converter/segments.h"
#include "prediction/predictor_interface.h"
#include "engine/engine_factory.h"
#include "engine/engine_interface.h"
#include "data_manager/data_manager_interface.h"

#include "base/clock_mock.h"
#include "base/file_util.h"
#include "base/logging.h"
#include "base/number_util.h"
#include "base/system_util.h"
#include "base/util.h"
#include "config/character_form_manager.h"
#include "config/config.pb.h"
#include "config/config_handler.h"
#include "converter/conversion_request.h"
#include "converter/segments.h"
#include "data_manager/testing/mock_data_manager.h"
#include "dictionary/pos_group.h"
#include "rewriter/number_rewriter.h"
#include "rewriter/variants_rewriter.h"
#include "rewriter/rewriter_interface.h"
#include "converter/segments.h"

#include "rewriter/user_segment_history_rewriter.h"
#endif

#define IN_RANGE(x, low, high)  ((low<=(x))&&((x)<=high))
#define IS_JA_HIRAGANA(x)   IN_RANGE(x, 0x3040, 0x309F)
#define IS_JA_KATAKANA(x)   ((IN_RANGE(x, 0x30A0, 0x30FF)&&((x)!=0x30FB))||IN_RANGE(x, 0xFF66, 0xFF9F))

#if TARGET_IPHONE_SIMULATOR
#else

void MakeSegmentsForSuggestion(const string key, mozc::Segments *segments) {
  segments->Clear();
  segments->set_max_prediction_candidates_size(10);
  segments->set_request_type(mozc::Segments::SUGGESTION);
  mozc::Segment *seg = segments->add_segment();
  seg->set_key(key);
  seg->set_segment_type(mozc::Segment::FREE);
  
}

void MakeSegmentsForPrediction(const string key, mozc::Segments *segments) {
  segments->Clear();
  segments->set_max_prediction_candidates_size(10);
  segments->set_request_type(mozc::Segments::PREDICTION);
  
  mozc::Segment *seg = segments->add_segment();
  seg->set_key(key);
  seg->set_segment_type(mozc::Segment::FREE);
}
#endif

static InputManager * sharedInputManager_ = nil;

@interface InputManager ()

@property (nonatomic, readwrite) NSArray *candidates;
@end

#if TARGET_IPHONE_SIMULATOR

@implementation InputManager

+(instancetype)sharedInputManager {
  if( sharedInputManager_ == nil ) {
    sharedInputManager_ = [[InputManager alloc] init];
  }
  
  return sharedInputManager_;
}

- (void)requestCandidatesForInput:(NSString *)input
{
  if( [self.delegate respondsToSelector:@selector(inputManager: didCompleteWithCandidates: )] )
  {
    [self.delegate inputManager:self didCompleteWithCandidates:self.candidates];
  }
  
}

-(void)releaseResources
{
}


@end

#else

@implementation InputManager {
  
  scoped_ptr<mozc::EngineInterface> engine;
  mozc::ConverterInterface *converter;
  mozc::PredictorInterface *predictor;
  
  scoped_ptr<const mozc::PosGroup> pos_group_;
  mozc::POSMatcher *pos_matcher_;
  const mozc::UserDataManagerInterface * data_manager_;
  mozc::UserSegmentHistoryRewriter * rewriter;
}

+(instancetype)sharedInputManager {
  if( sharedInputManager_ == nil ) {
    sharedInputManager_ = [[InputManager alloc] init];
  }
  
  return sharedInputManager_;
}

- (id)init
{
  self = [super init];
  if (self) {
    //[self createFile];
    const string tmpdir = [NSTemporaryDirectory() cStringUsingEncoding:NSUTF8StringEncoding];
    mozc::SystemUtil::SetUserProfileDirectory(tmpdir);
    
    engine.reset(mozc::EngineFactory::Create());
    converter = engine->GetConverter();
    CHECK(converter);
    predictor = engine->GetPredictor();
    CHECK(predictor);
    predictor->Reload();
  }
  
  return self;
}

-(void)releaseResources
{
  engine.release();
  NSLog(@"*** release engine ***");
}

-(void)setup
{
  const string tmpdir = [NSTemporaryDirectory() cStringUsingEncoding:NSUTF8StringEncoding];
  mozc::SystemUtil::SetUserProfileDirectory(tmpdir);
  
  mozc::config::Config config;
  mozc::config::ConfigHandler::GetDefaultConfig(&config);
  
  int cacheSize = config.GetCachedSize();
  NSLog(@"cacheSize %d",cacheSize);
}

-(void)test
{
  
}

-(void)createFile
{
  const string tmpdir = [NSTemporaryDirectory() cStringUsingEncoding:NSUTF8StringEncoding];
  
  //	scoped_ptr<UserSegmentHistoryRewriter> rewriter(
  //																	CreateUserSegmentHistoryRewriter());
  const string history_file = mozc::FileUtil::JoinPath(tmpdir,
                                                       "/segment.db");
  assert(mozc::FileUtil::FileExists(history_file));
  
}


- (void)requestCandidatesForInput:(NSString *)input
{
  
  NSTimeInterval signature = self.signature;
  
  input = [input copy];
  
  mozc::commands::Request request;
  mozc::Segments segments;
  
  
  //SetLearningLevel(mozc::config::Config::DEFAULT_HISTORY);
  
  NSMutableOrderedSet *candidates = [[NSMutableOrderedSet alloc] init];
  
  
  mozc::composer::Table table;
  mozc::composer::Composer composer(&table, &request);
  composer.InsertCharacterPreedit(input.UTF8String);
  mozc::ConversionRequest conversion_request(&composer, &request);
  
  
  //// Predict
  //	composer.DeleteRange(0, composer.GetLength());
  
  
  converter->StartConversionForRequest(conversion_request, &segments);
  
  
  NSUInteger charIndex = 0;
  
  
  for (int i = 0; i < segments.segments_size() && input.length > 0 ; ++i) {
    
    if( signature != self.signature ) break;
    
    const mozc::Segment &segment = segments.segment(i);
    
    
    NSString* segmentKey = [NSString stringWithUTF8String:segment.key().c_str()];
    
    
    NSRange range = NSMakeRange(charIndex, segmentKey.length);
    charIndex = NSMaxRange(range);
    
    
    RangeObject *mySegment = nil;
    
    
    for (int j = 0; j < segment.candidates_size(); ++j) {
      const mozc::Segment::Candidate &cand = segment.candidate(j);
      
      
      NSString* kanji = [NSString stringWithUTF8String:cand.value.c_str()];
      NSString* candidateInput = [NSString stringWithUTF8String:cand.key.c_str()];
      
      if( segmentKey.length == input.length )
      {
        InputCandidate* candidate = [[InputCandidate alloc] initWithInput:candidateInput candidate:kanji];
        [candidates addObject:candidate];
      }
    }
  }
  
  if( signature != self.signature ) return;
  
  
  
  if( input.length >= 1 )
  {
    converter->StartPredictionForRequest(conversion_request, &segments);
    
    
    segments.set_user_history_enabled(true);
    
    /*
     segments.set_max_history_segments_size(3);
     segments.mutable_segment(0)->move_candidate(segments.segment(0).candidates_size() - 1, 0);
     segments.mutable_segment(0)->mutable_candidate(0)->attributes
     |= mozc::Segment::Candidate::RERANKED;
     segments.mutable_segment(0)->set_segment_type(mozc::Segment::FIXED_VALUE);
     predictor->Finish(&segments);
     BOOL flag = predictor->WaitForSyncerForTest();
     NSLog(@"Sync %d", flag);
     */
    
    
    for (int i = 0; i < segments.segments_size(); ++i) {
      
      if( signature != self.signature ) break;
      
      const mozc::Segment &segment = segments.segment(i);
      
      for (int j = 0; j < segment.candidates_size(); ++j) {
        const mozc::Segment::Candidate &cand = segment.candidate(j);
        
        InputCandidate* candidate = [[InputCandidate alloc] initWithInput:[NSString stringWithUTF8String:segment.key().c_str()] candidate:[NSString stringWithUTF8String:cand.value.c_str()]];
        
        [candidates addObject:candidate];
        
      }
    }
  }

  
  if( signature != self.signature ) return;
  
  if( candidates.count == 0 )
  {
    InputCandidate* candidate = [[InputCandidate alloc] initWithInput:input candidate:input];
    [candidates addObject:candidate];
  }
  
  
  self.candidates = candidates.array;
  
  dispatch_async(dispatch_get_main_queue(), ^{
    
    if( signature == self.signature )
    {
      if( [self.delegate respondsToSelector:@selector(inputManager: didCompleteWithCandidates: )] )
      {
        [self.delegate inputManager:self didCompleteWithCandidates:self.candidates];
      }
      
      
      else if( [self.delegate respondsToSelector:@selector(inputManager: didCompleteWithCandidates:)] )
      {
        [self.delegate inputManager:self didCompleteWithCandidates:self.candidates];
      }
      
    }
  });
}

-(void)testMoveToFirstCandidate:(mozc::Segments)segments atIndex:(int) index
{
  segments.mutable_segment(0)->move_candidate(index, 0);
  
}

@end

#endif
