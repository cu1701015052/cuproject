//
//  FlatButton.m
//  FlatButtons
//
//  Created by JASON EVERETT on 6/22/13.
//  Copyright (c) 2013 JASON EVERETT. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.
//

#import "RoundedFlatButton.h"
#import "Tui.h"
#import <QuartzCore/QuartzCore.h>
#import "MNAttributedTextLabel.h"


@interface RoundedFlatButton()
-(void)wasPressed;
-(void)endedPress;
-(void)makeFlat:(RoundedFlatButton*)button withBackgroundColor:(UIColor*)backgroundColor;

@end

@implementation RoundedFlatButton

- (id)initWithFrame:(CGRect)frame withBackgroundColor:(UIColor*)backgroundColor
{
    self = [super initWithFrame:frame];
    if (self) {
        [self makeFlat:self withBackgroundColor:backgroundColor];
    }
    return self;
}


-(id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self makeFlat:self withBackgroundColor: RGBA(0,201,234,1)];
	}
	return self;
}

-(void)makeFlat:(RoundedFlatButton*)button withBackgroundColor:(UIColor*)backgroundColor
{
	//save our color so we can alter it upon a touch event
	
	[self setButtonTintColor:backgroundColor];
	
	label_ = [[MNAttributedTextLabel alloc] initWithFrame:self.bounds];
	label_.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	label_.alignCenter = YES;
	label_.userInteractionEnabled = NO;
//	label_.inverted = YES;
	[self addSubview:label_];
}

//When button is touched, grab our existing color and make it 20% darker (or lighter if its black)
//We will return it to its original state when the touch is lifted and touchesEnded:withEvent: is called

-(CGRect)titleRectForContentRect:(CGRect)contentRect
{
	return CGRectZero;
}

-(void)drawRect:(CGRect)rect
{
	if( !label_.attributedString ) return;
	
	CGFloat topPad = 8;
	
	if( _small ) topPad = 6;
	
	[self.buttonTintColor set];
	CGRect frameRect = label_.textRect;

	

	frameRect = CGRectInset(frameRect, -5, -2);
	
	UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:frameRect cornerRadius:6];
	
	if( self.selected )
	{
		[path fill];
		
	}else
	{
		[path stroke];
	}

//	NSString* title = [self titleForState:UIControlStateNormal];
//	NSAttributedString* attr = [[NSAttributedString alloc] initWithString:title attributes:[self attributes]];
//	CGSize size = attr.size;
//	
//	
//	CGPoint textOrigin;
//	textOrigin.x = 10;
//	textOrigin.y = (rect.size.height - size.height ) /2 - (textHeight_+fontOriginY_ - ascent_  ) + (textHeight_-size.height);
//	
//
//	[attr drawAtPoint: CGPointMake(10, textOrigin.y) ];

}

-(void)sizeToFit
{
	NSString* title = [self titleForState:UIControlStateNormal];

	NSAttributedString* attr = [[NSAttributedString alloc] initWithString:title attributes:[self attributes]];
	
	CGSize size = attr.size;
	size.width += 20;
	
	CGRect frame = self.frame;
	
	frame.size.width = size.width;
	
	self.frame = frame;
	
	

	label_.attributedString = attr;
	
	[self setNeedsDisplay];
}

-(NSDictionary*)attributes
{
	CTFontRef font = [UIFont boldSystemFontOfSize:14].createCTFont;
	
	
	CGFloat pointSize;
	
	if( self.small )
	{
		pointSize = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption2].pointSize;
		
	}else
	{
		pointSize = [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote].pointSize;
	}
	
	
	CTFontDescriptorRef descriptor = CTFontCopyFontDescriptor(font);
	CTFontRef adjustedFont = CTFontCreateWithFontDescriptor(descriptor, pointSize, nil);
	
	NSDictionary* attributes = @{ MNCTFontAttributeName: (__bridge id)adjustedFont ,
											NSForegroundColorAttributeName : self.selected?[UIColor whiteColor]:self.buttonTintColor
											};
	
	CGRect fontRect = CTFontGetBoundingBox(adjustedFont);

	textHeight_ = fontRect.size.height;
	fontOriginY_ = fontRect.origin.y;
	descent_ = CTFontGetDescent(adjustedFont);
	ascent_ = CTFontGetAscent(adjustedFont);

	if( adjustedFont ) CFRelease(adjustedFont);
	if( descriptor ) CFRelease(descriptor);
	
	return attributes;
}

-(void)setHighlighted:(BOOL)highlighted
{
	[super setHighlighted:highlighted];
	[self sizeToFit];
}

-(void)setSmall:(BOOL)small
{
	_small = small;
	[self sizeToFit];

}

-(void)setSelected:(BOOL)selected
{
	[super setSelected:selected];
	
	[self sizeToFit];
}

-(void)wasPressed
{
	originalColor_ = self.backgroundColor;
	
    UIColor *newColor;
    CGFloat red = 0.0, green = 0.0, blue = 0.0, alpha = 0.0, white = 0.0;
    
    //Check if we're working with atleast iOS 5.0
    if([self.backgroundColor respondsToSelector:@selector(getRed:green:blue:alpha:)]) {
        [self.backgroundColor getRed:&red green:&green blue:&blue alpha:&alpha];
        [self.backgroundColor getWhite:&white alpha:&alpha];
        
        //test if we're working with a grayscale, black or RGB color
        if(!(red + green + blue) && white){
            //grayscale
            newColor = [UIColor colorWithWhite:white - 0.2 alpha:alpha];
        } else if(!(red + green + blue) && !white) {
            //black
            newColor = [UIColor colorWithWhite:white + 0.2 alpha:alpha];
        } else{
            //RGB
            newColor = [UIColor colorWithRed:red - 0.2 green:green - 0.2 blue:blue - 0.2 alpha:alpha];
        }
    } else if(CGColorGetNumberOfComponents(self.backgroundColor.CGColor) == 4) {
        //for earlier than ios 5
        const CGFloat *components = CGColorGetComponents(self.backgroundColor.CGColor);
        red = components[0];
        green = components[1];
        blue = components[2];
        alpha = components[3];
        
        newColor = [UIColor colorWithRed:red - 0.2 green:green - 0.2 blue:blue - 0.2 alpha:alpha];
    } else if(CGColorGetNumberOfComponents(self.backgroundColor.CGColor) == 2){
        //if we have a non-RGB color
        CGFloat hue;
        CGFloat saturation;
        CGFloat brightness;
        [self.backgroundColor getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
        
        newColor = [UIColor colorWithHue:hue - 0.2 saturation:saturation - 0.2 brightness:brightness - 0.2 alpha:alpha];
    }
    
    self.backgroundColor = newColor;
    
}

-(void)endedPress
{
    //Reset our button to its original color
    self.backgroundColor = originalColor_;
}

@end
