//
//  DownloadedLaw.swift
//  LawXML
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import CloudKit

class Organizable: Object {
  @objc dynamic var uuid: String = ""
  @objc dynamic var filepath: String = "/"
  @objc dynamic var order: Int = 5000
  @objc dynamic var title: String = ""
  
  override static func primaryKey() -> String? {
    return "uuid"
  }
  
  override class func indexedProperties() -> [String] {
    return [ ]
  }
  
  override class func ignoredProperties() -> [String] {
    return ["children", "parent", "level", "canEditTitle"]
  }
  
  var children: [Organizable] = []
  weak var parent: Organizable? = nil
  var level = 1
  
  var canEditTitle: Bool {
    return true
  }
  
  func generatePathString() -> String {
    let string = (parent?.generatePathString() ?? "") + "/" + self.uuid
    return string
  }
  
  func deleteRelatedFiles() {
  }
  
  //MARK:- Transient Organizing
  func addToChildrenIfNecessary(in allObjects: [Organizable] )  {
    
    for obj in allObjects {
      guard obj.parent == nil else { continue }
      guard obj !== self else { continue }
      guard obj.filepath.hasPrefix(self.filepath + self.uuid ) else { continue }
      
      let selfPathCount = self.filepath.components(separatedBy: "/").filter ({ $0.isEmpty == false }).count
      let otherPathCount = obj.filepath.components(separatedBy: "/").filter ({ $0.isEmpty == false }).count
      
      if otherPathCount == selfPathCount + 1 {
        obj.level = self.level + 1
        children.append(obj)
        children.sort { $0.order < $1.order }
        obj.parent = self
        _ = obj.addToChildrenIfNecessary(in: allObjects)
      }
    }
  }
  
  func chilrenArray() -> [Organizable] {
    var array: [Organizable] = [self]
    
    for child in children {
     array += child.chilrenArray()
    }
    
    return array
  }
  
  func titleToDisplay() -> String {
    if isInvalidated { return "" }
   return "📂 " + title
  }
  
  func titleToUse() -> String {
    return title
  }
  
  func isAnscestory(of obj: Organizable) -> Bool {
    if obj.filepath.hasPrefix(self.filepath + self.uuid) { return true }
    return false
  }
  
}

extension Organizable {
  func expandedItems(_ level: Int) -> [(Organizable, Int)] {
    var items: [(Organizable, Int)] = [(self, level)]
    let path = self.filepath + self.uuid + "/"
    let source: [Organizable] = RealmManager.shared.organizableObjects(in: path, types: [.downloadedLaw, /*.bookmark,*/ .organizable])
    
    let closedFolders: [String] = UserDefaults.standard.object(forKey: "ClosedFolders") as? [String] ?? []
    if let _ = closedFolders.index(of: self.uuid) {
      
    }else {
      source.forEach {
        items += $0.expandedItems(level+1)
      }
    }
    return items
  }
}

class FetchedLinks: Object {
  @objc dynamic var lawNum: String = "" // 昭和三十四年法律第百二十一号
  @objc dynamic var lastFetchedLinksDate: Date = Date.distantPast
  
  @objc dynamic var fetchedLinks: Data? = nil { // json
  didSet {
      fetchedLinksCache = nil // NOT CALLED
    }
  }
  
  override static func ignoredProperties() -> [String] {
    return super.ignoredProperties() + ["transientLinks"]
  }
  
  override static func indexedProperties() -> [String] {
    return super.indexedProperties() + ["lawNum"]
  }
  
  static var transientLinks: [CKRecord] = []
  
  var fetchedLinksCache: [String: [[String: Any]]]? = nil
  func fetchedLinkArray() -> [String: [[String: Any]]] {
    
    if self.isInvalidated { return [:] }
    var recordArray: [[String: Any]] = []
    
    for record in FetchedLinks.transientLinks {
      if record["fromLaw"] as? String != lawNum { continue }
      
      // transientLinks contains links recently added by user
      var dict = [String: Any]()
      dict["recordName"] = record.recordID.recordName
      dict["fromAnchor"] = record["fromAnchor"]
      dict["fromLaw"] = record["fromLaw"]
      dict["fromLawEdition"] = record["fromLawEdition"]
      dict["occurrenceIndex"] = record["occurrenceIndex"]
      dict["string"] = record["string"]
      dict["toLaw"] = record["toLaw"]
      dict["toLawEdition"] = record["toLawEdition"]
      dict["toAnchor"] = record["toAnchor"]
      dict["category"] = record["category"]
      
      recordArray.append(dict)
    }
    
    func organizeByFromAnchor(_ array: [[String: Any]]) -> [String: [[String: Any]]] {
      var dict: [String: [[String: Any]]] = [:]
      array.forEach {
        if let fromAnchor = $0["fromAnchor"] as? String {
          if dict[fromAnchor] == nil {
            dict[fromAnchor] = []
          }
          dict[fromAnchor]!.append($0)
        }
      }
      return dict
    }
    
    if fetchedLinksCache == nil {
      do {
        if let data = fetchedLinks as NSData?,
          let inflated = data.inflate(),
          let plist = try JSONSerialization.jsonObject(with: inflated, options: .mutableContainers) as? [[String: Any]] {
          fetchedLinksCache = organizeByFromAnchor(plist + recordArray)
          return fetchedLinksCache!
        }
        fetchedLinksCache = organizeByFromAnchor(recordArray)
        return fetchedLinksCache!
        
      }catch _ {
        fetchedLinksCache = organizeByFromAnchor(recordArray)
        return fetchedLinksCache!
      }
    }else {
      recordArray.forEach {
        if let fromAnchor = $0["fromAnchor"] as? String {
          if fetchedLinksCache![fromAnchor] == nil {
            fetchedLinksCache![fromAnchor] = []
          }
          fetchedLinksCache![fromAnchor]!.append($0)
        }
      }
      
      return fetchedLinksCache!
    }
  }
  
}


class DownloadedLaw: Organizable, LawSource {
  
  enum Status: Int {
   case downloaded = 0, inCloud
  }
  
  @objc dynamic var lawTitle: String = "" // md5
  @objc dynamic var lawNum: String = "" // 昭和三十四年法律第百二十一号
  @objc dynamic var lawEdition: String = "" // md5
  @objc dynamic var filename: String = "" // 429M60080000008_20170710
  @objc dynamic var mishikoLawNum: String = "" //

  var status: Status {
    get {
      if self.isInvalidated { return .downloaded }
      var url = FileManager.default.appSupportUrl
      url.appendPathComponent(localFilename)
      if FileManager.default.fileExists(atPath: url.path) == false {
        return .inCloud
      }
      
//      if ArticleCacheManager.shared.realmExists(for: self) { return .downloaded }
//
//      return .isDownloading
      
      return .downloaded
    }
  }
//  @objc dynamic var fetchedLinks: Data? = nil // OBSOLETE
//  @objc dynamic var lastFetchedLinksDate: Date = Date.distantPast
  @objc dynamic var addedDate: Date? = nil
  
  override func titleToUse() -> String {
    if title.isEmpty == false { return title }
    return lawTitle
  }
  
  override func titleToDisplay() -> String {
    if isInvalidated { return "" }

    if mishikoLawNum.isEmpty {
      return titleToUse()
    }
    
    return "【改正】" + titleToUse() // 【未施行】
    
  }
  
  var downloadOption = DownloadOption.regular

//  var selections: RealmSwift.Results<SelectionObject>? = nil
//  var localFilename: String {
//    return DownloadedLaw.localFilenameWith(lawNum: lawNum, lawTitle: lawTitle, lawEdition: lawEdition)
//  }
//
//  var localImageDataFilename: String {
//    return DownloadedLaw.localImageDataFilenameWith(lawNum: lawNum, lawTitle: lawTitle, lawEdition: lawEdition)
//  }

//  var imageDataPath: String? {
//    get {
//      let imagePath = DownloadedLaw.localImageDataFilenameWith(lawNum: lawNum, lawTitle: lawTitle, lawEdition: lawEdition)
//      if FileManager.default.fileExists(atPath: imagePath) == false { return nil }
//      return imagePath
//    }
//  }
  var xml: Data? {
    set {
      if newValue == nil { return }
      let data = (newValue! as NSData).deflate()
      var url = FileManager.default.appSupportUrl
      url.appendPathComponent(localFilename)
      try! data?.write(to: url)
      _ = url.setExcludedFromBackup(true)
    }
    get {
      if self.isInvalidated { return nil }
      
      var url = FileManager.default.appSupportUrl
      url.appendPathComponent(localFilename)
      if false == FileManager.default.fileExists(atPath: url.path) {
        return nil
      }
      
      var xmlData: Data? = nil
      
      autoreleasepool {
        let data = try? Data(contentsOf: url)
        xmlData = (data as NSData?)?.inflate()
      }
      return xmlData

    }
  }
  

  
//  static func localFilenameWith(lawNum: String, lawTitle: String, lawEdition: String) -> String {
//
//    if let _ = lawNum.range(of: "内閣総理大臣決定") {
//      return "\(lawNum)\(lawTitle) \(lawEdition).lawxml"
//    }else {
//      return "\(lawNum) \(lawEdition).lawxml"
//    }
//  }
//
//  static func localImageDataFilenameWith(lawNum: String, lawTitle: String, lawEdition: String) -> String {
//    var path: String = DownloadedLaw.localFilenameWith(lawNum: lawNum, lawTitle: lawTitle, lawEdition: lawEdition)
//    path = path + ".imagedata"
//    return path
//  }
  

  override static func indexedProperties() -> [String] {
    return super.indexedProperties() + ["lawNum", "lawEdition"]
  }
  
  override static func ignoredProperties() -> [String] {
    return super.ignoredProperties() + ["downloadOption", "selections", "xml", "imageDataPath", "transientLinks", "localFilename", "status", "isDownloading"]
  }
  
  override func deleteRelatedFiles() {
    if RealmManager.shared.identicalLaws(self).count > 1 { return }
    
    var url = FileManager.default.appSupportUrl
    url.appendPathComponent(localFilename)
    if true == FileManager.default.fileExists(atPath: url.path) {
      try? FileManager.default.removeItem(at: url)
    }
    
    url = FileManager.default.appSupportUrl
    let imagePath = localImageDataFilename
    url.appendPathComponent(imagePath)

    if true == FileManager.default.fileExists(atPath: url.path) {
      try? FileManager.default.removeItem(at: url)
    }
    
    ArticleCacheManager.shared.deleteCache(for: self)
  }
  
  // key: fromAnchor, value: [[String: Any]] (dict array)
//  var fetchedLinksCache: [String: [[String: Any]]]? = nil
//  func fetchedLinkArray() -> [String: [[String: Any]]] {
//
//
//    if self.isInvalidated { return [:] }
//    var recordArray: [[String: Any]] = []
//
//    for record in transientLinks {
//      // transientLinks contains links recently added by user
//      var dict = [String: Any]()
//      dict["recordName"] = record.recordID.recordName
//      dict["fromAnchor"] = record["fromAnchor"]
//      dict["fromLaw"] = record["fromLaw"]
//      dict["fromLawEdition"] = record["fromLawEdition"]
//      dict["occurrenceIndex"] = record["occurrenceIndex"]
//      dict["string"] = record["string"]
//      dict["toLaw"] = record["toLaw"]
//      dict["toLawEdition"] = record["toLawEdition"]
//      dict["toAnchor"] = record["toAnchor"]
//      dict["category"] = record["category"]
//
//      recordArray.append(dict)
//    }
//
//    transientLinks = []
//
//    func organizeByFromAnchor(_ array: [[String: Any]]) -> [String: [[String: Any]]] {
//      var dict: [String: [[String: Any]]] = [:]
//      array.forEach {
//        if let fromAnchor = $0["fromAnchor"] as? String {
//          if dict[fromAnchor] == nil {
//            dict[fromAnchor] = []
//          }
//          dict[fromAnchor]!.append($0)
//        }
//      }
//      return dict
//    }
//
//    if fetchedLinksCache == nil {
//      do {
//        if let data = fetchedLinks as NSData?,
//          let inflated = data.inflate(),
//          let plist = try JSONSerialization.jsonObject(with: inflated, options: .mutableContainers) as? [[String: Any]] {
//          fetchedLinksCache = organizeByFromAnchor(plist + recordArray)
//          return fetchedLinksCache!
//        }
//        fetchedLinksCache = organizeByFromAnchor(recordArray)
//        return fetchedLinksCache!
//
//      }catch _ {
//        fetchedLinksCache = organizeByFromAnchor(recordArray)
//        return fetchedLinksCache!
//      }
//    }else {
//      recordArray.forEach {
//        if let fromAnchor = $0["fromAnchor"] as? String {
//          if fetchedLinksCache![fromAnchor] == nil {
//            fetchedLinksCache![fromAnchor] = []
//          }
//          fetchedLinksCache![fromAnchor]!.append($0)
//        }
//      }
//
//     return fetchedLinksCache!
//    }
//  }
  
}
