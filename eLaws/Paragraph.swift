//
//  Paragraph.swift
//  eLawsDownloader
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Cocoa
import RealmSwift

class Paragraph: Object {
  @objc dynamic public var lawNum: String!
  @objc dynamic public var anchor: String!
  @objc dynamic public var text: String!

}

class Word: Object {
  @objc dynamic public var word: String!
  @objc dynamic public var count: Int = 0
  
  
  override static func indexedProperties() -> [String] {
    return ["word"]
  }
}
