//
//  ElawsViewModel.swift
//  LawXML
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

protocol ELawsViewDataSource: class  {
  func unreadDidChange()
}

class ELawsViewModel {
  lazy var source: [Organizable] = RealmManager.shared.organizableObjects(in: filepath, types: [.downloadedLaw, .bookmark, .organizable])
  weak var delegate: ELawsViewDataSource? = nil
  var filepath: String
  var title: String
  var displayMode: Bool {
    get {
      return UserDefaults.standard.bool(forKey: "OutlineMode")
    }
    set {
      UserDefaults.standard.set(newValue, forKey: "OutlineMode")
    }
  }
  
  var hasUnreadForum = false {
    didSet {
      if hasUnreadForum != oldValue {
        delegate?.unreadDidChange()
      }
    }
  }
  
  func organizable(with text: String) -> [Organizable] {
    return RealmManager.shared.organizableObjects(with: text)
  }
  
  
  #if os(OSX)
  #elseif os(iOS)
  var lastOrganizable: UIViewController? = nil
  var traitCollection: UITraitCollection! {
    didSet {
      //delegate?.traitCollectionDidChange()
    }
  }
  #endif
  
  
  var backgroundColorScheme: BackgroundColorSchemeType {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
  
  func accessoryViewFrame(baseWidth: CGFloat) -> CGRect {
    return CGRect(x: baseWidth-20-7, y: (cellHeight-20)/2, width: 20, height: 20)
  }
  
  func baseFrame(baseWidth: CGFloat) -> CGRect {
    return CGRect(x: 0,y: 0,width: baseWidth,height: cellHeight)
  }

  #if os(OSX)
  func labelFrame(baseWidth: CGFloat, forItem item: Organizable) -> CGRect {
    return  CGRect(x: 0,y: 4,width: baseWidth,height: cellHeight - 8)
  }
  #else
  func labelFrame(baseWidth: CGFloat, forItem item: Organizable) -> CGRect {
    if (item as? DownloadedLaw)?.status == .downloaded {
      return  CGRect(x: 0,y: 4,width: baseWidth,height: cellHeight - 8)
    }else {
      return CGRect(x: 0,y: 0,width: baseWidth-cellHeight,height: cellHeight)
    }
  }
  #endif
  
  
  #if os(OSX)
  var cellHeight: CGFloat = 30

  var downloadImage: NSImage {
    let image = NSImage(named: "LawDownload")!//.colorizedImage(withTint: backgroundColorScheme.tintColor, alpha: 1.0, glow: false)
    return image
  }
  
  func font(for item: Organizable) -> NSFont {
    if item is DownloadedLaw  {
      return NSFont.boldSystemFont(ofSize: 14)
    }
    
    return NSFont.systemFont(ofSize: 14)
  }
  func highlightAttributes(for item: SelectionObject) -> [NSAttributedString.Key: Any] {
    var attributes: [NSAttributedString.Key: Any] = [:]
    
    let font = NSFont.systemFont(ofSize: 14)
    
    let color = item.highlightColor
    
    if color != .underline {
      attributes[.underlineColor] = UIColor.red
      attributes[.underlineColor] = NSUnderlineStyle.thick.rawValue
      
    }else {
      attributes[.backgroundColor] = color.color()
    }
    
    attributes[.font] =  font
    attributes[.foregroundColor] = backgroundColorScheme.textColor
    return attributes
  }
  #else
  var cellHeight: CGFloat = 44

  var downloadImage: UIImage {
  let image = UIImage(named: "LawDownload")!.colorizedImage(withTint: backgroundColorScheme.tintColor, alpha: 1.0, glow: false)
  return image
  }
  
  func font(for item: Organizable) -> UIFont {
    if item is DownloadedLaw  {
      return UIFont.preferredFont(forTextStyle: .headline)
    }
  
    return UIFont.preferredFont(forTextStyle: .subheadline)
  }
  
  func highlightAttributes(for item: SelectionObject) -> [NSAttributedString.Key: Any] {
    var attributes: [NSAttributedString.Key: Any] = [:]
    
    let font = UIFont.preferredFont(forTextStyle: .subheadline)
    
    let color = item.highlightColor
    
    if color != .underline {
      attributes[.underlineColor] = UIColor.red
      attributes[.underlineColor] = NSUnderlineStyle.thick.rawValue
      
    }else {
      attributes[.backgroundColor] = color.color()
    }
    
    attributes[.font] =  font
    attributes[.foregroundColor] = backgroundColorScheme.textColor
    return attributes
  }
  #endif

  func set(expanded: Bool, forItem item: Organizable! )  {
    var array = UserDefaults.standard.object(forKey: "OutlineExpansion") as? [String: Bool] ?? [:]
    
    array[item.uuid] = expanded
    UserDefaults.standard.set(array, forKey: "OutlineExpansion")
    UserDefaults.standard.synchronize()
    //(item as DocItem).expanded = expanded
    //dirty = true
  }
  
  func organizable(with uuid: String) -> Organizable? {
    let obj = RealmManager.shared.organizableObject(with: uuid)
    return obj
  }
  
  func outlineView(isItemExpandable item: Organizable!) -> Bool {
    if item is Bookmark { return false }
    
    let path = item.filepath + item.uuid + "/"
    let title = item.titleToDisplay()
    let viewModel = ELawsViewModel(filepath: path, title: title)
    
    return viewModel.source.count > 0
  }
  
  func outlineView(child index: Int, ofItem item: Organizable!) -> Organizable! {
    
    if item == nil {
      return source[index]
    }
    
    let path = item.filepath + item.uuid + "/"
    let title = item.titleToDisplay()
    let viewModel = ELawsViewModel(filepath: path, title: title)
    
    return viewModel.source[index]
  }
  
  func outlineView(isItemExpanded item: Organizable!) -> Bool {
    //let dict = item as DocItem
    //return dict.expanded
    let array = UserDefaults.standard.object(forKey: "OutlineExpansion") as? [String: Bool] ?? [:]
    
    return array[item.uuid] ?? false
  }
  
  func outlineView(canAcceptSubitem item: Organizable!) -> Bool {
    if item is Bookmark { return false }
    
    return true
  }
  
  func deleteItem(_ itemToDelete: Organizable!) {
    _ = RealmManager.shared.delete(organizable: itemToDelete)
    self.reload()
  }
  
  func outlineView(numberOfChildrenOfItem item: Organizable!) -> Int {
    if item == nil {
      return source.count
    }
    
    if item.isInvalidated { return 0 }

    let path = item.filepath + item.uuid + "/"
    let title = item.titleToDisplay()
    let viewModel = ELawsViewModel(filepath: path, title: title)
    
    return viewModel.source.count
  }
  
  func moveItem(_ item: Organizable!, toItem parentObj: Any?, at index: Int) {
    
    var destViewModel: ELawsViewModel
    var sourceModel: ELawsViewModel
    
    if let parentItem = parentObj as? Organizable {
      let path = parentItem.filepath + parentItem.uuid + "/"
      let title = item.titleToDisplay()
      destViewModel = ELawsViewModel(filepath: path, title: title)
    }else {
      destViewModel = ELawsViewModel(filepath: "/", title: "")
    }
    
    sourceModel = ELawsViewModel(filepath: item.filepath, title: "")
    
    if destViewModel.filepath == sourceModel.filepath {
      
      guard let idx = destViewModel.source.index(of: item) else { return }
      
      let element = destViewModel.source.remove(at: idx)
      if idx < index {
        destViewModel.source.insert(element, at: index - 1)
        
      }else {
        destViewModel.source.insert(element, at: index)
      }
      destViewModel.reorder()
      
      for item in destViewModel.source {
        print(item.titleToDisplay())
      }
      reload()
      for item in source {
        print(item.titleToDisplay())
      }
    }else {
      
      guard let idx = sourceModel.source.index(of: item) else { return }
      
      let element = sourceModel.source.remove(at: idx)
      RealmManager.shared.setFilepath(of: item, to: parentObj as? Organizable) // 追加
      destViewModel.reload()
      
      if let idx = destViewModel.source.index(of: element) {
        destViewModel.source.remove(at: idx) // 追加インデックスから取り除いて
      }
      destViewModel.source.insert(element, at: index) // 予定の先に追加
      
      destViewModel.reorder()
      reload()
      
    }
    
    
  }
  
  init(filepath: String = "/", title: String = "") {
    self.filepath = filepath
    self.title = title
    //source = RealmManager.shared.organizableObjects(in: filepath, types: [.downloadedLaw, .bookmark, .organizable])
  }
  
  func reload() {
    source = RealmManager.shared.organizableObjects(in: filepath, types: [.downloadedLaw, .bookmark, .organizable])
  }
  
  func reorder() {
    RealmManager.shared.reorder(source)
  }
  
  func addFolder() -> Organizable? {
    let foler = RealmManager.shared.addOrganizable(under: filepath)
    reorder()
    return foler
  }
  
  static func all() -> [Organizable] {
    let all = RealmManager.shared.allOrganizableObjects
    let objectsAtRoot = RealmManager.shared.organizableObjects(in: "/")
    for obj in objectsAtRoot {
      obj.addToChildrenIfNecessary(in: all)
    }
    
    var collectedObjects: [Organizable] = []
    for obj in objectsAtRoot {
      collectedObjects += obj.chilrenArray()
    }
    
    let root = Organizable()
    root.level = 0
    root.title = "法令"
    collectedObjects.insert(root, at: 0)
    
    return collectedObjects
  }
  
  func setFilepath(of obj: Organizable, to destination: Organizable?) {
    RealmManager.shared.setFilepath(of: obj, to: destination)
  }
  
  func updateUnread() {
    #if os(iOS)

    ForumRepository.shared.hasUnreadSubscribedBoard { hasUnread in
      self.hasUnreadForum = hasUnread
    }
    #endif
  }
}


