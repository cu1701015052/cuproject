//
//  MNSplitViewController.h
//  MNSplit
//
//  Created by Masatoshi Nishikata on 10/05/08.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LawSplitViewController.h"


@interface LawSplitViewController : UIViewController {
  NSArray<UIViewController*> * viewControllers_;
  BOOL vertical_;
  BOOL verticalInLandscape_;
}
  
+(CGFloat)animationDuration;
- (id _Nonnull) initWithViewControllers:(NSArray<UIViewController*>* _Nonnull)viewControllers;
-(void)loadView;
-(void)setVertical:(BOOL)flag;
-(void)rearrangeViews:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation ;
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;
  -(BOOL)currentlyVertical;

  
  @property (nonatomic, readwrite, getter = isAnimating) BOOL animating;
  
  @property (nonatomic, readwrite) CGFloat animationDuration;
  
  @property (nonatomic, readwrite, getter = isVertical ) BOOL vertical;
  @property (nonatomic, readwrite, getter = isVerticalInLandscape) BOOL verticalInLandscape;
  @property (nonatomic, strong) NSArray<UIViewController*> * _Nonnull viewControllers;
  @property (nonatomic, readwrite) BOOL hideOthersWhenHorizontallyCompact;

  
  @end

