//
//  Article.swift
//  MacParser
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
import CloudKit
import Realm
import RealmSwift

class Toc: Object {
  @objc dynamic var index: String = ""
  @objc dynamic var num: String?
  @objc dynamic var title: String?
  @objc dynamic var articleRange: String?
  @objc dynamic var anchor: String?
  
  static var husokuTitle = "附則"
  static var figTitle = "別表"

  static func fig() -> Toc {
    let toc = Toc()
    toc.num = "1"
    toc.title = Toc.figTitle
    toc.index = "9"
    return toc
  }
  static func husoku() -> Toc {
    let toc = Toc()
    toc.num = "1"
    toc.title = Toc.husokuTitle
    toc.index = "8"
    return toc
  }
  
  //    case "編章節款目条項号欄" : return "第\(body)\(prefix)"
  
  var level: Int {
    guard let title = title else { return 0 }
    guard title.count > 0 else { return 0 }
    if title == Toc.figTitle { return 0 }
    if title == Toc.husokuTitle { return 0 }
    
    let section = "第.+(?=　)"
    guard let sectionRange = title.range(of: section, options: [.regularExpression]) else { return 0 }
    
    let prefix = title[sectionRange]
    if let _ = prefix.range(of: "編") { return 0 }
    if let _ = prefix.range(of: "章") { return 1 }
    if let _ = prefix.range(of: "節") { return 2 }
    if let _ = prefix.range(of: "款") { return 3 }
    if let _ = prefix.range(of: "目") { return 4 }
    return 0
  }
  
  
  override static func primaryKey() -> String? {
    return "index"
  }
  
  override static func indexedProperties() -> [String] {
    return []
  }
  
  override static func ignoredProperties() -> [String] {
    return ["level"]
  }
}

//saving xml as string  655 kb
//saving xml as zip 227 kb

typealias Kajō = Row
class Row: Object {
  @objc dynamic var rowNumber: Int = NSNotFound
  @objc dynamic var serializedAttributedStringCompressed: Data = Data()
  @objc dynamic var diffSerializedAttributedStringCompressed: Data? = nil

  @objc dynamic var estimatedCharacterCount: Int = 0
  
  var serializedAttributedString: Data {
    set {
      serializedAttributedStringCompressed = (newValue as NSData).deflate()
    }
    
    get {
      return (serializedAttributedStringCompressed as NSData).inflate()
    }
  }
  
  var diffSerializedAttributedString: Data? {
    set {
      diffSerializedAttributedStringCompressed = (newValue as NSData?)?.deflate()
    }
    
    get {
      return (diffSerializedAttributedStringCompressed as NSData?)?.inflate()
    }
  }
  
  private var rawAttributedString: NSMutableAttributedString {
    let attr = NSMutableAttributedString(serializedData: serializedAttributedString) ?? NSMutableAttributedString()
    return attr
  }
  private var diffRawAttributedString: NSMutableAttributedString? {
    guard let data = diffSerializedAttributedString else { return nil }
    let attr = NSMutableAttributedString(serializedData: data)
    return attr
  }
  var rawString: NSString {
    return NSString(serializedData: serializedAttributedString) ?? ""
  }
  @objc dynamic var multipleAnchors: String?
  var anchors: [String] {
    get {
      guard let multipleAnchors = multipleAnchors else { return [] }
      return multipleAnchors.components(separatedBy: " ")
    }
    set {
      multipleAnchors = newValue.joined(separator: " ")
    }
  }

  override static func primaryKey() -> String? {
    return "rowNumber"
  }
  
  override static func indexedProperties() -> [String] {
    return ["rowNumber", "multipleAnchors"]
  }
  
  override static func ignoredProperties() -> [String] {
    return ["rawAttributedString","rawString", "downloadedLaw", "anchors", "serializedAttributedString"]
  }
  
  // transient attributes
  var downloadedLaw: DownloadedLaw!

  func attributedString() -> NSMutableAttributedString {
   return rawAttributedString
  }
  
  func diffAttributedString() -> NSMutableAttributedString? {
    return diffRawAttributedString
  }
  
  func selection(with range: NSRange, in displayedAttributedString: NSAttributedString) -> SelectionObject? {
    // REMOVE CONTROLCODE
    // ADD TEMP ATTR
    print(range)
    guard NSMaxRange(range) <= displayedAttributedString.length else { return nil }
    let tempSelectionAttr = NSAttributedString.Key(rawValue:"TempSelection")
    var originalAttributedString = NSMutableAttributedString(attributedString: displayedAttributedString)
    originalAttributedString.addAttribute(tempSelectionAttr, value: "selection", range: range)
    originalAttributedString = originalAttributedString.removeControlCode()
    originalAttributedString = originalAttributedString.reverseKanjiConversion()
    
    /*
     "二　期間を定めるのに月又は年をもつてしたときは、暦に従う。月又は年の始から期間を起算しないときは、その期間は、最後の月又は年においてその起算日に応当する日の前日に満了する。ただし、最後の月に応当する日がないときは、その月の末日に満了する。"
     */
    
    // Reverse Kanji
    
    var idx = 0
    var selectionAttributedString: NSAttributedString =  NSAttributedString()
    var actualSelectionRange = NSMakeRange(0,0)
    while idx < originalAttributedString.length {
      if let _ = originalAttributedString.attribute(tempSelectionAttr, at: idx, longestEffectiveRange: &actualSelectionRange, in: NSMakeRange(0, originalAttributedString.length)) {
        selectionAttributedString = originalAttributedString.attributedSubstring(from: actualSelectionRange)
        break
      }
      idx = NSMaxRange(actualSelectionRange)
    }
    
    //
    var start = 0
    var end = selectionAttributedString.length - 1
    var startAnchor: String? = nil
    var endAnchor: String? = nil
    var startEffectiveRange = NSMakeRange(0,0)
    var endEffectiveRange = NSMakeRange(0,0)
    
    while true {
      startAnchor = selectionAttributedString.attribute(.anchor, at: start, longestEffectiveRange: &startEffectiveRange, in: NSMakeRange(0, selectionAttributedString.length) ) as? String
      if startAnchor != nil && startAnchor!.isEmpty == false { break }
      start += 1
      if start > end { break }
    }
    
    
    while true {
      endAnchor = selectionAttributedString.attribute(.anchor, at: end, longestEffectiveRange: &endEffectiveRange, in: NSMakeRange(0, selectionAttributedString.length) ) as? String
      if endAnchor != nil && endAnchor?.isEmpty == false { break }
      end -= 1
      if start > end { break }
    }
    
    if endAnchor == nil || startAnchor == nil { return nil }
    
    var startParagraph: NSAttributedString!
    var endParagraph: NSAttributedString!
    
    idx = 0
    var range = NSMakeRange(0,0)
    while idx < originalAttributedString.length {
      if startAnchor == originalAttributedString.attribute(.anchor, at: idx, longestEffectiveRange: &range, in: NSMakeRange(0, originalAttributedString.length)) as? String {
        startParagraph = originalAttributedString.attributedSubstring(from: range)
        break
      }
      idx = NSMaxRange(range)
    }
    
    idx = 0
    range = NSMakeRange(0,0)
    while idx < originalAttributedString.length {
      if endAnchor == originalAttributedString.attribute(.anchor, at: idx, longestEffectiveRange: &range, in: NSMakeRange(0, originalAttributedString.length)) as? String {
        endParagraph = originalAttributedString.attributedSubstring(from: range)
        break
      }
      idx = NSMaxRange(range)
    }
    
    if startParagraph == nil || endParagraph == nil { return nil }
    
    
    let startString = selectionAttributedString.attributedSubstring(from: startEffectiveRange).string
    let endString = selectionAttributedString.attributedSubstring(from: endEffectiveRange).string
    
    let originalString = startParagraph!.string as NSString
    
    start = 0
    var startOccurrenceCount = 0
    
    while start < originalString.length {
      print("start \(start)")
      
      let range = originalString.range(of: startString, options: [], range: NSMakeRange(start, originalString.length - start))
      if nil != startParagraph.attribute(tempSelectionAttr, at: range.location, effectiveRange: nil) { break }
      startOccurrenceCount += 1
      if start > actualSelectionRange.location { startOccurrenceCount = -1; break }
      start = NSMaxRange(range)
    }
    
    if startOccurrenceCount == -1 {
      
      assert(false)
    }

    
    var attr = attributedString()
    attr = attr.removeControlCode()
    
    var startIndex = 0
    
    for _ in 0 ..< attr.length {
      var range = NSMakeRange(0, 0)
      let anchor = attr.attribute(.anchor, at: startIndex, longestEffectiveRange: &range, in: NSMakeRange(0, attr.length)) as? String
      
      if anchor != startAnchor {
        startIndex = NSMaxRange(range)
      }else {
        break
      }
    }
    print("start index \(startIndex)")
    //
    
    let selectionObject = SelectionObject()
    selectionObject.law = downloadedLaw
    selectionObject.uuid = shortUUIDString()
//    selectionObject.text = selectionAttributedString.string
    selectionObject.startAnchor = startAnchor!
    selectionObject.startString = startString
    selectionObject.startStringOccurrenceIndex = startOccurrenceCount
    
    //selectionObject.startJi.value = startJi
    //selectionObject.startMoji = startMoji
    //selectionObject.startMojiIndex.value = startMojiCounter
    selectionObject.endAnchor = endAnchor!
    if endAnchor != startAnchor {
      selectionObject.endString = endString
    }
    //selectionObject.endJi.value = endJi
    //selectionObject.endMoji = endMoji
    //selectionObject.endMojiIndex.value = endMojiCounter
    
    selectionObject.row = rowNumber
    selectionObject.startIndexInRow = startIndex
    //startSentenceRange.location
    // lowerBound
    
    return selectionObject
  }
}

