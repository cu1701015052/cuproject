//
//  Downloader.swift
//  LawXML
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation


class Downloader: NSObject, XMLParserDelegate {
  
  static var shared: Downloader = Downloader()
  var parser: XMLParser!
  var parsedDictionary: LawDescriptor? = nil
  var lastString: String = ""
  var parseCompletionHandler: ((LawSource?, Error?)->Void)?
  
  class Info: Equatable {
    static func ==(lhs: Downloader.Info, rhs: Downloader.Info) -> Bool {
      return lhs.uuid == rhs.uuid
    }
    
    private let uuid = shortUUIDString()
    var token: String
    var progressValue: Double? = nil
    var progress: ((_ progress: Double) -> Void)!
    var completion: (( _ law: LawSource?, _ error: Error?)->Void)?
    
    init(token: String) {
      self.token = token
      self.completion = { law, error in
        
        NotificationCenter.default.post(name: Info.didEndNotification, object: self, userInfo: [ "token" : token])
      }
      self.progress = { [weak self] progress in
        DispatchQueue.main.async {
        self?.progressValue = progress
        NotificationCenter.default.post(name: Info.progressNotification, object: self, userInfo: ["progress" : progress, "token" : token])
        }
      }
    }
    func didStart() {
      NotificationCenter.default.post(name: Info.didEndNotification, object: self, userInfo: [ "token" : token])
    }
    
    static let progressNotification = NSNotification.Name(rawValue:"InfoDownloadProgressNotification")
    static let didEndNotification = NSNotification.Name(rawValue:"InfoDownloadDidEndNotification")
    static let didStartNotification = NSNotification.Name(rawValue:"InfoDownloadDidStartNotification")

  }
  
  private var downloadInfoArray:[Downloader.Info] = []
  private func removeDownloadInfo(_ info: Downloader.Info) {
    let array = downloadInfoArray.filter { $0 != info }
    downloadInfoArray = array
  }
  
  func isDownloading(_ lawEdition: String) -> Downloader.Info? {
    let array = downloadInfoArray.filter { $0.token == lawEdition && lawEdition.isEmpty == false }
    return array.first
  }
  
  //MARK:- PUBLIC
  
  func downloadLawFromAPIThenCloudKitThenSakuraServerV2(_ egovLaw: LawSource, progress: @escaping (_ progress: Double) -> Void, completion: @escaping ( _ law: LawSource?, _ error: Error?)->Void ) {
    
    if GCNetworkReachability.forInternetConnection().isReachable() == false {
      completion( nil, LawXMLError.offline )
      return
    }
    
    downloadLawFromAPIV2(egovLaw) { law,  error in
      completion(law, error)
    }
  }
  
  
  func downloadLawFromSomewhere(_ egovLaw: LawSource, progress: @escaping (_ progress: Double) -> Void, completion: @escaping ( _ law: LawSource?, _ error: Error?)->Void ) {

    if GCNetworkReachability.forInternetConnection().isReachable() == false {
      completion( nil, LawXMLError.offline )
      return
    }
    
    downloadLawFromAPIV2(egovLaw) { law, error in
      completion(law, error)
    }
  }
  
  //MARK:- PRIVATE

  func downloadLawFromAPIV2(_ egovLaw: LawSource, completion: @escaping ( _ law: LawSource?, _ error: Error?)->Void) {
    let lawNum = egovLaw.lawNum
    let lawEdition = egovLaw.lawEdition
    
    let info = Info(token: lawEdition)
    downloadInfoArray.append(info)

    let lawUrlString = "https://elaws.e-gov.go.jp/api/1/lawdata/\(lawNum)"
    
    let lawUrl = URL(string: lawUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!
    
    _ = DownloadManager.shared.addDownloadTask(lawEdition, URL: lawUrl, progressHandler: { progress in info.progress(progress) }) { (success: Bool, data: Data?, error: Error?) -> Void in
      self.removeDownloadInfo(info)

      if error != nil {
        print(error!)
        DispatchQueue.main.async {
          completion(nil, error)
          info.completion?(nil, error)
        }
        return
      }
      do {
        
        let xml = try XML(xml: data!, encoding: .utf8)
        
        if let code: String = xml.at_xpath("//Code")?.text, code == "1" {
          
          DispatchQueue.main.async {
            var message: String = xml.at_xpath("//Message")?.text ?? "不明のエラー"
            message = "総務省のサーバーからのメッセージです。“\(message)”"
            let nserror = ELawsError(message: message)
            completion(nil, nserror)
            info.completion?(nil,  nserror)
          }
          return
        }
        
        
        let fullText = xml.at_xpath("//LawFullText")?.toXML
        if xml.at_xpath("//title")?.text == "Request Rejected" || fullText == nil {
          
          DispatchQueue.main.async {
            var message: String = xml.toXML ?? ""
            message = "総務省のサーバーからのメッセージです。“\(message)”"
            let nserror = ELawsError(message: message)
            completion(nil, nserror)
            info.completion?(nil, nserror)
          }
          return
        }
        
        // GET ATTACHMENT
        var unzippedPath: String? = nil
        if let imageDataString = xml.at_xpath("//ImageData")?.text,
          let data = NSData(base64Encoded: imageDataString, options: []) {
          let tempPath = (NSTemporaryDirectory() as NSString).appendingPathComponent("_tempImageData")
          if FileManager.default.fileExists(atPath: tempPath) {
            try? FileManager.default.removeItem(atPath: tempPath)
          }
          data.write(toFile: tempPath, atomically: false)
          
          unzippedPath = (NSTemporaryDirectory() as NSString).appendingPathComponent("_tempImageDataUnzipped")
          
          if FileManager.default.fileExists(atPath: unzippedPath!) {
            try? FileManager.default.removeItem(atPath: unzippedPath!)
          }
          
          ZipArchive.unzipFile(at: tempPath, to: unzippedPath!)
          
          try? FileManager.default.removeItem(atPath: tempPath)
        }
        
        
        var content: String = fullText!
        
        // Decode
        content = content.replacingOccurrences(of: "&apos;", with: "'")
        content = content.replacingOccurrences(of: "&quot;", with: "\"")
        content = content.replacingOccurrences(of: "&lt;", with: "<")
        content = content.replacingOccurrences(of: "&gt;", with: ">")
        
        let tempPath = (NSTemporaryDirectory() as NSString).appendingPathComponent("_tempLawFullText\(shortUUIDString()).realm")
        let tempUrl = URL(fileURLWithPath: tempPath)
        let data = content.data(using: .utf8)
        try? data?.write(to: tempUrl)
        
        var downloadedLaw: LawDescriptor? = nil
        self.parse(url: tempUrl) { (dict, parseError) in
          
          if FileManager.default.fileExists(atPath: tempUrl.path) {
            try? FileManager.default.removeItem(at: tempUrl)
          }
          
          DispatchQueue.main.async {
            if parseError != nil {
              
              if unzippedPath != nil {
                if FileManager.default.fileExists(atPath: unzippedPath!) {
                  try? FileManager.default.removeItem(atPath: unzippedPath!)
                }
              }
              completion(nil, parseError)
              info.completion?(nil, parseError)
              return
            }else {
              if /*let lawNum = dict["lawNum"],*/ let lawEdition = self.parsedDictionary?.lawEdition, let title = self.parsedDictionary?.lawTitle, let data = data  {                
                
                downloadedLaw = LawDescriptor(lawTitle: title, lawNum: lawNum, lawEdition: lawEdition, filename: "", filepath: "", downloadOption: .regular)
                downloadedLaw?.xml = data
                
              }
            }
            //            completion(dict["lawNum"], dict["lawEdition"], dict["lawTitle"], data, unzippedPath, nil, error)
            var unzippedUrl: URL? = nil
            if unzippedPath != nil {
              unzippedUrl = URL(fileURLWithPath: unzippedPath!)
            }
            
            if let path = unzippedUrl?.path {
              downloadedLaw?.writeImageData(path)
            }

            completion(downloadedLaw, nil)
            info.completion?(downloadedLaw, nil)
            
          }
        }
        
      }catch let error {
        DispatchQueue.main.async {
          completion(nil, error)
          info.completion?(nil, error)
        }
        return
      }
    }
    
    info.didStart()
  }
  
  
  func downloadLawFromAPIArticle(_ lawNum:  String, article: String, paragraph: String? = nil, completion: @escaping ( _ lawData: Data?, _ imageData: Data?, _ error: Error?)->Void) {
    
    let info = Info(token: lawNum)
    downloadInfoArray.append(info)

    var lawUrlString = "https://elaws.e-gov.go.jp/api/1/articles;lawNum=\(lawNum);article=\(article)"
    
    if paragraph != nil {
      lawUrlString += ";paragraph=\(paragraph!)"
    }
    
    let lawUrl = URL(string: lawUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!
    
    _ = DownloadManager.shared.addDownloadTask(lawNum, URL: lawUrl, progressHandler: { progress in info.progress(progress) }) { (success: Bool, data: Data?, error: Error?) -> Void in
      self.removeDownloadInfo(info)
      
      if error != nil {
        print(error!)
        DispatchQueue.main.async {
          completion(nil,nil, error)
          info.completion?(nil, error)
        }
        return
      }
      do {
        
        let xml = try XML(xml: data!, encoding: .utf8)
        
        if let code: String = xml.at_xpath("//Code")?.text, code == "1" {
          
          DispatchQueue.main.async {
            var message: String = xml.at_xpath("//Message")?.text ?? "不明のエラー"
            message = "総務省のサーバーからのメッセージです。“\(message)”"
            let nserror = ELawsError(message: message)
            completion(nil,nil,nserror)
            info.completion?(nil,  nserror)
          }
          return
        }
        
        
        let fullText = xml.at_xpath("//LawContents")?.toXML
        if xml.at_xpath("//title")?.text == "Request Rejected" || fullText == nil {
          
          DispatchQueue.main.async {
            var message: String = xml.toXML ?? ""
            message = "総務省のサーバーからのメッセージです。“\(message)”"
            let nserror = ELawsError(message: message)
            completion(nil,nil, nserror)
            info.completion?(nil, nserror)
          }
          return
        }
        
        // GET ATTACHMENT
        var zippedData: NSData! = nil
        if let imageDataString = xml.at_xpath("//ImageData")?.text,
          let data = NSData(base64Encoded: imageDataString, options: []) {
          zippedData = data
        }
        
        var content: String = fullText!
        
        // Decode
        content = content.replacingOccurrences(of: "&apos;", with: "'")
        content = content.replacingOccurrences(of: "&quot;", with: "\"")
        content = content.replacingOccurrences(of: "&lt;", with: "<")
        content = content.replacingOccurrences(of: "&gt;", with: ">")
        
        if let data = content.data(using: .utf8), zippedData != nil {
          
          DispatchQueue.main.async {
            completion(data, zippedData as Data, nil)
            info.completion?(nil, error)
          }
          
//          let processXML = ProcessSentenceXML()
//          processXML.parse(data: data, completion: { (tocs, mattr, error) in
//            DispatchQueue.main.async {
//              completion(nil, nil,nil)
//              info.completion?(nil, error)
//            }
//          })
        }else {
          DispatchQueue.main.async {
            let message = "No data"
            let nserror = ELawsError(message: message)

            completion(nil, nil,nserror)
            info.completion?(nil, nserror)
          }
        }

        
      }catch let error {
        DispatchQueue.main.async {
          completion(nil, nil,error)
          info.completion?(nil, error)
        }
        return
      }
    }
    
    info.didStart()
  }
  
  //MARK:-
  
  func parse(url: URL, completion: @escaping ((LawSource?, Error?)->Void)) {
    let parser = XMLParser(contentsOf: url)
    self.parser = parser
    self.parseCompletionHandler = completion
    parser?.delegate = self
    
    // GET MD5
    let md5 = NSString.md5StringFromFile(at: url) ?? ""
    
    parsedDictionary = LawDescriptor(lawTitle: "", lawNum: "", lawEdition: md5, filename: "", filepath: "",  downloadOption: .regular)
    
    parser?.parse()
  }
  
  func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
    if "LawNum" == elementName || "LawTitle" == elementName  {
      lastString = ""
    }
    if "MainProvision" == elementName || "TOC" == elementName || "SupplProvision" == elementName {
      abort()
    }
  }
  
  func parser(_ parser: XMLParser, foundCharacters string: String) {
    lastString += string
  }
  
  func parser(_: XMLParser, didEndElement: String, namespaceURI: String?, qualifiedName: String?) {
    if didEndElement == "LawNum" {
      //parser?.abortParsing()
      parsedDictionary?.lawNum = lastString.replacingOccurrences(of: " ", with: "")
    }
    
    if didEndElement == "LawTitle" {
      // parser?.abortParsing()
      parsedDictionary?.lawTitle = lastString
      
      if lastString == "令" {
        
      }
    }
    
    if "MainProvision" == didEndElement || "TOC" == didEndElement || "SupplProvision" == didEndElement {
      abort()
    }
    if "LawNum" == didEndElement || "LawTitle" == didEndElement  {
      lastString = ""
    }
  }
  
  func parserDidEndDocument(_ parser: XMLParser) {
    DispatchQueue.main.async {
      self.parseCompletionHandler?(nil, LawXMLError.xmlParseError)
    }
  }
  
  func abort(_ error: Error? = nil) {
    parser?.abortParsing()
    self.parser = nil
    DispatchQueue.main.async {
      self.parseCompletionHandler?(self.parsedDictionary, error)
    }
  }
}
