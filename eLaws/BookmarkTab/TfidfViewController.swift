//
//  Doc2VecViewController.swift
//  CUProject
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Cocoa
import NaturalLanguage
import RealmSwift

class TfidfViewModel {
  var source: TFIDFFetchResults
  var viewSize:CGSize = CGSize(width: 300,height: 300)

  init(source: TFIDFFetchResults) {
    self.source = source
  }
}

class TfidfViewController: NSViewController, Doc2vecSeedDelegate {
  
  @IBOutlet weak var arrayController: NSArrayController!
  @IBOutlet weak var texiField: NSTextField!
  @IBOutlet weak var indicator: NSProgressIndicator!
  @IBOutlet weak var searchButton: NSButton!
  var viewModel: TfidfViewModel!

  init(viewModel: TfidfViewModel, viewSize: CGSize) {
    self.viewModel = viewModel
    self.viewModel.viewSize = viewSize
    super.init(nibName: "TfidfViewController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder:aDecoder)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    title = "Tfidf"
    
    self.viewModel.source.doc2vecSeedDelegate = self
  }
  
  func doc2vecSeed(_ string: String) {
    texiField.stringValue = string
    searchText(string)
  }
  
  @IBAction func search(_ sender: Any) {
    let string = self.texiField.stringValue
    searchText(string)
  }
  
  func searchText(_ string: String) {
    if string.isEmpty {
      self.arrayController.content = []
      self.arrayController.rearrangeObjects()
      return
    }
    
    indicator.startAnimation(nil)
    searchButton.isEnabled = false
    texiField.isEnabled = false
    
    var words: String = ""
    
    let tagger = NLTagger(tagSchemes: [.lexicalClass])
    tagger.string = string
    let options: NLTagger.Options = [ .omitWhitespace]
    tagger.enumerateTags(in: string.startIndex..<string.endIndex, unit: .word, scheme: .lexicalClass, options: options) { tag, tokenRange in
      if let _ = tag {
        words += String(string[tokenRange])
        words += " "
        
      }
      
      return true
    }
    
    func humanReadableAnchor(_ anchor: String) -> String {
      // 条1
      if anchor.utf16.count <= 1 { return "" }
      
      let prefix = anchor[..<anchor.index(anchor.startIndex, offsetBy: 1)]
      var body = String(anchor[anchor.index(anchor.startIndex, offsetBy: 1)...])
      body = body.replacingOccurrences(of: "_", with: "の")
      
      switch prefix {
      case "編", "章", "節", "款", "目", "条", "項", "号", "欄" :
        // insert suffix before の
        if let range = body.range(of: "の") {
          body.insert(prefix.first!, at: range.lowerBound)
          return "第\(body)"
          
        }else {
          return "第\(body)\(prefix)"
        }
      case "文":
        if body == "main" { return "本文" }
        else if body == "proviso" { return "ただし書" }
        else { return "" }
      case "⑴": return "(\(body))"
      case "⒤":
        guard let number = Int(body) else { return "" }
        let base: NSString = "ⅰⅱⅲⅳⅴⅵⅶⅷⅸⅹⅺⅻ"
        if number > base.length || number < 1 { return "(\(number))" }
        return "(\(base.substring(with: NSMakeRange(number - 1, 1))))"
      case "イ":
        guard let number = Int(body) else { return "" }
        let base: NSString = "イロハヒホヘトチリヌルヲワカヨタレソツネナラムウヰノオクヤマケフコエテアサキユメミシヱヒモセスン"
        if number > base.length || number < 1 { return "" }
        return base.substring(with: NSMakeRange(number - 1, 1))
      case "✢", "✣", "✤":
        return body
      case "表":
        guard let number = Int(body) else { return "表" }
        return "表\(number + 1)"
      case "附":
        return "【附則】"
      case "図": return "図面"
      case "様": return "様式"
        
      default:
        return ""
        
      }
    }
    
    DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
      self.load(words) { data in
        self.indicator.stopAnimation(nil)
        self.searchButton.isEnabled = true
        self.texiField.isEnabled = true
        guard let data = data else { return }
        
        let url = Bundle.main.url(forResource: "preview", withExtension: "realm")!
        
        let config = Realm.Configuration(fileURL: url,  readOnly: true, objectTypes:[Preview.self])
        let realm = try! Realm(configuration: config)
        
        if let _ = String(data: data, encoding: .utf8) {
          
          do {
            let array = try JSONSerialization.jsonObject(with: data, options: []) as! [[Any]]
            
            let list = LawList()
            
            self.arrayController.content = array.map { p in
              let label = p[0] as! String
              let comps = label.components(separatedBy: "@")
              let law = list.lawsWithNum(comps[0])?.first
              
              let array = comps[1].components(separatedBy: "/")
              var humanReadable = ""
              for obj in array {
                let str = humanReadableAnchor(obj)
                humanReadable += str
              }
              humanReadable = humanReadable.replacingOccurrences(of: "条第", with: "条")
              humanReadable = humanReadable.replacingOccurrences(of: "項第", with: "項")
              
              let predicate = NSPredicate(format: "label LIKE %@", p[0] as! String)
              let obj = realm.objects(Preview.self).filter(predicate).first
              let content = obj?.content ?? ""
              return ["name": law?.lawTitle ?? comps[0] , "paragraph": humanReadable,  "relevance": p[1] as! NSNumber, "content": content]
              } as [[String: Any]]
            self.arrayController.rearrangeObjects()
            
            
          }catch let error {
            print(error.localizedDescription)
            
            NSAlert(error: error).beginSheetModal(for: self.view.window!, completionHandler: nil)
          }
          
          
        }
        
      }
    }
    
    
  }

  
  func load(_ string: String, completion:@escaping (_:Data?)->Void) {
   
    // MAKE TfidfDoc object
  
    let seed = Doc()
    seed.name = "seed"
    
    // CALL cosine( in Tfidf instance
    
    
    
    DispatchQueue.main.async {
      completion(nil)
    }
    
  }

}
