//
//  Preview.swift
//  Pipe
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
import RealmSwift

class Preview: Object {
  @objc dynamic var label: String = ""
  @objc dynamic var content: String = ""

  override static func indexedProperties() -> [String] {
    return super.indexedProperties() + ["label"]
  }
}
