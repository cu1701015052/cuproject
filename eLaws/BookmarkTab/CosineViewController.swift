//
//  CosineViewController.swift
//  CUProject
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//


import Cocoa
import RealmSwift

class CosineViewModel {
  var source: TFIDFFetchResults
  
  var viewSize:CGSize = CGSize(width: 300,height: 300)
  
  init(source: TFIDFFetchResults) {
    self.source = source
  }
}

class CosineViewController: NSViewController, NSTableViewDelegate, NSTableViewDataSource {
  var viewModel: CosineViewModel!
  var jumpAction: ((_ bookmark: Any)->Void)? = nil
  @objc dynamic var isLoading = false
  @objc dynamic var progress: NSNumber? = nil
  @objc var font: NSFont = NSFont.systemFont(ofSize: 10)
  @objc var rowHeight: Double = 12

  @IBOutlet weak var arrayController: NSArrayController!
  @IBOutlet weak var tableView: NSTableView!
  
  init(viewModel: CosineViewModel, viewSize: CGSize) {
    self.viewModel = viewModel
    self.viewModel.viewSize = viewSize
    super.init(nibName: "CosineViewController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder:aDecoder)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    title = "段落ごとのコサイン類似度"
    arrayController.content = viewModel.source.tfidf.docs

    viewModel.source.tfidf.addObserver(self, forKeyPath: "docs", options: .new, context: nil)
  }
  
  deinit {
    if isViewLoaded {
    viewModel.source.tfidf?.removeObserver(self, forKeyPath: "docs")
    }
  }
  
  override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    
    if keyPath != "docs" { return }

    arrayController.rearrangeObjects()
  }

}

@objc class DocCosineViewModel: NSObject {
  var source: EGovLaw
  @objc dynamic var docs = NSMutableArray()

  var viewSize:CGSize = CGSize(width: 300,height: 300)

  init(source: EGovLaw) {
    self.source = source
  }

  var cancel = false
  func cancelCalc() {
    cancel = true
  }
  
  func calc(_ source: TfidfDoc, progress: @escaping (Double)->Void, completion: @escaping ()->Void) {
    let docs = NSMutableArray()

    let tfidf = Tfidf()
    let dbUrl = Bundle.main.url(forResource: "tf-idf-index", withExtension: "realm")
    let config = Realm.Configuration(fileURL: dbUrl, readOnly: true, objectTypes:[EGovLaw.self])
    let realm = try! Realm(configuration: config)
    let results = realm.objects(EGovLaw.self)
    let resultsCount = results.count
    
    let allObj = NSMutableArray()
    for i in 0..<resultsCount {
      if cancel { return }

      if (i/10)*10 == i {
        DispatchQueue.main.async {
          progress(Double(i)/Double(resultsCount)/2)
        }
      }
      
      let obj = results[i]
      let doc = Doc()
      doc.tempTfidf = obj.tempTfidf
      doc.name = obj.lawTitle
      doc.label = obj.label
      allObj.add(doc)
    }
    
    let count = allObj.count
    DispatchQueue.concurrentPerform(iterations: count)  { (i:size_t) in
      autoreleasepool {
        if cancel { return }
        
        if (i/10)*10 == i {
          DispatchQueue.main.async {
          progress(Double(i)/Double(count)/2 + 0.5)
          }
        }
        
        let law = allObj[i] as! Doc
        let cosine = tfidf.cosine(source, with: law)
        law.tempCosine = cosine
        
        if docs.count < 50 {
          objc_sync_enter(self)
          docs.add(law)
          
          docs.sort { law0, law1  in
            if (law0 as! TfidfDoc).tempCosine > (law1 as! TfidfDoc).tempCosine {
              return ComparisonResult.orderedAscending
            }else {
              return ComparisonResult.orderedDescending
            }
          }
          objc_sync_exit(self)

        }else {
          
          if (docs[0] as! TfidfDoc).tempCosine < cosine {
            objc_sync_enter(self)
            docs.insert(law, at: 0)
            docs.removeLastObject()
            objc_sync_exit(self)
          }
        }
        
        
      }
    }
    
    
    DispatchQueue.main.async {
      self.willChangeValue(for: \.docs)
      self.docs.removeAllObjects()
      self.docs.addObjects(from: docs as! [Any])
      self.didChangeValue(for: \.docs)
      completion()
    }
  }
}

class DocCosineViewController: NSViewController, NSTableViewDelegate, NSTableViewDataSource {
  var viewModel: DocCosineViewModel!
  @IBOutlet weak var arrayController: NSArrayController!
  @IBOutlet weak var tableView: NSTableView!
  @objc dynamic var isLoading = false
  @objc dynamic var progress: NSNumber? = nil
  @objc var font: NSFont = NSFont.systemFont(ofSize: 12)
  @objc var rowHeight: Double = 15

  init(viewModel: DocCosineViewModel, viewSize: CGSize) {
    self.viewModel = viewModel
    self.viewModel.viewSize = viewSize
    super.init(nibName: "CosineViewController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder:aDecoder)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    title = "段落ごとのコサイン類似度"
    arrayController.content = viewModel.docs
    
    viewModel.addObserver(self, forKeyPath: "docs", options: .new, context: nil)
    
    let source = Doc()
    source.tempTfidf = viewModel.source.tempTfidf

    isLoading = true
    DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
      self.viewModel.calc(source, progress:{ p in
        
        self.progress = NSNumber(value: p)
      }) { [weak self] in
        self?.isLoading = false
        
      }
    }
    
  }
  
  override func viewWillDisappear() {
    super.viewWillDisappear()
    self.viewModel.cancelCalc()
  }
  
  deinit {
    if isViewLoaded {
    viewModel.removeObserver(self, forKeyPath: "docs")
    }
  }
  
  override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    
    if keyPath != "docs" { return }
    DispatchQueue.main.async {
      self.arrayController.rearrangeObjects()
    }
  }
  
}
