import cgi
import sys
import io
import json
from gensim.models.doc2vec import Doc2Vec
from gensim.models.doc2vec import TaggedDocument


def read_document(path):
  with open(path, 'r') as f:
    return f.read()


def split_into_words(text):
  multipleLines = text.splitlines()
  entries = []
  for line in multipleLines:
    words = line.split()
    if len(words) < 2:
      continue
    else:
      tag = words[0]
      del words[0]
      entries += [(tag, words)]
  return entries


def loadAndPrint():
#  text = read_document("law_doc2vec_corpus.txt")
#  entries = split_into_words(text)
#
#  training_docs = []
#
#  for entry in entries:
#    paragraph = TaggedDocument(words=entry[1], tags=[entry[0]])
#    training_docs.append(paragraph)
#
#  model = Doc2Vec(documents=training_docs, min_count=1, dm=0)
#  model.save('doc2vec.model')

  text = str(sys.argv[2])

  
  modelPath = str(sys.argv[1])
  model = Doc2Vec.load(modelPath)


  tokens = text.split()
  docvec = model.infer_vector(tokens)

  results = model.docvecs.most_similar([docvec], topn=100)
  jsonResults = json.dumps(results)
  print(jsonResults)


if __name__ == '__main__':
  loadAndPrint()
