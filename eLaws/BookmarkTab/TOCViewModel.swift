//
//  TOCViewModel.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project11.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

#if os(iOS)
  import UIKit
#else
  import Cocoa
  
  class UIFont: NSFont {
    
    enum UIFontTextStyle {
      case headline, body
    }
    
    static func preferredFont(forTextStyle textSize: UIFontTextStyle) -> NSFont {
      
      if textSize == .headline {
      return NSFont.boldSystemFont(ofSize:  NSFont.systemFontSize)
      }
      
      if textSize == .body {
        return NSFont.systemFont(ofSize: NSFont.systemFontSize)
      }
      
      return NSFont.systemFont(ofSize: 12)
    }
  }

#endif

class TocDict {
  var index: String = ""
  var num: String?
  var title: String?
  var articleRange: String?
  var anchor: String?
  var level: Int = 0

  var children: [TocDict] = []
  
  func addAsChild(_ obj: TocDict) -> Bool {

    if title?.hasPrefix("附則") == true && obj.title?.hasPrefix("附則") == true {
      children.append(obj)
      return true
    }
    
    
    if level == obj.level { return false }
    
    if level + 1 == obj.level {
      children.append(obj)
      return true
    }
    
    return children.last?.addAsChild(obj) ?? false
  }
  
  func tocWithAnchor(_ anchor: String) -> TocDict? {
    if self.anchor == anchor { return self }
    for obj in children {
      if let ans = obj.tocWithAnchor(anchor)  { return ans }
    }
    return nil
  }
  
  func bodyAttributedString(at index: Int?, backgroundColorScheme: BackgroundColorSchemeType) -> (NSAttributedString, NSAttributedString /*subtitle*/, Int /* indent */) {
    
    let dict = index == nil ? self : children[index!]
    guard let content = dict.title as NSString? else { return (NSAttributedString(),NSAttributedString(),0) }
    let regexString = "（.*）"
    
    var range = content.range(of: regexString, options: [.regularExpression])
    
    if range.length == 0 {
      range = NSMakeRange( content.length, 0 )
    }
    
    var subtitle = content.substring(with: range)
    if subtitle.isEmpty { subtitle = dict.articleRange ?? "" }
    
    let attributes: [NSAttributedString.Key: Any] = [.foregroundColor: backgroundColorScheme.subTextColor,
                                                    .font: UIFont.systemFont(ofSize: UIFont.smallSystemFontSize),
                                                    .verticalGlyphForm: 0]
    
    let msubtitle = NSMutableAttributedString(string: subtitle, attributes: attributes)
    
    let kanjiType = KanjiType(rawValue: UserDefaults.standard.integer(forKey: "KanjiType"))
    if kanjiType == .zenkaku {
      msubtitle.convertKanjiNumberToArabicNumber(convertToZenkaku: true)
    }else if kanjiType == .hankaku {
      msubtitle.convertKanjiNumberToArabicNumber(convertToZenkaku: false)
    }
    
    
    var body = content.substring( with: NSMakeRange(0, range.location) )
    
    // [　]*第[一二三四五六七八九〇十百].
    let section = "[　]*第.+　"
    let sectionRange = (body as NSString).range(of: section, options: [.regularExpression])
    
    let indentLevel = dict.level
    
    //    var countedSpaces = ""
    //    for _ in 0..<dict.level {
    //      countedSpaces += "　"
    //    }
    
    
    var font =  UIFont.systemFont( ofSize: UIFont.systemFontSize )
    if indentLevel < 2 {
      font = UIFont.boldSystemFont(ofSize: font.pointSize)
    }
    let bodyColor = backgroundColorScheme.textColor
    var bodyFont = UIFont.systemFont(ofSize: UIFont.systemFontSize)
    if indentLevel < 2 {
      bodyFont = UIFont.boldSystemFont(ofSize: bodyFont.pointSize)
    }else {
      bodyFont = UIFont.systemFont(ofSize: bodyFont.pointSize - 1)
    }
    body = body.replacingOccurrences(of: "\n", with: "")
    let attributedString = NSMutableAttributedString(string: body, attributes:[.font:bodyFont, .foregroundColor:bodyColor])
    
    attributedString.setAttributes([.font:font, .foregroundColor: backgroundColorScheme.tintColor, .verticalGlyphForm: 0], range: sectionRange)
    //attributedString.insert(NSAttributedString(string: countedSpaces), at: 0)
    return (attributedString, msubtitle, dict.level)
  }
  
}

class TOCViewModel {
  func setTocs(_ tocs: [Toc]) {
    hierachicalToc = []
    tocs.forEach {
      let dict = TocDict()
      
      dict.index = $0.index
      dict.num = $0.num
      dict.title = $0.title
      dict.articleRange = $0.articleRange
      dict.anchor = $0.anchor
      dict.level = $0.level
      
      if let lastObj = hierachicalToc.last {
        if lastObj.addAsChild(dict) == false {
          hierachicalToc.append(dict)
        }
      }else {
        hierachicalToc.append(dict)
      }
    }
  }
  
  var hierachicalToc: [TocDict] = []
  
  
  var viewSize: CGSize = CGSize(width: 300, height: 300)
//  var styleSheet: StyleSheet!
  var backgroundColorScheme: BackgroundColorSchemeType {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
  
  var preferredContentSize: CGSize {
    let height: CGFloat = min( viewSize.height - 180, 400 )
    return CGSize(width: min( 450, viewSize.width - 20 ), height:  max( 44,  min( height,  CGFloat(hierachicalToc.count) * 44.0) ) )
  }
}

