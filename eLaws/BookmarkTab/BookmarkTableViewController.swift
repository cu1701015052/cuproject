//
//  BookmarkTableViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

#if os(iOS)

import UIKit

class BookmarkTableViewController: UITableViewController {
  var viewModel: BookmarkViewModel!
  var jumpAction: ((_ bookmark: Bookmark)->Void)? = nil
  var didEditHandler: (()->Void)? = nil

  weak var editButton: UIButton?
  
  init(viewModel: BookmarkViewModel, viewSize: CGSize) {
    self.viewModel = viewModel
    self.viewModel.viewSize = viewSize
    super.init(nibName: nil, bundle: nil)
    let item = UITabBarItem(title: WLoc("TabBookmark"), image: UIImage(named:"TabBookmarkOff"), selectedImage: UIImage(named:"TabBookmarkOn"))
    self.tabBarItem = item
  }
  
  init() {
    super.init(nibName: nil, bundle: nil)
  }
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nil, bundle: nil)
  }
  
 required init?(coder aDecoder: NSCoder) {
    super.init(coder:aDecoder)
  }
  
  override var preferredContentSize: CGSize {
    set {}
    get {
      var contentSize = viewModel.preferredContentSize
      contentSize.height += self.tabBarController!.tabBar.frame.height
      return contentSize
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    NotificationCenter.default.addObserver(self, selector: #selector(reload), name: LawNotification.realmDidUpdateNotification, object: nil)

    let nib = UINib(nibName: "HighlightCell", bundle: nil)
    self.tableView.register(nib, forCellReuseIdentifier: "HighlightCell")

    tableView.backgroundColor = UIColor.clear
    tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: self.tabBarController!.tabBar.frame.height, right: 0)
    tableView.scrollIndicatorInsets = UIEdgeInsets(top: viewModel.headerHeight, left: 0, bottom: self.tabBarController!.tabBar.frame.height, right: 0)
    tableView.allowsSelectionDuringEditing = true
    if viewModel.backgroundColorScheme.inverted {
      tableView?.separatorColor = UIColor(white: 1, alpha: 0.3)
      tableView?.indicatorStyle = .white
    }
    
    title = WLoc("TabBookmark")
  }
  
  @objc func reload() {
    self.tableView.reloadData()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    tableView.reloadData()
  }
  
  // MARK: - Table view data source
  
  override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if viewModel.numberOfBookmarks == 0 { return nil }
    let style: UIBlurEffect.Style = viewModel.backgroundColorScheme.inverted ? .dark : .light
    let blur = UIBlurEffect(style: style)
    let view = UIVisualEffectView(frame: CGRect(x: 0, y: 0, width: 320, height: viewModel.headerHeight))
    view.effect = blur
    view.backgroundColor = .clear
    let label = UILabel(frame: CGRect(x: 10, y: 0, width: 200, height: viewModel.headerHeight))
    label.autoresizingMask = [.flexibleRightMargin]
    label.textColor = viewModel.backgroundColorScheme.subTextColor
    label.text = WLoc("TabBookmark")
    label.font = UIFont.preferredFont(forTextStyle: .subheadline)
    view.contentView.addSubview(label)

    let button = UIButton(type: .system)

    button.addTarget(self, action: #selector(edit), for: UIControl.Event.touchUpInside)
    button.setTitle(WLoc("Edit"), for: .normal)
    button.sizeToFit()
    button.autoresizingMask = [.flexibleLeftMargin]

    let width = button.frame.size.width
    button.frame = CGRect(x: 310 - width, y: 0, width: width, height: viewModel.headerHeight)
    view.contentView.addSubview(button)
    editButton = button
    return view
  }
  
  @objc func edit() {
    setEditing(!self.isEditing, animated: true)
    editButton?.isSelected = isEditing
    if isEditing {
      editButton?.setTitle(WLoc("Done"), for: .normal)

    }else {
      editButton?.setTitle(WLoc("Edit"), for: .normal)
    }

    editButton?.sizeToFit()
    if let width = editButton?.frame.size.width {
      editButton?.frame = CGRect(x: editButton!.superview!.bounds.size.width - 10 - width, y: 0, width: width, height: viewModel.headerHeight)
    }
  }
  
  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if viewModel.numberOfBookmarks == 0 { return 0 }
   return viewModel.headerHeight
  }
  
//  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//    return viewModel.rowHeight
//  }
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // #warning Incomplete implementation, return the number of rows
    return max( 1, viewModel.numberOfBookmarks)
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if viewModel.allBookmars?.count == 0 {
      let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
      cell.textLabel!.text = WLoc("No Bookmark")
      cell.textLabel!.textAlignment = NSTextAlignment.center
      cell.textLabel!.textColor  = viewModel.backgroundColorScheme.subTextColor
      cell.isUserInteractionEnabled = false
      cell.backgroundColor = UIColor.clear
      
      return cell
    }
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "HighlightCell", for: indexPath)
    cell.backgroundColor = .clear
    
    if viewModel.backgroundColorScheme.inverted {
      let view = UIView()
      view.backgroundColor = viewModel.backgroundColorScheme.tableSelectionColor
      cell.selectedBackgroundView = view
    }
    
    let label = cell.viewWithTag(1) as! UILabel

    label.attributedText = viewModel.bookmarkAttributedString(at: indexPath.row)
    return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    if self.isEditing {
      if let highlight = viewModel[indexPath.row] {
        let controller = FolderViewController(style: .grouped)
        controller.editingLocation = false
        controller.uuids = [highlight.uuid]
        
        self.setEditing(false, animated: false)
        self.navigationController?.pushViewController(controller, animated: true)
        self.tabBarController?.preferredContentSize = controller.preferredContentSize
      }
      return
    }
    
    if let highlight = viewModel[indexPath.row] {
      jumpAction?(highlight)
    }
    if tableView.indexPathForSelectedRow != nil {
      tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
    }
  }

  // Override to support conditional editing of the table view.
  override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    if viewModel.numberOfBookmarks == 0 { return false }
    return true
  }
  
  // Override to support editing the table view.
  override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
      // Delete the row from the data source
      
      viewModel.delete(at: indexPath.row)
      tabBarController?.preferredContentSize = preferredContentSize
      if viewModel.numberOfBookmarks == 0 {
        tableView.reloadData()
      }else {
        tableView.reloadData()
        //tableView.deleteRows(at: [indexPath], with: .fade)
      }
    } else if editingStyle == .insert {
      // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    
    didEditHandler?()
  }
}

#else

  import Cocoa
  
  class BookmarkTableViewControllerMac: NSViewController, NSTableViewDelegate, NSTableViewDataSource {
    var viewModel: BookmarkViewModel!
    var jumpAction: ((_ bookmark: Bookmark)->Void)? = nil
    var didEditHandler: (()->Void)? = nil
    
    @IBOutlet weak var editButton: NSButton?
    @IBOutlet weak var tableView: NSTableView!

    init(viewModel: BookmarkViewModel, viewSize: CGSize) {
      self.viewModel = viewModel
      self.viewModel.viewSize = viewSize
      super.init(nibName: "BookmarkTableViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
      super.init(coder:aDecoder)
    }
    
    override func viewDidLoad() {
      super.viewDidLoad()
      

      title = WLoc("TabBookmark")
      
      let center = NotificationCenter.default
      center.addObserver(self, selector: #selector(reload(_:)), name: LawNotification.bookmarkDidChangeExternally, object: nil)
      center.addObserver(self, selector: #selector(reload(_:)), name: LawNotification.settingsDidChange, object: nil)

    }
    
    @objc func reload(_ notification: Notification) {
      tableView?.reloadData()
    }
    
    override func viewWillAppear() {
      super.viewWillAppear()
      tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    func setEditing(_ edit: Bool) {
      
    }
    
    var isEditing = false
    
    @objc func edit() {
//      setEditing(!self.isEditing, animated: true)
//      editButton?.isSelected = isEditing
//      if isEditing {
//        editButton?.setTitle(WLoc("Done"), for: .normal)
//
//      }else {
//        editButton?.setTitle(WLoc("Edit"), for: .normal)
//      }
//
//      editButton?.sizeToFit()
//      if let width = editButton?.frame.size.width {
//        editButton?.frame = CGRect(x: editButton!.superview!.bounds.size.width - 10 - width, y: 0, width: width, height: viewModel.headerHeight)
//      }
    }
    
//    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//      if viewModel.numberOfBookmarks == 0 { return 0 }
//      return viewModel.headerHeight
//    }
    
//    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
//      return viewModel.rowHeight
//    }
    
    func numberOfRows(in tableView: NSTableView) -> Int {
      // #warning Incomplete implementation, return the number of rows
      return max( 1, viewModel.numberOfBookmarks)
    }
    
    func tableView(_ tableView: NSTableView, objectValueFor tableColumn: NSTableColumn?, row: Int) -> Any? {
      return String(row)
    }
    
    func tableView(_ tableView: NSTableView, willDisplayCell cell: Any, for tableColumn: NSTableColumn?, row: Int) {
      guard let cell = cell as? NSTextFieldCell else { return }

      if viewModel.allBookmars == nil || viewModel.allBookmars?.count == 0 {
        cell.stringValue = WLoc("No Bookmark")
//        cell.textLabel!.textAlignment = NSTextAlignment.center
//        cell.textLabel!.textColor  = viewModel.backgroundColorScheme.subTextColor
        cell.isEnabled = false
        cell.backgroundColor = UIColor.clear
        
        return
      }

      
      cell.attributedStringValue = viewModel.bookmarkAttributedString(at: row)
      return
    }
    
    func tableView(_ tableView: NSTableView, shouldSelectRow row: Int) -> Bool {
      if viewModel.numberOfBookmarks == 0 { return false }

      if self.isEditing {
        return false
      }
      
      if let highlight = viewModel[row] {
        jumpAction?(highlight)
      }
//      if tableView.indexPathForSelectedRow != nil {
//        tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
//      }
      
      return true
    }
    
    // Override to support conditional editing of the table view.
//    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//      // Return false if you do not want the specified item to be editable.
//      if viewModel.numberOfBookmarks == 0 { return false }
//      return true
//    }
//
//    // Override to support editing the table view.
//    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//      if editingStyle == .delete {
//        // Delete the row from the data source
//
//        viewModel.delete(at: indexPath.row)
//        tabBarController?.preferredContentSize = preferredContentSize
//        if viewModel.numberOfBookmarks == 0 {
//          tableView.reloadData()
//        }else {
//          tableView.deleteRows(at: [indexPath], with: .fade)
//        }
//      } else if editingStyle == .insert {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
//      }
//
//      didEditHandler?()
//    }
//  }
  
  }
#endif
