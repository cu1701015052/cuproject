//
//  BookmarkViewModel.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project11.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//
import Realm
import RealmSwift

#if os(OSX)
  import Cocoa
#else
  import UIKit
#endif

class BookmarkViewModel {
  
  var allBookmars: RealmSwift.Results<Bookmark>? {
    guard let law = self.law else { return nil }
    return RealmManager.shared.bookmarks(for: law)
  }
  var law: DownloadedLaw? {
    return parentViewModel?.selectedLaw as? DownloadedLaw
  }
  var source: FetchResults? {
    return parentViewModel?.source
  }
  private var parentViewModel: DetailViewModel?
  
  #if os(OSX)
  var rowHeight: CGFloat = 30
  #else
  var rowHeight: CGFloat = 44
  #endif
  
  
  var viewSize:CGSize = CGSize(width: 300,height: 300)
  let headerHeight: CGFloat = 30
  
  subscript(idx: Int) -> Bookmark? {
    guard let allBookmars = allBookmars else { return nil }
    guard idx < allBookmars.count else { return nil }
    return allBookmars[idx]
  }
  
  init?(viewModel: DetailViewModel) {
    self.parentViewModel = viewModel
  }
  
  var numberOfBookmarks: Int {
    guard let allBookmars = allBookmars else { return 0 }
    return allBookmars.count
  }
  
  var preferredContentSize: CGSize {
    let height: CGFloat = min( viewSize.height - 180, 400 )
    return CGSize(width: min( 450, viewSize.width - 20 ), height: headerHeight + height )
  }
  
  var backgroundColorScheme: BackgroundColorSchemeType {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
  
  func delete(at index: Int) {
    guard let allBookmars = allBookmars else { return }
    guard index < allBookmars.count else { return }
    let itemToDelete = allBookmars[index]
    
    if RealmManager.shared.delete(organizable: itemToDelete) {
      NotificationCenter.default.post(name: LawNotification.bookmarkDidChangeExternally, object: self, userInfo: nil)
    }
  }
  
  func bookmarkAttributedString(at index: Int) -> NSAttributedString {
    guard let allBookmars = allBookmars else { return NSAttributedString() }
    guard index < allBookmars.count else { return NSAttributedString() }// UNKNOWN ERR
    let bookmark = allBookmars[index]
    
    let title = "🔖 " + bookmark.titleToUse()
    
    let plainFont = UIFont.preferredFont(forTextStyle: .body)
    let headlineFont = UIFont.preferredFont(forTextStyle: .headline)

    let mattr = NSMutableAttributedString(string: "")
    let titleToUse = title
    
    let titleAttr = NSMutableAttributedString(string: titleToUse, attributes: [.font: headlineFont, .foregroundColor: backgroundColorScheme.textColor])
    
    if let notes = bookmark.notes {
    let bodyAttr = NSAttributedString(string: " " + notes, attributes: [.font: plainFont, .foregroundColor: backgroundColorScheme.textColor])
     titleAttr.append(bodyAttr)
    }

    mattr.append(titleAttr)
    
    return mattr
  }
  
  func bookmark(for uuid: String) -> Bookmark? {
    guard let law = self.law else { return nil }
    return  RealmManager.shared.bookmarks(for: law, uuid: uuid)?.first
  }
  
  func noteBookmark(for linkString: String? = nil, row idx: Int) -> Bookmark? {
    let bookmark = source?.noteBookmark(for: source![idx].1)
    if bookmark != nil { return bookmark! }
    
    if linkString == nil { return nil }
    return addBookmark(for: linkString!, title: "", row: idx)
  }
  
  func addBookmark(for linkString: String, title: String, row: Int) -> Bookmark? {
    guard let law = self.law else { return nil }
    guard law.isInvalidated == false else { return nil }
    
    let comps = linkString.components(separatedBy: "/")
    let body = comps.filter {
      LawXMLTypes.headerLinkPrefix.hasPrefix($0) == false && $0.isEmpty == false && $0.hasPrefix("#") == false
    }
    let anchor = body.joined(separator: "/")
    let bookmark = Bookmark()
    bookmark.uuid = shortUUIDString()
    bookmark.lawNo = law.lawNum
//    bookmark.lawTitle = law.lawTitle
//    bookmark.lawEdition = law.lawEdition
    bookmark.anchor = anchor
    bookmark.order = 5000
    bookmark.row = row
    if title.isEmpty == false {
      bookmark.title = title
    }
    bookmark.filepath = law.filepath + law.uuid + "/"
    
    if RealmManager.shared.add(bookmark) {
      source?.clearAllCellHeights()
      NotificationCenter.default.post(name: LawNotification.bookmarkDidChangeExternally, object: self, userInfo: nil)
      
      return bookmark
    }
    
    return nil
  }
  
  func updateBookmark(_ bookmark: Bookmark, title: String? = nil, notes: String? = nil) {
    //selectedArticle?.selections.append(selection)
    if RealmManager.shared.update(bookmark, title: title, notes: notes) {
      source?.clearAllCellHeights()
      NotificationCenter.default.post(name: LawNotification.bookmarkDidChangeExternally, object: self, userInfo: nil)
    }
  }
  
  func delete(bookmark: Bookmark) -> Bool {
    if RealmManager.shared.delete(organizable: bookmark) {
      source?.clearAllCellHeights()
      NotificationCenter.default.post(name: LawNotification.bookmarkDidChangeExternally, object: self, userInfo: nil)
      return true
    }
    return false
  }
}


class TagViewModel {
  
  private var allTags: RealmSwift.Results<Tag>? {
    guard let law = self.law else { return nil }
    return RealmManager.shared.tags(for: law)
  }
  var law: DownloadedLaw? {
    return parentViewModel?.selectedLaw as? DownloadedLaw
  }
  var source: FetchResults? {
    return parentViewModel?.source
  }
  private var parentViewModel: DetailViewModel?
  
  #if os(OSX)
  var rowHeight: CGFloat = 30
  #else
  var rowHeight: CGFloat = 44
  #endif
  
  
  var viewSize:CGSize = CGSize(width: 300,height: 300)
  let headerHeight: CGFloat = 30
  
  subscript(idx: Int) -> Tag? {
    guard let allTags = allTags else { return nil }
    guard idx < allTags.count else { return nil }
    return allTags[idx]
  }
  
  init?(viewModel: DetailViewModel) {
    self.parentViewModel = viewModel
  }

  var preferredContentSize: CGSize {
    let height: CGFloat = min( viewSize.height - 180, 400 )
    return CGSize(width: min( 450, viewSize.width - 20 ), height: headerHeight + height )
  }
  
  var backgroundColorScheme: BackgroundColorSchemeType {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
  
  func tags(from attributedString: NSAttributedString) -> [TagEntity] {
    
    // LOOK FOR KEY .tempLink
    var loc = 0
    var range: NSRange = NSMakeRange(0, 0)
    var fetchedTags: [Int]  = []
    while NSMaxRange(range) < attributedString.length {
      if let linkString = attributedString.attribute(.tempLink, at: loc, effectiveRange: &range) as? String {
        fetchedTags = tags(for: linkString)
        break
      }
      loc = NSMaxRange(range)
    }
    
    if let entites = RealmManager.shared.tagEntites(for: fetchedTags) {
      return Array(entites)
    }
    
    return []
  }
  
  func tags(for linkString: String) -> [Int] {
    guard let law = self.law else { return [] }
    guard law.isInvalidated == false else { return [] }
    
    let comps = linkString.components(separatedBy: "/")
    let body = comps.filter {
      LawXMLTypes.headerLinkPrefix.hasPrefix($0) == false && $0.isEmpty == false && $0.hasPrefix("#") == false
    }
    var anchor = body.joined(separator: "/")
    if anchor.hasSuffix("/") == false {
      anchor = anchor + "/"
    }
    if let tags = RealmManager.shared.tags(for: law, anchor: anchor) {
      let tagDictionary = Array(tags).map {
         return $0.tagNumber
      }
      
      return tagDictionary as [Int]
    }else {
      return []
    }
  }
  
  func setTags(_ tags: [Int], for linkString: String) {
    guard let law = self.law else { return }
    guard law.isInvalidated == false else { return }
    
    let comps = linkString.components(separatedBy: "/")
    let body = comps.filter {
      LawXMLTypes.headerLinkPrefix.hasPrefix($0) == false && $0.isEmpty == false && $0.hasPrefix("#") == false
    }
    var anchor = body.joined(separator: "/")
    if anchor.hasSuffix("/") == false {
      anchor = anchor + "/"
    }
    let existingTags = RealmManager.shared.tags(for: law, anchor: anchor)
    
    var newTags: [Tag] = []
    tags.forEach {
      let tag = Tag()
      tag.lawNo = law.lawNum
      tag.anchor = anchor
      tag.tagNumber = $0
      newTags.append(tag)
    }
    
    _ = RealmManager.shared.replace(existingTags: existingTags, with: newTags)

    
    NotificationCenter.default.post(name: LawNotification.bookmarkDidChangeExternally, object: self, userInfo: nil)

  }
  
}
