//
//  HighlightListTableViewModel.swift
//  Reader
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//
//

import Foundation
import Realm
import RealmSwift
import QuartzCore

/*
 
 
 var anchor: String? = ""
 let fp = FilePackage.shared
 if let anchorText = highlight.nearestAnchor {
 (anchor,_) = fp.anchor(for: anchorText, in: packageUrl)
 
 let ( plist, linkDictionaries, _) = fp.loadPackage(at: packageUrl, onlyPlist: false, zoomScale: 1.0)
 let theLink = linkDictionaries?.filter { $0.link == anchorText }
 print(theLink?.first?.content)
 
 }
 
 
 */

class HighlightListTableViewModel {
 
  var allHighlights: RealmSwift.Results<SelectionObject>? {
    guard let law = self.law else { return nil }
    return RealmManager.shared.selections(for: law)
  }
  var rowHeight: CGFloat = 86
  var viewSize:CGSize = CGSize(width: 300,height: 300)
//  var law: DownloadedLaw!
  var law: DownloadedLaw? {
    return parentViewModel?.selectedLaw as? DownloadedLaw
  }
  var source: FetchResults? {
    return parentViewModel?.source
  }
  var parentViewModel: DetailViewModel?
  let headerHeight: CGFloat = 30

  subscript(idx: Int) -> SelectionObject? {
    guard let allHighlights = allHighlights else { return nil }
    guard idx < allHighlights.count else { return nil }
    return allHighlights[idx]
  }
  
  init(viewModel: DetailViewModel) {
    self.parentViewModel = viewModel
  }
  
  var preferredContentSize: CGSize {
    let height: CGFloat = min( viewSize.height - 180, 400 )
    return CGSize(width: min( 450, viewSize.width - 20 ), height: headerHeight + height)
  }
  
  var backgroundColorScheme: BackgroundColorSchemeType {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
  
  func highlightAttributedString(at index: Int) -> NSAttributedString {
    guard let allHighlights = allHighlights else { return NSAttributedString() }
    guard index < allHighlights.count else { return NSAttributedString() }// UNKNOWN ERR

    let highlight = allHighlights[index]
    
    let title = highlight.titleToDisplay()
    
    let boldFont = UIFont.preferredFont(forTextStyle: .headline)
    let plainFont = UIFont.preferredFont(forTextStyle: .body)
    
    let mattr = NSMutableAttributedString(string: "")
    let titleAttr = NSAttributedString(string: title + " ", attributes: [.font: boldFont, .foregroundColor: backgroundColorScheme.textColor])
    
    let highlightColor = HighlightColor(rawValue: Int(highlight.style))
    var attributes = [.font: plainFont, .backgroundColor: highlightColor.color() ?? UIColor.clear, .foregroundColor: backgroundColorScheme.textColor] as [NSAttributedString.Key : Any]
    if highlightColor == .mask {
      attributes = [.font: plainFont, .backgroundColor: highlightColor.color(), .foregroundColor: highlightColor.color()]

    }else if highlightColor.isUnderline  {
      attributes = [.font: plainFont, .underlineStyle: NSNumber(value: 2),  .underlineColor: highlightColor.color(), .foregroundColor: backgroundColorScheme.textColor]
    }
    
    var stringRep = highlight.startString
    if highlight.endString != nil {
      stringRep += "…"
    }
    let bodyAttr = NSAttributedString(string: stringRep, attributes: attributes)
    mattr.append(titleAttr)
    mattr.append(bodyAttr)
    
    if highlight.notes?.isEmpty == false {
      let bodyAttr = NSAttributedString(string: "💬", attributes: attributes)
      mattr.append(bodyAttr)
    }
    
    return mattr
  }
  
  var numberOfHighlights: Int {
    guard let allHighlights = allHighlights else { return 0 }
    return allHighlights.count
  }
  
  func delete(at index: Int) {
    guard let allHighlights = allHighlights else { return }
    guard index < allHighlights.count else { return }
    let itemToDelete = allHighlights[index]
    
    if RealmManager.shared.delete(object: itemToDelete) {
      NotificationCenter.default.post(name: LawNotification.highlightDidChangeExternally, object: self, userInfo: nil)
    }
  }
  
  //
  
  func delete(selectionWith uuid: String) -> Bool {
    source?.clearAllCellHeights()
    if RealmManager.shared.delete(selectionWith: uuid) {
      NotificationCenter.default.post(name: LawNotification.highlightDidChangeExternally, object: self, userInfo: nil)
      return true
    }
    return false
  }
  
  func selection(with uuid: String) -> SelectionObject? {
    return RealmManager.shared.selection(with: uuid)
  }
  
  func appendSelection(range: NSRange, color: HighlightColor, in attributedString: NSAttributedString, of article: Row) -> SelectionObject? {
    guard let downloadedLaw = self.parentViewModel?.selectedLaw as? DownloadedLaw else { return nil }
    guard downloadedLaw.isInvalidated == false else { return nil }
    guard article.isInvalidated == false else { return nil }

    article.downloadedLaw = downloadedLaw
    if let selection = article.selection(with: range, in: attributedString) {
//
//      selection.filepath = downloadedLaw.filepath + downloadedLaw.uuid + "/"
//      selection.order = 5000
      
      //selectedArticle?.selections.append(selection)
      selection.style = Int8(color.rawValue)
      if RealmManager.shared.add(selection) {
        source?.clearAllCellHeights()
        NotificationCenter.default.post(name: LawNotification.highlightDidChangeExternally, object: self, userInfo: nil)
        
        return selection
      }
    }
    
    return nil
  }
  
  func updateSelection(uuid: String, color: HighlightColor?, notes: String? = nil) {
    //selectedArticle?.selections.append(selection)
    if RealmManager.shared.update(selectionWith: uuid, color: color, notes: notes) {
      source?.clearAllCellHeights()
      NotificationCenter.default.post(name: LawNotification.highlightDidChangeExternally, object: self, userInfo: nil)
    }
  }

}
