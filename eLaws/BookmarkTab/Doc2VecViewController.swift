//
//  Doc2VecViewController.swift
//  CUProject
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Cocoa
import NaturalLanguage
import RealmSwift

class Doc2VecViewModel {
  var source: TFIDFFetchResults
  var viewSize:CGSize = CGSize(width: 300,height: 300)

  init(source: TFIDFFetchResults) {
    self.source = source
  }
}

class Doc2VecViewController: NSViewController, Doc2vecSeedDelegate {
  
  @IBOutlet weak var tfidfRelevanceSuffix: NSButton!
  @IBOutlet weak var tfidfRelevance: NSButton!
  @IBOutlet weak var doc2vecSuffix: NSButton!
  @IBOutlet weak var arrayController: NSArrayController!
  @IBOutlet weak var texiField: NSTextField!
  @IBOutlet weak var indicator: NSProgressIndicator!
  @IBOutlet weak var searchButton: NSButton!
  var viewModel: Doc2VecViewModel!
  @objc dynamic var filterWithSuffix = false {
    didSet {
      search(self)
    }
  }
  
  init(viewModel: Doc2VecViewModel, viewSize: CGSize) {
    self.viewModel = viewModel
    self.viewModel.viewSize = viewSize
    super.init(nibName: "Doc2VecViewController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder:aDecoder)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    title = "Doc2vec"
    
    self.viewModel.source.doc2vecSeedDelegate = self
  }
  
  func doc2vecSeed(_ string: String) {
    texiField.stringValue = string

    var suffix: String? = nil
    
    if string.count > 5 {
      suffix = String(string[string.index(string.endIndex, offsetBy: -5)..<string.endIndex])
    }
    
    searchText(string, suffix: suffix)
    
  }
  
  @IBAction func tfidfSearch(_ sender: Any) {
    let baseString = self.texiField.stringValue

    func words(_ text: String) -> [String: Int] {
      let tagger = NLTagger(tagSchemes: [.lexicalClass])
      tagger.string = text
      let options: NLTagger.Options = [.omitWhitespace]
      var words: [String: Int] = [:]
      tagger.enumerateTags(in: text.startIndex..<text.endIndex, unit: .word, scheme: .lexicalClass, options: options) { tag, tokenRange in
        if let _ = tag {
          let subtext = String((text as String)[tokenRange])
          
          if words[subtext] == nil {
            words[subtext]  = 1
          }else {
            words[subtext] = (words[subtext] ?? 0) + 1
          }

        }
        return true
      }

      return words
    }
    
    func tfidf(_ str1: String) -> [String: Double] {
      let url = Bundle.main.url(forResource: "tfidf_corpus", withExtension: "realm")!
      
      let config = Realm.Configuration(fileURL: url,  readOnly: true, objectTypes:[Word.self])

      
      var dictionary: [String: Double] = [:]
      
      let numberOfDocs: Double = 1593618
      let counts = words(str1)
      let keys: [String] = Array(counts.keys)
      
      DispatchQueue.concurrentPerform(iterations: keys.count)  { (i:size_t) in

        autoreleasepool {
          let realm = try! Realm(configuration: config)

          let key = keys[i]
          let value = counts[key] ?? 0

          let numberOfDocsWithWord = Double( realm.objects(Word.self).filter("word == %@", key).first?.count ?? 0 )
          
          let tf = Double(value)
          let idf = log(numberOfDocs/numberOfDocsWithWord)
          objc_sync_enter(self)
          dictionary[key] = tf*idf
          objc_sync_exit(self)
          
        }
      }

      let sum = sqrt(dictionary.values.reduce(0) { $0 + $1*$1 })
      
      for key in dictionary.keys {
        dictionary[key] = dictionary[key]! / sum
      }
      
      return dictionary
    }
    
    let baseTifdf = tfidf(baseString)
    
    func cosine(_ str0: String) -> Double {
      var tfidf0 = tfidf(str0)
      var tfidf1 = baseTifdf
      
      var value: Double = 0
      for key in tfidf0.keys {
        value += tfidf0[key]! * (tfidf1[key] ?? 0)
      }
      return value
    }
    
    let url = Bundle.main.url(forResource: "paragraph_corpus", withExtension: "realm")!
    
    let config = Realm.Configuration(fileURL: url,  readOnly: true, objectTypes:[Paragraph.self])
    let realm = try! Realm(configuration: config)
    
    let rows = realm.objects(Paragraph.self)
    
    let array: [(String, String, String)] = rows.compactMap {
      let lawNum = $0.lawNum ?? ""
      let anchor = $0.anchor ?? ""
      let text = $0.text ?? ""
      
//      guard ["昭和四十五年法律第四十八号", "平成十年法律第八十三号", "平成五年法律第四十七号", "昭和二十二年法律第五十四号", "平成五年法律第四十七号", "平成八年法律第百九号", "平成十七年法律第八十六号", "昭和三十四年法律第百二十一号"].contains(lawNum)
//
//       else  { return nil }
      
      return (lawNum, anchor, text)
    }
    
    
    var countArray:[Int] = [Int](repeating: 0, count: 10)
    var contentArray: [String] = []
    
    print("**** \(Date())")
    DispatchQueue.concurrentPerform(iterations: array.count)  { (i:size_t) in
      autoreleasepool {
        let row = array[i]
        let val = cosine(row.2)
        
        if val < 0.1 {
          countArray[0] += 1
          
        }else if val < 0.2 {
          countArray[1] += 1
          
        }else if val < 0.3 {
          countArray[2] += 1
          
        }else if val < 0.4 {
          countArray[3] += 1
          
        }else if val < 0.5 {
          countArray[4] += 1
          
        }else if val < 0.6 {
          countArray[5] += 1
          
        }else if val < 0.7 {
          countArray[6] += 1
          
        }else if val < 0.8 {
          countArray[7] += 1
          
        }else if val < 0.9 {
          countArray[8] += 1
          
        }else {
          countArray[9] += 1
          
        }
        
        if val >= 0.5 {
          contentArray.append( row.0 + " (\(val)):" + row.1 + ":" + row.2 )
        }
      }
    }
    print("**** \(Date())")
    
    for i in 0 ..< 10 {
      print("**** \(i): \(countArray[i])")
    }
    
    print("\n val >= 0.5")
    
    contentArray.forEach {
      print($0)
    }
    
    

  }
  
  @IBAction func paragraphSearch(_ sender: Any) {
    func levenshtein(_ str1: String, _ str2: String) -> Double {
      
      let n = str1.count
      let m = str2.count
      
      var d: [[Int]] = [[Int]](repeating: [Int](repeating: 0, count: m + 1), count: n + 1)
      
      for i in 0 ... n {
        d[i][0] = i
      }
      
      for j in 0 ... m {
        d[0][j] = j
      }
      
      for i in 1 ... n {
        for j in 1 ... m {
          let cost = str1[str1.index(str1.startIndex, offsetBy: i - 1)] == str2[str2.index(str2.startIndex, offsetBy: j - 1)] ? 0 : 1
          d[i][j] = min(d[i - 1][j] + 1,
                        d[i][j - 1] + 1,
                        d[i - 1][j - 1] + cost)
        }
      }
      
      return Double(d[n][m]) / Double(max(n,m))
    }
    
    let string = self.texiField.stringValue

    let url = Bundle.main.url(forResource: "paragraph_corpus", withExtension: "realm")!
    
    let config = Realm.Configuration(fileURL: url,  readOnly: true, objectTypes:[Paragraph.self])
    let realm = try! Realm(configuration: config)

    let rows = realm.objects(Paragraph.self)
    
    // DEBUG
//    let targetNum = ["昭和四十五年法律第四十八号","昭和六十年法律第四十三号", "平成十年法律第八十三号", "昭和二十二年法律第五十四号", "平成十七年法律第八十六号", "平成五年法律第四十七号", "昭和三十四年法律第百二十一号"]
    
    let array: [(String, String, String)] = rows.compactMap {
      let lawNum = $0.lawNum ?? ""
      let anchor = $0.anchor ?? ""
      let text = $0.text ?? ""
//      if targetNum.contains(lawNum) == false { return nil }
      return (lawNum, anchor, text)
    }
    
    var countArray:[Int] = [Int](repeating: 0, count: 10)
    var contentArray: [String] = []
    
    print("**** \(Date())")
    DispatchQueue.concurrentPerform(iterations: array.count)  { (i:size_t) in
      autoreleasepool {
        let row = array[i]
        let val = levenshtein(row.2 , string)
        
        if val < 0.1 {
          countArray[0] += 1
          
        }else if val < 0.2 {
          countArray[1] += 1

        }else if val < 0.3 {
          countArray[2] += 1

        }else if val < 0.4 {
          countArray[3] += 1

        }else if val < 0.5 {
          countArray[4] += 1

        }else if val < 0.6 {
          countArray[5] += 1

        }else if val < 0.7 {
          countArray[6] += 1

        }else if val < 0.8 {
          countArray[7] += 1

        }else if val < 0.9 {
          countArray[8] += 1

        }else {
          countArray[9] += 1

        }
        
        if val < 0.7 {
          contentArray.append( row.0 + ":" + row.1 + " (\(val)) :" + row.2 )
        }
      }
    }
    print("**** \(Date())")
    
    for i in 0 ..< 10 {
      print("**** \(i): \(countArray[i])")
    }
    
    print("\n i < 0.7")

    contentArray.forEach {
      print($0)
    }
    

    
    /*
 全部で約120 min かかる
 */

  }
  
  @IBAction func search(_ sender: Any) {
    let string = self.texiField.stringValue
    
    var suffix: String? = nil
    
    if string.count > 5 {
      suffix = String(string[string.index(string.endIndex, offsetBy: -5)..<string.endIndex])
    }
    
    searchText(string, suffix: suffix)
//    searchTextDumpingResults(strin)
  }
  
  func searchTextDumpingResults(_ string: String, suffix: String? = nil) {
    if string.isEmpty {
      self.arrayController.content = []
      self.arrayController.rearrangeObjects()
      return
    }
    print(Date())
    
    indicator.startAnimation(nil)
    searchButton.isEnabled = false
    texiField.isEnabled = false
    
    var words: String = ""
    
    let tagger = NLTagger(tagSchemes: [.lexicalClass])
    tagger.string = string
    let options: NLTagger.Options = [ .omitWhitespace]
    tagger.enumerateTags(in: string.startIndex..<string.endIndex, unit: .word, scheme: .lexicalClass, options: options) { tag, tokenRange in
      if let _ = tag {
        words += String(string[tokenRange])
        words += " "
        
      }
      
      return true
    }
    
    
    func humanReadableAnchor(_ anchor: String) -> String {
      // 条1
      if anchor.utf16.count <= 1 { return "" }
      
      let prefix = anchor[..<anchor.index(anchor.startIndex, offsetBy: 1)]
      var body = String(anchor[anchor.index(anchor.startIndex, offsetBy: 1)...])
      body = body.replacingOccurrences(of: "_", with: "の")
      
      switch prefix {
      case "編", "章", "節", "款", "目", "条", "項", "号", "欄" :
        // insert suffix before の
        if let range = body.range(of: "の") {
          body.insert(prefix.first!, at: range.lowerBound)
          return "第\(body)"
          
        }else {
          return "第\(body)\(prefix)"
        }
      case "文":
        if body == "main" { return "本文" }
        else if body == "proviso" { return "ただし書" }
        else { return "" }
      case "⑴": return "(\(body))"
      case "⒤":
        guard let number = Int(body) else { return "" }
        let base: NSString = "ⅰⅱⅲⅳⅴⅵⅶⅷⅸⅹⅺⅻ"
        if number > base.length || number < 1 { return "(\(number))" }
        return "(\(base.substring(with: NSMakeRange(number - 1, 1))))"
      case "イ":
        guard let number = Int(body) else { return "" }
        let base: NSString = "イロハヒホヘトチリヌルヲワカヨタレソツネナラムウヰノオクヤマケフコエテアサキユメミシヱヒモセスン"
        if number > base.length || number < 1 { return "" }
        return base.substring(with: NSMakeRange(number - 1, 1))
      case "✢", "✣", "✤":
        return body
      case "表":
        guard let number = Int(body) else { return "表" }
        return "表\(number + 1)"
      case "附":
        return "【附則】"
      case "図": return "図面"
      case "様": return "様式"
        
      default:
        return ""
        
      }
    }
    //words  String  "この 法律 で 「 発明 」 と は 、 自然 法則 を 利用 し た 技術 的 思想 の 創作 の うち 高度 の もの を いう 。 "
    let useDoc2VecSuffixDatabase = doc2vecSuffix.state == .on
    DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
      self.load(words, useDoc2VecSuffixDatabase) { data in
        print(Date())

        self.indicator.stopAnimation(nil)
        self.searchButton.isEnabled = true
        self.texiField.isEnabled = true
        
        let url = Bundle.main.url(forResource: "preview", withExtension: "realm")!
        
        let config = Realm.Configuration(fileURL: url,  readOnly: true, objectTypes:[Preview.self])
        let realm = try! Realm(configuration: config)
        
        if let _ = String(data: data, encoding: .utf8) {
          
          
          do {
            let array = try JSONSerialization.jsonObject(with: data, options: []) as! [[Any]]
            
            let list = LawList()
            let tfidf = Tfidf()
            
            var r: [Int] = [0,0,0,0,0,0,0,0,0,0]
            
            array.forEach { p in
              let label = p[0] as! String
              
              if let score = p[1] as? Float {
                
                if score < 0.1 { r[0] += 1 }
                else if score < 0.2 { r[1] += 1 }
                else if score < 0.3 { r[2] += 1 }
                else if score < 0.4 { r[3] += 1 }
                else if score < 0.5 { r[4] += 1 }
                else if score < 0.6 { r[5] += 1 }
                else if score < 0.7 { r[6] += 1 }
                else if score < 0.8 { r[7] += 1 }
                else if score < 0.9 { r[8] += 1 }
                else if score <= 1.0 { r[9] += 1 }
                
              }
            }
            
            print("0-0.1: \(r[0])")
            print("0.1-0.2: \(r[1])")
            print("0.2-0.3: \(r[2])")
            print("0.3-0.4: \(r[3])")
            print("0.4-0.5: \(r[4])")
            print("0.5-0.6: \(r[5])")
            print("0.6-0.7: \(r[6])")
            print("0.7-0.8: \(r[7])")
            print("0.8-0.9: \(r[8])")
            print("0.9-1.0: \(r[9])")

            
            
   
          }catch let error {
            print(error.localizedDescription)
            
            NSAlert(error: error).beginSheetModal(for: self.view.window!, completionHandler: nil)
          }
          
          
          //          do {
          //            let array = try JSONSerialization.jsonObject(with: data, options: []) as! [[Any]]
          //
          //            let list = LawList()
          //
          //            self.arrayController.content = array.map { p in
          //              let label = p[0] as! String
          //              let comps = label.components(separatedBy: "@")
          //              let law = list.lawsWithNum(comps[0])?.first
          //
          //              let array = comps[1].components(separatedBy: "/")
          //              var humanReadable = ""
          //              for obj in array {
          //                let str = humanReadableAnchor(obj)
          //                humanReadable += str
          //              }
          //              humanReadable = humanReadable.replacingOccurrences(of: "条第", with: "条")
          //              humanReadable = humanReadable.replacingOccurrences(of: "項第", with: "項")
          //
          //              let predicate = NSPredicate(format: "label LIKE %@", p[0] as! String)
          //              let obj = realm.objects(Preview.self).filter(predicate).first
          //              let content = obj?.content ?? ""
          //
          //
          //              return ["name": law?.lawTitle ?? comps[0] , "paragraph": humanReadable,  "tempCosine": p[1] as! NSNumber, "content": content]
          //              } as [[String: Any]]
          //            self.arrayController.rearrangeObjects()
          //
          //
          //
          //          }catch let error {
          //            print(error.localizedDescription)
          //
          //            NSAlert(error: error).beginSheetModal(for: self.view.window!, completionHandler: nil)
          //          }
          
          
        }
        
      }
    }
    
    
  }
  
  func searchText(_ string: String, suffix: String? = nil) {
    if string.isEmpty {
      self.arrayController.content = []
      self.arrayController.rearrangeObjects()
      return
    }
    
    indicator.startAnimation(nil)
    searchButton.isEnabled = false
    texiField.isEnabled = false
    
    var words: String = ""
    
    let tagger = NLTagger(tagSchemes: [.lexicalClass])
    tagger.string = string
    let options: NLTagger.Options = [ .omitWhitespace]
    tagger.enumerateTags(in: string.startIndex..<string.endIndex, unit: .word, scheme: .lexicalClass, options: options) { tag, tokenRange in
      if let _ = tag {
        words += String(string[tokenRange])
        words += " "
        
      }
      
      return true
    }
    
    
    func humanReadableAnchor(_ anchor: String) -> String {
      // 条1
      if anchor.utf16.count <= 1 { return "" }
      
      let prefix = anchor[..<anchor.index(anchor.startIndex, offsetBy: 1)]
      var body = String(anchor[anchor.index(anchor.startIndex, offsetBy: 1)...])
      body = body.replacingOccurrences(of: "_", with: "の")
      
      switch prefix {
      case "編", "章", "節", "款", "目", "条", "項", "号", "欄" :
        // insert suffix before の
        if let range = body.range(of: "の") {
          body.insert(prefix.first!, at: range.lowerBound)
          return "第\(body)"
          
        }else {
          return "第\(body)\(prefix)"
        }
      case "文":
        if body == "main" { return "本文" }
        else if body == "proviso" { return "ただし書" }
        else { return "" }
      case "⑴": return "(\(body))"
      case "⒤":
        guard let number = Int(body) else { return "" }
        let base: NSString = "ⅰⅱⅲⅳⅴⅵⅶⅷⅸⅹⅺⅻ"
        if number > base.length || number < 1 { return "(\(number))" }
        return "(\(base.substring(with: NSMakeRange(number - 1, 1))))"
      case "イ":
        guard let number = Int(body) else { return "" }
        let base: NSString = "イロハヒホヘトチリヌルヲワカヨタレソツネナラムウヰノオクヤマケフコエテアサキユメミシヱヒモセスン"
        if number > base.length || number < 1 { return "" }
        return base.substring(with: NSMakeRange(number - 1, 1))
      case "✢", "✣", "✤":
        return body
      case "表":
        guard let number = Int(body) else { return "表" }
        return "表\(number + 1)"
      case "附":
        return "【附則】"
      case "図": return "図面"
      case "様": return "様式"
        
      default:
        return ""
        
      }
    }
    //words  String  "この 法律 で 「 発明 」 と は 、 自然 法則 を 利用 し た 技術 的 思想 の 創作 の うち 高度 の もの を いう 。 "
    let useDoc2VecSuffixDatabase = doc2vecSuffix.state == .on
    let useTfidfFiltering = self.tfidfRelevance.state == .on
    let useTfidfFilteringSiffix = self.tfidfRelevanceSuffix.state == .on
    
    DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
      self.load(words, useDoc2VecSuffixDatabase) { data in
        self.indicator.stopAnimation(nil)
        self.searchButton.isEnabled = true
        self.texiField.isEnabled = true
        
        let url = Bundle.main.url(forResource: "preview", withExtension: "realm")!
        
        let config = Realm.Configuration(fileURL: url,  readOnly: true, objectTypes:[Preview.self])
        let realm = try! Realm(configuration: config)
        
        if let _ = String(data: data, encoding: .utf8) {
          
          
          do {
            let array = try JSONSerialization.jsonObject(with: data, options: []) as! [[Any]]
            
            let list = LawList()
            let tfidf = Tfidf()

            var results =  array.map { p in
              let label = p[0] as! String
              let comps = label.components(separatedBy: "@")
              let law = list.lawsWithNum(comps[0])?.first
              
              let array = comps[1].components(separatedBy: "/")
              var humanReadable = ""
              for obj in array {
                let str = humanReadableAnchor(obj)
                humanReadable += str
              }
              humanReadable = humanReadable.replacingOccurrences(of: "条第", with: "条")
              humanReadable = humanReadable.replacingOccurrences(of: "項第", with: "項")
              
              let predicate = NSPredicate(format: "label LIKE %@", p[0] as! String)
              let obj = realm.objects(Preview.self).filter(predicate).first
              let content = obj?.content ?? ""
              
              
              
              return ["name": law?.lawTitle ?? comps[0] , "paragraph": humanReadable, "attributedString": NSAttributedString(string: content),  "tempCosine": p[1] as! NSNumber,  "relevance": p[1] as! NSNumber, "content": content]
            } as [[String: Any]]
            
            let filterBySuffix = self.filterWithSuffix
            
            if filterBySuffix {
              results = results.filter {
                guard let filterWithSuffix = suffix else { return true }
                //                guard self.filterWithSuffix == true else { return true }
                
                
//                if "前項の場合を除くほか、第三項の場合において、その請求権者が受けた損害の全部又は一部が、当該書類の虚偽記載等によつて生ずべき当該有価証券の値下り以外の事情により生じたことが認められ、かつ、当該事情により生じた損害の性質上その額を証明することが極めて困難であるときは、裁判所は、口頭弁論の全趣旨及び証拠調べの結果に基づき、賠償の責めに任じない損害の額として相当な額の認定をすることができる。" == ($0["content"] as? String) {
//                  
//                  print($0["content"] as? String)
//                }
                
                if true == ($0["content"] as? String)?.hasSuffix(filterWithSuffix) { return true }
                return false
                
                }
            }
            
            
            
            // SORT BY TF IDF RELEVANCE
            if useTfidfFiltering {
              let strings: [String] = results.map { return $0["content"] as! String }
              let names: [String] = results.map { return $0["name"] as! String }
              let parapraphs: [String] = results.map { return $0["paragraph"] as! String }
              
              let theDoc = tfidf.add(document: string, with: "Base", type: Doc.self, useSuffix: useTfidfFilteringSiffix)
              theDoc.content = string
              
              tfidf.add(strings: strings, with: names, and: parapraphs, type: Doc.self) { [weak tfidf] success in
                tfidf?.calcTfidfValues()
                
                for doc in tfidf!.docs {
                  (doc as! Doc).tempCosine = tfidf!.cosine(theDoc, with:(doc as! Doc))
                  let attr = (doc as! Doc).attributedString(with: tfidf!)
                  (doc as! Doc).attributedString = attr
                }
                
                let sortDescriptor = NSSortDescriptor(key: "tempCosine", ascending: false)
                tfidf!.docs.remove(theDoc)
                tfidf!.docs.sort(using: [sortDescriptor])
                self.arrayController.content = tfidf!.docs
                
                self.arrayController.rearrangeObjects()
              }
            }else {
              
              // DOC2VEC
              
              results.forEach {
               print("\($0["name"] as! String) \($0["paragraph"] as! String) \($0["relevance"] as! String)")
              }
              
              self.arrayController.content = results
              self.arrayController.rearrangeObjects()
            }
            
          }catch let error {
            print(error.localizedDescription)
            
            NSAlert(error: error).beginSheetModal(for: self.view.window!, completionHandler: nil)
          }
          
 
//          do {
//            let array = try JSONSerialization.jsonObject(with: data, options: []) as! [[Any]]
//
//            let list = LawList()
//
//            self.arrayController.content = array.map { p in
//              let label = p[0] as! String
//              let comps = label.components(separatedBy: "@")
//              let law = list.lawsWithNum(comps[0])?.first
//
//              let array = comps[1].components(separatedBy: "/")
//              var humanReadable = ""
//              for obj in array {
//                let str = humanReadableAnchor(obj)
//                humanReadable += str
//              }
//              humanReadable = humanReadable.replacingOccurrences(of: "条第", with: "条")
//              humanReadable = humanReadable.replacingOccurrences(of: "項第", with: "項")
//
//              let predicate = NSPredicate(format: "label LIKE %@", p[0] as! String)
//              let obj = realm.objects(Preview.self).filter(predicate).first
//              let content = obj?.content ?? ""
//
//
//              return ["name": law?.lawTitle ?? comps[0] , "paragraph": humanReadable,  "tempCosine": p[1] as! NSNumber, "content": content]
//              } as [[String: Any]]
//            self.arrayController.rearrangeObjects()
//
//
//
//          }catch let error {
//            print(error.localizedDescription)
//
//            NSAlert(error: error).beginSheetModal(for: self.view.window!, completionHandler: nil)
//          }
          

        }
        
      }
    }
    
    
  }

  func buildPreview() {
    let desktop = FileManager.default.urls(for: .desktopDirectory,  in: .userDomainMask)[0]
    let docUrl = desktop.appendingPathComponent("preview.realm")
    
    let config = Realm.Configuration(fileURL: docUrl,  objectTypes:[Preview.self])
    let realm = try! Realm(configuration: config)
    
    try! realm.write {
      
      let url = Bundle.main.url(forResource: "law_doc2vec_corpus", withExtension: "txt")!
      let string = try! String(contentsOf: url, encoding: .utf8)
      let comps = string.components(separatedBy: "\n")
      
      for line in comps {
        autoreleasepool {
          let pair = line.components(separatedBy: " ")
          let label = pair[0]
          let body = pair.dropFirst().joined(separator: "")
          
          let preview = Preview()
          preview.label = label
          preview.content = body
          realm.add(preview)
          
        }
      }
    }
  }
  
  func load(_ string: String, _ useDoc2VecSuffixDatabase: Bool,  completion:@escaping (_:Data)->Void) {
//    loadParagraphBasedTfidf(string)
//    exit(0)
    
    let task = Process()
    let messagePipe = Pipe()
    
    task.launchPath = "~/anaconda3/bin/python"
    
    let pyPath = Bundle.main.url(forResource: "doc", withExtension: "py")!
//    let modelPath = Bundle.main.url(forResource: "doc2vec", withExtension: "model")!
    let modelPath = useDoc2VecSuffixDatabase ? Bundle.main.url(forResource: "doc2vec_suffix", withExtension: "model")! :  Bundle.main.url(forResource: "doc2vec", withExtension: "model")!


    task.qualityOfService = .userInteractive
    task.arguments = [pyPath.path, modelPath.path, string]
    task.standardOutput = messagePipe
    task.launch()
    //    task.waitUntilExit()
    
    let totalData = NSMutableData()
    while task.isRunning {
      let data = messagePipe.fileHandleForReading.availableData
      if data.count > 0 {
        totalData.append(data)
      }
    }

    print("** Python done")
    
    DispatchQueue.main.async {
      completion(totalData as Data )
    }
    
  }

  func loadParagraphBasedTfidf(_ string: String) {
    let dbUrl = Bundle.main.url(forResource: "paragraph_tfidf", withExtension: "realm")!
    let config = Realm.Configuration(fileURL: dbUrl, readOnly: true, objectTypes:[RowEGovLaw.self])
    let realm = try! Realm(configuration: config)

    var words: [String] = []
    
    let tagger = NLTagger(tagSchemes: [.lexicalClass])
    tagger.string = string
    let options: NLTagger.Options = [ .omitWhitespace]
    tagger.enumerateTags(in: string.startIndex..<string.endIndex, unit: .word, scheme: .lexicalClass, options: options) { tag, tokenRange in
      if let _ = tag {
        words.append( String(string[tokenRange]) )
      }
      
      return true
    }
    
    
    for word in words {
      let count = realm.objects(RowEGovLaw.self).filter("text CONTAINS %@", word).count
      print(count)
    }
  }
}
