//
//  BookmarkTableViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import RealmSwift
class OutlineClosed: Object {
  @objc dynamic var lawNum: String = ""
  @objc dynamic var anchor: String = ""
  @objc dynamic var closed: Bool = false
  
  override static func indexedProperties() -> [String] {
    return ["lawNum", "anchor"]
  }
}

#if os(iOS)
  import UIKit

class TOCBaseViewController: UINavigationController {
  var viewSize: CGSize = CGSize(width: 300, height: 300)
  var jumpAction: ((_ linkDictionary: TocDict)->Void)? = nil
  var viewModel: DetailViewModel!
  var barButtonItem: UIBarButtonItem!
  init(viewModel: DetailViewModel, viewSize: CGSize) {
    
    super.init(navigationBarClass: nil, toolbarClass: nil)
    self.viewModel = viewModel
    self.viewSize = viewSize
    //    self.viewModel.viewSize = viewSize
    //      self.viewModel.styleSheet = viewModel.styleSheet
    
    //    if viewModel.source != nil {
    //      self.viewModel.tocs = Array(viewModel.source!.tocs)
    //    }else {
    //      self.viewModel.tocs = []
    //    }
    
    let state = UserDefaults.standard.bool(forKey: "TOCBaseViewControllerOutlineMode")
    setOutlineView(state)

    
    self.tabBarItem = UITabBarItem(title: WLoc("TOC"), image: UIImage(named:"TabTOC"), selectedImage: UIImage(named:"TabTOCOn"))
    self.tabBarItem.tag = 0
    navigationBar.barStyle = viewModel.styleSheet.backgroundColorScheme.barStyle
    navigationBar.barTintColor = viewModel.styleSheet.backgroundColorScheme.secondaryBackgroundColor
    navigationBar.titleTextAttributes = [.foregroundColor: viewModel.styleSheet.backgroundColorScheme.textColor, .font: UIFont.preferredFont(forTextStyle: .headline)]
    
  }
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override var preferredContentSize: CGSize {
    set {}
    get {
      let height: CGFloat = min( viewSize.height - 100, 600 )
      return CGSize(width: min( 450, viewSize.width - 20 ), height: height )
    }
  }
  
  
  @IBAction func viewChanged(_ sender: UIBarButtonItem) {
    
    let state = UserDefaults.standard.bool(forKey: "TOCBaseViewControllerOutlineMode")
    
    setOutlineView(!state)
    UserDefaults.standard.set(!state, forKey: "TOCBaseViewControllerOutlineMode")
  }
  
  func setOutlineView(_ flag: Bool = true) {
    if flag == true {
      barButtonItem = UIBarButtonItem(image: UIImage(named: "ViewTable"), style: .plain, target: self, action: #selector(viewChanged(_:)))
      let controller = TOCTableViewController(viewModel: viewModel, viewSize: viewSize)
      viewControllers = [controller]
      controller.navigationItem.rightBarButtonItem = barButtonItem
      
    }else {
      barButtonItem = UIBarButtonItem(image: UIImage(named: "ViewOutline"), style: .plain, target: self, action: #selector(viewChanged(_:)))
      let controller = TOCOutlineViewController(viewModel: viewModel, viewSize: viewSize)
      controller.lawNum = viewModel.source?.law.lawNum ?? ""
      viewControllers = [controller]
      controller.navigationItem.rightBarButtonItem = barButtonItem
      
    }
  }
}



class TOCOutlineViewController: UIViewController, MNOutlineViewDelegate, MNOutlineViewDataSource {

  let realm: Realm? = {

    
    
    do {
      let dir = FileManager.default.appSupportUrl
      if FileManager.default.fileExists(atPath: dir.path, isDirectory: nil) == false {
        try FileManager.default.createDirectory(at: dir, withIntermediateDirectories: true, attributes: nil)
      }
      let url =  dir.appendingPathComponent("OutlineClosed.realm")
      let config = Realm.Configuration(fileURL: url, encryptionKey: nil, readOnly: false, objectTypes:[OutlineClosed.self])

      let realm = try Realm(configuration: config)
      return realm
      
    }catch let error {
      print("*** \(error.localizedDescription)")
    }
    
    return nil
  }()
  
  var closedArray: RealmSwift.Results<OutlineClosed>? = nil
  var lawNum: String = ""
  {
    didSet {
      let predicate = NSPredicate(format: "lawNum LIKE %@", lawNum)
      
      let existingObj = realm?.objects(OutlineClosed.self).filter(predicate)
      
      if existingObj != nil {
        closedArray = existingObj
      }else {
        closedArray = nil
      }
    }
  }
  
  var viewModel: TOCViewModel!
  var hierachicalToc: [TocDict] = []
  lazy var outlineView: MNOutlineView! = { [weak self] in
    guard let strongSelf = self else { return nil }
    let bounds = strongSelf.view.bounds
    let outlineView = MNOutlineView(frame: bounds)
    outlineView.delegate = strongSelf
    outlineView.dataSource = strongSelf
    outlineView.setAllowsMultipleSelectionDuringEditing(true)
    outlineView.setLawSeparatorColor(strongSelf.viewModel.backgroundColorScheme.subTextColor)
    MNOutlineView.setFlatTintColor(strongSelf.viewModel.backgroundColorScheme.tintColor)
    outlineView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    outlineView.showGripperOnlyWhenEditing = true
    outlineView.canCollapse = false
    outlineView.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
    strongSelf.view.addSubview(outlineView)
    return outlineView
    }()
  
  init( viewModel: DetailViewModel?, viewSize: CGSize, title: String? = nil  ) {
    self.viewModel = TOCViewModel()
    super.init(nibName: nil, bundle: nil)
    self.viewModel.viewSize = viewSize
    //      self.viewModel.styleSheet = viewModel.styleSheet
    
    if let toc = viewModel?.source?.tocs {
      self.viewModel.setTocs(Array(toc))
    }else {
      self.viewModel.setTocs( [] )
    }
    
    hierachicalToc = self.viewModel.hierachicalToc
    self.title = title == nil ? WLoc("TOC") : title
  }
  
  override init( nibName:String?, bundle:Bundle? ) {
    super.init( nibName: nil, bundle:bundle)
  }
  
  init() {
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder:aDecoder)
  }

  
  override func viewDidLoad() {
    outlineView.backgroundColor = viewModel.backgroundColorScheme.secondaryBackgroundColor
    
    outlineView.dataSource = self
    outlineView.delegate = self
    outlineView.reloadData()
  }
  
  func outlineCell(item: TocDict) -> UIView {
    let backgroundColorScheme = viewModel.backgroundColorScheme
    
    let baseView = UIView(frame: CGRect(x: 0,y: 0, width: 320, height: 44))
    baseView.autoresizingMask = .flexibleWidth
    baseView.backgroundColor = UIColor.clear
    
    let label = UILabel(frame: CGRect(x: 0,y: 4, width: 320, height: 44 - 8))
    label.autoresizingMask = .flexibleWidth
    label.lineBreakMode = .byTruncatingMiddle
    label.minimumScaleFactor = 0.8
    label.adjustsFontSizeToFitWidth = true
    label.numberOfLines = 0
    label.allowsDefaultTighteningForTruncation = true
    
    baseView.addSubview( label )
    
    
    let (attributedString, subtitle, indentLevel) = item.bodyAttributedString(at: nil, backgroundColorScheme: viewModel.backgroundColorScheme)
    
    let attr = NSMutableAttributedString(attributedString: attributedString)
    attr.append(subtitle)
    label.attributedText = attr
    
    return baseView
  }
  
  func outlineView(_ outlineView: MNOutlineView!, viewForItem item: Any!, forRowAt indexPath: IndexPath!) -> UIView! {

    let baseView = outlineCell(item: item as! TocDict)
    return baseView
  }
  
  func outlineView(_ outlineView: MNOutlineView!, child index: Int, ofItem item: Any!) -> Any! {
    if item == nil {
     return hierachicalToc[index]
    }

    return (item as! TocDict).children[index]
  }
  
  func outlineView(_ outlineView: MNOutlineView!, isItemExpandable item: Any!) -> Bool {
    guard let item = item as? TocDict else { return false }
    return item.children.count > 0
  }
  
  func outlineView(_ outlineView: MNOutlineView!, isItemExpanded item: Any!) -> Bool {
    guard let item = item as? TocDict else { return false }
    
    
    let anchor = item.anchor ?? ""
    let predicate = NSPredicate(format: "lawNum LIKE %@ && anchor LIKE  %@", lawNum, anchor)
    
    let existingObj = closedArray?.filter(predicate).first

    if existingObj?.closed == true {
      return false
    }
    return true
  }
  
  func outlineView(_ outlineView: MNOutlineView!, numberOfChildrenOfItem item: Any!) -> Int {
    if item == nil { return hierachicalToc.count }
    guard let item = item as? TocDict else { return 0 }
    return item.children.count
  }
  
  func outlineView(_ outlineView: MNOutlineView!, moveItem item: Any!, toItem parentItem: Any!, at index: Int) {
    
  }
  
  func outlineView(_ outlineView: MNOutlineView!, setItem item: Any!, expanded flag: Bool) {
    
    let anchor = ((item as! TocDict).anchor ?? "")
    let predicate = NSPredicate(format: "lawNum LIKE %@ && anchor LIKE  %@", lawNum, anchor)

    let existingObj = closedArray?.filter(predicate).first
    
    
    try? realm?.write {
      
      if existingObj != nil {
        existingObj!.closed = !flag
      }else {
        let obj = OutlineClosed()
        obj.lawNum = lawNum
        obj.anchor = anchor
        obj.closed = !flag
        realm?.add(obj)
      }
    }
    UserDefaults.standard.set(!flag, forKey: "OutlineClosed " + lawNum + ((item as! TocDict).anchor ?? ""))
    
  }

  func outlineView(_ outlineView: MNOutlineView!, heightForRowAt index: UInt, ofItem parentItem: Any!) -> CGFloat {
    return 44
  }
  
  func outlineView(_ outlineView: MNOutlineView!, didSelectItem item: Any!, in cell: MNOutlineViewCell!, at indexPath: IndexPath!) {
    (navigationController as? TOCBaseViewController)?.jumpAction?(item as! TocDict)

  }
}


  class TOCTableViewController: UITableViewController {
    var viewModel: TOCViewModel!
    var hierachicalToc: [TocDict] = []

    var backgroundColorScheme: BackgroundColorSchemeType {
      let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
      let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
      return backgroundColorScheme
    }
    
    init( viewModel: DetailViewModel?, viewSize: CGSize, title: String? = nil  ) {
      self.viewModel = TOCViewModel()
      super.init(nibName: nil, bundle: nil)
      self.viewModel.viewSize = viewSize
//      self.viewModel.styleSheet = viewModel.styleSheet
      
      if let toc = viewModel?.source?.tocs {
        self.viewModel.setTocs(Array(toc))
      }else {
        self.viewModel.setTocs( [] )
      }
      
      hierachicalToc = self.viewModel.hierachicalToc
      
     self.title = title == nil ? WLoc("TOC") : title

    }
    
    override init( nibName:String?, bundle:Bundle? ) {
      super.init( nibName: nil, bundle:bundle)
    }
    
    init() {
      super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
      super.init(coder:aDecoder)
    }

    override func viewDidLoad() {
      super.viewDidLoad()
      
      tableView.backgroundColor = backgroundColorScheme.secondaryBackgroundColor
      tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: self.tabBarController!.tabBar.frame.height, right: 0)
      
      if backgroundColorScheme.inverted {
        tableView.indicatorStyle = .white
        tableView.separatorColor = UIColor(white: 1, alpha: 0.3)
      }
      
    }

    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
      return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return max( 1, hierachicalToc.count )
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
      if hierachicalToc.count == 0 {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel!.text = WLoc("No Toc")
        cell.textLabel!.textAlignment = NSTextAlignment.center
        cell.textLabel!.textColor  = backgroundColorScheme.subTextColor
        cell.isUserInteractionEnabled = false
        cell.backgroundColor = UIColor.clear
        
        return cell
      }
      
      let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") ?? UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
      
      cell.backgroundColor = UIColor.clear
      
      let (attributedString, subtitle, indentLevel) = hierachicalToc[indexPath.row].bodyAttributedString(at: nil, backgroundColorScheme: viewModel.backgroundColorScheme)
      cell.textLabel!.attributedText = attributedString
      cell.textLabel?.numberOfLines = 0
      cell.textLabel?.minimumScaleFactor = 0.5
      cell.accessoryView = nil
      cell.indentationLevel = indentLevel
      if subtitle.length != 0 {
        cell.detailTextLabel!.attributedText = subtitle
      }else {
        cell.detailTextLabel!.attributedText = nil
      }
      
      if hierachicalToc[indexPath.row].children.count > 0 {
        let button = UIButton(type: .custom)
        let image = UIImage(named: "TOCMore")?.colorizedImage(withTint: viewModel.backgroundColorScheme.tintColor, alpha: 1, glow: false)
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(accessoryTapped(_:)), for: .touchUpInside)
        button.tag = indexPath.row
        button.sizeToFit()
        cell.accessoryView = button
      }
      
      if backgroundColorScheme.inverted {
        let view = UIView()
        view.backgroundColor = backgroundColorScheme.tableSelectionColor
        cell.selectedBackgroundView = view
      }
      
      return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
      let dict = hierachicalToc[indexPath.row]
      (navigationController as? TOCBaseViewController)?.jumpAction?(dict)
    }
    
    @IBAction func accessoryTapped(_ sender: UIButton?) {
      guard let row = sender?.tag else { return }
      let source = hierachicalToc[row].children
      let (attributedString, _, _) = hierachicalToc[row].bodyAttributedString(at: nil, backgroundColorScheme: viewModel.backgroundColorScheme)

      let controller = TOCTableViewController(viewModel: nil, viewSize: viewModel.viewSize, title: attributedString.string)
      controller.hierachicalToc = source
      navigationController?.pushViewController(controller, animated: true)
    }
    
  }


#else

import Cocoa

  class TOCTableViewControllerMac: NSViewController, NSOutlineViewDelegate, NSOutlineViewDataSource {
    let realm: Realm? = {
      let dir = FileManager.default.appSupportUrl
      
      if FileManager.default.fileExists(atPath: dir.path, isDirectory: nil) == false {
        try? FileManager.default.createDirectory(at: dir, withIntermediateDirectories: true, attributes: nil)
      }
      
      let url =  dir.appendingPathComponent("OutlineClosed.realm")
      let config = Realm.Configuration(fileURL: url, encryptionKey: nil, readOnly: false, objectTypes:[OutlineClosed.self])
      
      let realm = try? Realm(configuration: config)
      return realm
    }()
    
    var closedArray: RealmSwift.Results<OutlineClosed>? = nil
    var lawNum: String = ""
    {
      didSet {
        let predicate = NSPredicate(format: "lawNum LIKE %@", lawNum)
        
        let existingObj = realm?.objects(OutlineClosed.self).filter(predicate)
        
        if existingObj != nil {
          closedArray = existingObj
        }else {
          closedArray = nil
        }
      }
    }
    
    var viewModel: TOCViewModel!
    var hierachicalToc: [TocDict] = []
    var jumpAction: ((_ tocDict: TocDict)->Void)? = nil
    
    @IBOutlet weak var tableView: NSOutlineView!
    
    init( viewModel: DetailViewModel?, viewSize: CGSize  ) {
      self.viewModel = TOCViewModel()
      super.init(nibName: "TOCTableViewController", bundle: nil)
      self.viewModel.viewSize = viewSize
//      self.viewModel.styleSheet = viewModel.styleSheet
      
      if let toc = viewModel?.source?.tocs {
        self.viewModel.setTocs(Array(toc))
      }else {
        self.viewModel.setTocs( [] )
      }
      
      hierachicalToc = self.viewModel.hierachicalToc
    }
    
    required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
      super.viewDidLoad()
      
      tableView.backgroundColor = UIColor.clear
//      tableView.contentInset = NSEdgeInsetsMake(0, 0, self.tabBarController!.tabBar.frame.height, 0)
//      tableView.scrollIndicatorInsets = NSEdgeInsetsMake(viewModel.headerHeight, 0, self.tabBarController!.tabBar.frame.height, 0)
      
      if viewModel.backgroundColorScheme.inverted {
//        tableView.indicatorStyle = .white
//        tableView.separatorColor = UIColor(white: 1, alpha: 0.3)
      }
      
      let center = NotificationCenter.default
      center.addObserver(self, selector: #selector(reload(_:)), name: LawNotification.settingsDidChange, object: nil)
    }
  
    @objc func reload(_ notification: Notification) {
      tableView?.reloadData()
      
//      tableView?.enclosingScrollView?.scrollerKnobStyle = .dark
    }
    
    // MARK: - Outline view data source
    func outlineView(_ outlineView: NSOutlineView, numberOfChildrenOfItem item: Any?) -> Int {
      if item == nil { return hierachicalToc.count }
      guard let item = item as? TocDict else { return 0 }
      return item.children.count
    }
    
    func outlineView(_ outlineView: NSOutlineView, shouldExpandItem item: Any) -> Bool {
      self.outlineView(outlineView, setItem: item, expanded: true)
      return true
    }
    
    func outlineView(_ outlineView: NSOutlineView, shouldCollapseItem item: Any) -> Bool {
      self.outlineView(outlineView, setItem: item, expanded: false)
     return true
    }
    
    func outlineView(_ outlineView: NSOutlineView!, setItem item: Any!, expanded flag: Bool) {
      
      let anchor = ((item as! TocDict).anchor ?? "")
      let predicate = NSPredicate(format: "lawNum LIKE %@ && anchor LIKE  %@", lawNum, anchor)
      
      let existingObj = closedArray?.filter(predicate).first
      
      
      try? realm?.write {
        
        if existingObj != nil {
          existingObj!.closed = !flag
        }else {
          let obj = OutlineClosed()
          obj.lawNum = lawNum
          obj.anchor = anchor
          obj.closed = !flag
          realm?.add(obj)
        }
      }
      UserDefaults.standard.set(!flag, forKey: "OutlineClosed " + lawNum + ((item as! TocDict).anchor ?? ""))
      
    }
    
    func outlineView(_ outlineView: NSOutlineView, willDisplayCell cell: Any, for tableColumn: NSTableColumn?, item: Any) {
      guard let cell = cell as? NSTextFieldCell else { return }
      let item = item as! TocDict
      let (attributedString, subtitle, _) = item.bodyAttributedString(at: nil, backgroundColorScheme: viewModel.backgroundColorScheme)
      
      let attr = NSMutableAttributedString(attributedString: attributedString)
      attr.append(subtitle)
      cell.attributedStringValue = attr
    }

    func outlineView(_ outlineView: NSOutlineView, child index: Int, ofItem item: Any?) -> Any {
      if item == nil {
        return hierachicalToc[index]
      }
      
      return (item as! TocDict).children[index]
    }
    
    func outlineView(_ outlineView: NSOutlineView, isItemExpandable item: Any) -> Bool {
      guard let item = item as? TocDict else { return false }
      
      return item.children.count > 0
    }
    
    func outlineView(_ outlineView: NSOutlineView!, isItemExpanded item: Any!) -> Bool {
      guard let item = item as? TocDict else { return false }
      
      
      let anchor = item.anchor ?? ""
      let predicate = NSPredicate(format: "lawNum LIKE %@ && anchor LIKE  %@", lawNum, anchor)
      
      let existingObj = closedArray?.filter(predicate).first
      
      if existingObj?.closed == true {
        return false
      }
      return true
    }
    
    func outlineView(_ outlineView: NSOutlineView!, moveItem item: Any!, toItem parentItem: Any!, at index: Int) {
      
    }
    
    func outlineView(_ outlineView: NSOutlineView, shouldEdit tableColumn: NSTableColumn?, item: Any) -> Bool {
     return false
    }
    func outlineView(_ outlineView: NSOutlineView, shouldSelectItem item: Any) -> Bool {
      jumpAction?(item as! TocDict)
      return true
    }
    
}



#endif

