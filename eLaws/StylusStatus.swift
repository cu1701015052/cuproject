//
//  StylusStatus.swift
//  LawXML
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation


class StylusStatus {
  
  let presetStylusNumbers = 0..<5
  
  var highlightByStylusDrawing = false
  var highlightByStylusAttributes: [NSAttributedString.Key: Any] {
    if highlightByStylusHighlightColor == .mask {
      let  attrs = [.mask: shortUUIDString()] as [NSAttributedString.Key : Any]
      
      return attrs
      
    }else if highlightByStylusHighlightColor.isUnderline == true {
      let  attrs = [NSAttributedString.Key(rawValue: "MNTempUStyle"): 0x02 /*kCTUnderlineStyleThick*/,
        NSAttributedString.Key(rawValue: "MNTempUColor"): highlightByStylusHighlightColor.color().cgColor] as [NSAttributedString.Key : Any]
      
      return attrs
      
    }else {
      return [ NSAttributedString.Key.highlightColor: highlightByStylusHighlightColor.color().cgColor]
    }
  }
  
  var highlightByStylusHighlightColor: HighlightColor {
    get {
    let ud = UserDefaults.standard
    let tag = ud.integer(forKey: "StylusStatusPenSelected")
    let key = "StylusStatusPen\(tag)"
    
    let color =  HighlightColor(rawValue: ud.integer(forKey: key))
    return color
    }

  }
  
  init() {
    
  }
  
  
  func color(for tag: Int) -> HighlightColor {
    let ud = UserDefaults.standard
    let key = "StylusStatusPen\(tag)"
    
    if nil == ud.object(forKey: key) {
      ud.set(HighlightColor(rawValue: tag).rawValue, forKey: key)
    }
    ud.synchronize()
    
    return HighlightColor(rawValue: ud.integer(forKey: key))
  }
  
  func setColor(_ color: HighlightColor, forTag tag: Int) {
    guard presetStylusNumbers.contains(tag) else { return }
    let key = "StylusStatusPen\(tag)"
    let ud = UserDefaults.standard
    ud.set(color.rawValue, forKey: key)
    
  }
  
  func isSelected(_ tag: Int) -> Bool {
    guard presetStylusNumbers.contains(tag) else { return false  }
    let key = "StylusStatusPenSelected"
    let ud = UserDefaults.standard
    
    if nil == ud.object(forKey: key) {
      ud.set(tag, forKey: key)
    }
    
    return ud.integer(forKey: key) == tag
  }
  
  func select(_ tag: Int) {
    guard presetStylusNumbers.contains(tag) else { return  }
    
    let key = "StylusStatusPenSelected"
    let ud = UserDefaults.standard
    ud.set(tag, forKey: key)
    
  }
  
  
  var selectedTag: Int {
    let key = "StylusStatusPenSelected"
    let ud = UserDefaults.standard
    
    if nil == ud.object(forKey: key) {
      ud.set(0, forKey: key)
    }
    
    return ud.integer(forKey: key)
  }
  
  func penImage(for color: HighlightColor) -> UIImage {
    let image: UIImage
    let maskImage: UIImage
    
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    
    if color.isUnderline {
      if backgroundColorScheme.inverted {
        image = UIImage(named:"pen0dark")!
        
      }else {
        image = UIImage(named:"pen0")!
      }
      
      maskImage = UIImage(named:"pen0mask")!.colorizedImage(withTint: color.color(), alpha: 1, glow: false)
      
    }else {
      if backgroundColorScheme.inverted {
        image = UIImage(named:"pen1dark")!
        
      }else {
        image = UIImage(named:"pen1")!
      }
      maskImage = UIImage(named:"pen1mask")!.colorizedImage(withTint: color.color(), alpha: 1, glow: false)
      
    }
    
    #if os(iOS)
    
    UIGraphicsBeginImageContextWithOptions(image.size, false, 0)
    
    image.draw(at: .zero)
    maskImage.draw(at: .zero)
    
    let compositedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    #endif
    
    #if os(OSX)
    let compositedImage: NSImage? = NSImage(size: image.size)
    compositedImage?.lockFocus()
    let bounds = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.width)
    image.draw(in: bounds)
    maskImage.draw(in: bounds)
    
    compositedImage?.unlockFocus()
    
    #endif
    
    return compositedImage!
  }
}
