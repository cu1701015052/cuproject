//
//  DownloadManager.swift
//  https://github.com/mzeeshanid/MZDownloadManager
//
//

#if os(OSX)
  import Cocoa
#elseif os(iOS)
  import UIKit
#endif

let kMZDownloadKeyURL = "URL"
let kMZDownloadKeyStartTime = "startTime"
let kMZDownloadKeyFileName = "fileName"
let kMZDownloadKeyProgress = "progress"
let kMZDownloadKeyTask = "downloadTask"
let kMZDownloadKeyStatus = "requestStatus"
let kMZDownloadKeyDetails = "downloadDetails"
let kMZDownloadKeyResumeData = "resumedata"
let RequestStatusDownloading = "RequestStatusDownloading"
let RequestStatusPaused = "RequestStatusPaused"
let RequestStatusFailed = "RequestStatusFailed"

class DownloadInfo {
	var URL: URL?
	var name: String
	var date: Date
	var status: String
	var task: URLSessionTask?
	var progress_: Double?
  var maxByte: Int?
	var data: Data?
	var progressHandler: (_ progress: Double) -> Void
	var completionHander: (_ success: Bool, _ data: Data?, _ error: Error?) -> Void
	
  init(URL: URL?, name: String, maxByte: Int? = nil, task: URLSessionTask? = nil, progressHandler: @escaping (_ progress: Double) -> Void, completionHander: @escaping (_ success: Bool, _ data: Data?, _ error: Error?) -> Void) {
		self.URL = URL
		self.name = name
    self.maxByte = maxByte
		self.date = Date()
		self.status = RequestStatusDownloading
		self.task = task
		self.completionHander = completionHander
		self.progressHandler = progressHandler
	}
	
	var progress: Double? {
    get {
      return progress_
    }
		set {
			progress_ = newValue
			
			if newValue != nil {
        progressHandler( newValue! )
      }
    }
  }
  
  var taskDescription: String {
    get {
      
      let dict: [String: Any] = [kMZDownloadKeyURL: URL?.path ?? "", kMZDownloadKeyFileName: name]
      
      let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
      let jsonString = NSString(data: jsonData!, encoding: String.Encoding.utf8.rawValue)
      return jsonString! as String
    }
  }
}

class DownloadManager: NSObject, URLSessionDelegate, URLSessionDownloadDelegate, URLSessionDataDelegate {
	
	var downloadingArray: [DownloadInfo] = []
	var session: URLSession? = nil
	
  static var shared = DownloadManager()
		
  override init() {
    super.init()
    URLCache.shared.diskCapacity = 0
    URLCache.shared.removeAllCachedResponses()
  }
	
  func downloadSession() -> URLSession {
    
    if session == nil {
      let configuration = URLSessionConfiguration.default
      #if os(iOS)
        let ua = ObjectiveCClass.userAgentString()
        configuration.httpAdditionalHeaders = ["User-Agent": ua]
      #endif
      session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
    }
		return session!
	}

//  func addDownloadTask(_ name: String, request: URLRequest, progressHandler: @escaping (_ progress: Double) -> Void, completionHandler: @escaping (_ success: Bool, _ data: Data?, _ error: Error?) -> Void) -> DownloadInfo {
//    let session = downloadSession()
//    session.configuration.httpShouldSetCookies = true
//    var request = request
//    request.httpShouldHandleCookies = true
//
//    //request.addValue(sessionID, forHTTPHeaderField: "JSESSIONID")
//    let downloadTask = session.dataTask(with: request) {data, response, error in
//
//      if let httpResponse = response as? HTTPURLResponse, let fields = httpResponse.allHeaderFields as? [String : String] {
//
//        let cookies = HTTPCookie.cookies(withResponseHeaderFields: fields, for: response!.url!)
//        HTTPCookieStorage.shared.setCookies(cookies, for: response!.url!, mainDocumentURL: nil)
//        for cookie in cookies {
//          var cookieProperties = [HTTPCookiePropertyKey: Any]()
//          cookieProperties[.name] = cookie.name
//          cookieProperties[.value] = cookie.value
//          cookieProperties[.domain] = cookie.domain
//          cookieProperties[.path] = cookie.path
//          cookieProperties[.version] = NSNumber(value: cookie.version)
//          cookieProperties[.expires] = NSDate().addingTimeInterval(31536000)
//
//          let newCookie = HTTPCookie(properties: cookieProperties)
//          HTTPCookieStorage.shared.setCookie(newCookie!)
//
//          print("name: \(cookie.name) value: \(cookie.value)")
//        }
//      }
//
//
//      if let cookies = HTTPCookieStorage.shared.cookies(for: request.url!) {
//        for cookie in cookies {
//          print("cookie \(cookie)")
//        }
//      }
//
//    }
//
//    downloadTask.resume()
//
//    let downloadInfo = DownloadInfo(URL: request.url!, name: name, task: downloadTask, progressHandler: progressHandler, completionHander: completionHandler)
//    downloadTask.taskDescription = downloadInfo.taskDescription
//
//    // Download started
//    downloadingArray.append(downloadInfo)
//    return downloadInfo
//  }
  
  func addDownloadTask(_ name: String, URL: URL, maxByte: Int? = nil, progressHandler: @escaping (_ progress: Double) -> Void, completionHandler: @escaping (_ success: Bool, _ data: Data?, _ error: Error?) -> Void) -> DownloadInfo {
    
    let request = URLRequest(url: URL, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 60.0)
    
    let session = downloadSession()
    
    if maxByte != nil {
      let dataTask = session.dataTask(with: request)
      
      let downloadInfo = DownloadInfo(URL: URL, name: name, maxByte: maxByte, task: dataTask, progressHandler: progressHandler, completionHander: completionHandler)
      dataTask.taskDescription = downloadInfo.taskDescription
      dataTask.priority = 1.0
      
      // Download started
      downloadingArray.append(downloadInfo)
      dataTask.resume()

      return downloadInfo
      
    }else {
      
      let downloadTask = session.downloadTask(with: request)
      downloadTask.priority = 1.0

      let downloadInfo = DownloadInfo(URL: URL, name: name, maxByte: maxByte, task: downloadTask, progressHandler: progressHandler, completionHander: completionHandler)
      downloadTask.taskDescription = downloadInfo.taskDescription
      
      // Download started
      downloadingArray.append(downloadInfo)
      downloadTask.resume()
      return downloadInfo
    }
  }
  
	//MARK:-  NSURLSession Delegates
	
	func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
		for info in downloadingArray {
			if downloadTask !==  info.task { continue }
      
			let progress = Double(downloadTask.countOfBytesReceived)/Double(downloadTask.countOfBytesExpectedToReceive)
			info.progress = progress

			break
		}
	}

  
	func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
		for info in downloadingArray {
			if downloadTask !== info.task { continue }
			
			// Finish
			
			print("Finish @ \(location)")
			let theData = try? Data(contentsOf: location, options: .uncached)
			info.data = theData
			break
		}
	}
	
	func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
		for info in downloadingArray {
			if info.task !== task { continue }
			var success = true
			
			if error != nil { success = false }
			info.completionHander( success, info.data, error )
			
			if let error = error as NSError? {
				
				if error.code != NSURLErrorCancelled {
					info.status = RequestStatusFailed as String
					
					// Error
					print("Error @ \(info)")
				}
			}else {
				
				// Complete
				print("Complete @ \(info)")
			}
			break
		}
	}
  
	// DATA DELEGATE

  
  func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
    print("@ received \(data.count)")
    for info in downloadingArray {
      if dataTask !== info.task { continue }
      
      // Finish
      if info.data == nil {
        info.data = data
      }else {
        info.data! += data
      }
      
      if info.maxByte != nil && info.data!.count > info.maxByte! {
        dataTask.cancel()
      }
      break
    }
  }
  
//  func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, willCacheResponse proposedResponse: CachedURLResponse, completionHandler: @escaping (CachedURLResponse?) -> Void) {
//    for info in downloadingArray {
//      if info.task !== dataTask { continue }
//
//      info.completionHander( true, info.data, nil )
//
//      print("Complete @ \(info)")
//      completionHandler(nil)
//      break
  //    }
  //
  //  }
}

class UnzippingUtil {
  private var uuid = shortUUIDString()
  
  func unzipData(_ zippedData: Data, completion:@escaping (_ decompressedAtUrl: URL?, _ xmlData: Data?, _ error: Error?)->Void) {
    let tempPath = (NSTemporaryDirectory() as NSString).appendingPathComponent("_tempUnzippedFile\(uuid)")
    let tempZipPath = (NSTemporaryDirectory() as NSString).appendingPathComponent("_tempZippedFile\(uuid)")

    var destUrl: URL? = URL(fileURLWithPath: tempPath)
    var xmlData: Data? = nil
    
    // LOOK FOR XML
    
    if FileManager.default.fileExists(atPath: tempPath) {
      try! FileManager.default.removeItem(atPath: tempPath)
    }
    let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
    queue.async {
      do {
        try zippedData.write(to: URL(fileURLWithPath: tempZipPath))
        ZipArchive.unzipFile(at: tempZipPath, to: tempPath)
        
        let unzippedContents = try FileManager.default.contentsOfDirectory(at: destUrl!, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles)
        
        destUrl = nil
        for item in unzippedContents {
          if item.lastPathComponent.hasPrefix("_") == false &&  item.lastPathComponent.hasPrefix(".") == false {
            destUrl = item
            break;
          }
        }
        
        if destUrl == nil {
          DispatchQueue.main.async {
            completion(nil, nil, LawXMLError.fileError)
            if FileManager.default.fileExists(atPath: tempPath) {
              try! FileManager.default.removeItem(atPath: tempPath)
            }
            if FileManager.default.fileExists(atPath: tempZipPath) {
              try! FileManager.default.removeItem(atPath: tempZipPath)
            }
          }
          return
        }
        
        guard let xmlPath = FileManager.default.subpaths(atPath: destUrl!.path)?.filter ({ $0.hasSuffix(".xml") }).first else {
          DispatchQueue.main.async {
            completion(nil, nil, LawXMLError.unknown)
            if FileManager.default.fileExists(atPath: tempPath) {
              try! FileManager.default.removeItem(atPath: tempPath)
            }
            if FileManager.default.fileExists(atPath: tempZipPath) {
              try! FileManager.default.removeItem(atPath: tempZipPath)
            }
          }
          return
        }
        

        
        xmlData = try Data(contentsOf: destUrl!.appendingPathComponent(xmlPath))
        DispatchQueue.main.async {
          _ = destUrl?.setExcludedFromBackup()
          completion(destUrl, xmlData, nil)
        }
      }catch let error {
        DispatchQueue.main.async {
          completion(nil, nil, error)
          if FileManager.default.fileExists(atPath: tempPath) {
            try! FileManager.default.removeItem(atPath: tempPath)
          }
          if FileManager.default.fileExists(atPath: tempZipPath) {
            try! FileManager.default.removeItem(atPath: tempZipPath)
          }
        }
      }
    }
  }
}
