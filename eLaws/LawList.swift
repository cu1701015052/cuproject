//
//  LawList.swift
//  LawXML
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
import Realm
import RealmSwift


class LawList {

  lazy var realm: Realm! = {
    // LOOK FOR DESKTOP ON MAC

    let dbUrl = Bundle.main.url(forResource: "tf-idf-index", withExtension: "realm")
    let config = Realm.Configuration(fileURL: dbUrl, encryptionKey: nil, readOnly: true, objectTypes:[EGovLaw.self, EGovLawLink.self])
    
    do {
      let realm = try Realm(configuration: config)
      return realm
    }catch let err {
      print(err.localizedDescription)
    }
    return nil
  }()
  
  func searchLaw(_ string: String) -> [EGovLaw]? {
    if string.isEmpty { return [] }
    if string.utf16.count < 2 {
      let predicate2 = NSPredicate(format: "lawTitle CONTAINS %@ ", string)
      let results2 = Array(realm.objects(EGovLaw.self).filter(predicate2).sorted(byKeyPath: "filename", ascending: false))
      return results2
    }
    
    if string.range(of: " ") != nil ||  string.range(of: "　") != nil {
      
      let array = string.components(separatedBy: CharacterSet.whitespaces)
      let predicates = array.filter({$0.isEmpty == false}).map { NSPredicate(format: "lawTitle CONTAINS %@ ", $0) }
      let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
      let results = Array(realm.objects(EGovLaw.self).filter(predicate).sorted(byKeyPath: "filename", ascending: false)
)
      return results

      
    }else {
      if string == "平成" ||  string == "昭和" || string == "大正" || string == "明治" || string == "令和" { return [] }
      let predicate0 = NSPredicate(format: "(lawTitle == %@ || lawNum == %@)", string, string)
      let results0 = Array(realm.objects(EGovLaw.self).filter(predicate0).sorted(byKeyPath: "filename", ascending: false))
      
      let predicate1 = NSPredicate(format: "(lawTitle BEGINSWITH %@ || lawNum BEGINSWITH %@)", string, string)
      let results1 = Array(realm.objects(EGovLaw.self).filter(predicate1).sorted(byKeyPath: "filename", ascending: false))
      
//      results1 = results1.sorted { $0.lawTitle.utf16.count < $1.lawTitle.utf16.count } //ここでソートが狂ってしまう。
      
      let predicate2 = NSPredicate(format: "(lawTitle CONTAINS %@ || lawNum CONTAINS %@)", string, string)
      let results2 = Array(realm.objects(EGovLaw.self).filter(predicate2).sorted(byKeyPath: "filename", ascending: false))
      
      var results = results0
      results1.forEach { if results.contains($0) == false { results.append($0) } }
      results2.forEach { if results.contains($0) == false { results.append($0) } }
      return results
      
    }
  }
  
  func lawsWithNameLike(_ string: String, distinct: Bool = false, limit: Int? = nil) -> RealmSwift.Results<EGovLaw>? {
    if distinct == false {
      return realm.objects(EGovLaw.self).filter(NSPredicate(format: "lawTitle == %@", string))
    }
    
    return realm.objects(EGovLaw.self).filter(NSPredicate(format: "lawTitle == %@", string)).distinct(by: ["lawTitle"])
  }
  
  func lawsWithNum(_ string: String, orderByFilenameAscending: Bool = true) -> RealmSwift.Results<EGovLaw>? {

    return realm.objects(EGovLaw.self).filter(NSPredicate(format: "lawNum == %@", string)).sorted(byKeyPath: "filename", ascending: orderByFilenameAscending)
  }
  
  func lawsWithEdition(_ string: String) -> RealmSwift.Results<EGovLaw>? {
    if string.isEmpty { return nil }
    return realm.objects(EGovLaw.self).filter(NSPredicate(format: "lawEdition == %@", string))
  }
  
  var allLaws: RealmSwift.Results<EGovLaw> {
    return realm.objects(EGovLaw.self)
  }
  
  func lawsWithSuffix(_ string: String, distinct: Bool = false) -> RealmSwift.Results<EGovLaw>? {
    
    if distinct == false {
      return realm.objects(EGovLaw.self).filter(NSPredicate(format: "lawTitle ENDSWITH %@", string))
    }
    
    return realm.objects(EGovLaw.self).filter(NSPredicate(format: "lawTitle ENDSWITH %@", string)).distinct(by: ["lawTitle"])
  }
  
}


