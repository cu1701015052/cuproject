//
//  Selection.swift
//  MacParser
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

final class SelectionObject: Object {

  //MARK:-
  var law: DownloadedLaw! {
    didSet {
      if law != nil {
      self.lawNo = law.lawNum
//      self.lawEdition = law.lawEdition
//      self.lawTitle = law.lawTitle
      }
    }
  }
  @objc dynamic var uuid: String = ""

  @objc dynamic var lawNo: String = ""
//  @objc dynamic var lawTitle: String = ""
//  @objc dynamic var lawEdition: String = ""
  @objc dynamic var style: Int8 = 0
  @objc dynamic var notes: String? = nil
//  @objc dynamic var text: String = "" // WHOLE TEXT FOR SEARCHING

  @objc dynamic var row: Int = 0
  @objc dynamic var startIndexInRow: Int = 0
  
  @objc dynamic var startAnchor: String = ""
  //let startJi = RealmOptional<Int>()
  //@objc dynamic var startMoji: String? = nil
  //let startMojiIndex = RealmOptional<Int>()
  @objc dynamic var startString: String = ""
  @objc dynamic var startStringOccurrenceIndex: Int = -1
  
  @objc dynamic var endAnchor: String = ""
  @objc dynamic var endString: String? = nil //OPTIONAL

  //let endJi = RealmOptional<Int>()
  //@objc dynamic var endMoji: String? = nil
  //let endMojiIndex = RealmOptional<Int>()
  //@objc dynamic var endStringOccurrenceIndex: Int = -1 //OPTIONAL

  
  
   func titleToDisplay() -> String {
    return titleToUse()
  }
  
   func titleToUse() -> String {
    
    let array = startAnchor.components(separatedBy: "/")
    var humanReadable = ""
    for obj in array {
      let str = ProcessSentenceXML.humanReadableAnchor(obj)
      humanReadable += str
    }
    humanReadable = humanReadable.replacingOccurrences(of: "条第", with: "条")
    humanReadable = humanReadable.replacingOccurrences(of: "項第", with: "項")

    return humanReadable
  }
  
  //MARK:-
  override static func primaryKey() -> String? {
    return "uuid"
  }
  
   override static func indexedProperties() -> [String] {
    return super.indexedProperties() + ["lawNo", /*"lawEdition",*/ "startAnchor", "endAnchor"]
  }
  
  override static func ignoredProperties() -> [String] {
    return super.ignoredProperties() + ["law"]
  }
  
  var highlightColor: HighlightColor {
    return HighlightColor(rawValue: Int(style))
  }
  
  func range(in attributedString: NSAttributedString, anchor: String) -> (NSRange, Bool) {
    guard self.isInvalidated == false else { return (NSMakeRange(NSNotFound, 0), false) }
    var hasNote = false
    var targetRange = NSMakeRange(0, attributedString.length)
    var range = NSMakeRange(0, attributedString.length)

    // GET PROVISO RANGE
    var foundRange = false
    var idx = 0
    while idx < attributedString.length {
      if anchor == attributedString.attribute(.anchor, at: idx, longestEffectiveRange: &targetRange, in: NSMakeRange(0, attributedString.length)) as? String {
        foundRange = true
        break
      }
      idx = NSMaxRange(targetRange)
    }
    if foundRange == false {
     return (NSMakeRange(NSNotFound, 0), false)
    }
    
    switch anchor {
    case startAnchor:
      
      var count = startStringOccurrenceIndex
      var idx = targetRange.location
      var aRange: NSRange? = nil
      while idx < NSMaxRange(targetRange) {
        guard idx < attributedString.length else { break }
        guard NSMaxRange(targetRange) - idx <= attributedString.length else { break }
        aRange = (attributedString.string as NSString).range(of: startString, options: [], range: NSMakeRange(idx, NSMaxRange(targetRange) - idx))
        if aRange == nil { break }
        if aRange!.location == NSNotFound { break }
        if count <= 0 { break }
        count -= 1
        idx = NSMaxRange(aRange!)
      }
      
      if aRange != nil {
        range = aRange!
      }else {
        range = NSMakeRange(NSNotFound, 0)
      }
      hasNote = notes != nil
      
    case endAnchor:
      // full length
      
      if endString != nil {
        range = NSMakeRange(targetRange.location,  endString!.utf16.count)
      }
      
      hasNote = notes != nil
    default:
      range = targetRange
    }
    
    range = NSIntersectionRange(range, NSMakeRange(0, attributedString.length))
    return (range, hasNote)
  }
  
  /*
   func convertActualStartJi(in attributedString: NSAttributedString) -> Int? {
   var count = startMojiIndex.value!
    var string = attributedString.string as NSString
    var idx = 0
    while true {
      if startMoji == string.substring(with: NSMakeRange(idx,1)) {
        if count == 0 { break }
        count -= 1
      }
      idx += 1
      if idx >= string.length { return nil }
    }
    
    return idx
  }
  
  func convertActualEndJi(in attributedString: NSAttributedString) -> Int? {
    var count = endMojiIndex.value!
    var string = attributedString.string as NSString
    var idx = 0
    while true {
      if endMoji == string.substring(with: NSMakeRange(idx,1)) {
        if count == 0 { break }
        count -= 1
      }
      idx += 1
      if idx >= string.length { return nil }
    }
    
    return idx
  }
 */
}


struct AnchorLocator {
  // LAW DESCRIPTION
  var law: String // 昭和三十四年法律第百二十一号
  var lawEdition: String? // md5 of xml string
  
  // ANCHOR DESCRIPTION
  var anchor: String? // 条2/項1
  
  // FROM ANCHOR
  var string: String? // 第３２条
  var occurrenceIndex: Int? // 0
}
//
//extension XMLDocument {
//
//  var lawNum: String? {
//   return at_xpath("LawNum")?.text?.replacingOccurrences(of: " ", with: "")
//  }
//
//  var lawTitle: String? {
//    return at_xpath("//LawTitle")?.text
//  }
//
//}

