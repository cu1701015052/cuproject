//
//  FetchResults.swift
//  LawXML
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

/*
 MANAGES TABLE LAYOUTING AND FETCH RESULT
 */
import Foundation
import Realm
import RealmSwift

enum MatchPattern {
 case regular, bookmarkNotes, selectionNotes, tag
}
typealias Match = (row: Int, matchIndex: Int, matchRange: NSRange, matchPattern: MatchPattern, userInfo: String?)

class InMemryFetchResults: FetchResults {
  var articles: [Row]!
  init(law: LawSource, articles: [Row]) {
    super.init(law: law)!
    self.articles = articles
  }
  
  override subscript(idx: Int) -> (NSMutableAttributedString, Row)! {
    guard idx < articles.count else { return nil }

    let article = articles[idx]
    
    let attr = attributedStringFromRow(row: article)
    return (attr, article)
  }
}

class FilterSetting {
  
  enum FilterType {
    case any, all
  }
  
  enum FilterNotType {
    case normal, not
  }
  
  
  var filterType: FilterType = .any
  var filterNotType: FilterNotType = .normal
  var filterTagNumbers: [Int] = []
  var isOn: Bool = false
  var name: String = ""
  
  init(name: String) {
    self.name = name
    
    let anyType = UserDefaults.standard.bool(forKey: "FilterType")
    let notType = UserDefaults.standard.bool(forKey: "FilterNotType")
//    let flag = UserDefaults.standard.bool(forKey: "FilterOn")

    self.filterType = anyType ? .any : .all
    self.filterNotType = notType ? .normal : .not
    self.filterTagNumbers = UserDefaults.standard.object(forKey: "FilterTagNumbers") as? [Int] ?? []
    self.isOn = false
  }
  
  func save() {
    UserDefaults.standard.set(filterType == .any ? true : false, forKey: "FilterType")
    UserDefaults.standard.set(filterNotType == .normal ? true : false, forKey: "FilterNotType")
//    UserDefaults.standard.set(isOn, forKey: "FilterOn")
    UserDefaults.standard.set(filterTagNumbers, forKey: "FilterTagNumbers")
  }
}


class FetchResults {
  

  var realm: Realm
  //var articles: RealmSwift.Results<Row>
  var tocs: RealmSwift.Results<Toc>
  fileprivate var bookmarks: RealmSwift.Results<Bookmark>?
  var allTags: RealmSwift.Results<Tag>? = nil

  var unfilteredCount: Int
  var filteredCount: Int = 0

  var count: Int {
    if filterSetting.isOn { return filteredCount }
    return unfilteredCount
  }
  
  private var estimatedCharacterCounts: [Int]
  private var preciseCellHeights: (cellHeights: [Int32?], width: CGFloat)! = nil
  private var estimatedCellHeights: (cellHeights: [Int32?], width: CGFloat)! = nil
  var law: LawSource
  var styleSheet: StyleSheet! {
    didSet {
      reloadBookmarks()
      #if os(iOS)
      clearAllCellHeights()
      clearAttributedStringCache()
      #endif
    }
  }
  
//  var filterType: FilterSetting.FilterType = .any
//  var filterNotType: FilterSetting.FilterNotType = .normal
//  var filterTagNumbers: [Int] = []
  
  var filteredRows: [Row]? = nil
  var filterSetting: FilterSetting! {
    didSet {
    updateFilter()
    }
  }
  
  func updateFilter() {
    if filterSetting.isOn {
      
      if filterSetting.filterTagNumbers.count == 0 {
        allTags = nil
        
      }else {
        
        let predicate = NSPredicate(format: "tagNumber IN %@", filterSetting.filterTagNumbers)
        allTags = RealmManager.shared.tags(for: law)?.filter(predicate)
      }
    }else {
      allTags = nil
    }
    
    filteredRows = Array(realm.objects(Row.self).sorted(byKeyPath: "rowNumber").filter( { self.filter(row: $0) }))
    filteredCount = filteredRows!.count
    
    estimatedCharacterCounts = filteredRows!.map { $0.estimatedCharacterCount }
    
    #if os(iOS)
    clearAllCellHeights()
    clearAttributedStringCache()
    #endif
  }
  
  let prefilterTagPredicate = NSPredicate(format: "anchor BEGINSWITH $jo")

  func filter(row: Row) -> Bool {
    if filterSetting.isOn == false { return true }
    guard let allTags = allTags else { return true }
    
    // IF tags CONTAINS ANY ON row.anchors, FILTER OUT
    if filterSetting.filterType == .any {
      var fetchedCandidates: (String, [(String, Int)])? = nil
      for anchor in row.anchors {
        if anchor.isEmpty { continue }
        let range = anchor.range(of: "/")
        let jo = range != nil ? String(anchor[..<range!.upperBound]) : (anchor + "/")
        let candidates =  fetchedCandidates?.0 == jo ? fetchedCandidates!.1 : Array(allTags.filter(prefilterTagPredicate.withSubstitutionVariables(["jo": jo])).map({($0.anchor, $0.tagNumber)}))
        let terminatedAnchor = anchor + "/"
        
        for tag in candidates {
          if terminatedAnchor.hasPrefix(tag.0){
            return filterSetting.filterNotType == .normal ? true : false
          }
        }
        
        fetchedCandidates = (jo, candidates)
      }
      return filterSetting.filterNotType == .normal ? false : true
    }
    
    
    if filterSetting.filterType == .all {

      for anchor in row.anchors {
        if anchor.isEmpty { continue }
        let range = anchor.range(of: "/")
        let jo = range != nil ? String(anchor[..<range!.lowerBound]) : (anchor + "/")
        
        let candidates = allTags.filter(prefilterTagPredicate.withSubstitutionVariables(["jo": jo]))
        
        if candidates.count < filterSetting.filterTagNumbers.count { continue }
        let terminatedAnchor = anchor + "/"
        
        var tagCount = 0
        var tempFilterTagNumbers = filterSetting.filterTagNumbers
        for tag in candidates {
          if terminatedAnchor.hasPrefix(tag.anchor){
            if let idx = tempFilterTagNumbers.index(of: tag.tagNumber) {
              tempFilterTagNumbers.remove(at: idx)
            }
            tagCount += 1
          }
        }
        

        if tempFilterTagNumbers.count == 0 && tagCount >= filterSetting.filterTagNumbers.count {
          return filterSetting.filterNotType == .normal ? true : false
        }
      }
      return filterSetting.filterNotType == .normal ? false : true
    }
    

    return true
  }
  
  init?(law: LawSource) {
    guard let realm = ArticleCacheManager.shared.realm(for: law) else { return nil }
    self.realm = realm
    self.law = law
    //self.articles = realm.objects(Row.self).sorted(byKeyPath: "rowNumber", ascending: true)
    self.tocs = realm.objects(Toc.self).sorted(byKeyPath: "index", ascending: true)
    self.bookmarks = RealmManager.shared.bookmarks(for: law)!

    self.estimatedCharacterCounts = realm.objects(Row.self).sorted(byKeyPath: "rowNumber", ascending: true).map { $0.estimatedCharacterCount }
    
    let elementCount = realm.objects(Row.self).count
    
    estimatedCellHeights = (Array<Int32?>(repeating: nil, count: elementCount), -1)
    preciseCellHeights = (Array<Int32?>(repeating: nil, count: elementCount), -1)

    self.unfilteredCount = elementCount
  }
  
  private func reloadBookmarks() {
    self.bookmarks = RealmManager.shared.bookmarks(for: law)
  }
  
  private var rowArticleCache: [Int: (NSMutableAttributedString, Row)] = [:]
  subscript(idx: Int) -> (NSMutableAttributedString, Row)! {
    
    let cache = rowArticleCache[idx]
    if cache != nil { return cache! }

    if filterSetting.isOn {
      
      assert(Thread.isMainThread == true, "** Not main theard")
      
      if filteredCount <= idx { return nil }
      let article = filteredRows![idx]
      
      let attr = attributedStringFromRow(row: article)
      
      rowArticleCache[idx] = (attr, article)
      return (attr, article)
      
      
    }else {
      assert(Thread.isMainThread == true, "** Not main theard")
      let predicate = NSPredicate(format: "rowNumber == %d", idx)
      if let article = realm.objects(Row.self).filter(predicate).first {
        
        let attr = attributedStringFromRow(row: article)
        
        rowArticleCache[idx] = (attr, article)
        return (attr, article)
      }else {
        assert(false, "*** Not found FetchResults subscript")
        return nil
      }
    }
  }
  
  func unfilteredRow(_ idx: Int) -> (NSMutableAttributedString, Row)! {
    
    assert(Thread.isMainThread == true, "** Not main theard")
    let predicate = NSPredicate(format: "rowNumber == %d", idx)
    if let article = realm.objects(Row.self).filter(predicate).first {
      
      let attr = attributedStringFromRow(row: article)
      
      return (attr, article)
    }else {
      assert(false, "*** Not found FetchResults subscript")
      return nil
    }
  }
  
  func hasBookmarks(at idx: Int) -> Bool {
    guard let obj = self[idx] else { return false }
    return bookmarks(for: obj.1).count > 0
  }
  
  func deleteBookmarks(at idx: Int) {
    bookmarks(for: self[idx].1)
      .forEach {
        _ = RealmManager.shared.delete(organizable: $0)
    }
    NotificationCenter.default.post(name: LawNotification.bookmarkDidChangeExternally, object: self, userInfo: nil)
    reloadBookmarks()
  }
  
  func noteBookmark(for row: Row) -> Bookmark? {
    let bookmark = bookmarks(for: row, includingNoteBookmarks: true)
      .filter { $0.notes?.isEmpty == false }.first
    return bookmark
  }
  
  private func bookmarks(for row: Row, includingNoteBookmarks: Bool = false) -> [Bookmark] {
    let anchorsInRow: [String] = row.anchors.map { string in
      if string.hasSuffix("/頭") { return String(string.dropLast(2)) }
      if string.hasSuffix("/文") { return String(string.dropLast(2)) }
      return string
    }
    if anchorsInRow.isEmpty { return [] }
    
    let bookmarkPredicate = NSPredicate(format: "anchor IN %@", anchorsInRow)
    /*
     anchor: "条10"
     "条13/項1/号2"
 [0] = "条10/頭"
 [1] = "条10/項1/文"
     
     [0] = "条13/項1/号2/頭"
     [1] = "条13/項1/号2/文"
 */
    var bookmarksForThisRow: [Bookmark] = []
    if bookmarks != nil { bookmarksForThisRow = Array(bookmarks!.filter(bookmarkPredicate)) }
    
    if includingNoteBookmarks == false {
      bookmarksForThisRow = bookmarksForThisRow.filter { ($0.notes == nil || $0.notes?.isEmpty == true) }
    }
    
    return bookmarksForThisRow
  }

  func chapterWithAnchor(_ anchor: String) -> Row? {
    let predicate = NSPredicate(format: "multipleAnchors CONTAINS %@", anchor)
    return realm.objects(Row.self).filter(predicate).sorted(byKeyPath: "rowNumber", ascending: true).first
  }
  
//  var attributedStringCache: Dictionary<Int, NSMutableAttributedString> = [:]
  func attributedString(at index: Int, forDictation: Bool = false) -> NSMutableAttributedString {
    guard self.count > index else { return NSMutableAttributedString() }
    let article = self[index]!
    return article.0
  }
  
  func plainText() -> String {
    
    assert(Thread.isMainThread == true, "** Not main theard")
    let text = NSMutableString()
    let rawStrings = realm.objects(Row.self).map { $0.rawString }
    
    for string in rawStrings {
      autoreleasepool {
        text.append(string as String)
      }
    }
    
    return text as String
  }
  
  func attributedStringFromRow(row: Row) -> NSMutableAttributedString {
    return attributedStringFromRow(row: row, forDictation: false)
  }
  
  fileprivate func attributedStringFromRow(row: Row, forDictation: Bool = false) -> NSMutableAttributedString {
    
    let attr = row.attributedString()
    
    // ADD BOOKMARK
    let bookmarksForRow = bookmarks(for: row)
    
    //TODO: STYLE SHEET
    
    var idx = 0
    var range = NSMakeRange(0,0)
    
    //ATTRIBUTES
    while idx < attr.length {
      if let value = attr.attribute(.elementNameAttributeName, at: idx, longestEffectiveRange: &range, in: NSMakeRange(0,attr.length)) as? String {
        var style: [NSAttributedString.Key: Any] = styleSheet.body
        switch value {
        case "LawTitle": style = styleSheet.lawTitle
        case "ArticleCaption": style = styleSheet.caption
        case "ArticleTitle", "ParagraphNum", "ItemTitle", "Subitem1Title", "Subitem2Title", "Subitem3Title", "Subitem4Title", "Subitem5Title", "Subitem6Title", "Subitem7Title", "SupplProvisionLabel", "TOCLabel", "ChapterTitle", "SectionTitle","SubsectionTitle", "PartTitle", "DivisionTitle",  "TableStructTitle", "AppdxTableTitle", "RemarksLabel", "ArithFormulaNum", "NoteStructTitle", "AppdxFigTitle", "FormatStructTitle", "StyleStructTitle", "FigStructTitle", "TOCPreambleLabel", "AppdxNoteTitle", "RelatedArticleNum", "SupplProvisionAppdxStyleTitle", "AppdxStyleTitle", "AppdxFormatTitle", "SupplProvisionAppdxTableTitle"  :
          style = styleSheet.title
          
        default: break
        }
        
        attr.addAttributes(style, range: range)
      }
      
      idx = NSMaxRange(range)
    }
    
    // EMBED BOOKMARK COUNT
    if bookmarksForRow.count > 0 {
      idx = 0
      while idx < attr.length {
        if let value = attr.attribute(.tempLink, at: idx, longestEffectiveRange: &range, in: NSMakeRange(0,attr.length)) as? String, value.hasPrefix(LawXMLTypes.headerLinkPrefix) == true {
          attr.addAttributes([.tempBookmarkCount: bookmarksForRow.count], range: NSMakeRange(range.location, 1))
          break
        }
        idx = NSMaxRange(range)
      }
    }
    
    // ADD KAISEI
    if row.diffSerializedAttributedString != nil {
      idx = 0
      while idx < attr.length {
        if let value = attr.attribute(.tempLink, at: idx, longestEffectiveRange: &range, in: NSMakeRange(0,attr.length)) as? String, value.hasPrefix(LawXMLTypes.headerLinkPrefix) == true {
          attr.addAttributes([.kaisei: styleSheet.backgroundColorScheme.tintColor.cgColor], range: range)
          break
        }
        idx = NSMaxRange(range)
      }
    }

    
    
//    // EMBED KAISEI ** DEBUG
//    idx = 0
//    while idx < attr.length {
//      if let value = attr.attribute(.tempLink, at: idx, longestEffectiveRange: &range, in: NSMakeRange(0,attr.length)) as? String, value.hasPrefix(LawXMLTypes.headerLinkPrefix) == true {
//        attr.addAttributes([.kaisei: styleSheet.backgroundColorScheme.tintColor.cgColor], range: range)
//        break
//      }
//      idx = NSMaxRange(range)
//    }
//
    
    // PARAGRAPH STYLE
    idx = 0
    while idx < attr.length {
      if let value = attr.attribute(.elementNameAttributeName, at: idx, longestEffectiveRange: &range, in: NSMakeRange(0,attr.length)) as? String {
        var style: [NSAttributedString.Key: Any] = [:]

        switch value {
        case "ArticleCaption": style[.paragraphStyle] = styleSheet.captionParagraph
        case "ParagraphNum", "ArticleTitle": style[.paragraphStyle] = styleSheet.primaryParagraph
        case "ItemTitle": style[.paragraphStyle] = styleSheet.secondaryParagraph
        case "Subitem1Title": style[.paragraphStyle] = styleSheet.tertiaryParagraph
        case "Subitem2Title": style[.paragraphStyle] = styleSheet.subitem2Paragraph
        case "Subitem3Title": style[.paragraphStyle] = styleSheet.subitem3Paragraph
        case "Subitem4Title": style[.paragraphStyle] = styleSheet.subitem4Paragraph
        case "Subitem5Title": style[.paragraphStyle] = styleSheet.subitem5Paragraph
        case "Subitem6Title": style[.paragraphStyle] = styleSheet.subitem6Paragraph
        case "Subitem7Title": style[.paragraphStyle] = styleSheet.subitem7Paragraph
        case "ChapterTitle", "SectionTitle","SubsectionTitle", "PartTitle", "DivisionTitle", "SupplProvisionLabel":  style[.paragraphStyle] = styleSheet.chapterTitleParagraph
        default: break
          
        }
        
        attr.addAttributes(style, range: NSMakeRange(0,attr.length))
        break
      }
      
      idx = NSMaxRange(range)
    }
    
    if attr.length == 1 && attr.string == "\n" { // EMPTY LINE
      
      var style: [NSAttributedString.Key: Any] = [:]
      style[.paragraphStyle] = styleSheet.primaryParagraph
      attr.addAttributes(style, range: NSMakeRange(0,attr.length))
    }
    
    // ADD FETCHED LINKS
    let anchors = row.anchors

    for anchor in anchors {
      addFetchedLinks(to: attr, forAnchor: anchor)
      addSelections(attributedString: attr, forAnchor: anchor)
    }
    if styleSheet.addParenthesisAttributes {
      attr.appendParenthesisStyle(styleSheet.parenthesis, secondary: styleSheet.parenthesisSecondary, tertiary: styleSheet.parenthesisTertiary)
    }
    
    // CONVERT Kanji
    if styleSheet.kanjiType == .zenkaku {
      attr.convertKanjiNumberToArabicNumber(convertToZenkaku: true)
    }else if styleSheet.kanjiType == .hankaku {
      attr.convertKanjiNumberToArabicNumber(convertToZenkaku: false)
    }
    
    // ADD NOTES
    if let noteBookmark = noteBookmark(for: row), let _ = noteBookmark.notes {
      
      idx = 0
      while idx < attr.length {
        if let value = attr.attribute(.tempLink, at: idx, longestEffectiveRange: &range, in: NSMakeRange(0,attr.length)) as? String, value.hasPrefix(LawXMLTypes.headerLinkPrefix) == true {
          
          attr.addAttribute(.bookmarkNotesSerialIdName, value: noteBookmark.uuid, range: range)
          let notes = styleSheet.blockquote(noteBookmark)
          notes.insert(NSAttributedString(string:"\n"), at: 0)
          attr.insert(notes, at: NSMaxRange(range))


          break
        }
        idx = NSMaxRange(range)
      }
    }
    
    return attr
  }
  
  func diffAttributedStringFromRow(row: Row) -> NSMutableAttributedString? {
    
    guard let attr = row.diffAttributedString() else { return nil }
    
    
    //TODO: STYLE SHEET
    
    var idx = 0
    var range = NSMakeRange(0,0)
    
    // CONVERT DIFF
    idx = 0
    while idx < attr.length {
      if let num  = attr.attribute(.diffOperation, at: idx, longestEffectiveRange: &range, in: NSMakeRange(0,attr.length)) as? Int {
        
        if num == 2 {
          attr.addAttribute(.backgroundColor, value: UIColor(red: 0, green: 0, blue: 1, alpha: 0.5), range: range)
        }else {
          attr.addAttribute(.strikethroughColor, value: UIColor.red, range: range)
          attr.addAttribute(.strikethroughStyle, value: NSNumber(value: Int16(NSUnderlineStyle.double.rawValue)), range: range)

        }
      }
      idx = NSMaxRange(range)
    }
    

    idx = 0
    
    //ATTRIBUTES
    while idx < attr.length {
      if let value = attr.attribute(.elementNameAttributeName, at: idx, longestEffectiveRange: &range, in: NSMakeRange(0,attr.length)) as? String {
        var style: [NSAttributedString.Key: Any] = styleSheet.body
        switch value {
        case "LawTitle": style = styleSheet.lawTitle
        case "ArticleCaption": style = styleSheet.caption
        case "ArticleTitle", "ParagraphNum", "ItemTitle", "Subitem1Title", "Subitem2Title", "Subitem3Title", "Subitem4Title", "Subitem5Title", "Subitem6Title", "Subitem7Title", "SupplProvisionLabel", "TOCLabel", "ChapterTitle", "SectionTitle","SubsectionTitle", "PartTitle", "DivisionTitle",  "TableStructTitle", "AppdxTableTitle", "RemarksLabel", "ArithFormulaNum", "NoteStructTitle", "AppdxFigTitle", "FormatStructTitle", "StyleStructTitle", "FigStructTitle", "TOCPreambleLabel", "AppdxNoteTitle", "RelatedArticleNum", "SupplProvisionAppdxStyleTitle", "AppdxStyleTitle", "AppdxFormatTitle", "SupplProvisionAppdxTableTitle"  :
          style = styleSheet.title
          
        default: break
        }
        
        attr.addAttributes(style, range: range)
      }
      
      idx = NSMaxRange(range)
    }
    
    // PARAGRAPH STYLE
    idx = 0
    while idx < attr.length {
      if let value = attr.attribute(.elementNameAttributeName, at: idx, longestEffectiveRange: &range, in: NSMakeRange(0,attr.length)) as? String {
        var style: [NSAttributedString.Key: Any] = [:]
        
        switch value {
        case "ArticleCaption": style[.paragraphStyle] = styleSheet.captionParagraph
        case "ParagraphNum", "ArticleTitle": style[.paragraphStyle] = styleSheet.primaryParagraph
        case "ItemTitle": style[.paragraphStyle] = styleSheet.secondaryParagraph
        case "Subitem1Title": style[.paragraphStyle] = styleSheet.tertiaryParagraph
        case "Subitem2Title": style[.paragraphStyle] = styleSheet.subitem2Paragraph
        case "Subitem3Title": style[.paragraphStyle] = styleSheet.subitem3Paragraph
        case "Subitem4Title": style[.paragraphStyle] = styleSheet.subitem4Paragraph
        case "Subitem5Title": style[.paragraphStyle] = styleSheet.subitem5Paragraph
        case "Subitem6Title": style[.paragraphStyle] = styleSheet.subitem6Paragraph
        case "Subitem7Title": style[.paragraphStyle] = styleSheet.subitem7Paragraph
        case "ChapterTitle", "SectionTitle","SubsectionTitle", "PartTitle", "DivisionTitle", "SupplProvisionLabel":  style[.paragraphStyle] = styleSheet.chapterTitleParagraph
        default: break
          
        }
        
        attr.addAttributes(style, range: NSMakeRange(0,attr.length))
        break
      }
      
      idx = NSMaxRange(range)
    }
    
    if attr.length == 1 && attr.string == "\n" { // EMPTY LINE
      
      var style: [NSAttributedString.Key: Any] = [:]
      style[.paragraphStyle] = styleSheet.primaryParagraph
      attr.addAttributes(style, range: NSMakeRange(0,attr.length))
    }
    
    
    // CONVERT Kanji
    if styleSheet.kanjiType == .zenkaku {
      attr.convertKanjiNumberToArabicNumber(convertToZenkaku: true)
    }else if styleSheet.kanjiType == .hankaku {
      attr.convertKanjiNumberToArabicNumber(convertToZenkaku: false)
    }
    
    // VERTICAL
    attr.addAttribute(.verticalGlyphForm, value: 0, range: NSMakeRange(0,attr.length))

    
    return attr
  }

  
  func getAllLinkedAttributedStrings(completion: @escaping ([NSMutableAttributedString])->Void) {
    
    let rawAttributedStringData: [Data] = realm.objects(Row.self).sorted(byKeyPath: "rowNumber", ascending: true)
      .map { $0.serializedAttributedString }
    
    DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
      
      var returnArray = Array<NSMutableAttributedString>(repeating: NSMutableAttributedString(), count: rawAttributedStringData.count)
      let linker = LinkMaker(prefix: LawXMLTypes.internalLinkPrefix, styleSheet: self.styleSheet)

      DispatchQueue.concurrentPerform(iterations: rawAttributedStringData.count)  { (i:size_t) in
        let rawAttr = NSMutableAttributedString(serializedData: rawAttributedStringData[i]) ?? NSMutableAttributedString()
        
        linker.addLinks(to: rawAttr)
        
        objc_sync_enter(self)
        returnArray[i] = rawAttr
        objc_sync_exit(self)
      }
      
      DispatchQueue.main.async {
        completion(returnArray)
      }
    }
  }
  
//  func buildCache(for article: Article, forDictation: Bool = false) {
//    //print("** buildCache \(article.lowerBound)")
//    article.styleSheet = styleSheet
//    article.downloadedLaw = law
//
//    let attributedStrings = article.attributedStrings(withLinks: true, forDictation: forDictation)
//
//    for n in 0 ..< attributedStrings.count {
//      let attrForRow = attributedStrings[n]
//      attributedStringCache[n + Int(article.lowerBound)] = attrForRow
//    }
//  }
  
  func findAnchor(_ anchor: String,  extendeToGo: Bool = true) -> Int? {
    let predicate = NSPredicate(format: "multipleAnchors CONTAINS %@", anchor)
    let results = realm.objects(Row.self).filter(predicate).sorted(byKeyPath: "rowNumber", ascending: true).filter( { self.filter(row: $0) })
    
    // 項があって号がない場合 号でも検索
    if extendeToGo == true && results.count == 0 && anchor.range(of: "項") != nil && anchor.range(of: "号") == nil {
      let alternativeAnchor = anchor.replacingOccurrences(of: "項", with: "項1/号")
      return findAnchor(alternativeAnchor)
    }
    
    if filterSetting.isOn {
      if let obj = results.first, let filteredRowNumber = filteredRows?.index(of: obj) {
      return filteredRowNumber
      }else {
       return nil
      }
      
    }else {
      return results.first?.rowNumber
    }
  }
  
  func tags(from attributedString: NSAttributedString) -> [TagEntity] {
    
    // LOOK FOR KEY .tempLink
    var loc = 0
    var range: NSRange = NSMakeRange(0, 0)
    var fetchedTags: [Int]  = []
    while NSMaxRange(range) < attributedString.length {
      if let linkString = attributedString.attribute(.tempLink, at: loc, effectiveRange: &range) as? String {
        fetchedTags = tags(for: linkString)
        break
      }
      loc = NSMaxRange(range)
    }
    
    if let entites = RealmManager.shared.tagEntites(for: fetchedTags) {
      return Array(entites)
    }
    
    return []
  }
  
  func tags(for linkString: String) -> [Int] {
    guard let law = law as? DownloadedLaw else { return [] }
    guard law.isInvalidated == false else { return [] }
    
    let comps = linkString.components(separatedBy: "/")
    let body = comps.filter {
      LawXMLTypes.headerLinkPrefix.hasPrefix($0) == false && $0.isEmpty == false && $0.hasPrefix("#") == false
    }
    var anchor = body.joined(separator: "/")
    if anchor.hasSuffix("/") == false {
      anchor = anchor + "/"
    }
    if let tags = RealmManager.shared.tags(for: law, anchor: anchor) {
      let tagDictionary = Array(tags).map {
        return $0.tagNumber
      }
      
      return tagDictionary as [Int]
    }else {
      return []
    }
  }
  
  
  func findTag(_ tagNumber: Int) -> (totalMatchCount: Int, results: [Match], time: Double) {
    guard let law = law as? DownloadedLaw else { return (0, [], 0) }
    guard let fetchResults = RealmManager.shared.tags(for: law, tagNumber: tagNumber) else { return (0, [], 0) }
    
    var usedTags: [String] = []
    
    let t0 = Date().timeIntervalSinceReferenceDate
    func filterWithTagNumber(_ row: Row) -> Bool {
      
      var fetchedCandidates: (String, [(String, Int)])? = nil
      for anchor in row.anchors {
        if anchor.isEmpty { continue }
        let range = anchor.range(of: "/")
        let jo = range != nil ? String(anchor[..<range!.upperBound]) : (anchor + "/")
        let candidates =  fetchedCandidates?.0 == jo ? fetchedCandidates!.1 : Array(fetchResults.filter(prefilterTagPredicate.withSubstitutionVariables(["jo": jo])).map({($0.anchor, $0.tagNumber)}))
        let terminatedAnchor = anchor + "/"
        
        for tag in candidates {
          if terminatedAnchor.hasPrefix(tag.0) {
            let attributedString = row.attributedString()
            let tagNumbers = tags(from: attributedString).map { $0.tagNumber }
            if tagNumbers.contains(tagNumber) {
            return true
            }
          }
        }
        
        fetchedCandidates = (jo, candidates)
      }
      return false
    }
    
    var idx = 0
    
    let filteredRows = filterSetting.isOn ? realm.objects(Row.self).sorted(byKeyPath: "rowNumber", ascending: true).filter { self.filter(row: $0) && filterWithTagNumber($0) } :  realm.objects(Row.self).sorted(byKeyPath: "rowNumber", ascending: true).filter { filterWithTagNumber($0) }
    
    
    let matches: [Match] =  filteredRows.map {
      
      if self.filterSetting.isOn {
        let match: Match = (self.filteredRows!.index(of: $0)!, idx, NSMakeRange(0,1), .tag, nil)
        idx += 1
        return match
      }else {
        let match: Match = ($0.rowNumber, idx, NSMakeRange(0,1), .tag, nil)
        idx += 1
        return match
      }
    }
    
    let t1 = Date().timeIntervalSinceReferenceDate
    return (matches.count, matches, t1-t0)
  }
  
  func findString(_ query: String) -> (totalMatchCount: Int, results: [Match], time: Double) {
    
    var totalMatchCount = 0
    let t0 = Date.timeIntervalSinceReferenceDate
    var findArray: [Match] = []
    
    let allArticles = realm.objects(Row.self).sorted(byKeyPath: "rowNumber", ascending: true).filter { self.filter(row: $0) }
    
    let serializedAttributedStrings = Array(allArticles.map { $0.serializedAttributedString })
    let rowIndexArray = filterSetting.isOn ? Array( (0..<allArticles.count).map { $0 }) : Array(allArticles.map { $0.rowNumber })

    var findStrings: [String] = [query]
    
    if query.range(of: " ") != nil ||  query.range(of: "　") != nil {
      findStrings = query.components(separatedBy: CharacterSet.whitespaces)
    }
    
    DispatchQueue.concurrentPerform(iterations: serializedAttributedStrings.count)  { ( n:size_t) in
      guard let string = NSString(serializedData: serializedAttributedStrings[n] ) else { return }
      var matchIndex = 0
      
      for findString in findStrings {
        if string.range(of: findString).location == NSNotFound { continue }
        
        var findRange = NSMakeRange(0, string.length)
        while true {
          
          let matchRange = string.range(of: findString, options: .caseInsensitive, range: findRange, locale: nil)
          if matchRange.length == 0 {
            totalMatchCount += matchIndex
            return
          }
          
          objc_sync_enter(self)
          findArray.append( (rowIndexArray[n], matchIndex, matchRange, .regular, nil) )
          objc_sync_exit(self)
          matchIndex += 1
          findRange = NSMakeRange(NSMaxRange(matchRange), string.length - NSMaxRange(matchRange))
        }
      }
    }

    if let law = law as? DownloadedLaw {
      // FIND BOOKMARK NOTES
      if let results0 = RealmManager.shared.bookmarks(for: law, findString: query) {
        for selection in results0 {
          let matchRange = NSMakeRange(0, 1)
          var findRange: Range<String.Index> = selection.notes!.startIndex ..< selection.notes!.endIndex
          while let foundRange = selection.notes?.range(of: query, options: String.CompareOptions.caseInsensitive, range: findRange) {
            findRange = foundRange.upperBound ..< selection.notes!.endIndex
            findArray.append( (selection.row, 0, matchRange, .bookmarkNotes, nil) )
          }
        }
      }
      
      // FIND SELECTION NOTES
      if let results1 = RealmManager.shared.selections(for: law, findString: query) {
        for selection in results1 {
          autoreleasepool {
            let matchRange = NSMakeRange(selection.startIndexInRow + selection.startString.count, 1)
            findArray.append( (selection.row, 0, matchRange, .selectionNotes, selection.uuid) )
          }
        }
      }
    }

    // SORT
    findArray.sort {
      if $0.row < $1.row { return true }
      if $0.row > $1.row { return false }
      
      return $0.matchRange.location < $1.matchRange.location
    }
    
    
    // ADJUST MATCH INDICES
    var matchIndex = 0
    var row = 0
    findArray = findArray.map { match in
      if match.matchPattern == .selectionNotes { return match }

      if match.row != row {
        row = match.row
        matchIndex = 0
      }else {
        matchIndex += 1
      }
      return (match.row, matchIndex, match.matchRange, match.matchPattern, nil)
    }
    
    let t1 = Date.timeIntervalSinceReferenceDate
    return (totalMatchCount, findArray, t1 - t0)
  }
  
  /*
  func object(withUuid uuid: String) -> Article {
    
    var obj: Kajō! = nil
    let results = realm.objects(Kajō.self).filter("uuid == %@", uuid)
    
    assert( results.count != 0, "** Error")
    obj = results[0]
    
   return obj
   }
   */
  
  func attributedStringToPrintForRowIndex(_ fromRow: Int) -> NSAttributedString {
    let attributedString = NSMutableAttributedString()
    var fromRow = fromRow
    let baseAnchors = self[fromRow].1.anchors
    var prefix = baseAnchors.first
    baseAnchors.forEach {
      if prefix == nil { prefix = $0 }
      else {
        let thisPrefix = $0.commonPrefix(with: prefix!)
        prefix = thisPrefix
      }
    }
    
    if prefix?.isEmpty != false { return NSAttributedString() }
    
    if fromRow > 0 {
      let captionRowAnchors = self[fromRow-1].1.anchors
      if let captionAnchor = captionRowAnchors.first, prefix!.hasPrefix(captionAnchor), captionAnchor.count > 1 {
        fromRow -= 1
        prefix = prefix?.commonPrefix(with: captionAnchor)
      }
    }
    RowLoop: for n in fromRow..<self.count {
      let anchors = self[n].1.anchors
      
      for anchor in anchors {
        if anchor.hasPrefix(prefix!) == false { break RowLoop }
      }
      
      attributedString.append(self.attributedString(at: n))
      if attributedString.length > 0 && attributedString.string.hasSuffix("\n") == false {
        let attributes = attributedString.attributes(at: attributedString.length - 1, effectiveRange: nil)
        attributedString.append(NSAttributedString(string:"\n", attributes: attributes))
      }
    }
    return attributedString
  }
  
  //MARK:-

  func hasPreciseCellHeight(at index: Int, forWidth width: CGFloat) -> Bool {
    
    if preciseCellHeights.width != width { return false }
    guard preciseCellHeights.cellHeights.count > index else { return false }
    if preciseCellHeights.cellHeights[index] == nil { return false }
    return true
  }

  func clearCellHeight(at index: Int) {
    objc_sync_enter(self)
    preciseCellHeights.cellHeights[index] = nil
    estimatedCellHeights.cellHeights[index] = nil
//    rowArtcleCache = [:]
    objc_sync_exit(self)

  }
  
  func clearAllCellHeights() {
    objc_sync_enter(self)
    estimatedCellHeights = (Array<Int32?>(repeating: nil, count: self.count), -1)
    preciseCellHeights = (Array<Int32?>(repeating: nil, count: self.count), -1)
    objc_sync_exit(self)

//    attributedStringCache = [:]
//    rowArtcleCache = [:]
  }
  
  func estimatedCellHeight(at index: Int, forWidth width: CGFloat) -> CGFloat {
    guard width > 0 else { return 0 }
    
    objc_sync_enter(self)
    let count = estimatedCellHeights.cellHeights.count
    let estimatedHeight = estimatedCellHeights.cellHeights[index]
    let h = self.estimatedCharacterCounts[index]
    objc_sync_exit(self)

    guard count > index else { return 0 }
    if estimatedHeight != nil { return CGFloat(estimatedHeight!) }
    
    let height = TextViewCell.estimatedCellHeightFor(length: h, width: width, styleSheet: styleSheet)
    objc_sync_enter(self)
    if count > index {
    estimatedCellHeights.cellHeights[index] = Int32(height)
    }
    objc_sync_exit(self)

    return height
  }
  
  var magicNumbers: [CGFloat] = []

  func preciseCellHeight(at index: Int, forWidth width: CGFloat) -> (MNFramesetter?, CGFloat) {
    guard width > 0 else { return (nil, 0) }

    var framesetter: MNFramesetter? = nil
    objc_sync_enter(self)
    if preciseCellHeights.width != width {
      preciseCellHeights = (Array<Int32?>(repeating: nil, count: self.count), width)
    }

    let count = preciseCellHeights.cellHeights.count
    objc_sync_exit(self)

    guard count > index else { return (nil, 0) }

    objc_sync_enter(self)
    if preciseCellHeights.cellHeights[index] == nil {
//      print("----- preciseCellHeight \(index) ")

      let attr = attributedString(at: index)
      let (theFramesetter, height) = TextViewCell.cellHeightFor(attributedString: attr, width: width, styleSheet: styleSheet)
      preciseCellHeights.cellHeights[index] = Int32(height)
      framesetter = theFramesetter
    }

    let height = preciseCellHeights.cellHeights[index]
    objc_sync_exit(self)

    if height == nil {
      let exception = NSException(name: NSExceptionName.objectInaccessibleException, reason: "** preciseCellHeight height is nil", userInfo: nil)
      debugPrint("** preciseCellHeight height is nil")
      exception.raise()
      return (nil, 5)
    }
    
    
    
    /*
    
    let estimated = CGFloat(estimatedCellHeights.cellHeights[index]!)
    
    magicNumbers.append( CGFloat(height!) / estimated )
    let i: CGFloat = 0
    let total: CGFloat = magicNumbers.reduce(i) {$0 + $1}
      
    print( "ave \(total / CGFloat(magicNumbers.count))")
    
    */
    
    return (framesetter, CGFloat(height!))
  }
  
  func clearAttributedStringCache(keepCellHeights: Bool = false) {
    objc_sync_enter(self)
    rowArticleCache = [:]
    if keepCellHeights == false {
    estimatedCellHeights = (Array<Int32?>(repeating: nil, count: self.count), -1)
    preciseCellHeights = (Array<Int32?>(repeating: nil, count: self.count), -1)
    }
    objc_sync_exit(self)
  }
  
  //MARK:-
  var fetchedLinks: FetchedLinks? = nil
  
  func addFetchedLinks(to attributedString: NSMutableAttributedString, forAnchor anchor: String) {

    guard let recordsDicByFromAnchor = fetchedLinks?.fetchedLinkArray() else { return }
    
    recordsDicByFromAnchor[anchor]?.forEach {
        
        var toLaw = $0["toLaw"] as? String // nil/empty = delete link
        var toAnchor = $0["toAnchor"] as? String // nil/empty = delete link
        
        if toLaw?.isEmpty == true { toLaw = nil }
        if toAnchor?.isEmpty == true { toAnchor = nil }
        
        guard let string = $0["string"] as? String else { return }
        var n = $0["occurrenceIndex"] as! Int
        var findRange = NSMakeRange(0, attributedString.length)
        
        var anchorToEmbed: String
        if toLaw != nil && toAnchor != nil {
          anchorToEmbed = LawXMLTypes.fetchedLinkPrefix + "法" + toLaw! + "/" + toAnchor!
        }else if toLaw != nil && toAnchor == nil {
          anchorToEmbed = LawXMLTypes.fetchedLinkPrefix + "法" + toLaw!
        }else if toLaw == nil && toAnchor != nil {
          anchorToEmbed = LawXMLTypes.fetchedLinkPrefix  + toAnchor!
        }else {
          anchorToEmbed = ""
        }
        
        while true {
          
          let range = (attributedString.string as NSString).range(of: string, options: .literal, range: findRange)
          
          if n == 0 {
            
            if anchorToEmbed.isEmpty {
            attributedString.removeAttribute(.tempTempLink, range: range)
            attributedString.removeAttribute(.tempTempLinkColor, range: range)
            
          }else {
              if let recordName = $0["recordName"] {
              anchorToEmbed += "/#\(recordName)"
              
              let attributes: [NSAttributedString.Key: Any] = [.tempTempLink: anchorToEmbed, .tempTempLinkColor: styleSheet.backgroundColorScheme.tintColor]
              
              attributedString.addAttributes(attributes, range: range)
            }
          }
          
          break
        }
        
        if range.location == NSNotFound || range.length == 0 { break }
        findRange = NSMakeRange( NSMaxRange(range), attributedString.length - NSMaxRange(range))
        n -= 1
      }
    }
    
    // WRITE TEMP TEMP LINK TO TEMP LINK
    var range = NSMakeRange(0,0)
    var idx = 0
    while idx < attributedString.length {
      
      if let value = attributedString.attribute(.tempTempLink, at: idx, longestEffectiveRange: &range, in: NSMakeRange(0,attributedString.length)) {
        
        let attributes: [NSAttributedString.Key: Any] = [.tempLink: value, .tempLinkColor: styleSheet.backgroundColorScheme.tintColor]
        
        attributedString.addAttributes(attributes, range: range)
      }
      
      idx = NSMaxRange(range)
    }
  }
  
  private var currentSelections: [String] = [] // UUID array

  func addSelections(attributedString: NSMutableAttributedString, forAnchor anchor: String) {
    
    // ADD SELECTION
    guard let law = law as? DownloadedLaw else { return }
    guard law.isInvalidated == false else { return }
    guard let selections = RealmManager.shared.selections(for: law, startAnchor: anchor) else { return }
    
    for selection in selections {
      if currentSelections.contains(selection.uuid) == false {
        currentSelections.append(selection.uuid)
      }
    }
    
    // DRAW CURRENT SELECTIONS
    for selectionUuid in currentSelections {
      
      guard let selection = RealmManager.shared.selection(with: selectionUuid) else { continue }
      if selection.isInvalidated { continue }
      
      let (range, hasNote) = selection.range(in: attributedString, anchor: anchor)
      if range.length == 0 { continue }
      
      let highlightColor = selection.highlightColor
      var attrs: [NSAttributedString.Key: Any]
      
      if highlightColor == .mask {
        attrs = [.mask: selection.uuid /*, .highlightColor: HighlightColor.unmasked.color().cgColor*/ ]

      }else if highlightColor.isUnderline {
        attrs = [NSAttributedString.Key(rawValue: "MNTempUStyle"): 0x02 /*kCTUnderlineStyleThick*/,
          NSAttributedString.Key(rawValue: "MNTempUColor"): highlightColor.color().cgColor, .highlightSerialIdName: selection.uuid]

      }else {

        attrs = [.highlightColor: highlightColor.color().cgColor,  .highlightSerialIdName: selection.uuid]
      }
      attributedString.addAttributes(attrs, range: range)
      
      if hasNote {
        //MAKE SPACE FOR 💬
        
        var existingAttr = attributedString.attributes(at: NSMaxRange(range)-1, effectiveRange: nil)
        existingAttr[.controlName] = NSAttributedString.Key.ControlType.inlineNote
        existingAttr[.controlName] = NSAttributedString.Key.ControlType.inlineNote

        let bubbleAttr = NSAttributedString(string: "　", attributes: existingAttr)
        attributedString.insert(bubbleAttr, at: NSMaxRange(range))
        
      }
    }//SELECTION LOOP ENDS
    
    // REMOVE SELECTION
    guard let endSelections = RealmManager.shared.selections(for: law, endAnchor: anchor) else { return }
    
    for selection in endSelections {
      if  currentSelections.contains(selection.uuid) == true {
        if let idx = currentSelections.index(of: selection.uuid) {
          currentSelections.remove(at: idx)
        }
      }
    }
  }

}

/*
 会社法９５４にある
 「９４３条１号」に飛べない
 
 */



