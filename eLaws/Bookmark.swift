//
//  Bookmark.swift
//  LawXML
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

final class Bookmark: Organizable {
  
  @objc dynamic var lawNo: String = ""
//  @objc dynamic var lawTitle: String = ""
//  @objc dynamic var lawEdition: String = ""
  @objc dynamic var notes: String? = nil
  @objc dynamic var anchor: String = ""
  @objc dynamic var row: Int = 0 // MEANINGLESS
  
  override static func indexedProperties() -> [String] {
    return super.indexedProperties() + ["lawNo", /*"lawTitle", "lawEdition", */"row", "notes"]
  }
  
  override static func ignoredProperties() -> [String] {
    return super.ignoredProperties()
  }
  
  override func titleToDisplay() -> String {
    if notes?.isEmpty == false {
      return "🔖 " + titleToUse() + " " + notes!
    }else {
      return "🔖 " + titleToUse()
    }
  }
  
  override func titleToUse() -> String {
    if title.isEmpty == false { return title }
    let humanReadable = ProcessSentenceXML.convertLinkToHumanReadable(anchor)
    return humanReadable
  }
}

final class Tag: Object {
  
  @objc dynamic var lawNo: String = ""
  @objc dynamic var anchor: String = ""
  @objc dynamic var tagNumber: Int = 0
  
//  var color: TagColor {
//    get {
//      return TagColor(rawValue: colorType)
//    }
//    set {
//      colorType = newValue.rawValue
//    }
//  }

  override static func indexedProperties() -> [String] {
    return super.indexedProperties() + ["lawNo", "anchor", "tagNumber"]
  }

}


final class TagEntity: Object {
  
  @objc dynamic var tagNumber: Int = 0
  @objc dynamic var order: Int = 0
  @objc dynamic var title: String = ""
  @objc dynamic var colorType: Int = 0

  var color: TagColor {
    get {
      return TagColor(rawValue: colorType)
    }
    set {
      colorType = newValue.rawValue
    }
  }

  override static func ignoredProperties() -> [String] {
    return ["color"]
  }
  
}
