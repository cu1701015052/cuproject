//
//  MNSplitViewController.m
//  MNSplit
//
//  Created by Masatoshi Nishikata on 10/05/08.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "LawSplitViewController.h"

#pragma GCC diagnostic ignored "-Wundeclared-selector"

@implementation LawSplitViewController
  
  
#define TRANSITION_ANIMATION_DURATION 0.35
  
  
  
+(CGFloat)animationDuration
  {
    return TRANSITION_ANIMATION_DURATION;
  }
  
  -(UIViewController *)childViewControllerForStatusBarStyle {
    return self.viewControllers[0];
  }
  
  -(BOOL)shouldAutomaticallyForwardAppearanceMethods {
    return YES;
  }
  
- (id _Nonnull) initWithViewControllers:(NSArray<UIViewController*>* _Nonnull)viewControllers
  {
    self = [super init];
    if (self != nil) {
      
      self.vertical = NO;
      self.verticalInLandscape = NO;
      BOOL iPad = (UIUserInterfaceIdiomPad == [[UIDevice currentDevice] userInterfaceIdiom] );
      self.hideOthersWhenHorizontallyCompact = iPad;
      self.viewControllers = viewControllers;
      
    }
    return self;
  }
//  -(UITraitCollection *)traitCollection {
//    UITraitCollection * collection = [super traitCollection];
//    UITraitCollection * collection2 = [UITraitCollection traitCollectionWithTraitsFromCollections:@[collection, [UITraitCollection traitCollectionWithVerticalSizeClass: UIUserInterfaceSizeClassCompact]]];
//    return collection2;
//  }
//  
  - (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection {
    [super traitCollectionDidChange: previousTraitCollection];

    [self setNeedsStatusBarAppearanceUpdate];
    [self rearrangeViews: self.view.bounds.size withTransitionCoordinator: nil];
    for( UIViewController * controller in viewControllers_) {
      [controller traitCollectionDidChange:previousTraitCollection];
    }
  }
  
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator;
  {
    [super viewWillTransitionToSize: size withTransitionCoordinator: coordinator];
    [self rearrangeViews: size withTransitionCoordinator: coordinator];
  }
  
//  -(BOOL)prefersStatusBarHidden {
//    if( viewControllers_.count == 1 ) {
//      if( [self traitCollection].verticalSizeClass == UIUserInterfaceSizeClassCompact)
//      return YES;
//      return NO;
//    }
//    if (@available(iOS 11.0, *)) {
//      if( _vertical && self.view.safeAreaInsets.top == 0 )
//      return YES;
//    } else {
//      if( [self currentlyVertical] )
//      return YES;
//    };
//    return NO;
//  }
//  
-(void)loadView
  {
    // Create view
    
    UIView *splitView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
    
    [splitView setAutoresizingMask:UIViewAutoresizingFlexibleWidth |  UIViewAutoresizingFlexibleHeight];
    self.view = splitView;
    
    // Setup
    
    self.view.backgroundColor = [UIColor colorWithWhite:0.15 alpha:1];
    
    for( UIViewController * controller in self.viewControllers) {
      controller.view.clipsToBounds = YES;
      [splitView addSubview: controller.view];
      [controller.view setAutoresizingMask: UIViewAutoresizingNone];

    }
    
    
  }
  
  - (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    [self rearrangeViews: self.view.bounds.size  withTransitionCoordinator: nil];
  }
  
  -(BOOL)currentlyVertical
  {
    CGSize size = self.view.bounds.size;
    if( self.vertical && size.height > size.width ) {
      return YES;
    }
    
    else if( self.verticalInLandscape && size.height < size.width ) {
      return YES;
    }
    
    return NO;
  }
  
  
#pragma mark - Setting View Controllers
  
-(void)setViewControllers:(NSArray <UIViewController*>*)viewControllers
  {
    
    if( [self.viewControllers isEqualToArray: viewControllers] ) return;
    
    
    for( UIViewController * controller in self.viewControllers) {
      [[NSNotificationCenter defaultCenter] postNotificationName:@"LawSplitViewControllerWillChangeView" object:controller];
      
      [controller willMoveToParentViewController:nil];
      [controller.view removeFromSuperview];
      [controller removeFromParentViewController];
      
    }
    
    if( !viewControllers )
    {
      viewControllers_ = nil;
      return;
    }
    
    viewControllers_ = viewControllers;
    
    //[topViewController_ willMoveToParentViewController:self]; // MNPopover...  ShowPo
    
    for( UIViewController * controller in self.viewControllers) {
      
      [self addChildViewController: controller];
      //	[self rearrangeViewsForOrientation :self.interfaceOrientation];
      controller.view.clipsToBounds = YES;
      [self.view insertSubview:controller.view atIndex:0];
      
      [controller didMoveToParentViewController:self];
    }
    [self rearrangeViews: self.view.bounds.size  withTransitionCoordinator: nil];
    //	[self bringChildViewsToFront];
    
  }
  
  
-(NSArray<UIViewController*> * _Nonnull)viewControllers {
  return viewControllers_;
}
  
  
-(void)rearrangeViews:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
  if( !self.isViewLoaded ) return;
  
  CGRect myBounds = self.view.bounds; //540 x 1024 になっている
  
  
#if TARGET_IPHONE_SIMULATOR
  NSLog(@"%@ %@ %d",NSStringFromClass([self class]), NSStringFromSelector( _cmd ), __LINE__);
  NSLog(@"myBounds %@",NSStringFromCGRect(myBounds));
#endif
  
  CGFloat width, height;
  CGFloat topHeight = size.height;
  
  width = size.width;
  height = size.height;
  
  if( self.vertical && size.height > size.width ) {
    topHeight = size.height / [self.viewControllers count];
  }
  
  else if( self.verticalInLandscape && size.height < size.width ) {
    topHeight = size.height / [self.viewControllers count];
  }
  
  
  CGFloat lineWidth = 1.0;
  if( self.viewControllers.count == 1 ) {
    lineWidth = 0;
  }
  CGFloat topValue = 0;
  if( (size.height > size.width && self.vertical) || (size.height < size.width && self.verticalInLandscape) )
  {
    if( self.hideOthersWhenHorizontallyCompact && self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClassCompact ) {
      for( UIViewController * controller in self.viewControllers) {
        controller.view.hidden = YES;
      }
      self.viewControllers[0].view.frame = CGRectMake(0, 0, size.width, size.height);
      self.viewControllers[0].view.hidden = NO;
      
    }else {
    for( UIViewController * controller in self.viewControllers) {
      CGFloat viewHeight = topHeight - lineWidth;
      CGRect frame = CGRectMake(0, topValue, width, viewHeight);
      
      if( !CGRectEqualToRect(frame, controller.view.frame) ) {
        if( coordinator != nil ) {
          [controller viewWillTransitionToSize: frame.size withTransitionCoordinator:coordinator];
        }
        controller.view.frame = frame;
        controller.view.autoresizingMask = UIViewAutoresizingNone;
      }
      controller.view.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
      controller.view.hidden = NO;
      
      if( self.viewControllers.count > 1 ) {
        UITraitCollection * trait = [UITraitCollection traitCollectionWithTraitsFromCollections: @[self.traitCollection, [UITraitCollection traitCollectionWithVerticalSizeClass: UIUserInterfaceSizeClassCompact]]];
        [self setOverrideTraitCollection:trait forChildViewController: controller];
      }else {
        [self setOverrideTraitCollection:nil forChildViewController: controller];

      }
      
      topValue = CGRectGetMaxY(frame) + lineWidth;
    }
    }
    
  }else {
    if( self.hideOthersWhenHorizontallyCompact && self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassCompact ) {
      for( UIViewController * controller in self.viewControllers) {
        controller.view.hidden = YES;
      }
      self.viewControllers[0].view.frame = CGRectMake(0, 0, size.width, size.height);
      self.viewControllers[0].view.hidden = NO;

    }else {
    for( UIViewController * controller in self.viewControllers) {
      
      CGFloat viewWidth = size.width/[self.viewControllers count] - lineWidth;
      
      CGRect frame = CGRectMake(topValue, 0, viewWidth, height);
      
      if( !CGRectEqualToRect(frame, controller.view.frame) ) {
        if( coordinator != nil ) {
          [controller viewWillTransitionToSize: frame.size withTransitionCoordinator:coordinator];
        }
      controller.view.frame = frame;
        controller.view.autoresizingMask = UIViewAutoresizingNone;

      }
      controller.view.hidden = NO;
      
      if( self.viewControllers.count > 1 ) {
        UITraitCollection * trait = [UITraitCollection traitCollectionWithTraitsFromCollections: @[self.traitCollection]];
        
        [self setOverrideTraitCollection:trait forChildViewController: controller];
      }else {
        [self setOverrideTraitCollection:nil forChildViewController: controller];

      }
      
      topValue = CGRectGetMaxX(frame) + lineWidth;
      
    }
    }
  }
  
  for( UIViewController * controller in self.viewControllers) {
    if( [controller respondsToSelector:@selector(splitViewDidRearrange)] ) {
      [controller performSelector:@selector(splitViewDidRearrange)];
    }
  }
  
  for( UIViewController * controller in self.viewControllers) {
    if( self.viewControllers.count == 1 ) {
      self.viewControllers[0].view.layer.cornerRadius = 0;
    }else {
      controller.view.layer.cornerRadius = 8;
    }
  }
  
  
  [self setNeedsStatusBarAppearanceUpdate];

  // Bring sub view controlelr's view front
  //	[self bringChildViewsToFront];
}
  
  //-(void)bringChildViewsToFront
  //{
  //	// Bring sub view controlelr's view front
  //	NSArray *childViewControllers = self.childViewControllers;
  //
  //	for( NSUInteger hoge = 0; hoge < childViewControllers.count; hoge++ )
  //	{
  //		UIViewController* controller = [ childViewControllers objectAtIndex: childViewControllers.count-hoge-1];
  //
  //		if( controller.view.superview == self.view )
  //		{
  //			[self.view bringSubviewToFront:controller.view];
  //		}
  //
  //
  //	}
  //
  //}
#pragma mark - Setting Properties
  
-(void)setVertical:(BOOL)flag {
  vertical_ = flag;
  [self rearrangeViews: self.view.bounds.size  withTransitionCoordinator: nil];
}
  
-(void)setVerticalInLandscape:(BOOL)flag {
  verticalInLandscape_ = flag;
  [self rearrangeViews: self.view.bounds.size  withTransitionCoordinator: nil];
}
  
-(BOOL)isVertical {
  return vertical_;
}

-(BOOL)isVerticalInLandscape {
  return verticalInLandscape_;
}
  
@end
