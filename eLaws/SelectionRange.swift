//
//  SelectionRange.swift
//  LawXML
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation

struct SelectionRange: Equatable {
  static func == (lhs: SelectionRange, rhs: SelectionRange) -> Bool {
    return lhs.start == rhs.start && lhs.end == rhs.end
  }
  
  
  struct Location: Equatable {
    static func == (lhs: Location, rhs: Location) -> Bool {
      return lhs.index == rhs.index && lhs.characterLoc == rhs.characterLoc
    }
    
    var index: Int = 0
    var characterLoc: Int = 0
    
    func isEqualTo(_ rhs: Location) -> Bool {
      return index == rhs.index && characterLoc == rhs.characterLoc
    }
    
    func isGreaterThan(_ rhs: Location) -> Bool {
      if index < rhs.index { return false }
      if index > rhs.index { return true }
      if characterLoc > rhs.characterLoc { return true }
      return false
    }
  }
  
  var start = Location()
  var end = Location()
  var isEmpty: Bool {
    return start.index == end.index && start.characterLoc == end.characterLoc
  }
  
  var isWithinSingleIndex: Bool {
    return start.index == end.index
  }
  
  func indexHasSelection(_ index: Int) -> Bool {
    if isEmpty == true { return false }
    return start.index <= index && index <= end.index
  }
}
  
