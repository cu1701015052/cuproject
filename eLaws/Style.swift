//
//  Style.swift
//  MacParser
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
#if os(iOS)
  import UIKit
  typealias NSColor = UIColor
#else
  import Cocoa
#endif


enum Style {
  
  static var fontSize: CGFloat = 17
  static var parenthesisAttributes: [NSAttributedString.Key: Any] {
    
    return [.foregroundColor: NSColor.gray, .font: CTFontCreateWithName("HiraKakuProN-W3" as CFString, max(8, round(Style.fontSize * 0.9)), nil) ]
  }
  
  static var secondaryParenthesisAttributes: [NSAttributedString.Key: Any] {
    
    return [.foregroundColor: NSColor.lightGray, .font: CTFontCreateWithName("HiraKakuProN-W3" as CFString, max(8, round(Style.fontSize * 0.8)), nil) ]
  }
  
  case articleCaption
  case articleTitle
  case paragraphNum
  case itemTitle
  case subitem1Title
  case subitem2Title
  case subitem3Title
  case body
  case preamble
  case chapter
  
  func attributes(using styleSheet: StyleSheet) -> [NSAttributedString.Key: Any] {
    switch self {
    case .articleCaption: return styleSheet.caption
    case .articleTitle: return styleSheet.title
    case .paragraphNum: return styleSheet.title
    case .itemTitle: return styleSheet.title
    case .subitem1Title: return styleSheet.title
    case .subitem2Title: return styleSheet.title
    case .subitem3Title: return styleSheet.title
    case .body: return styleSheet.body
    case .preamble: return styleSheet.body
    case .chapter: return styleSheet.title
    }
  }
  
  func paragraphStyle(using styleSheet: StyleSheet) -> NSParagraphStyle? {
    switch self {
    case .articleCaption: return styleSheet.captionParagraph
    case .articleTitle: return styleSheet.primaryParagraph
    case .paragraphNum: return styleSheet.primaryParagraph
    case .itemTitle: return styleSheet.secondaryParagraph
    case .subitem1Title: return styleSheet.tertiaryParagraph
    case .subitem2Title: return styleSheet.subitem2Paragraph
    case .subitem3Title: return styleSheet.subitem3Paragraph
    case .body: return nil
    case .preamble: return styleSheet.preambleParagraph
    case .chapter: return nil
    }
  }
  
  var groupSelection: Bool {
    switch self {
    case .articleCaption: return true
    case .articleTitle: return true
    case .paragraphNum: return true
    case .itemTitle: return true
    case .subitem1Title: return true
    case .subitem2Title: return true
    case .subitem3Title: return true
    case .body: return false
    case .chapter: return true
    case .preamble: return false
    }
  }
  
  var shouldReturnBefore: Bool {
    switch self {
    case .articleCaption: return false
    case .articleTitle: return true
    case .paragraphNum: return false
    case .itemTitle: return false
    case .subitem1Title: return false
    case .subitem2Title: return false
    case .subitem3Title: return false
    case .body: return false
    case .chapter: return true
    case .preamble: return false

    }
  }
  
  var shouldInsertSpaceAfter: Bool {
    switch self {
    case .articleCaption: return false
    case .articleTitle: return true
    case .paragraphNum: return true
    case .itemTitle: return true
    case .subitem1Title: return true
    case .subitem2Title: return true
    case .subitem3Title: return true
    case .body: return false
    case .chapter: return false
    case .preamble: return false
    }
  }
  
  var color: NSColor {
    switch self {
    case .articleCaption: return .gray
    case .articleTitle: return .black
    case .paragraphNum: return .black
    case .itemTitle: return .black
    case .subitem1Title: return .black
    case .subitem2Title: return .black
    case .subitem3Title: return .black
    case .body: return .black
    case .chapter: return .gray
    case .preamble: return .black

    }
  }
 
}
