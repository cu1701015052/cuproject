//
//  LawSourceProtocol.swift
//  LawXML
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation

enum DownloadOption {
  case regular, forceDownload, useCache, inMemory
}

protocol LawSource {
  
  var lawTitle: String { get } // md5
  var lawNum: String { get }  // 昭和三十四年法律第百二十一号
  var lawEdition: String { get } // md5
  var filename: String { get }
  var filepath: String { get }

//  var xml: Data? { get }
  var downloadOption: DownloadOption { get }
}


struct LawDescriptor: LawSource {
  var lawTitle: String
  var lawNum: String
  var lawEdition: String
  var filename: String
  var filepath: String

//  var xml: Data? = nil
  var downloadOption = DownloadOption.regular
}

class InMemoryLawSourceObject: LawSource {
  var lawTitle: String = ""
  var lawNum: String = ""
  var lawEdition: String = ""
  var filename: String = ""
  var filepath: String = ""
  var xmlData: Data?
  var xml: Data? {
    set {
      xmlData = newValue
    }
    get {
      return xmlData
    }
  }
  var downloadOption: DownloadOption { return .inMemory }
}

// FILES
extension LawSource {
  
  var localFilename: String {
    
    if let _ = lawNum.range(of: "内閣総理大臣決定") {
      return "\(lawNum)\(lawTitle) \(lawEdition).lawxml"
    }else {
      return "\(lawNum) \(lawEdition).lawxml"
    }
  }
  
  var localImageDataFilename: String {
    let path = localFilename + ".imagedata"
    return path
    
  }
  
  var xml: Data? {
    set {
      if newValue == nil { return }
      let data = (newValue! as NSData).deflate()
      var url = FileManager.default.appSupportUrl
      url.appendPathComponent(localFilename)
      try! data?.write(to: url)
      _ = url.setExcludedFromBackup(true)
    }
    get {
     // if self.isInvalidated { return nil }
      
      var url = FileManager.default.appSupportUrl
      url.appendPathComponent(localFilename)
      if false == FileManager.default.fileExists(atPath: url.path) {
        return nil
      }
      
      var xmlData: Data? = nil
      
      autoreleasepool {
        let data = try? Data(contentsOf: url)
        xmlData = (data as NSData?)?.inflate()
      }
      return xmlData
      
    }
  }
  
  func writeImageData(_ path: String) {
    let fm = FileManager.default
    guard true == fm.fileExists(atPath: path) else { return }
    
    var url = fm.appSupportUrl
    let imagePath = localImageDataFilename
    url.appendPathComponent(imagePath)
    
    do {
      try fm.moveItem(atPath: path, toPath: url.path)
      _ = url.setExcludedFromBackup(true)
    }catch _ {
      
    }
    
  }

  var threadSafeDescriptor: LawDescriptor {
    return LawDescriptor(lawTitle: lawTitle, lawNum: lawNum, lawEdition: lawEdition, filename: "", filepath: "", downloadOption: downloadOption)
  }
}
