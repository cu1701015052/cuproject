//
//  String.swift
//  MacParser
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

/*
 
 Download whole law content
 http://elaws.e-gov.go.jp/api/1/lawdata/昭和三十四年法律第百二十一号
 
 Downoad particular article
 
 http://elaws.e-gov.go.jp/api/1/articles;lawNum=平成十五年法律第五十七号;article=第十一条
 //lawNum={法令番号};article={条};paragraph={項};appdxTable={別表}
 
 
 
 */

import Foundation

extension String {
  
  func toZenkaku() -> String {
    
    let thisString = self
    var returnValue = ""
    for character in thisString
    {
      var kanji:String = ""
      switch character{
      case "0" : kanji = "０"
      case "1" : kanji = "１"
      case "2" : kanji = "２"
      case "3" : kanji = "３"
      case "4" : kanji = "４"
      case "5" : kanji = "５"
      case "6" : kanji = "６"
      case "7" : kanji = "７"
      case "8" : kanji = "８"
      case "9" : kanji = "９"
      default: kanji = String( character )
      }
      returnValue += kanji
    }
    return returnValue
  }
  
  func convertNumberToKanji(convertToZenkaku: Bool) -> String {
    
    /*
     
     三百七十一
     
     get from left
     
     3
     x100
     7
     x10
     1
     */
    
    
    let myStr: String = self as String
    var total = 0
    var currentDigit: Int = 0
    
    for character in myStr
    {
      
      if character == "千" { total += max( currentDigit, 1) * 1000; currentDigit = 0; }
      else if character == "百" { total += max( currentDigit, 1) * 100; currentDigit = 0; }
      else if character == "十"  { total += max( currentDigit, 1) * 10; currentDigit = 0;  }
      else  {
        if currentDigit != 0  {
          
          if currentDigit == 10 {
            total = total * 10
          }else {
            
            total += currentDigit
            total = total * 10
          }
          currentDigit = 0
        }
        
        switch character {
          
        case "一" : currentDigit = 1
        case "二" : currentDigit = 2
        case "三" : currentDigit = 3
        case "四" : currentDigit = 4
        case "五" : currentDigit = 5
        case "六" : currentDigit = 6
        case "七" : currentDigit = 7
        case "八" : currentDigit = 8
        case "九" : currentDigit = 9
        case "〇" : currentDigit = 10
        default: currentDigit = 0
        }
      }
    }
    
    
    if currentDigit == 10 {
      //    total = total * 10
      
    }else {
      total += currentDigit
    }
    
    // 数字を全角化
    
    if !convertToZenkaku { return String(total) }
    
    let thisString = String(total)
    var returnValue = ""
    for character in thisString {
      var kanji: String = ""
      switch character{
      case "0" : kanji = "０"
      case "1" : kanji = "１"
      case "2" : kanji = "２"
      case "3" : kanji = "３"
      case "4" : kanji = "４"
      case "5" : kanji = "５"
      case "6" : kanji = "６"
      case "7" : kanji = "７"
      case "8" : kanji = "８"
      case "9" : kanji = "９"
      default: kanji = ""
      }
      
      returnValue += kanji
    }
    
    return returnValue
  }
  

}

extension NSMutableString {
  
  func convertKanjiNumberToArabicNumber(convertToZenkaku: Bool) {
    
    let regex1 = "(?<=第)[一二三四五六七八九十百千〇]+(?=条|條|項|規則)"
    let regex3 = "[一二三四五六七八九十百千]+(?=年|月|日)"
    let regex7 = "(?<=明治|大正|平成|昭和|令和|西暦)[一二三四五六七八九十百千]+"

    let regex2 = "(?<=(条|規則)の)[一二三四五六七八九十百千]+"
    let regex4 = "(?<=(条|規則)の(\\d|\\d\\d)の)[一二三四五六七八九十百千]+"
    let regex5 = "(?<=(条|規則)の(\\d|\\d\\d)の(\\d|\\d\\d)の)[一二三四五六七八九十百千]+"
    
    let regex6 = "(?<=法律第)[一二三四五六七八九十百千〇]+(?=号)"
    
    //四条の二の二の二
    func convertMe(_ regex: String, convertToZenkaku: Bool) {
      
      //print("Converting with regex \(regex)")
      
      var findRange = NSMakeRange(0, length)
      
      while true {
        
        let range: NSRange = self.range(of: regex, options: [.regularExpression, .caseInsensitive],  range: findRange)
        
        if range.location == NSNotFound { break }
        
        let kanji = self.substring(with: range)
        let convertedString = kanji.convertNumberToKanji(convertToZenkaku: convertToZenkaku)
        replaceCharacters(in: range, with: convertedString ) // 50%
        
        let loc = NSMaxRange(range) - range.length + (convertedString as NSString).length
        findRange = NSMakeRange(loc, length - loc)
      }
    }
    
    convertMe(regex1, convertToZenkaku: convertToZenkaku)
    convertMe(regex3, convertToZenkaku: convertToZenkaku)
    convertMe(regex7, convertToZenkaku: convertToZenkaku)

    convertMe(regex2, convertToZenkaku: convertToZenkaku)
    convertMe(regex4, convertToZenkaku: convertToZenkaku)
    convertMe(regex5, convertToZenkaku: convertToZenkaku)
    
    convertMe(regex6, convertToZenkaku: convertToZenkaku)
    
  }
}

extension NSString {
  
  func articleAnchor(suffix: String, in range: NSRange) -> String {
    if range.length == 0 { return "" }
    /*
     第百七十一条の二の二
     ↓
     １７１の２の２
     ↓
     条171_2_2
     
     第二項
     ↓
     項2
     
     
     第二号
     ↓
     号2
     
     */
    
    var substring = self.substring(with: range)
    substring = substring.replacingOccurrences(of: "第", with: "")
    substring = substring.replacingOccurrences(of: suffix, with: "")
    substring = substring.replacingOccurrences(of: "の", with: "_")
    substring = substring.replacingOccurrences(of: "ノ", with: "_")
    
    let regex = "[一二三四五六七八九十百千〇]+"
    
    
    while true {
      
      guard let range = substring.range(of: regex, options: [.regularExpression]) else { break }
      
      let kanji = String(substring[range])
      let convertedString = kanji.convertNumberToKanji(convertToZenkaku: false)
      substring = substring.replacingCharacters(in: range, with: convertedString )
      
    }
    
    substring = suffix + substring
    return substring
  }
}

extension NSAttributedString {
  
  func getAnchor(at index: Int) -> (String?, Int?) {
    if length == 0 { return (nil, nil) }
    
    let anchor = attribute(.anchor, at: index, effectiveRange: nil) as? String
    //let ji = attribute(AdditionalAttribute.ji, at: index, effectiveRange: nil) as? Int

    var jiIndex: Int? = nil
    var range = NSMakeRange(0,0)
    let ji = attribute(.ji, at: index, longestEffectiveRange: &range, in: NSMakeRange(0, length))
    if ji != nil {
      jiIndex = index - range.location
    }
    
    return (anchor, jiIndex)
  }
  
  func occurrenceIndex(of string: String, at index: Int, in anchor: String) -> Int {
   
    var occurrenceIndex = 0

    var range = NSMakeRange(0,0)
    var n = 0
    while true {
      let thisAnchor = attribute(.anchor, at: n, longestEffectiveRange: &range, in: NSMakeRange(0, self.length)) as? String
      n = NSMaxRange(range)
      
      if thisAnchor == anchor {
        while true {
        let thisRange = (self.string as NSString).range(of: string, options: NSString.CompareOptions.literal, range: range)
          if thisRange.location == index || thisRange.length == 0 {
           break
          }
          
          occurrenceIndex += 1
          range = NSMakeRange(NSMaxRange(thisRange), NSMaxRange(range) - NSMaxRange(thisRange) )
        }
        break
      }
      if n >= self.length { break }
    }
    return occurrenceIndex
  }
  
  
  func reverseKanjiConversion() -> NSMutableAttributedString {
    
    let mstr = NSMutableAttributedString(attributedString: self)
    var idx = 0
    var range = NSMakeRange(0,0)
    
    while true {
      if let value = mstr.attribute(.kanji, at: idx, longestEffectiveRange: &range, in: NSMakeRange(0,mstr.length)) as? String  {
        let attributes = mstr.attributes(at: idx, effectiveRange: nil)
        let substitute = NSAttributedString(string: value, attributes: attributes)
        mstr.replaceCharacters(in: range, with: substitute)
      }
      
      idx = NSMaxRange(range)
      if idx >= mstr.length { break }
    }
    
    return mstr
  }
}

var globalLinkSerialNumber: Int = 0
extension NSMutableAttributedString {
  
  func convertKanjiNumberToArabicNumber(convertToZenkaku: Bool, additionalAttribute: NSAttributedString.Key? = nil, value: Any? = nil) {
    
    let regex1 = "(?<=第)[一二三四五六七八九十百千〇]+(?=条|條|項|規則)"
    let regex3 = "[一二三四五六七八九十百千]+(?=年|月|日)"
    let regex7 = "(?<=明治|大正|平成|昭和|令和|西暦)[一二三四五六七八九十百千]+"

    let regex2 = "(?<=(条|規則)の)[一二三四五六七八九十百千]+"
    let regex4 = "(?<=(条|規則)の(\\d|\\d\\d)の)[一二三四五六七八九十百千]+"
    let regex5 = "(?<=(条|規則)の(\\d|\\d\\d)の(\\d|\\d\\d)の)[一二三四五六七八九十百千]+"
    
    let regex6 = "(?<=法律第)[一二三四五六七八九十百千〇]+(?=号)"
    
    //四条の二の二の二
    func convertMe(_ regex: String, convertToZenkaku: Bool) {
      
      //print("Converting with regex \(regex)")
      
      var findRange = NSMakeRange(0, length)
      
      while true {
        
        let range: NSRange = (self.string as NSString).range(of: regex, options: [.regularExpression, .caseInsensitive],  range: findRange)
        
        if range.location == NSNotFound { break }
        
        let kanji = (self.string as NSString).substring(with: range)
        let convertedString = kanji.convertNumberToKanji(convertToZenkaku: convertToZenkaku)
        replaceCharacters(in: range, with: convertedString ) // 50%
        
        var attributes: [NSAttributedString.Key: Any] = [.kanji: kanji]
        if additionalAttribute != nil && value != nil {
          attributes[additionalAttribute!] = value!
        }
        
        addAttributes(attributes, range: NSMakeRange(range.location, convertedString.utf16.count)) //50%
        
        let loc = NSMaxRange(range) - range.length + (convertedString as NSString).length
        findRange = NSMakeRange(loc, length - loc)
      }
    }
    
    convertMe(regex1, convertToZenkaku: convertToZenkaku)
    convertMe(regex3, convertToZenkaku: convertToZenkaku)
    convertMe(regex7, convertToZenkaku: convertToZenkaku)

    convertMe(regex2, convertToZenkaku: convertToZenkaku)
    convertMe(regex4, convertToZenkaku: convertToZenkaku)
    convertMe(regex5, convertToZenkaku: convertToZenkaku)
    
    convertMe(regex6, convertToZenkaku: convertToZenkaku)
    
  }
  
  func appendParenthesisStyle(_ attributes: [NSAttributedString.Key: Any], secondary: [NSAttributedString.Key: Any]?, tertiary: [NSAttributedString.Key: Any]?,  findRange: NSRange? = nil) {
    
    /*
     SPAN class="parenthesis"
     リンクを付加する条件
     B タグ内ではない
     
     最大広さの括弧を検出
     */
    
    
    /*
     substring version 26.344820022583
     unichar version 2.34652501344681
     */
    
    
    
    var findRange = findRange
    if findRange == nil { findRange = NSMakeRange(0, length) }
    
    let roundParenthesisOpen: unichar = ("（" as NSString).character(at: 0)
    let roundParenthesisClose = ("）" as NSString).character(at: 0)
    let roundParenthesSet = CharacterSet(charactersIn: "（）")
    
    var hasSecondParenthesis = false

    while true {
      
      // 1. get (
      let range: NSRange = (self.string as NSString).range(of: "（", options: [.literal],  range: findRange!)
      
      // 2. End
      if range.location == NSNotFound { break }
      if NSMaxRange(range) > length { break }
      
      // 3. Body anchor
      if "Sentence" != self.attribute(.elementNameAttributeName, at: range.location, effectiveRange: nil) as? String {
        let loc = NSMaxRange(findRange!)
        findRange = NSMakeRange(loc, length - loc)

        continue
      }
      
      // 4. Look for closing ）
      
      var pcount = 1
      var loc = NSMaxRange(range)
      
      let htmlLength = length
      while true {
        if loc >= htmlLength { loc = NSNotFound; break }
        
        let parenthesisRange = (self.string as NSString).rangeOfCharacter(from: roundParenthesSet, options: [.literal], range: NSMakeRange(loc, htmlLength - loc ))
        if parenthesisRange.location == NSNotFound { loc = NSNotFound; break }
        
        loc = parenthesisRange.location
        let mojiUnichar = (self.string as NSString).character(at: loc)
        
        if mojiUnichar == roundParenthesisOpen {
          pcount += 1
          if pcount == 2 {
            hasSecondParenthesis = true
          }
          if pcount > 2 {

          }
        }
        else if mojiUnichar == roundParenthesisClose { pcount -= 1 }
        
        if pcount == 0 { break }
        
        loc += 1
      }
      if loc == NSNotFound { break }
      // 5. Build String with tag
      let theRange = NSMakeRange( range.location, loc - range.location + 1)
      
      // 5-1. Only one letter?
      
      if loc == NSNotFound || theRange.length == 3 || theRange.location == NSNotFound || NSMaxRange(theRange) > length {
        
        let loc = NSMaxRange(range)
        findRange = NSMakeRange(loc, length - loc)
        
        continue
      }
      
      // 5-2. Only numbers inside the string?
      
      let content = (self.string as NSString).substring(with: NSMakeRange(theRange.location+1, theRange.length-2))
      
      if Int(content) != nil {
        
        let loc = NSMaxRange(range)
        findRange = NSMakeRange(loc, length - loc)
        
        continue
      }
      
      // 6. add attrs
      addAttributes(attributes, range: theRange)
      
      if hasSecondParenthesis && secondary != nil && theRange.length > 2 {
        appendParenthesisStyle(secondary!, secondary: tertiary, tertiary:nil, findRange: NSMakeRange(theRange.location + 1, theRange.length - 2))
      }
      
      
      loc = NSMaxRange(theRange)
      findRange = NSMakeRange(loc, length - loc)
      
      
    } // WHILE LOOP
  }
}

