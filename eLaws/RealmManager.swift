//
//  RealmManager.swift
//  LawXML
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import CloudKit

class RealmManager {
  static var shared = RealmManager()
  var realm: Realm!
  
  let schemaVersion: UInt64 = 8

  var blockToExecuteOnBackground: ((_ completion:  @escaping ()->Void)->Void)? = nil

  
  init() {
    readRealmFile()
  }
  
  func installSampleFiles() {
  }
  
  func readRealmFile() {
    let dbUrl = FileManager.default.appSupportUrl
    _ = dbUrl.setExcludedFromBackup(false)
    
    guard let contents = try? FileManager.default.contentsOfDirectory(at: dbUrl, includingPropertiesForKeys: [URLResourceKey.contentModificationDateKey], options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles) else { return }
    
    if contents.count == 0 {
      installSampleFiles()
      
    }else {
      NSUbiquitousKeyValueStore.default.set(true, forKey: "SampleInstalled")
    }
    
    var sortedContents = contents.filter({$0.pathExtension == "realm"}).sorted(by: { (url1:URL, url2:URL) -> Bool in
      
      var modificationDate1 :Date = Date.distantPast
      var modificationDate2 :Date = Date.distantPast
      
      if let attributes = try? FileManager.default.attributesOfItem(atPath: url1.path) {
        modificationDate1 = attributes[FileAttributeKey.modificationDate] as? Date ?? Date.distantPast
      }
      
      if let attributes = try? FileManager.default.attributesOfItem(atPath: url2.path) {
        modificationDate2 = attributes[FileAttributeKey.modificationDate] as? Date ?? Date.distantPast
      }
      
      return  modificationDate1.compare( modificationDate2 ) == .orderedDescending
      
    })
    
    sortedContents = sortedContents.filter { $0.lastPathComponent.hasPrefix("LawXML") }
    
    var readFlag = false
    
    for url in sortedContents {
      print("loading \(url)")
      if readRealmFile(at: url) { readFlag = true; break }
    }

    if sortedContents.count == 0 || readFlag == false {
      let xmlUrl = dbUrl.appendingPathComponent("LawXML.realm")
      _ = readRealmFile(at: xmlUrl)
    }
    

    // INSTALL TAG ENTITIES
    
    if realm != nil {
      if allTagEntities.count == 0 {
        
        (0...7).forEach {
          let entity = TagEntity()
          let color = TagColor(rawValue: $0)
          entity.title = "名称未設定タグ \($0+1)"
          entity.color = color
          entity.tagNumber = $0
          entity.order = $0 * 10 + 10
          
          try? realm.write() {
            realm.add(entity)
          }
        }
      }
    }
    
    // REMOVE BACKUPS
    let daysToKeepBackups: Double = 5
    // remove backup file 5 days or older
    
    let today = Date()
    let fm = FileManager.default
    
    do {
      var backupsToDelete: [URL] = []
      for content: URL in sortedContents {
        
        if let attributes = try? FileManager.default.attributesOfItem(atPath: content.path) {
          if let modificationDate = attributes[FileAttributeKey.modificationDate] as? Date {
            
            if today.timeIntervalSinceReferenceDate -
              modificationDate.timeIntervalSinceReferenceDate > daysToKeepBackups * 24 * 60 * 60 {
              backupsToDelete.append(content)
            }
            
          }
        }
      }
      
      for url in backupsToDelete {
        if url == self.realm?.configuration.fileURL { continue }
        try fm.removeItem(at: url)
      }
      
    } catch {
    }
  }
  
  func readRealmFile(at url: URL) -> Bool {
    do {
      /*
       Schema version 1: Origina
       2: added filename (429M60080000008_20170710)
       */
      
      let readOnly: Bool
      var encrypted: Bool = false
      var version: UInt64? = 0

      readOnly = false
      encrypted = false
      
      if FileManager.default.fileExists(atPath: url.path) {
        version = try? RealmSwift.schemaVersionAtURL(url, encryptionKey: nil)
        if version == nil {
          do {
            version = try RealmSwift.schemaVersionAtURL(url)
            encrypted = true
          }catch let error {
            print("error \(error.localizedDescription)")
            return false
          }
          
        }else {
          encrypted = false
        }
      }
      
      var canCompact = true
      if UserDefaults.standard.bool(forKey: "ReadingRealm") == true {
        canCompact = false
      }
      
      UserDefaults.standard.set(true, forKey: "ReadingRealm")
      UserDefaults.standard.synchronize()
      let config = Realm.Configuration(fileURL: url, readOnly: readOnly, schemaVersion: schemaVersion, migrationBlock: { migration, oldSchemaVersion in  },  shouldCompactOnLaunch: readOnly ? nil :  { totalBytes, usedBytes in
        if version != self.schemaVersion { return false }
        if canCompact == false { return false }
        return (Double(usedBytes) / Double(totalBytes)) < 0.5
      }, objectTypes:[DownloadedLaw.self, Organizable.self, Bookmark.self, SelectionObject.self, Tag.self, TagEntity.self])
      
      
      if encrypted && canCompact {
        let newUrl = FileManager.default.appSupportUrl.appendingPathComponent("LawXML \(shortUUIDString()).realm")

        let realm = try Realm(configuration: config)
        try realm.writeCopy(toFile: newUrl, encryptionKey: nil)
        
        
        let config = Realm.Configuration(fileURL: newUrl, encryptionKey: nil, readOnly: readOnly, schemaVersion: schemaVersion, migrationBlock: { migration, oldSchemaVersion in  },  objectTypes:[DownloadedLaw.self, Organizable.self, Bookmark.self, SelectionObject.self, Tag.self, TagEntity.self])

        
        self.realm = try Realm(configuration: config)
      }else {
        self.realm = try Realm(configuration: config)
      }
      UserDefaults.standard.set(false, forKey: "ReadingRealm")
      UserDefaults.standard.synchronize()
      return true
    }catch let error {
      print("error \(error.localizedDescription)")
      
    }
    return false
  }
  
  func receive(data: Data) {
    let fm = FileManager.default
    let tempPath = (NSTemporaryDirectory() as NSString).appendingPathComponent("_tempRealm.realm")
    let tempUrl = URL(fileURLWithPath: tempPath)
    
    if fm.fileExists(atPath: tempPath) {
      try? fm.removeItem(at: tempUrl)
    }
    try? data.write(to: tempUrl)
    
    autoreleasepool {
      
      var dbUrl = fm.appSupportUrl
      dbUrl.appendPathComponent("LawXML \(shortUUIDString()).realm")
      
      try? fm.moveItem(at: tempUrl, to: dbUrl)
      try? fm.setAttributes([FileAttributeKey.modificationDate: Date()], ofItemAtPath: dbUrl.path)
      readRealmFile()
      
      let center = NotificationCenter.default
      center.post(name: LawNotification.realmDidUpdateFromExternNotification, object: nil, userInfo: nil)
    }
  }
  
  func didSave() {
    RealmManager.shared.blockToExecuteOnBackground = nil
  }

  
  // Backup
  var lastBackupDateString = ""
  lazy var backupFilenameFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyyMMdd"
    return formatter
  }()
  
  func keepBackup(forceBackup: Bool = false, writeToCopy: Bool = false) {
    
    let ud = UserDefaults.standard
    let todaysString = backupFilenameFormatter.string(from: Date())
    
    if lastBackupDateString == "" {
      lastBackupDateString = ud.string(forKey: "LastBackupDate") ?? ""
    }
    
    if todaysString != lastBackupDateString || forceBackup == true {
      lastBackupDateString = todaysString

      var copyDbUrl = FileManager.default.appSupportUrl
      copyDbUrl.appendPathComponent("LawXML \(shortUUIDString()).realm")
      
      if FileManager.default.fileExists(atPath: copyDbUrl.path) {
        try? FileManager.default.removeItem(at: copyDbUrl)
      }
      
      do {
        if writeToCopy {
          try realm.writeCopy(toFile: copyDbUrl)
        }else {
          try realm.write {
            if let url = realm.configuration.fileURL {
              try FileManager.default.copyItem(at: url, to: copyDbUrl)
            }
          }
        }
        ud.set(lastBackupDateString, forKey: "LastBackupDate")

      } catch _ {
        
      }
    }
    
  }
  
  func add(_ organizable: Object) -> Bool {
    do {
      try realm.write() {
        realm.add(organizable)
      }
      
      didSave()
    }catch _ {
      return false
    }
    return true
  }
  
  func delete(selectionWith uuid: String) -> Bool {
    if let selection = selection(with: uuid) {
      return self.delete(object: selection)
    }
    
    return false
  }
  
  func replace(existingTags: RealmSwift.Results<Tag>? = nil, with newTags: [Tag]) -> Bool {
    do {
      try realm.write() {
        if existingTags != nil {
          realm.delete(existingTags!)
        }
        realm.add(newTags)
      }
      
      didSave()
    }catch _ {
      return false
    }
    return true
  }
  
  
  
  func update(selectionWith uuid: String, color: HighlightColor?, notes: String? = nil) -> Bool {
    do {
      
      try realm.write() {
        if let selection = selection(with: uuid) {
          if color != nil {
            selection.style = Int8(color!.rawValue)
          }
          if notes != nil {
            if notes!.isEmpty {
              selection.notes = nil
            }else {
              selection.notes = notes
            }
          }
        }
      }
      didSave()
      
    }catch _ {
      return false
    }
    return true
  }
  
  func selection(with uuid: String) -> SelectionObject? {
    let objects = realm.objects(SelectionObject.self).filter(NSPredicate(format: "uuid == %@", uuid))
    return objects.first
  }
  
  func selections(for law: DownloadedLaw, findString: String? = nil, startAnchor: String? = nil, endAnchor: String? = nil) -> RealmSwift.Results<SelectionObject>? {
    guard law.isInvalidated == false else { return nil }
    assert( (startAnchor != nil && endAnchor != nil ) == false )
    
    let sortDescriptors: [SortDescriptor] = [SortDescriptor(keyPath: "row", ascending: true),
                                             SortDescriptor(keyPath: "startIndexInRow", ascending: true)]

    if findString != nil {
      let predicate = NSPredicate(format: "lawNo == %@ && notes CONTAINS %@", law.lawNum, findString!)
      return realm.objects(SelectionObject.self).filter(predicate).sorted(by: sortDescriptors)
    }
    
    if startAnchor != nil {
      return realm.objects(SelectionObject.self).filter(NSPredicate(format: "lawNo == %@ && startAnchor == %@", law.lawNum, startAnchor!)).sorted(by: sortDescriptors)
    }
    
    if endAnchor != nil {
      return realm.objects(SelectionObject.self).filter(NSPredicate(format: "lawNo == %@ && endAnchor == %@", law.lawNum, endAnchor!)).sorted(by: sortDescriptors)
    }
    
    return realm.objects(SelectionObject.self).filter(NSPredicate(format: "lawNo == %@", law.lawNum)).sorted(by: sortDescriptors)
  }
  
  func bookmarks(for law: LawSource, uuid: String? = nil, findString: String? = nil) -> RealmSwift.Results<Bookmark>? {
    if true == (law as? Object)?.isInvalidated { return nil }
    
    if findString != nil {
      let predicate = NSPredicate(format: "lawNo == %@ && notes CONTAINS %@", law.lawNum, findString!)
      return realm.objects(Bookmark.self).filter(predicate)
    }
    
    if uuid == nil {
      return realm.objects(Bookmark.self).filter(NSPredicate(format: "lawNo == %@", law.lawNum)).sorted(byKeyPath: "row")
    }
    let predicate = NSPredicate(format: "lawNo == %@ && uuid == %@", law.lawNum, uuid!)
    return realm.objects(Bookmark.self).filter(predicate)
  }
  
  func tags(for law: LawSource, anchor: String? = nil) -> RealmSwift.Results<Tag>? {
    if true == (law as? Object)?.isInvalidated { return nil }
    
    if anchor == nil {
      return realm.objects(Tag.self).filter(NSPredicate(format: "lawNo == %@", law.lawNum))
    }
    
    let predicate = NSPredicate(format: "lawNo == %@ && anchor == %@", law.lawNum, anchor!)
    return realm.objects(Tag.self).filter(predicate)
  }
  
  func tags(for law: LawSource, tagNumber: Int) -> RealmSwift.Results<Tag>? {
    if true == (law as? Object)?.isInvalidated { return nil }
    let predicate = NSPredicate(format: "lawNo == %@ && tagNumber == %d", law.lawNum, tagNumber)
    return realm.objects(Tag.self).filter(predicate)
  }
  
  func tagEntites(for tagNumbers: [Int]) -> RealmSwift.Results<TagEntity>? {
    let predicate = NSPredicate(format: "tagNumber in %@", tagNumbers)
    return realm.objects(TagEntity.self).filter(predicate).sorted(byKeyPath: "order")
  }
  
  func delete(tagEntity obj: TagEntity, inheritToEntity: TagEntity? = nil) -> Bool {
    guard false == obj.isInvalidated else { return true } // ALREADY DELETED
    
    NotificationCenter.default.post(name: LawNotification.realmWillUpdateNotification, object: nil)
    
    // DELETE ALL TAGS WITH THIS TAG NUMBER
    var success = false
    let tagNumber = obj.tagNumber
    var filterTagNumbers = UserDefaults.standard.object(forKey: "FilterTagNumbers") as? [Int] ?? []
    if let idx = filterTagNumbers.index(of: tagNumber) {
     filterTagNumbers.remove(at: idx)
      UserDefaults.standard.set(filterTagNumbers, forKey: "FilterTagNumbers")
    }
    do {
      let predicate: NSPredicate = NSPredicate( format: "tagNumber == %d", tagNumber)
      let tags = realm.objects(Tag.self).filter(predicate)
      
      try realm.write() {
        realm.delete(obj)
        
        if inheritToEntity != nil {
          tags.forEach {
            $0.tagNumber = inheritToEntity!.tagNumber
          }
          
        }else {
          realm.delete(tags)
        }
        success = true
      }
      
      didSave()
    }catch _ {
      
    }
    
    NotificationCenter.default.post(name: LawNotification.realmDidUpdateNotification, object: nil)
    return success
  }
  
  func reorder(tagEntities: [TagEntity]) -> Bool {
    
    NotificationCenter.default.post(name: LawNotification.realmWillUpdateNotification, object: nil)
    
    var success = false
    do {
      
      try realm.write() {
        var order = 10
        tagEntities.forEach {
          $0.order = order
          
          order += 10
        }
        success = true
      }
      
      didSave()
    }catch _ {
      
    }
    
    NotificationCenter.default.post(name: LawNotification.realmDidUpdateNotification, object: nil)
    return success
  }

  
  func update(tagEntity obj: TagEntity, color: TagColor? = nil, title: String? = nil) -> Bool {
    guard false == obj.isInvalidated else { return false }
    
    NotificationCenter.default.post(name: LawNotification.realmWillUpdateNotification, object: nil)
    
    var success = false
    do {
      
      try realm.write() {
        if color != nil {
          obj.color = color!
        }
        if title != nil {
          obj.title = title!
        }
        success = true
      }
      
      didSave()
    }catch _ {
      
    }
    
    NotificationCenter.default.post(name: LawNotification.realmDidUpdateNotification, object: nil)
    return success
  }
  
  func addNewTagEntity() -> TagEntity? {
    let tagEntites = RealmManager.shared.allTagEntities
    
    let tagEntity = TagEntity()
    if let lastOrder = tagEntites.last?.order {
      tagEntity.order = lastOrder + 10
    }else {
      tagEntity.order = 10
    }
    tagEntity.title = "New Tag"
    tagEntity.color = .red
    
    // LOOK FOR UNUSED TAG NUMBER
    var tagNumber = tagEntites.count + 1
    while  tagEntites.filter({ $0.tagNumber == tagNumber }).count != 0 {
      tagNumber += 1
    }
    tagEntity.tagNumber = tagNumber
    if RealmManager.shared.add(tagEntity) {
      return tagEntity
    }
    return nil
  }
  
  var allTagEntities: RealmSwift.Results<TagEntity> {
    return realm.objects(TagEntity.self).sorted(byKeyPath: "order")
  }
  
  var allDownloadedLaws: RealmSwift.Results<DownloadedLaw> {
    return realm.objects(DownloadedLaw.self)
  }
  
  var allOrganizableObjects: [Organizable] {
    let obj1 = Array(realm.objects(DownloadedLaw.self)) as [Organizable]
//    let obj2 = Array(realm.objects(SelectionObject.self)) as [Organizable]
    let obj3 = Array(realm.objects(Bookmark.self)) as [Organizable]
    let obj4 = Array(realm.objects(Organizable.self)) as [Organizable]
    
    return obj1 + /*obj2 +*/ obj3 + obj4
  }
  
  func organizableObjects(with text: String) -> [Organizable] {
    let predicate: NSPredicate = NSPredicate( format: "title CONTAINS %@ || lawTitle CONTAINS %@", text, text)
    let obj1 = Array(realm.objects(DownloadedLaw.self).filter(predicate)) as [Organizable]
    //    let obj2 = Array(realm.objects(SelectionObject.self)) as [Organizable]
    
    let predicate2: NSPredicate = NSPredicate( format: "title CONTAINS %@ || notes CONTAINS %@", text, text)
    let obj3 = Array(realm.objects(Bookmark.self).filter(predicate2)) as [Organizable]
    
    let predicate3: NSPredicate = NSPredicate( format: "title CONTAINS %@", text)
    let obj4 = Array(realm.objects(Organizable.self).filter(predicate3)) as [Organizable]
    
    
    
    return obj1 + /*obj2 +*/ obj3 + obj4
  }
  
  enum OrganizableType {
    case downloadedLaw, selectionObject, bookmark, organizable, all
  }
  
  func organizableObjects(in folderpath: String, types: [OrganizableType] = []) -> [Organizable] {
    guard realm != nil else { return [] }
    let predicate: NSPredicate = NSPredicate( format: "filepath == %@", folderpath)
    var obj1: [Organizable] = []
//    var obj2: [Organizable] = []
    var obj3: [Organizable] = []
    var obj4: [Organizable] = []
    if types.contains(.downloadedLaw) || types.isEmpty {
      obj1 = Array(realm.objects(DownloadedLaw.self).filter(predicate)) as [Organizable]
    }
//    if types.contains(.selectionObject) || types.isEmpty {
//      obj2 = Array(realm.objects(SelectionObject.self).filter(predicate)) as [Organizable]
//    }
    if types.contains(.bookmark) || types.isEmpty {
      obj3 = Array(realm.objects(Bookmark.self).filter(predicate)) as [Organizable]
    }
    if types.contains(.organizable) || types.isEmpty {
      obj4 = Array(realm.objects(Organizable.self).filter(predicate)) as [Organizable]
    }
    
    let array = obj1 + /*obj2 +*/ obj3 + obj4
    
    return array.sorted { $0.order < $1.order }
  }
  
  func organizableObject(with uuid: String) -> Organizable? {
    
    let predicate: NSPredicate = NSPredicate( format: "uuid == %@", uuid)
    
    if let obj1 = realm.objects(DownloadedLaw.self).filter(predicate).first { return obj1 }
    if let obj3 = realm.objects(Bookmark.self).filter(predicate).first { return obj3 }
    if let obj4 = realm.objects(Organizable.self).filter(predicate).first { return obj4 }
    
//    if let obj2 = realm.objects(SelectionObject.self).filter(predicate).first { return obj2 }
    
    return nil
  }

  func law(with lawEdition: String) -> DownloadedLaw? {
    
    let predicate: NSPredicate = NSPredicate( format: "lawEdition == %@", lawEdition)
    if let obj1 = realm.objects(DownloadedLaw.self).filter(predicate).first { return obj1 }
    return nil
  }
  
  func law(withNum lawNum: String) -> DownloadedLaw? {
    
    let predicate: NSPredicate = NSPredicate( format: "lawNum == %@", lawNum)
    if let obj1 = realm.objects(DownloadedLaw.self).filter(predicate).first { return obj1 }
    return nil
  }
  
  func addOrganizable(under filepath: String) -> Organizable? {
    var obj: Organizable? = Organizable()
    obj!.filepath = filepath
    obj!.uuid = shortUUIDString()
    obj!.title = "新規フォルダ"
    obj!.order = Int(Date().timeIntervalSinceReferenceDate)
    do {
      try realm.write() {
        realm.add(obj!)
      }
    }catch _ {
      obj = nil
    }
    
    return obj
  }
  
  func reorder(_ organizables: [Organizable]) {
    
    do {
      var n = 50
      try realm.write() {
        for organizable in organizables {
          organizable.order = n
          n += 10
        }
      }
      
      didSave()
    }catch let error {
      print(error.localizedDescription)
    }
  }
  
  func update(_ obj: Organizable, title: String? = nil, notes: String? = nil) -> Bool {
    var success = false
    do {
      try realm.write() {
        
        if title != nil {
          if title == obj.titleToUse() {
            //obj.title = ""
          }else {
            obj.title = title!
          }
        }
        
        if notes != nil, let bookmark = obj as? Bookmark {
          if bookmark.notes != notes {
            bookmark.notes = notes
          }
        }
      }
      
      didSave()
      success = true
    }catch _ {
      success = false
    }
    return success
  }
  
  func setFilepath(of obj: Organizable, to newPath: String) {
    do {
      try realm.write() {

        obj.filepath = newPath
      }
      didSave()
    }catch _ {
    }

  }
  
  func setFilepath(of obj: Organizable, to destination: Organizable?) {
    
    let currentPathForChildren = obj.filepath + obj.uuid + "/"
    let newPath: String
    let newPathForChildren: String
    if let destination = destination {
      newPath = destination.filepath + destination.uuid + "/"
      newPathForChildren = destination.filepath + destination.uuid + "/" + obj.uuid + "/"
      
    }else {
      newPath = "/"
      newPathForChildren =  "/" + obj.uuid + "/"
    }
    let predicate: NSPredicate = NSPredicate( format: "filepath BEGINSWITH %@", currentPathForChildren)
    
    let obj1 = Array(realm.objects(DownloadedLaw.self).filter(predicate)) as [Organizable]
//    let obj2 = Array(realm.objects(SelectionObject.self).filter(predicate)) as [Organizable]
    let obj3 = Array(realm.objects(Bookmark.self).filter(predicate)) as [Organizable]
    let obj4 = Array(realm.objects(Organizable.self).filter(predicate)) as [Organizable]
    
    let array = obj1 /*+ obj2 */ + obj3 + obj4
    
    do {
      try realm.write() {
        array.forEach {
          let filepath = $0.filepath.replacingOccurrences(of: currentPathForChildren, with: newPathForChildren)
          $0.filepath = filepath
        }
        
        obj.filepath = newPath
        obj.order = 5000

      }
      didSave()
    }catch _ {
    }
  }
  
  func delete(organizable obj: Organizable) -> Bool {
    guard false == obj.isInvalidated else { return true } // ALREADY DELETED
    
    if obj is Bookmark {
    }else {
      RealmManager.shared.keepBackup(forceBackup: true)
    }
    
    NotificationCenter.default.post(name: LawNotification.realmWillUpdateNotification, object: nil)

    let currentPathForChildren = obj.filepath + obj.uuid + "/"
    let predicate = NSPredicate( format: "filepath BEGINSWITH %@", currentPathForChildren)
    
    let obj1 = Array(realm.objects(DownloadedLaw.self).filter(predicate)) as [Organizable]
    //let obj2 = Array(realm.objects(SelectionObject.self).filter(predicate)) as [Organizable]
    let obj3 = Array(realm.objects(Bookmark.self).filter(predicate)) as [Organizable]
    let obj4 = Array(realm.objects(Organizable.self).filter(predicate)) as [Organizable]
    
    let array = obj1 /*+ obj2 */+ obj3 + obj4
    var success = false
    do {
      
      try realm.write() {
        
        array.forEach { $0.deleteRelatedFiles() }
        obj.deleteRelatedFiles()
        realm.delete(obj)
        realm.delete(array)
        success = true
      }
      
      didSave()
    }catch _ {
      
    }
    
    NotificationCenter.default.post(name: LawNotification.realmDidUpdateNotification, object: nil)
    return success
  }
  
  func delete(object obj: SelectionObject) -> Bool {
    guard false == obj.isInvalidated else { return true } // ALREADY DELETED
        
    var success = false
    do {
      
      try realm.write() {
        
        realm.delete(obj)
        success = true
      }
      
      didSave()
    }catch _ {
      
    }
    
    return success
  }
  
  func identicalLaws(_ law: DownloadedLaw) -> [DownloadedLaw] {
    
    let predicate = NSPredicate( format: "lawNum == %@ && lawTitle == %@ && lawEdition == %@", law.lawNum, law.lawTitle, law.lawEdition)    
    let allLaws = realm.objects(DownloadedLaw.self).filter(predicate)
    
    return Array(allLaws) as [DownloadedLaw]
  }
  
  func update(law: DownloadedLaw, lawEdition: String, lawTitle: String, filename: String) throws ->  Bool {
    guard law.isInvalidated == false else { return true }
    NotificationCenter.default.post(name: LawNotification.realmWillUpdateNotification, object: nil)

    try law.realm?.write() {
      
      law.lawTitle = lawTitle
      law.lawEdition = lawEdition
      law.filename = filename
//      if xml != nil {
//        law.xml = xml! // GENERATING FILE OCCURS HERE
//      }
//      if imageDataPath != nil {
//        law.writeImageData(imageDataPath!)  //
//      }
    }
    
    didSave()
    NotificationCenter.default.post(name: LawNotification.realmDidUpdateNotification, object: nil)
    return true
  }
  
  func add(lawNum: String, lawEdition: String, lawTitle: String, mishikoLawNum: String = "", filename:String = "", under: String) throws -> DownloadedLaw {
    let law = DownloadedLaw()
    
    law.uuid = shortUUIDString()
    law.lawNum = lawNum.replacingOccurrences(of: " ", with: "")
    law.lawTitle = lawTitle
    law.lawEdition = lawEdition
    law.mishikoLawNum = mishikoLawNum
    law.filepath = under
    law.filename = filename
    law.order = Int(Date().timeIntervalSinceReferenceDate)
    law.addedDate = Date()
//    if xml != nil {
//      law.xml = xml! // GENERATING FILE OCCURS HERE
//    }
//    if imageDataPath != nil {
//      law.writeImageData(imageDataPath!)  //
//    }
    try realm.write() {
      realm.add(law)
    }
    
    didSave()
    NotificationCenter.default.post(name: LawNotification.realmDidUpdateNotification, object: nil)
    return law
  }
  
  @available(watchOSApplicationExtension 3.0, *)
  func recordToDictionary(_ record: CKRecord) -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary["recordName"] = record.recordID.recordName
    dictionary["fromAnchor"] = record["fromAnchor"]
    dictionary["fromLaw"] = record["fromLaw"]
    dictionary["fromLawEdition"] = record["fromLawEdition"]
    dictionary["occurrenceIndex"] = record["occurrenceIndex"]
    dictionary["string"] = record["string"]
    dictionary["toLaw"] = record["toLaw"]
    dictionary["toLawEdition"] = record["toLawEdition"]
    dictionary["toAnchor"] = record["toAnchor"]
    dictionary["category"] = record["category"]

    return dictionary
  }
  
  //MARK:- Fetched Links
  
  private let fetchedLinksRealm: Realm? = {
    let dir = FileManager.default.urls(for: .cachesDirectory,  in: .userDomainMask)[0]
    
    let url =  dir.appendingPathComponent("FetchedLinks.realm")
    let config = Realm.Configuration(fileURL: url, encryptionKey: nil, readOnly: false, objectTypes:[FetchedLinks.self])
    
    let realm = try? Realm(configuration: config)
    return realm
  }()
  
  func fetchedLinks(for lawNum: String) -> FetchedLinks? {
    let objects = fetchedLinksRealm!.objects(FetchedLinks.self).filter(NSPredicate(format: "lawNum == %@", lawNum)).first
  
    return objects
    
  }

  func addFetchedLinks(_ records: [CKRecord], to lawNum: String, _ fetchedLinksLaw: FetchedLinks?) -> FetchedLinks? {
    var fetchedLinksLaw = fetchedLinksLaw
    if fetchedLinksLaw == nil {
      try? fetchedLinksRealm?.write {
        fetchedLinksLaw = FetchedLinks()
        fetchedLinksLaw!.lawNum = lawNum
        fetchedLinksRealm?.add(fetchedLinksLaw!)
      }
    }
    
    var array: [[String: Any]] = []
    for record in records {
      array.append(recordToDictionary(record))
    }
    do {
      
      if let data = fetchedLinksLaw?.fetchedLinks as NSData?,
        let inflated = data.inflate(),
        let plist = try JSONSerialization.jsonObject(with: inflated, options: .mutableContainers) as? [[String: Any]] {
        array = plist + array
      }
      
      let data = try JSONSerialization.data(withJSONObject: array)
      
      try fetchedLinksRealm?.write() {
        fetchedLinksLaw?.fetchedLinks = (data as NSData).deflate()
        fetchedLinksLaw?.fetchedLinksCache = nil
        fetchedLinksLaw?.lastFetchedLinksDate = Date()
      }
      
    }catch _ {
      print("*** JSON ERROR")
    }
    return fetchedLinksLaw
  }
  
}

let ArticleDatabaseVersion: Int = 2

class ArticleCacheManager {
  static var shared = ArticleCacheManager()
  static let schemaVersion: UInt64 = 2

  class func cacheName(for law: LawSource) -> String {
    let name = law.localFilename as NSString
    return "\(name.deletingPathExtension) \(ArticleDatabaseVersion).realm"
  }
  
  class func obsoleteCacheNames(for law: LawSource) -> [String] {
    let name = law.localFilename as NSString
    return (1..<ArticleDatabaseVersion).map { "\(name.deletingPathExtension) \($0).realm" }
  }
  
  func realmExists(for law: LawSource) -> URL? {
    if (law as? DownloadedLaw)?.isInvalidated == true { return nil }
    
    // CHECK CACH FOLDER
    let cacheName = ArticleCacheManager.cacheName(for: law)

    // CHECK DOC FOLDER
    let docDir = FileManager.default.appSupportUrl
    var docUrl = docDir.appendingPathComponent(cacheName)
    if FileManager.default.fileExists(atPath: docUrl.path) {
      return docUrl
    }
    
    // CHECK CACHE FOLDER
    let cacheDir = FileManager.default.urls(for: .cachesDirectory,  in: .userDomainMask)[0]
    docUrl = cacheDir.appendingPathComponent(cacheName)
    if FileManager.default.fileExists(atPath: docUrl.path) { return docUrl }
    
    return nil
  }
  
  
  func realmExists(for cacheName: String) -> URL? {
    
    // CHECK DOC FOLDER
    let docDir = FileManager.default.appSupportUrl
    var docUrl = docDir.appendingPathComponent(cacheName)
    if FileManager.default.fileExists(atPath: docUrl.path) { return docUrl }
    
    // CHECK CACHE
    let cacheDir = FileManager.default.urls(for: .cachesDirectory,  in: .userDomainMask)[0]
    docUrl = cacheDir.appendingPathComponent(cacheName)
    if FileManager.default.fileExists(atPath: docUrl.path) { return docUrl }
    
    return nil
  }
  
//  func moveIfNecessary(_ law: LawSource) {
//    if (law as? DownloadedLaw)?.isInvalidated == true { return }
//
//    // CHECK CACH FOLDER
//    let dir = FileManager.default.urls(for: .cachesDirectory,  in: .userDomainMask)[0]
//    let name = DownloadedLaw.localFilenameWith(lawNum: law.lawNum, lawTitle: law.lawTitle, lawEdition: law.lawEdition) as NSString
//    let cacheUrl = dir.appendingPathComponent("\(name.deletingPathExtension) \(ArticleDatabaseVersion).realm")
//
//    if FileManager.default.fileExists(atPath: cacheUrl.path) {
//      let docDir = FileManager.default.appSupportUrl
//
//      let docUrl = docDir.appendingPathComponent("\(name.deletingPathExtension) \(ArticleDatabaseVersion).realm")
//
//      do {
//        try FileManager.default.moveItem(at: cacheUrl, to: docUrl)
//        _ = docUrl.setExcludedFromBackup()
//      }catch { }
//
//    }
//  }
  
  func realm(for law: LawSource) -> Realm? {
//    moveIfNecessary(law)
    var realmUrl: URL
    if let url = realmExists(for: law) {
      realmUrl = url
    } else {
      return nil
    }
    
    let config = Realm.Configuration(fileURL: realmUrl, encryptionKey: nil, readOnly: false, schemaVersion: ArticleCacheManager.schemaVersion, migrationBlock: { migration, oldSchemaVersion in  }, objectTypes:[Toc.self, Row.self])
    let realm = try! Realm(configuration: config)
    return realm
  }
  
  func deleteCache(for law: LawSource) {

    var name = law.localFilename as NSString
    name = name.deletingPathExtension as NSString
    
    let dir = FileManager.default.appSupportUrl
    let contents = try? FileManager.default.contentsOfDirectory(at: dir, includingPropertiesForKeys: [], options: [.skipsHiddenFiles])
    
    contents?.filter { $0.lastPathComponent.hasPrefix(name as String) }
      .forEach {
        try? FileManager.default.removeItem(at: $0)
    }
    
  }
  
  func write(_ articles: [Row], _ toc: [Toc], for law: LawSource, inCacheFolder: Bool = false) -> Error? {
    defer {  objc_sync_exit(self) }
    objc_sync_enter(self)
    
    let fm = FileManager.default
    var dir: URL
    
    dir = fm.urls(for: .cachesDirectory,  in: .userDomainMask)[0]
    
    if law.lawEdition.isEmpty { return nil }
    
    
    let name = law.localFilename as NSString
    let pathComponent = "\(name.deletingPathExtension) \(ArticleDatabaseVersion)"
    
    let cacheDir = fm.urls(for: .cachesDirectory,  in: .userDomainMask)[0]
    let cacheUrl = dir.appendingPathComponent("\(pathComponent).realm")
    
    if fm.fileExists(atPath: cacheUrl.path) {
      return nil
    }
    
    let cacheName = pathComponent + shortUUIDString()
    let cacheCopyUrl = cacheDir.appendingPathComponent("_temp\(cacheName).realm")
    let lock = cacheDir.appendingPathComponent("_temp\(cacheName).realm.lock")
    let management = cacheDir.appendingPathComponent("_temp\(cacheName).realm.management")
    
    do {
      if fm.fileExists(atPath: cacheUrl.path) {
        try fm.removeItem(at: cacheUrl)
      }
      if fm.fileExists(atPath: cacheCopyUrl.path) {
        try fm.removeItem(at: cacheCopyUrl)
      }
      
      if fm.fileExists(atPath: lock.path) {
        try FileManager.default.removeItem(at: lock)
      }
      
      if fm.fileExists(atPath: management.path) {
        try FileManager.default.removeItem(at: management)
      }
    }catch let error {
      return error// ????
    }
    
    let config = Realm.Configuration(fileURL: cacheCopyUrl, encryptionKey: nil, readOnly: false, schemaVersion: ArticleCacheManager.schemaVersion, objectTypes:[Toc.self, Row.self])
    do {
      let realm = try Realm(configuration: config)
      
      try realm.write() {
        realm.add(articles)
        realm.add(toc)
        try realm.writeCopy(toFile: cacheUrl)
      }
      
      if fm.fileExists(atPath: cacheCopyUrl.path) {
        try fm.removeItem(at: cacheCopyUrl)
      }
      
      if fm.fileExists(atPath: lock.path) {
        try FileManager.default.removeItem(at: lock)
      }
      
      if fm.fileExists(atPath: management.path) {
        try FileManager.default.removeItem(at: management)
      }
      
      _ = cacheUrl.setExcludedFromBackup()
      
      DispatchQueue.main.async {
        RealmManager.shared.didSave()
      }
    }catch let error {
      return error// ????
    }
   

    return nil;
  }
}
