//
//  LawNameListInfo.swift
//  MacParser
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
import Realm
import RealmSwift


//final class LawNameListInfo: Object {
//  @objc dynamic var lawName: String = ""
//  @objc dynamic var lawNo: String = ""
//
//  override static func indexedProperties() -> [String] {
//    return ["lawName", "lawNo"]
//  }
//
//  override func isEqual(_ object: Any?) -> Bool {
//    guard let obj = object as? LawNameListInfo else { return false }
//
//    if obj.lawNo == self.lawNo && obj.lawName == self.lawName { return true }
//    return false
//  }
//}


final class IndexedLaw: Object {
  @objc dynamic var lawName: String = ""
  @objc dynamic var lawNo: String = ""
  @objc dynamic var xml: String = ""
  @objc dynamic var bz2: Data = Data()

  override static func indexedProperties() -> [String] {
    return ["lawName", "lawNo", "xml"]
  }
}
