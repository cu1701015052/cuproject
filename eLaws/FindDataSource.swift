//
//  FindDataSource.swift
//  LawXML
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation


class FindDataSource {

  private var findArray: [Match]? = nil
  var findIndex = 0

  init(findString: String? = nil, findArray: [Match],  findIndex: Int? = nil) {
    self.findString = findString
    self.findArray = findArray
    if findIndex != nil {
     self.findIndex = findIndex!
    }
  }
  
//  func find(_ findString: String) {
//    guard let findString = (findString as NSString).omitSpacesAtBothEnds() else { return }
//    self.findString = findString
//    findArray = []
//
//    let (_, array, time) = source.findString(findString)
//
//    findIndex = 0
//    findTime = String(format: "%.2f", time)
//    findArray = array
//  }
//
  var findStringRaw: String? = nil
  var findString: String?  {
    set {
      var string: String? = newValue
      
      let findStrings = newValue?.components(separatedBy: CharacterSet.whitespaces).filter { $0.isEmpty == false }

      string = findStrings?.joined(separator: "|")

      findStringRaw = string
    }
    get {
      return findStringRaw
    }
  }
  var count: Int {
    return findArray?.count ?? 0
  }
  
  subscript(n: Int) -> Match {
    return findArray![n]
  }
  
  var match: Match {
    return findArray![findIndex]
  }
  
  var findTime: String? = nil
  var findTotalString: String? {
    
    if let findCount = findArray?.count, findCount > 0 {
      let localizedString = NSString(format: WLoc("Found %d") as NSString, findCount)
      return "\(String(findIndex + 1)) / " + (localizedString as String)
    }
    else {
      if findString == nil { return nil }
      return WLoc("Not Found")
    }
  }
  
  var canFindPrevious: Bool {
    if findArray == nil { return false }
    return findIndex != 0
  }
  
  var canFindNext: Bool {
    if findArray == nil { return false }
    return findIndex + 1 < findArray!.count
  }
  
  func clearFind() {
    findArray = nil
  }
  
}
