//
//  BrowserViewModel.swift
//  LawXML
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import CloudKit

protocol BrowserViewModelDelegate: class {
  func fetchingDidChange(fetching: Bool, hasUpdate: Bool)
  func contentOffsetDidChange()
  func unreadDidChange()

  // SELECTION
  func selectionDidChange(asHighlight: Bool, attributes: [NSAttributedString.Key: Any]?)
  func adjustedSelectionRangeFor(_ range: SelectionRange, oldRange: SelectionRange) -> SelectionRange
}

class DetailViewModel {
  
  struct Offset: Equatable {
    static func ==(lhs: Offset, rhs: Offset) -> Bool {
      return lhs.indexPath == rhs.indexPath && lhs.localOffset == rhs.localOffset && lhs.destinationLink == rhs.destinationLink
    }
    var indexPath: MyIndexPath?
    var localOffset: CGFloat?
    var destinationLink: String?
    
    var isNull: Bool {
      return indexPath == nil && localOffset == nil && destinationLink == nil
    }
    
    func plist() -> [String: Any] {
      var plist: [String: Any] = [:]
      plist["offsetIndexPath.section"] = indexPath?.section
      plist["offsetIndexPath.row"] = indexPath?.row
      plist["offsetIndexPath.localOffset"] = localOffset
      plist["offsetIndexPath.destinationLink"] = destinationLink
      return plist
    }
    
    static func fromPlist(_ plist: [String: Any]) -> Offset {
      
      let section: Int? = plist["offsetIndexPath.section"] as? Int
      let row: Int? = plist["offsetIndexPath.row"] as? Int
      let localOffset: CGFloat? = plist["offsetIndexPath.localOffset"] as? CGFloat
      let destinationLink: String? = plist["offsetIndexPath.destinationLink"] as? String
      
      var indexPath: MyIndexPath? = nil
      if row != nil && section != nil {
        indexPath = MyIndexPath(row: row!, section: section!)
      }
      let offset = Offset(indexPath: indexPath, localOffset: localOffset, destinationLink: destinationLink)
      
      return offset
      
    }
  }
  
  var hasUnreadForum = false {
    didSet {
      if hasUnreadForum != oldValue {
        delegate?.unreadDidChange()
      }
    }
  }
  
  var screenTimeTimer: Date?
  var screenTimeDate: Int?

  var highlightByStylus = false
  lazy var stylusStatus = StylusStatus()

  weak var delegate: BrowserViewModelDelegate? = nil
  var selectedLaw: LawSource? = nil {
    didSet {
      if selectedLaw != nil && !(selectedLaw is DownloadedLaw) && selectedLaw?.downloadOption != .forceDownload {
        if let nextDownloadedLaw = self.getLawFromRealmDatabase(lawSource: selectedLaw!) {
          self.selectedLaw = nextDownloadedLaw
        }
      }
      
    }
  }
  
  var filterSetting = FilterSetting(name: "")
  
  var html: String? = nil
  
  fileprivate var selectedRange_: SelectionRange = SelectionRange()
  var progressHandler: ((_ progressMessage: String)->Void)? = nil

  var title: String {
    return selectedLaw?.lawTitle ?? ""
  }
  var hideTitleOnOpening = true
  
  var hasUpdate: Bool {
    
    if let law = selectedLaw, (law as? DownloadedLaw)?.isInvalidated == false, (law as? DownloadedLaw)?.mishikoLawNum.isEmpty == true, let latestLaw = LawList().lawsWithNum(law.lawNum, orderByFilenameAscending: true)?.last {
      
      if let results = LawList().lawsWithEdition(law.lawEdition), results.count > 0, latestLaw.lawEdition != selectedLaw?.lawEdition {
        return true
      }
    }
    return false
  }
  
  
  var backTitle: String {
  let title = self.title
    if title.utf16.count > 10 {
        return title[...title.index(title.startIndex, offsetBy: 9)] + "…"
    }else {
      return title
    }
  }
  var insidePicker: Bool = false
  var insideDiff: Bool = false {
    didSet {
      if insideDiff {
        insidePicker = true
      }
    }
  }
  
  var isFetching: Bool = false
  var hasCloudKitAccount__ = false
  
  // SOURCE
  var source: FetchResults? = nil {
    didSet {
      source?.filterSetting = filterSetting
      source?.styleSheet = styleSheet
    }
  }
  
  var styleSheet: StyleSheet!
  var isVertical: Bool {
    get {
      return UserDefaults.standard.bool(forKey: "VerticalMode")
    }
    set {
      let ud = UserDefaults.standard
      ud.set( newValue, forKey: "VerticalMode")
    }
  }
  var isVerticalFullScreen: Bool {
    get {
      if insidePicker { return false }
      return UserDefaults.standard.bool(forKey: "VerticalFullScreenMode")
    }
    set {
      let ud = UserDefaults.standard
      ud.set( newValue, forKey: "VerticalFullScreenMode")
    }
  }
  var isVerticalTab: Bool {
    set {
      let ud = UserDefaults.standard
      ud.set( !newValue, forKey: "VerticalTabViewHorizontal")
    }
    get {
      return !UserDefaults.standard.bool(forKey: "VerticalTabViewHorizontal")
    }
  }

  var clickableAttributes: [NSAttributedString.Key] {
    if highlightByStylus {
      return [.highlightSerialIdName, .tempLink, .controlName, .embeddedAttachement, .mask]
      
    }else {
      return [.tempLink, .highlightSerialIdName, .controlName, .embeddedAttachement, .mask]
    }
  }
  //var tocs: [Toc] = []
  //var articles: [Article] = []
  var anchors: [String] = []
  var contentOffsetCache: Offset = Offset()
  var contentOffset: Offset  {
    get {
      return contentOffsetCache
    }
    set {
      if contentOffsetCache != newValue {
        contentOffsetCache = newValue
        delegate?.contentOffsetDidChange()
      }
    }
  }
  
  var startupContentOffset: Offset? = nil
  
  var contentOffsetStack: [Offset] = []
  var contentOffsetForwardStack: [Offset] = []
  
  var sectionHeaderViewHeight: CGFloat = 25
  var autoHideToolbars: Bool {
    if isVerticalFullScreen { return true }
    return UserDefaults.standard.bool(forKey: "AutoHideToolbars")
  }
  
  var startupAnchor: String? = nil {
    didSet {
      if startupAnchor == nil {
        contentOffsetStack.removeAll()
        contentOffsetStack.append(Offset())
      }
    }
  }
  
//  var insideFullTextSearch = false

  init(law: LawSource, delegate: BrowserViewModelDelegate? = nil) {
    self.selectedLaw = law
    
    if !(law is DownloadedLaw) && law.downloadOption != .forceDownload {
      if let nextDownloadedLaw = self.getLawFromRealmDatabase(lawSource: law) {
        self.selectedLaw = nextDownloadedLaw
      }
    }
    
    self.delegate = delegate
    
    _ = updateStyleSheet()
    
    let center = NotificationCenter.default
    center.addObserver(self, selector: #selector(realmDidUpdate), name: LawNotification.realmDidUpdateFromExternNotification, object: nil)
  }
  
  func sectionHeaderTitle(forRow index: Int) -> NSAttributedString {
    if let anchor = source?[index].1.anchors.first {
      return NSAttributedString(string: ProcessSentenceXML.convertLinkToHumanReadable(anchor))
    }
    return NSAttributedString()
  }
  

  //MARK: -
  func getLawFromRealmDatabase(lawSource: LawSource) -> DownloadedLaw? {
    
    if lawSource.lawEdition.isEmpty == false {
      let predicate: NSPredicate = NSPredicate( format: "lawEdition == %@", lawSource.lawEdition)
      let obj = RealmManager.shared.allDownloadedLaws.filter(predicate).sorted(byKeyPath: "addedDate", ascending: true).first
      return obj
      
    }else {
      let predicate: NSPredicate = NSPredicate( format: "lawNum == %@", lawSource.lawNum)
      let candidates = RealmManager.shared.allDownloadedLaws.filter(predicate).sorted(byKeyPath: "addedDate", ascending: false)
      
      if lawSource.filepath.isEmpty == false {
        
        var pathComs = lawSource.filepath.components(separatedBy: "/").filter { $0.isEmpty == false }
        
        while pathComs.count > 0 {
          let lastComp = pathComs.last
          for law in candidates {
            if lastComp == law.uuid { return law }
          }
          pathComs = Array(pathComs.dropLast())
        }
      }
      return candidates.first
    }
  }
 
  func highlightSerialIdsInSelectedRange() -> [String] {
    var allSerialIds: [String] = []
    
    getSelectedRangeForEachCell { kajō, range, actualRange, isLast, attributedString in
      allSerialIds += attributedString.highlightSerialIdsInRange(range)
    }
    return allSerialIds
  }

  func anchorString(at row: Int) -> String {
    let anchor = anchors[row]
    
    let humanReadable = ProcessSentenceXML.convertLinkToHumanReadable(anchor)
    return "\(anchor) [\(humanReadable)]"
  }

  func tocString(at row: Int) -> String {
    return source?.tocs[row].title ?? ""
  }
  
  func husoku() -> String {
    guard let xml = selectedLaw?.xml else { return "Could not read" }
    let doc = try? XML(xml: xml, encoding: .utf8)
    
    let fusokus = doc!.xpath("//SupplProvision")
    
    var string = ""
    
    for fusoku in fusokus {
      string += fusoku.text ?? ""
      string += "\n"
    }
    
    return string
  }
  
  func xmlSource() -> String {
    guard let xml = selectedLaw?.xml else { return "Could not read" }
    let doc = try? XML(xml: xml, encoding: .utf8)
  
    return doc?.toXML ?? ""
  }
  
  func linkUrl(_ linkString: String) -> URL? {
    guard let _ = selectedLaw?.lawNum else { return nil }
    guard linkString.range(of: LawXMLTypes.headerLinkPrefix) != nil else { return nil }
    
    guard let encodedName = selectedLaw?.lawNum.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) else { return nil }
 
    guard let trimmedAnchor = self.trimmedAnchor(linkString) else {
      return nil
    }
    
    guard let encodedAnchor = trimmedAnchor.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) else { return nil }
    
    let urlString = "jpaccyber-u1701015052CUProjectelaws://lawNum\(encodedName)/\(encodedAnchor)"

    return URL(string: urlString)
  }

  func hanreiUrl(_ linkString: String) -> URL? {
    let comps = linkString.components(separatedBy: "/")
    let body = comps.filter {
      (LawXMLTypes.internalLinkPrefix.hasPrefix($0) == false && LawXMLTypes.fetchedLinkPrefix.hasPrefix($0) == false ) && $0.isEmpty == false && $0.hasPrefix("#") == false
    }
    
    var humanReadable = ""
    for obj in body {
      let str = ProcessSentenceXML.humanReadableAnchor(obj)
      humanReadable += str
    }
    
    guard let lawTitle = selectedLaw?.lawTitle else { return nil }
    
    var queryString1 = ""
    var queryString2 = ""
    
    queryString1 = (lawTitle + humanReadable)
    queryString1 = queryString1.replacingOccurrences(of: "第", with: "")
    queryString2 = queryString1.replacingOccurrences(of: "民事訴訟法", with: "民訴法")
    queryString2 = queryString2.replacingOccurrences(of: "刑事訴訟法", with: "刑訴法")
    queryString2 = queryString2.replacingOccurrences(of: "日本国憲法", with: "憲法")
    queryString2 = queryString2.replacingOccurrences(of: "行政事件訴訟法", with: "行訴法")
    
    if queryString2 == queryString1 { queryString2 = (lawTitle + humanReadable) }
    
    queryString1 = queryString1.addingPercentEncoding(  withAllowedCharacters: CharacterSet.urlQueryAllowed)!
    queryString2 = queryString2.addingPercentEncoding(  withAllowedCharacters: CharacterSet.urlQueryAllowed)!
    
    let courtURLString: String = "http://www.courts.go.jp/app/hanrei_jp/list1?action_search=%E6%A4%9C%E7%B4%A2&filter%5BcourtName%5D=&filter%5BcourtType%5D=&filter%5BbranchName%5D=&filter%5BjikenGengo%5D=&filter%5BjikenYear%5D=&filter%5BjikenCode%5D=&filter%5BjikenNumber%5D=&filter%5BjudgeGengoFrom%5D=&filter%5BjudgeYearFrom%5D=&filter%5BjudgeMonthFrom%5D=&filter%5BjudgeDayFrom%5D=&filter%5BjudgeGengoTo%5D=&filter%5BjudgeYearTo%5D=&filter%5BjudgeMonthTo%5D=&filter%5BjudgeDayTo%5D=&filter%5Btext1%5D=" + queryString1 + "&filter%5Btext2%5D=" + queryString2 + "&filter%5Btext3%5D=&filter%5Btext4%5D=&filter%5Btext5%5D=&filter%5Btext6%5D=&filter%5Btext7%5D=&filter%5Btext8%5D=&filter%5Btext9%5D="
    
    let url = URL(string: courtURLString)
    return url
  }
  
  func hanrei2Url(_ linkString: String) -> URL? {
    let comps = linkString.components(separatedBy: "/")
    
    let body = comps.filter {
      (LawXMLTypes.internalLinkPrefix.hasPrefix($0) == false && LawXMLTypes.fetchedLinkPrefix.hasPrefix($0) == false ) && $0.isEmpty == false && $0.hasPrefix("#") == false
    }
    
    var humanReadable = ""
    for obj in body {
      let str = ProcessSentenceXML.humanReadableAnchor(obj)
      humanReadable += str
    }
    
    guard let lawTitle = selectedLaw?.lawTitle else { return nil }

    var queryString1 = ""
    var queryString2 = ""
    
    queryString1 = (lawTitle + humanReadable)
    queryString1 = queryString1.replacingOccurrences(of: "第", with: "")
    queryString1 = queryString1.replacingOccurrences(of: "日本国憲法", with: "憲法")
    
    if queryString2 == queryString1 { queryString2 = (lawTitle + humanReadable) }
    
    queryString1 = queryString1.addingPercentEncoding(  withAllowedCharacters: CharacterSet.urlQueryAllowed)!
    queryString2 = queryString2.addingPercentEncoding(  withAllowedCharacters: CharacterSet.urlQueryAllowed)!
    
    let courtURLString: String = "https://thoz.org/hanrei/s/?q=" + queryString1
    
    let url = URL(string: courtURLString)
    return url
  }
  
  func shouldDisplayPDF(_ snippet: String) -> String! {
    
    //"<table><tr><td class=\" bottom_0 top_0 left_0 right_0\"><div class=\"FigStruct\"><img src=\"__BASE_LOCATION__./pict/S25F30901000017-002.pdf\"></div></td></tr>\n</table>"

    // src as S25F30901000017-002.pdf

    guard let range = snippet.range(of: "(?<=/).[^/]*\\.pdf", options: .regularExpression, range: nil, locale: nil) else { return nil }
    
    return String(snippet[range])
  }
  
  func tableHTML(with snippet: String) -> String {
    
    var snippet = snippet
    // __BASE_LOCATION__ replace snippet
    var url = FileManager.default.appSupportUrl
    guard let name = (selectedLaw as? DownloadedLaw)?.localImageDataFilename else { return "" }
    url.appendPathComponent(name)
    snippet = snippet.replacingOccurrences(of: "__BASE_LOCATION__", with: name + "/")
    let templateName = "TableHTML"
    let templateUrl = Bundle.main.url(forResource: templateName, withExtension: "html")!
    var template = try! String(contentsOf: templateUrl, encoding: .utf8)
    template = template.replacingOccurrences(of: "_TABLE_STRUCT_", with: snippet)
    
    if self.isVertical {
      template = template.replacingOccurrences(of: "horizontal-tb", with: "vertical-rl")
      template = template.replacingOccurrences(of: "  <body>", with: "  <body onLoad=\"scroll(10000,0);\">")

    }
    
    return template
  }
  
  func figureUrl(with src: String) -> URL? {
    var url = FileManager.default.appSupportUrl
    url.appendPathComponent((selectedLaw as! DownloadedLaw).localImageDataFilename)
    var attachmentUrl = url.appendingPathComponent(src)
    
    // inside
    if FileManager.default.fileExists(atPath: attachmentUrl.path) { return attachmentUrl }
    
    // Look for pict folder
    let filesEnumerator = FileManager.default.enumerator(at: url, includingPropertiesForKeys: nil, options: .skipsHiddenFiles, errorHandler: nil)
    
    while let fileURL = filesEnumerator?.nextObject() as? URL {
      
      if fileURL.lastPathComponent == "pict" {
        attachmentUrl = fileURL.appendingPathComponent((src as NSString).lastPathComponent)
        break
      }
    }
    
    if FileManager.default.fileExists(atPath: attachmentUrl.path) { return attachmentUrl }
    
    return nil
  }
  
  var totalStorageSize: Int64? {
    guard let selectedLaw = selectedLaw as? DownloadedLaw else { return nil }
    let docDir = FileManager.default.appSupportUrl
    let figFolderUrl = docDir.appendingPathComponent(selectedLaw.localImageDataFilename)
    let indexUrl = docDir.appendingPathComponent(selectedLaw.localFilename)
    let name = selectedLaw.localFilename as NSString
    let realmUrl = docDir.appendingPathComponent("\(name.deletingPathExtension) \(ArticleDatabaseVersion).realm")

    let realmSize = FileManager.default.fileSize(at: realmUrl)
    let indexSize = FileManager.default.fileSize(at: indexUrl)
    let figFolderSize = FileManager.default.folderSize(at: figFolderUrl)

    return realmSize + indexSize + figFolderSize
  }
  
  //MARK:- PROCESSING LINK STRINGS

  func trimmedAnchor(_ linkString: String) -> String? {
    let comps = linkString.components(separatedBy: "/")
    guard comps.count > 2 else { return nil }
    
    var body = comps.filter {
      (LawXMLTypes.internalLinkPrefix.hasPrefix($0) == false && LawXMLTypes.fetchedLinkPrefix.hasPrefix($0) == false && LawXMLTypes.headerLinkPrefix.hasPrefix($0) == false ) && $0.isEmpty == false && $0.hasPrefix("#") == false
    }
    
    // 条49/号1
    if linkString.range(of: "号") != nil &&  linkString.range(of: "項") == nil {
      let insertString = "項1"
      for n in 0 ..< body.count {
        let obj = body[n]
        if obj.hasPrefix("号") {
          body.insert(insertString, at: n)
          break
        }
      }
    }
    
    let trimmedAnchor = body.joined(separator: "/")
    return trimmedAnchor
  }
  
  func linkLocator(fromAttributedString: NSAttributedString, rangeToAdd: NSRange) -> (AnchorLocator?, LawXMLError?) {
    guard let lawNum = selectedLaw?.lawNum else { return (nil, .unknown) }
    guard let md5 = selectedLaw?.lawEdition else { return (nil, .unknown) }
    
    let (anchor, _) = fromAttributedString.getAnchor(at: rangeToAdd.location)
    let (endAnchor, _) = fromAttributedString.getAnchor(at: NSMaxRange(rangeToAdd) - 1)
    
    if anchor != endAnchor {
      return (nil, .selectionRangeTooLong)
    }
    
    if anchor?.range(of: "文") == nil {
      return (nil, .selectionRangeWrong)
    }
    
    let string = fromAttributedString.attributedSubstring(from: rangeToAdd).reverseKanjiConversion().string
    let occurrenceIndex = fromAttributedString.occurrenceIndex(of: string, at: rangeToAdd.location, in: anchor!)
    print(string)
    let fromLocator = AnchorLocator(law: lawNum, lawEdition: md5, anchor: anchor, string: string, occurrenceIndex: occurrenceIndex)
  
    
    return (fromLocator, nil)
  }
  
  //MARK:- ACTIONS

  
  func load(completionHandler: @escaping ((Error?)->Void)) {

    guard let selectedLaw = selectedLaw else {
      completionHandler(LawXMLError.unknown)
      return
    }
    
    if let selectedLaw = selectedLaw as? DownloadedLaw, selectedLaw.isInvalidated == true {
      completionHandler(LawXMLError.unknown)
      return
    }
    
    // REPORT
//    if UserDefaults.standard.bool(forKey: "EnergySavingMode") == false {
//
//      Answers.logContentView(withName: "View Law",
//                             contentType: selectedLaw.lawTitle,
//                             contentId: selectedLaw.lawEdition,
//                             customAttributes: [:])
//    }
//
    
    var threadSafeLaw = selectedLaw.threadSafeDescriptor

    if selectedLaw.downloadOption == .inMemory {
      // 2. Build Cache
      
      if ArticleCacheManager.shared.realmExists(for: selectedLaw) != nil {
        
        let fetchResults = TFIDFFetchResults(law: selectedLaw)
        self.source = fetchResults
        
        completionHandler(nil)
        return
      }
      
      
      var xml = selectedLaw.xml

      if selectedLaw is InMemoryLawSourceObject {
        xml = (selectedLaw as! InMemoryLawSourceObject).xml
      }else {
      }
      
      
      if xml == nil {
        completionHandler(LawXMLError.unknown)
        return
      }
      
      DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
        
        let processSentenceXML = ProcessSentenceXML()
        processSentenceXML.divide(xml: xml!, progress: self.progressHandler ?? {message in}) { (tocs, articles, error) in
          self.progressHandler?("Writing…")
          
          if error != nil {
            DispatchQueue.main.async {
              completionHandler(error)
            }
            
          }else {
            let error = ArticleCacheManager.shared.write(articles, tocs, for: threadSafeLaw, inCacheFolder: true)
            DispatchQueue.main.async {
              if error != nil {
                completionHandler(error)
                return
              }
              
              let fetchResults = TFIDFFetchResults(law: selectedLaw)
              self.source = fetchResults
              
              completionHandler(nil)
            }
          }
        }
      }
      return
    }
    
    
    if selectedLaw.downloadOption == .regular && !(selectedLaw is DownloadedLaw) {
      if let law = self.getLawFromRealmDatabase(lawSource: selectedLaw) {
        self.selectedLaw = law
      }
    }
    
    var url = FileManager.default.appSupportUrl
    url.appendPathComponent(selectedLaw.localFilename)

    // 1. Cache
    if selectedLaw.downloadOption != .forceDownload, ArticleCacheManager.shared.realmExists(for: selectedLaw) != nil, FileManager.default.fileExists(atPath: url.path) == true {
      
      DispatchQueue.main.async {
        let fetchResults = TFIDFFetchResults(law: selectedLaw)
        self.source = fetchResults

        completionHandler(nil)
      }
      
    }else if let xml = selectedLaw.xml, selectedLaw.downloadOption != .forceDownload, let downloadedLaw = self.selectedLaw as? DownloadedLaw, downloadedLaw.isInvalidated == false {
      // 2. Build Cache
      DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
        
        let processSentenceXML = ProcessSentenceXML()
        processSentenceXML.divide(xml: xml, progress: self.progressHandler ?? {message in}) { (tocs, articles, error) in
          self.progressHandler?("Writing…")
          
          if error != nil {
            DispatchQueue.main.async {
              completionHandler(error)
            }
            
          }else {
            let error = ArticleCacheManager.shared.write(articles, tocs, for: threadSafeLaw)
            DispatchQueue.main.async {
              if error != nil {
                completionHandler(error)
                return
              }
              let fetchResults = TFIDFFetchResults(law: downloadedLaw)
              self.source = fetchResults

              completionHandler(nil)
            }
          }
        }
      }
      
    }else { // 3. Download
      
      // FORCE DOWNLOAD
      
      // DOWNLOAD FILE
      Downloader.shared.downloadLawFromSomewhere(selectedLaw, progress: { [weak self] progressDouble in
        guard let strongSelf = self else { return }
        if progressDouble > 0 {
          let formatter = NumberFormatter()
          formatter.numberStyle = .percent
          formatter.maximumFractionDigits = 1
          
          if let string = formatter.string(for: progressDouble) {
            strongSelf.progressHandler?("Downloaded: \(string) ")
          }
        }else {
          let downloadedSize = ByteCountFormatter.string(fromByteCount: Int64(-progressDouble), countStyle: ByteCountFormatter.CountStyle.file)
          strongSelf.progressHandler?("Downloaded: \(downloadedSize) ")
        }
        
      }) { [weak self] (downloadedLaw, error) in
        guard let strongSelf = self else { return }

        if error != nil {
          completionHandler(error)
          return
        }
        
        if (downloadedLaw as? DownloadedLaw)?.isInvalidated == true {
          completionHandler(error)
          return
        }
        
        guard let downloadedLaw = downloadedLaw, downloadedLaw.xml != nil else {
          completionHandler(error)
          return
        }
        
        if !(strongSelf.selectedLaw is DownloadedLaw) {
          strongSelf.selectedLaw = strongSelf.getLawFromRealmDatabase(lawSource: downloadedLaw)
        }
        
        let lawEdition = downloadedLaw.lawEdition
        let lawNum = downloadedLaw.lawNum
        let lawTitle = downloadedLaw.lawTitle

        
        if strongSelf.selectedLaw == nil {
          strongSelf.selectedLaw = try! RealmManager.shared.add(lawNum: lawNum, lawEdition: lawEdition, lawTitle: lawTitle, under: "/")
        }else if (strongSelf.selectedLaw as? DownloadedLaw)?.lawEdition.isEmpty == true {
          _ = try! RealmManager.shared.update(law: strongSelf.selectedLaw as! DownloadedLaw, lawEdition: lawEdition, lawTitle: lawTitle, filename: downloadedLaw.filename)
        }
         
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
          
          let processSentenceXML = ProcessSentenceXML()
          processSentenceXML.divide(xml: downloadedLaw.xml!, progress: strongSelf.progressHandler ?? {message in}) { (tocs, articles, error) in
            
            if error != nil {
              DispatchQueue.main.async {
                completionHandler(error)
              }
              
            }else {
              
              threadSafeLaw.lawEdition = lawEdition
              _ = ArticleCacheManager.shared.write(articles, tocs, for: threadSafeLaw)
              DispatchQueue.main.async {
                if let law = strongSelf.selectedLaw as? DownloadedLaw {
                  let fetchResults = TFIDFFetchResults(law: law)
                  strongSelf.source = fetchResults

                }
                completionHandler(nil)
              }
            }
          }
        }
      }
    }
  }
  
  enum ForceDownloadOption {
    case replace, add, cancel
  }
  
  typealias CheckForUpdatesDecisionHandler = (_ type: ForceDownloadOption)->Void
  
  func forceDownload(checkForUpdatesHandler: @escaping (_ addedLaw: DownloadedLaw?, _ decisionHandler: @escaping CheckForUpdatesDecisionHandler)->Void, completionHandler: @escaping ((Error?)->Void)) {
    guard let selectedLaw = selectedLaw as? DownloadedLaw else {
      completionHandler(LawXMLError.unknown)
      return
    }
    
    // FORCE DOWNLOAD
    let currentLawEdition = selectedLaw.lawEdition
    
    let source = LawDescriptor(lawTitle: selectedLaw.lawTitle, lawNum: selectedLaw.lawNum, lawEdition: "", filename: selectedLaw.filename, filepath: "", downloadOption: .regular)

    // DOWNLOAD FILE
    Downloader.shared.downloadLawFromSomewhere(source, progress: { [weak self] progressDouble in
      guard let strongSelf = self else { return }
      if progressDouble > 0 {
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        if let string = formatter.string(for: progressDouble) {
          strongSelf.progressHandler?("Downloaded: \(string) ")
        }
      }else {
        let downloadedSize = ByteCountFormatter.string(fromByteCount: Int64(-progressDouble), countStyle: ByteCountFormatter.CountStyle.file)
        strongSelf.progressHandler?("Downloaded: \(downloadedSize) ")
      }
      
    }) { [weak self] (downloadedLaw, error) in
      guard let strongSelf = self else { return }

      if error != nil {
        completionHandler(error)
        return
      }
      
      if downloadedLaw == nil || downloadedLaw?.xml == nil {
        completionHandler(LawXMLError.downloadedLawIsEmpty)
        return
      }
      
      DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
        
        let processSentenceXML = ProcessSentenceXML()
        processSentenceXML.divide(xml: downloadedLaw!.xml!, progress: strongSelf.progressHandler ?? {message in}) { (tocs, articles, error) in
          
          guard error == nil else {
            DispatchQueue.main.async {
              completionHandler(error)
            }
           return
          }
          
          guard let downloadedLaw = downloadedLaw else {
            DispatchQueue.main.async {
              completionHandler(LawXMLError.unknown)
            }
            return
          }
                    
          DispatchQueue.main.async {
            print(currentLawEdition)
            print(downloadedLaw.lawEdition)
          }
          
          if currentLawEdition == downloadedLaw.lawEdition && downloadedLaw.filename.isEmpty == false {
            DispatchQueue.main.async {
              completionHandler(nil)
            }
          }else {
            let error = ArticleCacheManager.shared.write(articles, tocs, for: downloadedLaw, inCacheFolder: true)

            DispatchQueue.main.async {
              guard error == nil else {
                completionHandler(error)
                return
              }
              
              do {
                
                try _ = RealmManager.shared.update(law: selectedLaw, lawEdition: downloadedLaw.lawEdition, lawTitle: downloadedLaw.lawTitle, filename: downloadedLaw.filename)
                completionHandler(nil)
                
              }catch let error {
                completionHandler(error)
              }
              
            }
            
          }
        }
      }
      
    }
  }
  
  
  var overrideKanjiType: KanjiType? = nil

  func updateStyleSheet(_ bgColorScheme: BackgroundColorSchemeType? = nil) -> StyleSheet {
    source?.clearAttributedStringCache()
    
    let bgColorScheme: BackgroundColorSchemeType?  = BackgroundColorSchemeType.White

    let fontSize = StyleSheet.fontSize
    let styleSheet = StyleSheet()
    let verticalGlyphValue = self.isVertical ? 1 : 0
    let ud = UserDefaults.standard
    let obj = ud.object(forKey: "BackgroundColorScheme") as? String
    var backgroundColorScheme: BackgroundColorSchemeType
    if bgColorScheme == nil {
      backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
      styleSheet.backgroundColorScheme = backgroundColorScheme
    }
    else {
      styleSheet.backgroundColorScheme = bgColorScheme!
      backgroundColorScheme = bgColorScheme!
    }
    
    let lineSpacing = CGFloat( UserDefaults.standard.double(forKey: "LineSpacing") )
    let kern: CGFloat = CGFloat( UserDefaults.standard.double(forKey: "Kern") )
    
    let preambleParagraph = NSMutableParagraphStyle()
    preambleParagraph.firstLineHeadIndent = 15
    preambleParagraph.lineSpacing = lineSpacing
    preambleParagraph.paragraphSpacing = lineSpacing
    preambleParagraph.headIndent = 0
    styleSheet.preambleParagraph = preambleParagraph
    
    let primaryParagraphStyle = NSMutableParagraphStyle()
    primaryParagraphStyle.firstLineHeadIndent = 0
    primaryParagraphStyle.headIndent = 15
    primaryParagraphStyle.lineSpacing = lineSpacing
    primaryParagraphStyle.paragraphSpacing = lineSpacing
    primaryParagraphStyle.paragraphSpacingBefore = 0.0
    styleSheet.primaryParagraph = primaryParagraphStyle
    
    let secondaryParagraphStyle = NSMutableParagraphStyle()
    secondaryParagraphStyle.firstLineHeadIndent = 15
    secondaryParagraphStyle.headIndent = 30
    secondaryParagraphStyle.lineSpacing = lineSpacing
    secondaryParagraphStyle.paragraphSpacing = lineSpacing

    styleSheet.secondaryParagraph = secondaryParagraphStyle
    
    let tertiaryParagraph = NSMutableParagraphStyle()
    tertiaryParagraph.firstLineHeadIndent = 30
    tertiaryParagraph.headIndent = 45
    tertiaryParagraph.lineSpacing = lineSpacing
    tertiaryParagraph.paragraphSpacing = lineSpacing

    styleSheet.tertiaryParagraph = tertiaryParagraph
    
    let subitem2Paragraph = NSMutableParagraphStyle()
    subitem2Paragraph.firstLineHeadIndent = 45
    subitem2Paragraph.headIndent = 60
    subitem2Paragraph.lineSpacing = lineSpacing
    subitem2Paragraph.paragraphSpacing = lineSpacing

    styleSheet.subitem2Paragraph = subitem2Paragraph
    
    let subitem3Paragraph = NSMutableParagraphStyle()
    subitem3Paragraph.firstLineHeadIndent = 60
    subitem3Paragraph.headIndent = 75
    subitem3Paragraph.lineSpacing = lineSpacing
    subitem3Paragraph.paragraphSpacing = lineSpacing

    styleSheet.subitem3Paragraph = subitem3Paragraph
    
    let subitem4Paragraph = NSMutableParagraphStyle()
    subitem4Paragraph.firstLineHeadIndent = 70
    subitem4Paragraph.headIndent = 85
    subitem4Paragraph.lineSpacing = lineSpacing
    subitem4Paragraph.paragraphSpacing = lineSpacing

    styleSheet.subitem4Paragraph = subitem4Paragraph
    
    let subitem5Paragraph = NSMutableParagraphStyle()
    subitem5Paragraph.firstLineHeadIndent = 80
    subitem5Paragraph.headIndent = 95
    subitem5Paragraph.lineSpacing = lineSpacing
    subitem5Paragraph.paragraphSpacing = lineSpacing

    styleSheet.subitem5Paragraph = subitem5Paragraph
    
    let subitem6Paragraph = NSMutableParagraphStyle()
    subitem6Paragraph.firstLineHeadIndent = 90
    subitem6Paragraph.headIndent = 105
    subitem6Paragraph.lineSpacing = lineSpacing
    subitem6Paragraph.paragraphSpacing = lineSpacing

    styleSheet.subitem6Paragraph = subitem6Paragraph
    
    let subitem7Paragraph = NSMutableParagraphStyle()
    subitem7Paragraph.firstLineHeadIndent = 100
    subitem7Paragraph.headIndent = 115
    subitem7Paragraph.lineSpacing = lineSpacing
    subitem7Paragraph.paragraphSpacing = lineSpacing

    styleSheet.subitem7Paragraph = subitem7Paragraph
    
    
    let captionParagraphStyle = NSMutableParagraphStyle()
    captionParagraphStyle.firstLineHeadIndent = 0
    captionParagraphStyle.headIndent = 0
    captionParagraphStyle.paragraphSpacingBefore = 0
    captionParagraphStyle.lineSpacing = lineSpacing
    captionParagraphStyle.paragraphSpacing = lineSpacing

    styleSheet.captionParagraph = captionParagraphStyle
    
    let chapterTitleParagraph = NSMutableParagraphStyle()
    chapterTitleParagraph.firstLineHeadIndent = 0
    chapterTitleParagraph.headIndent = 0
    chapterTitleParagraph.paragraphSpacingBefore = fontSize * 1.2
    chapterTitleParagraph.paragraphSpacing = fontSize * 1.2

    styleSheet.chapterTitleParagraph = chapterTitleParagraph
    
    
    let lawTitleParagraph = NSMutableParagraphStyle()
    lawTitleParagraph.firstLineHeadIndent = 0
    lawTitleParagraph.headIndent = 0
    lawTitleParagraph.paragraphSpacingBefore = 0
    lawTitleParagraph.paragraphSpacing = fontSize * 1.2
    styleSheet.lawTitleParagraph = lawTitleParagraph
    
    let fontType = FontType(rawValue: UserDefaults.standard.integer(forKey: "FontName")) ?? .mincho
    let font: CTFont = fontType.ctFont(with: fontSize)
    styleSheet.body = [.foregroundColor: backgroundColorScheme.textColor, .font: font, .kern: kern, .verticalGlyphForm: verticalGlyphValue]

    let blockquoteParagraphStyle = NSMutableParagraphStyle()
    blockquoteParagraphStyle.firstLineHeadIndent = 0
    blockquoteParagraphStyle.headIndent = 0
    blockquoteParagraphStyle.lineSpacing = lineSpacing
    blockquoteParagraphStyle.paragraphSpacing = lineSpacing
    blockquoteParagraphStyle.paragraphSpacingBefore = 0.0
    
    styleSheet.blockquote = [.foregroundColor: backgroundColorScheme.textColor, .font: font, .kern: kern, .paragraphStyle: blockquoteParagraphStyle, .verticalGlyphForm: verticalGlyphValue]
    
    styleSheet.title = [.foregroundColor: backgroundColorScheme.textColor,
                        .font: fontType.ctFontForTitle(with: fontSize),
                        .kern: kern, .verticalGlyphForm: verticalGlyphValue]
    
    styleSheet.caption = [.foregroundColor: backgroundColorScheme.textColor,
                          .font: fontType.ctFontForCaption(with: fontSize),
                          .verticalGlyphForm: verticalGlyphValue]
    
    styleSheet.lawTitle = [.foregroundColor: backgroundColorScheme.textColor,
                           .font: fontType.ctFontForLawTitle(with: fontSize),
                           .verticalGlyphForm: verticalGlyphValue]
    
    styleSheet.parenthesis = [.foregroundColor: backgroundColorScheme.subTextColor,
                              .font: fontType.ctFontForParenthesis(with: fontSize),
                              .verticalGlyphForm: verticalGlyphValue]
    
    styleSheet.parenthesisSecondary = [.foregroundColor: backgroundColorScheme.subSubTextColor,
                                       .font: fontType.ctFontForParenthesis(with: fontSize, level: 2),
                              .verticalGlyphForm: verticalGlyphValue]

    styleSheet.parenthesisTertiary = [.foregroundColor: backgroundColorScheme.subSubSubTextColor,
                              .font: fontType.ctFontForParenthesis(with: fontSize, level: 3),
                              .verticalGlyphForm: verticalGlyphValue]

    
    if let obj = UserDefaults.standard.object(forKey: "DimParenthesis") as? NSNumber, obj.boolValue == false {
      styleSheet.addParenthesisAttributes = false
    }else {
      styleSheet.addParenthesisAttributes = true
    }
    
    if let type = KanjiType(rawValue: ud.integer(forKey: "KanjiType")) {
      styleSheet.kanjiType = type
    }
    
    if overrideKanjiType != nil {
     styleSheet.kanjiType = overrideKanjiType!
    }
    
    self.styleSheet = styleSheet
    source?.styleSheet = styleSheet
    
    // UPDATE FILTER
    source?.updateFilter()
    return styleSheet
  }

  @objc func realmDidUpdate() {
    print("realmDidUpdate")
    delegate?.fetchingDidChange(fetching: isFetching, hasUpdate: true)
  }
  
  func goBack() -> Bool {
    guard let num = contentOffsetStack.last, num.isNull == false else { return false }
    
    contentOffsetStack.removeLast()
    contentOffsetForwardStack.append(contentOffset)
    print("setting content offset \(num)")
    self.contentOffset = num
    return true
  }
  
  func goFoward() -> String? {
    guard let num = contentOffsetForwardStack.last else { return nil }
    contentOffsetForwardStack.removeLast()
    contentOffsetStack.append(contentOffset)
    
    if num.indexPath == nil {
      return num.destinationLink
    }else {
      self.contentOffset = num
      return nil
    }
  }
  
  func addHistory(_ object: String? = nil) {
    
    if object != nil {
      let offset = Offset(indexPath: nil, localOffset: nil, destinationLink: object)
      contentOffsetForwardStack = [offset]
    }else {
      contentOffsetForwardStack.removeAll(keepingCapacity: false)
      if let last = contentOffsetStack.last, last == contentOffset {
        return
      }
      contentOffsetStack.append(contentOffset)
      print("addHistory \(contentOffsetStack)")

    }
  }
  
  func updateUnread() {
  }
  
  //MARK:- RESTORATION
  var restorationIdentifier: String?
  func restorationPlist() -> [String: Any] {
    guard let selectedLaw = selectedLaw else { return [:] }
    var plist: [String: Any] = [:]
    plist["lawNum"] = selectedLaw.lawNum
    plist["lawTitle"] = selectedLaw.lawTitle
    plist["lawEdition"] = selectedLaw.lawEdition
    plist["html"] = html

    plist.merge(contentOffset.plist()) { (current, new) in current }
    
    plist["restorationIdentifier"] = self.restorationIdentifier
    plist["colorScheme"] = styleSheet.backgroundColorScheme.rawValue
    
    var stack: [Offset] = []
    
    for n in 0..<contentOffsetStack.count {
      let offset = contentOffsetStack[n]
      if n > 0 && stack.last == offset {
       continue
      }
      
      if contentOffset == offset { continue }
      stack.append(offset)
    }
//    contentOffsetStack = stack
    
//    stack = []
//
//    for n in 0..<contentOffsetForwardStack.count {
//      let offset = contentOffsetForwardStack[n]
//      if n > 0 && stack.last == offset {
//        continue
//      }
//
//      if contentOffset == offset { continue }
//      stack.append(offset)
//    }
//    contentOffsetForwardStack = stack
    
    plist["contentOffsetStack"] = contentOffsetStack.map { $0.plist() }
    plist["contentOffsetForwardStack"] = contentOffsetForwardStack.map { $0.plist() }

    return plist
  }
  
  static func restoreViewModel(from plist: [String: Any]) -> DetailViewModel? {
    guard let lawNum = plist["lawNum"] as? String else { return nil }
    guard let lawTitle = plist["lawTitle"] as? String else { return nil }
    guard let restorationIdentifier = plist["restorationIdentifier"] as? String else { return nil }
    let lawEdition = plist["lawEdition"] as? String ?? ""
    let source = LawDescriptor(lawTitle:lawTitle, lawNum: lawNum, lawEdition: lawEdition, filename: "", filepath: "",  downloadOption: .regular)
    let viewModel = DetailViewModel(law:source)
    viewModel.restorationIdentifier = restorationIdentifier
    viewModel.hideTitleOnOpening = false
    
    let offset = Offset.fromPlist(plist)
    if offset.isNull == false {
      viewModel.startupContentOffset = offset
      if offset.indexPath?.row == 0 {
        viewModel.hideTitleOnOpening = true
      }
      
      viewModel.contentOffsetStack = ((plist["contentOffsetStack"] as? [[String: Any]]) ?? []).map { Offset.fromPlist($0) }
      viewModel.contentOffsetForwardStack = ((plist["contentOffsetForwardStack"] as? [[String: Any]]) ?? []).map { Offset.fromPlist($0) }
    }
    
    viewModel.html = plist["html"] as? String
    return viewModel
  }
  
  func getSelectedString() -> NSAttributedString {
    let fromAttributedString: NSMutableAttributedString = NSMutableAttributedString()
    getSelectedRangeForEachCell { kajō, range, actualRange, isLast, attributedString in
      
      if actualRange.length != 0 {
        fromAttributedString.append(attributedString.attributedSubstring(from: actualRange))
        if isLast == false {
          fromAttributedString.append(NSAttributedString(string: "\n"))
        }
      }
    }
    
    return fromAttributedString
  }
}

//MARK:- SELECTION

extension DetailViewModel {
  
  var selectedRange: SelectionRange {
    get {
      return selectedRange_
    }
    set {
      var didChange = false
      if let adjustedRange = delegate?.adjustedSelectionRangeFor(newValue, oldRange: selectedRange_) {
        if selectedRange_ != adjustedRange {
          didChange = true
          selectedRange_ = adjustedRange
        }
      }else {
        if selectedRange_ != newValue {
          didChange = true
          selectedRange_ = newValue
        }
      }
      
      if didChange {
        if stylusStatus.highlightByStylusDrawing {
          delegate?.selectionDidChange(asHighlight: true, attributes: stylusStatus.highlightByStylusAttributes)
          
        }else {
          delegate?.selectionDidChange(asHighlight: false, attributes: nil)
        }
      }
    }
  }
  
  func selectWordRange(_ wordRange: NSRange?, inRow row: Int) {
    guard let wordRange = wordRange else {
      selectedRange = SelectionRange()
      return
    }
    selectedRange = SelectionRange(start: SelectionRange.Location(index: row, characterLoc: wordRange.location),
                                   end: SelectionRange.Location(index: row, characterLoc: NSMaxRange(wordRange)))
  }
  
  func getSelectedRangeForEachCell(block: (Row, _ visibleRange: NSRange, _ actualRange: NSRange, _ isLastRow: Bool, _ attributedString: NSAttributedString) -> Void) {
    guard let source = source else { return }
    for n in selectedRange_.start.index ... selectedRange_.end.index {
      let (attributedString, row) = source[n]
      
      var range: NSRange!
      
      if selectedRange_.isWithinSingleIndex {
        range = NSMakeRange(selectedRange_.start.characterLoc, selectedRange_.end.characterLoc - selectedRange_.start.characterLoc)
        
      }else {
        
        if selectedRange_.start.index == n {
          range = NSMakeRange(selectedRange_.start.characterLoc, attributedString.length - selectedRange_.start.characterLoc)
          
        }else if selectedRange_.end.index == n {
          range = NSMakeRange(0, selectedRange_.end.characterLoc)
          
        }else {
          range = NSMakeRange(0, attributedString.length)
        }
      }
      let actualRange = attributedString.actualRangeWithoutControlCharacters(with: range)
      
      block(row, range, actualRange, selectedRange_.end.index == n, attributedString)
    }
  }
  
}

extension FileManager {
  
  func fileSize(at url: URL) -> Int64 {
    guard self.fileExists(atPath: url.path) else { return 0 }
    do {
      let fileAttributes = try attributesOfItem(atPath: url.path)
      let fileSizeNumber = fileAttributes[FileAttributeKey.size] as? NSNumber
      let fileSize = fileSizeNumber?.int64Value
      return fileSize!
    } catch {
      print("error reading filesize, NSFileManager extension fileSizeAtPath")
      return 0
    }
  }
  
  func folderSize(at url: URL) -> Int64 {
    guard self.fileExists(atPath: url.path) else { return 0 }

    var size : Int64 = 0
    do {
      let files = try subpathsOfDirectory(atPath: url.path)
      for i in 0 ..< files.count {
        size += fileSize(at: url.appendingPathComponent(files[i]))
      }
    } catch {
      print("error reading directory, NSFileManager extension folderSizeAtPath")
    }
    return size
  }
  
  func format(size: Int64) -> String {
    let folderSizeStr = ByteCountFormatter.string(fromByteCount: size, countStyle: ByteCountFormatter.CountStyle.file)
    return folderSizeStr
  }
}


