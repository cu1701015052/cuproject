//
//  ProcessSentenceXML.swift
//  eLaws
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//


//TODO:-段落のじさげ
import Foundation


extension String {
  
  var anchorPrefix: String {
    switch self {
    case "Sentence": return "文"
    case "Column": return "欄"
    case "Subitem1": return "イ"
    case "Subitem2": return "⑴"
    case "Subitem3": return "⒤"
    case "Subitem4": return "✢"
    case "Subitem5": return "✣"
    case "Subitem6": return "✤"
    case "Subitem7": return "✥"
    case "Item": return "号"
    case "Paragraph": return "項"
    case "Article": return "条"
    case "ArticleTitle", "ParagraphNum", "ItemTitle", "Subitem1Title", "Subitem2Title", "Subitem3Title", "Subitem4Title", "Subitem5Title", "Subitem6Title", "Subitem7Title": return "頭"
      
    case "Division", "TOCDivision": return "目"
    case "Subsection", "TOCSubsection": return "款"
    case "Section", "TOCSection": return "節"
    case "Chapter", "TOCChapter": return "章"
    case "Part", "TOCPart": return "編"
    case "SupplProvision": return "附"
    case "MainProvision": return "本"
    case "AppdxFig": return "図"
    case "AppdxTable": return "表"
    case "AppdxStyle": return "様" // 様式
    case "SupplProvisionAppdxTable": return "図"
    case "Preamble": return "前"
    case "EnactStatement": return "宣"
    case "LawTitle": return "題"
    default: return ""
    }
  }
}

class ProcessSentenceXML: NSObject, XMLParserDelegate {
  
  struct Regex {
    static let regex = "第[一二三四五六七八九〇十百千]+(条|條|規則)([のノ一二三四五六七八九〇十百第項号]*[一二三四五六七八九〇十百第項号])*"
    
    static let article = "第[一二三四五六七八九〇十百千0-9０-９]+(条|條|規則)([のノ]+[一二三四五六七八九〇十百0-9０-９]+)*"
    static let paragraph = "第*[一二三四五六七八九〇十百0-9０-９]+項([のノ]+[一二三四五六七八九〇十百0-9０-９]+)*"
    static let item = "第*[一二三四五六七八九〇十百0-9０-９]+号([のノ]+[一二三四五六七八九〇十百0-9０-９]+)*"
  }
  func encodedStringToAnchor(_ string: String) -> String {
    var string = string
    string = string.replacingOccurrences(of: "_", with: "//号")
    string = string.replacingOccurrences(of: ".", with: "//項")
    string = string.replacingOccurrences(of: "-", with: "_")
    string = string.replacingOccurrences(of: "(", with: "条")
    string = string.replacingOccurrences(of: ")", with: "")
    return string
  }
  
  
  func encodeForTOC2(_ string: NSString) -> String {
    
    // (3)-2.5_2
    //５条２号 -> (5)_6
    
    let mstrg = NSMutableString(string: string)
    mstrg.replaceOccurrences(of: "の", with: "-", options: .literal, range: NSMakeRange(0, mstrg.length))
    
    //条+num -> "."
    //項 -> "_"
    //号 -> ""
    
    // 自作の場合は、第4条　優先権第D.項第 (1)号 🔱1@8@4
    
    mstrg.replaceOccurrences(of: "[0-9]+(?=号)", with: "_$0", options: .regularExpression, range: NSMakeRange(0, mstrg.length))
    mstrg.replaceOccurrences(of: "[条第]+(?=[0-9])", with: ".", options: .regularExpression, range: NSMakeRange(0, mstrg.length))
    mstrg.replaceOccurrences(of: "項(?=[0-9])", with: "_", options: .regularExpression, range: NSMakeRange(0, mstrg.length))
    mstrg.replaceOccurrences(of: "[条項号]", with: "", options: .regularExpression, range: NSMakeRange(0, mstrg.length))
    mstrg.replaceOccurrences(of: "^[0-9]*", with: "($0)", options: .regularExpression, range: NSMakeRange(0, mstrg.length))
    
    return mstrg as String
  }
  
  
  func convertNaturalStringToAnchor(_ string: String) -> String? {
    
    let article = Regex.article
    let paragraph = Regex.paragraph
    let item = Regex.item
    
    let findRange = NSMakeRange(0, (string as NSString).length)
    
    //5条の2第5項2号 "5条6項8号"
    let range = findRange//: NSRange = (string as NSString).range(of: regex, options: [.regularExpression, .caseInsensitive],  range: findRange)
    if range.length == 0 { return nil }
    
    let articleRange = (string as NSString).range(of: article, options: [.regularExpression, .caseInsensitive],  range: range)
    let paragraphRange = (string as NSString).range(of: paragraph, options: [.regularExpression, .caseInsensitive],  range: range)
    let itemRange = (string as NSString).range(of: item, options: [.regularExpression, .caseInsensitive],  range: range)
    
    let articleAnchor = string.articleAnchor(suffix: "条", in: articleRange)
    let paragraphAnchor = string.articleAnchor(suffix: "項", in: paragraphRange)
    let itemAnchor = string.articleAnchor(suffix: "号", in: itemRange)
    
    var anchor: String = articleAnchor
    
    if paragraphAnchor.isEmpty == false {
      anchor += "/" + paragraphAnchor
    }
    
    if itemAnchor.isEmpty == false {
      anchor += "/" + itemAnchor
    }
    
    
    return anchor
  }
  
  //MARK:-
  
  //PARSE TIME COUNTER
  var appdxTableNumber: Int = 0
  var appdxFigNumber: Int = 0
  var appdxStyleNumber: Int = 0
  var supplProvisionNumber: Int = 0
  var enactStatementNumber: Int = 0

  var elementNumber: Int = 0

  class ElementDescriptor {
    static let segmentElements = ["Law", "LawBody", "TOC", "TOCSection", "TOCArticle", "TOCSubsection", "TOCDivision", "TOCPart", "MainProvision", "TOCChapter", "TOCSupplProvision", "Chapter", "Section", "Subsection", "Part", "Division", "SupplProvision", "AppdxTable", "ItemSentence", "Subitem1Sentence", "Subitem2Sentence","Subitem3Sentence","Subitem4Sentence","Subitem5Sentence","Subitem6Sentence", "Subitem7Sentence", "ParagraphSentence", "List", "ListSentence","Sublist1Sentence", "Sublist2Sentence", "Sublist1", "Sublist2", "Item", "Subitem1", "Subitem2", "Subitem3", "Subitem4", "Subitem5", "Subitem6", "Subitem7", "Paragraph", "Article", "Column", "TableStruct", "Remarks", "AppdxFormat", "FormatStruct", "Format", "AppdxStyle", "SupplProvisionAppdxStyle", "StyleStruct", "Style", "FigStruct", "Appdx", "ArithFormula", "AppdxNote", "NoteStruct", "AmendProvisionSentence", "AmendProvision", "NewProvision", "QuoteStruct", "Preamble", "AppdxFig", "SupplProvisionAppdx", "SupplProvisionAppdxTable" ]

    
    var element: String {
      didSet {
        self.anchor = getAnchor()
        isSegmentElement = ElementDescriptor.segmentElements.contains(element)
      }
    }
    var attributeDict: [String: String] {
      didSet {
        self.anchor = getAnchor()
      }
    }
    var isSegmentElement: Bool = false
    var renderedAttributedString: NSMutableAttributedString
    var anchorPath: String? = nil
    var anchor: String?
    
    init(elementName: String, attributeDict: [String : String]) {
      renderedAttributedString = NSMutableAttributedString()
      self.element = elementName
      self.attributeDict = attributeDict
      self.anchor = getAnchor()
      isSegmentElement = ElementDescriptor.segmentElements.contains(element)

    }
    
    
    func getAnchor() -> String? {
      
      switch element {
      case "Sentence":
        if attributeDict["Function"] == "main" {
          return element.anchorPrefix + "main"
          
        } else if attributeDict["Function"] == "proviso" {
          return element.anchorPrefix + "proviso"
          
        } else {
          if let num = attributeDict["Num"] {
            return element.anchorPrefix + String(num)
            
          }else {
            return element.anchorPrefix
            
          }
        }
      case "Item", "Paragraph", "Article", "Subitem1", "Subitem2", "Subitem3", "Subitem4", "Subitem5", "Subitem6", "Subitem7",
           "Division", "TOCDivision", "Subsection", "TOCSubsection", "Section", "Column",
           "TOCSection",  "Chapter", "TOCChapter", "Part", "TOCPart", "SupplProvision", "AppdxFig", "AppdxTable", "AppdxStyle", "EnactStatement", "Preamble" :
        if let num = attributeDict["Num"] {
          return element.anchorPrefix + String(num)
        }
        
      default:
        let prefix = element.anchorPrefix
        if prefix.isEmpty == false { return prefix }
      }
      return nil
    }
    
  }
  
  //MARK: -

//  let segmentElements = ["Law", "LawBody", "TOC", "TOCSection", "TOCArticle", "TOCSubsection", "TOCDivision", "TOCPart", "MainProvision", "TOCChapter", "TOCSupplProvision", "Chapter", "Section", "Subsection", "Part", "Division", "SupplProvision", "AppdxTable", "ItemSentence", "Subitem1Sentence", "Subitem2Sentence","Subitem3Sentence","Subitem4Sentence","Subitem5Sentence","Subitem6Sentence", "Subitem7Sentence", "ParagraphSentence", "List", "ListSentence","Sublist1Sentence", "Sublist2Sentence", "Sublist1", "Sublist2", "Item", "Subitem1", "Subitem2", "Subitem3", "Subitem4", "Subitem5", "Subitem6", "Subitem7", "Paragraph", "Article", "Column", "TableStruct", "Remarks", "AppdxFormat", "FormatStruct", "Format", "AppdxStyle", "SupplProvisionAppdxStyle", "StyleStruct", "Style", "FigStruct", "Appdx", "ArithFormula", "AppdxNote", "NoteStruct", "AmendProvisionSentence", "AmendProvision", "NewProvision", "QuoteStruct", "Preamble", "AppdxFig", "SupplProvisionAppdx", "SupplProvisionAppdxTable" ]
  
  
  var anchorPath: String {
    var path = textStack.last?.anchorPath
    if path != nil { return path! }
    
    path = ""
    
    for obj in textStack {
      if let anchor = obj.anchor {
        if path!.isEmpty == false {
          path = path! + "/"
          
        }
        path = path! + anchor
      }
    }
    //MainProvisionは条まで省略
    let originalPath = path!
    #if DOWNLOADER_BUILD
    if path!.hasPrefix("本") && ( path!.range(of: "条") != nil || path!.range(of: "項") != nil ) {
      var comps = path!.components(separatedBy: "/")
      comps = comps.filter {
        
        return $0.hasPrefix("本") == false && $0.hasPrefix("編") == false && $0.hasPrefix("章") == false && $0.hasPrefix("節") == false && $0.hasPrefix("款") == false && $0.hasPrefix("目") == false
      }
      path = comps.joined(separator: "/")
    }
    #endif
    let converted = trimAnchorPath(originalPath)
    
    #if DOWNLOADER_BUILD
    assert( converted == path!, "\(converted) - \(path!)")
    #endif

    let obj = textStack.last
    obj?.anchorPath = converted
    
    return converted
  }
  
  func trimAnchorPath(_ string: String) -> String {
    // この関数の中で、ユニコード１６は、それぞれ "本編章節款目条項/" を示している
    
    if string.hasPrefix("本") == false { return string }

    var trimmed: String = ""
    var has条or項 = false
    var isIgnoring = false
    var nextToSlash = true

    for character in string.utf16 {
      if nextToSlash {
        if character == 26412 /*"本"*/ || character == 32232 || character == 31456 || character == 31680 || character == 27454 || character == 30446  {
          isIgnoring = true
        }
        
        nextToSlash = false
      }
     
      if character == 26465 || character == 38917  {
        has条or項 = true
      }
      
      if false == isIgnoring {
        trimmed += String(utf16CodeUnits: [character], count: 1)
      }
      
      if character == 47 {
        nextToSlash = true
        isIgnoring = false
      } else {
        nextToSlash = false
      }
      
    }
    if has条or項 == true {
      
      if trimmed.hasSuffix("/") {
        trimmed = String(trimmed.dropLast())
      }
      
      
      return trimmed
    }
    return string
  }
  
  //MARK:- DIVIDE
  
  var progressMessage: ((_ progressMessage: String)->Void)?
  let processQueue_: DispatchQueue = DispatchQueue(label: "jp.ac.cyber-u.1701015052.CUProject.ProcessSentenceQueue" )

  func divide(xml: Data, reconstruct: Bool = false, addLinks: Bool = true,  progress: @escaping (_ progressMessage: String)->Void, completion: @escaping (([Toc], [Row], Error?) -> Void) ) {
    progress("Processing…")

    self.progressMessage = progress
    self.parse(data: xml, reconstruct: reconstruct) { (tocs, mattr, error) in
      guard error == nil else {
        completion( [], [], error )
        return
      }
      progress("Processing…")
      let linker = LinkMaker(prefix: LawXMLTypes.internalLinkPrefix, styleSheet: StyleSheet())

      var artciles: [Row] = []
      var idx = 0
      let sliced = mattr.sliced()
      let count = sliced.count
      var compressedData: Array<Data> = Array<Data>(repeating: Data(), count: count)

      sliced.forEach { attr in
        //
        let article = Row()
        
        autoreleasepool {
          let mutableAttributedString = NSMutableAttributedString(attributedString: attr)
          
          // GET ANCHOR
          var charIndex = 0
          var range = NSMakeRange(0,0)
          var anchors: [String] = []
          var searchRange = NSMakeRange(0,mutableAttributedString.length)
          while charIndex < mutableAttributedString.length {
            if  let anchor = mutableAttributedString.attribute(.anchor, at: charIndex, longestEffectiveRange: &range, in: searchRange) as? String {
              if anchor.isEmpty == false && anchors.contains(anchor) == false {
                anchors.append(anchor)
              }
            }
            charIndex = NSMaxRange(range)
            searchRange = NSMakeRange(charIndex,mutableAttributedString.length-charIndex)
          }
          
          article.rowNumber = idx
          article.anchors = anchors
          article.estimatedCharacterCount = mutableAttributedString.length
          
          // ADD LINKS
          if reconstruct == false && addLinks == true {
            linker.addLinks(to: mutableAttributedString)
          }
          
          let i = idx
          self.processQueue_.async {
            autoreleasepool {
              if let data = (mutableAttributedString.serializedData() as NSData?)?.deflate() {
                objc_sync_enter(self)
                compressedData[i] = data
                objc_sync_exit(self)
              }
            }
          }
          artciles.append(article)
          progress("Processing… (\(idx+1)/\(count))")

          idx += 1
        }
      }
      self.processQueue_.sync {  }
      for idx in 0 ..< count {
        artciles[idx].serializedAttributedStringCompressed = compressedData[idx]
        compressedData[idx] = Data()
      }
      completion( tocs, artciles, nil )
    }
  }

  
  //MARK:- PARSE
  var parser: XMLParser!
  var textStack: [ElementDescriptor] = []
  var parseCompletionHandler: (([Toc], NSMutableAttributedString, Error?)->Void)?
  var lastElementName: String = ""
  var tableXML: [String] = []
  var tocs: [Toc] = []
  var lastAppdxTableTitle: String? = nil
  var lastAppdxFigTitle: String? = nil
  var suppleProvisionDictionary: [String: String]? = nil
  var AppdxFigTitle: String? = nil
  var reconstrutedAttributedString: NSMutableAttributedString? = nil
  var reconstrutedAttributedStringIndex: Int = 0
  
  func parse(data: Data, reconstruct: Bool = false, completion: @escaping (([Toc], NSMutableAttributedString, Error?)->Void)) {
    
    if reconstruct {
      reconstrutedAttributedString = NSMutableAttributedString()
      reconstrutedAttributedStringIndex = 0
    }else {
      reconstrutedAttributedString = nil
    }
    
    // ESCAPE TABLE XML
    let (string, tableXMLs) = escapeTables(data)
    tableXML = tableXMLs
    let searchRange = string.startIndex..<string.endIndex
    
    // BUILD TOC
    if let rangeOpen = string.range(of: "<TOC(>| [^<>/]*>)", options: .regularExpression, range: searchRange),
      let rangeClose = string.range(of: "</TOC>", range: rangeOpen.upperBound..<string.endIndex) {
      
      let tocRange = rangeOpen.lowerBound..<rangeClose.upperBound
      let tocString = String(string[tocRange])
      if let node = try? XML(xml: tocString, encoding: .utf8).at_xpath("//TOC"), node != nil {
        tocs = extractTOC(node!)
      }
    }
    
    ///
    let parser = XMLParser(data: string.data(using: .utf8)!)
    self.parser = parser
    self.parseCompletionHandler = completion
    parser.delegate = self
    appdxTableNumber = 0
    appdxFigNumber = 0
    appdxStyleNumber = 0
    supplProvisionNumber = 0
    enactStatementNumber = 0
    elementNumber = 0
    textStack = []
    parser.parse()
  }
  
  func parserDidEndDocument(_ parser: XMLParser) {
    self.progressMessage = nil
  }
  
  func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
    var attributeDict = attributeDict
    
    if reconstrutedAttributedString != nil {
      var xmlSnippet = "<\(elementName)"
      attributeDict.forEach {
        xmlSnippet += " \($0.key)=\"\($0.value)\""
      }
      xmlSnippet += ">"
      
      reconstrutedAttributedString?.append(NSAttributedString(string: xmlSnippet))
    }

    if "SupplProvision" == elementName {
      appdxTableNumber = 0
      appdxFigNumber = 0
      appdxStyleNumber = 0
      attributeDict["Num"] = String(supplProvisionNumber)
    }
    
    if "AppdxTable" == elementName {
      attributeDict["Num"] = String(appdxTableNumber)
      appdxTableNumber += 1
    }

    if "AppdxFig" == elementName {
      attributeDict["Num"] = String(appdxFigNumber)
    }
    
    if "AppdxStyle" == elementName {
      attributeDict["Num"] = String(appdxStyleNumber)
      appdxStyleNumber += 1
    }
    
    if "SupplProvision" == elementName {
      suppleProvisionDictionary = attributeDict
    }
    
    if "EnactStatement" == elementName || "Preamble" == elementName {
      attributeDict["Num"] = String(enactStatementNumber)
      enactStatementNumber += 1
    }
    
    let element = ElementDescriptor(elementName: elementName, attributeDict: attributeDict)

    textStack.append(element)
    elementNumber += 1
  }
  
  func parser(_ parser: XMLParser, foundCharacters string: String) {
    
    let text = NSMutableAttributedString(string: string)

    if reconstrutedAttributedString != nil {
      let mattr = NSMutableAttributedString(string: string)
      
      for hoge in 0 ..< (string as NSString).length {
        mattr.addAttribute(.reconstructionIndex, value: reconstrutedAttributedStringIndex, range: NSMakeRange(hoge, 1))
        text.addAttribute(.reconstructionIndex, value: reconstrutedAttributedStringIndex, range: NSMakeRange(hoge, 1))
        reconstrutedAttributedStringIndex += 1
      }
      reconstrutedAttributedString?.append(mattr)
    }
    
    let currentItem = textStack.last!
    if currentItem.isSegmentElement { return } // SLOW

    textStack.last!.renderedAttributedString.append(text)
  }
  
  func parser(_: XMLParser, didEndElement: String, namespaceURI: String?, qualifiedName: String?) {
    autoreleasepool {
    reconstrutedAttributedString?.append(NSAttributedString(string: "</\(didEndElement)>"))
    
    let anchorPath = self.anchorPath
    let lastDictionary = textStack.removeLast()
    guard didEndElement == lastDictionary.element else {
      print("*** XML error")
      return
    }
  
    var unknownTag = true
    let attr = lastDictionary.renderedAttributedString
    addAttribute(attr, key: .elementNameAttributeName, value: didEndElement)
    
    if didEndElement == "ArticleTitle" {
      self.progressMessage?(attr.string)
    }
    
    switch didEndElement {
    case "Diff":
      

      if lastDictionary.attributeDict["operation"] == "DELETE" {
        attr.addAttribute(.diffOperation, value: 1, range: NSMakeRange(0, attr.length))
      }
      
      if lastDictionary.attributeDict["operation"] == "INSERT" {
        attr.addAttribute(.diffOperation, value: 2, range: NSMakeRange(0, attr.length))
      }
      attr.removeReturn()
      textStack.last?.renderedAttributedString.append(attr)
      return
    case "ParagraphNum":
      if attr.length != 0 {
        embedHeaderLink(attr)
        addAnchor(attr, anchorPath: anchorPath)

        attr.addSpace()
        textStack.last!.renderedAttributedString.append(attr)
      }
    case "Article":
      attr.addReturnBefore()
      textStack.last!.renderedAttributedString.append(attr)

    case "Paragraph":
      if textStack.last?.element == "Article" && lastDictionary.attributeDict["Num"] == "1" {
        attr.addSpaceBefore()
      }else {
        
        // 字下げ
        if attr.length > 0 && attr.attribute(.elementNameAttributeName, at: 0, effectiveRange: nil) as? String == "Sentence" {
          attr.addSpaceBefore()
        }
        
        attr.addReturnBefore()
      }
      textStack.last!.renderedAttributedString.append(attr)
    case "ParagraphCaption":
      attr.removeReturn()
      attr.addReturn()
      textStack.last!.renderedAttributedString.append(attr)

    case "ArticleCaption", "ArticleTitle", "LawTitle", "EnactStatement", "TOCLabel", "ChapterTitle", "SectionTitle","SubsectionTitle", "PartTitle", "DivisionTitle", "ArticleRange", "TableStructTitle", "SupplProvisionAppdxTableTitle", "AppdxTableTitle", "RemarksLabel", "AppdxFormatTitle", "AppdxStyleTitle", "SupplProvisionAppdxStyleTitle", "ArithFormulaNum", "AppdxNoteTitle", "TOCPreambleLabel", "FigStructTitle", "StyleStructTitle", "FormatStructTitle", "AppdxFigTitle", "NoteStructTitle" :
      
      attr.removeReturn()
      
      if didEndElement == "ArticleTitle" || didEndElement == "AppdxTableTitle" || didEndElement == "SupplProvisionAppdxTable" ||  didEndElement == "AppdxFigTitle" || didEndElement == "AppdxStyleTitle" || didEndElement == "ChapterTitle" || didEndElement == "SectionTitle" || didEndElement == "SubsectionTitle" || didEndElement == "PartTitle" || didEndElement == "DivisionTitle" || didEndElement == "SupplProvisionAppdxTableTitle" {
        embedHeaderLink(attr)
      }

      if didEndElement == "AppdxTableTitle" {
       lastAppdxTableTitle = attr.string
      }
      
      if didEndElement == "SupplProvisionAppdxTableTitle" {
        lastAppdxTableTitle = attr.string
      }
      
      if didEndElement == "AppdxFigTitle" {
        lastAppdxFigTitle = attr.string
      }
      
      addAnchor(attr, anchorPath: anchorPath)
      
      attr.addReturnBefore()
      if didEndElement == "LawTitle" {
        attr.addReturn()
      }
      if didEndElement == "EnactStatement" {
        attr.addSpaceBefore()
      }
      
      textStack.last!.renderedAttributedString.append(attr)

    case  "Item", "Subitem1", "Subitem2", "Subitem3", "Subitem4", "Subitem5", "Subitem6", "Subitem7",  "TableStruct", "ListSentence":
      addAnchor(attr, anchorPath: anchorPath)
      attr.addReturnBefore()
      textStack.last!.renderedAttributedString.append(attr)

    case "ItemTitle", "Subitem1Title", "Subitem2Title", "Subitem3Title", "Subitem4Title", "Subitem5Title", "Subitem6Title", "Subitem7Title":
      
      embedHeaderLink(attr)
      addAnchor(attr, anchorPath: anchorPath)

      if attr.length != 0 {
        attr.addSpace()
        textStack.last!.renderedAttributedString.append(attr)
      }

    case "Column":
      if lastDictionary.attributeDict["Num"] != "1" {
        attr.addSpaceBefore()
      }
      textStack.last!.renderedAttributedString.append(attr)
    case "Table":
    
      if let string = tableXML.first {
        
        let tableStruct = try? XML(xml: string, encoding: .utf8).at_xpath("//Table")!
        var attributes: [NSAttributedString.Key: String] = [:]
        autoreleasepool {
          let tableHTML = convertTableStruct(tableStruct!)
          attributes[.embeddedAttachement] = tableHTML
          attributes[.elementNameAttributeName] = "Table"
        }
        let jumpString = NSMutableAttributedString(string: " 【表（タップして表示）】 \n", attributes: attributes)
        
        textStack.last!.renderedAttributedString.append(jumpString)
        tableXML.removeFirst()
      }

    case "Rt":
      if textStack.last?.element == "Ruby", let mattr = textStack.last?.renderedAttributedString {
        mattr.removeReturn()
        mattr.addAttribute(.ruby, value: lastDictionary.renderedAttributedString.string, range: NSMakeRange(0,mattr.length))
        mattr.addAttribute(.rubyTextId, value: shortUUIDString(), range: NSMakeRange(0,mattr.length))
        
        return
      }
    case "Ruby":
      attr.removeReturn()
      textStack.last!.renderedAttributedString.append(attr)
      
    case "Fig":
      var attributes: [NSAttributedString.Key: String] = [:]
      let source = lastDictionary.attributeDict["src"]
      
      #if FULL_TEXT_SEARCH_BUILD
        let attr = NSAttributedString(string: "\n 【図（データなし）】 \n", attributes: [.anchor: ""])
        textStack.last!.renderedAttributedString.append(attr)

      #else
        if source?.isEmpty == false {
          attributes[.embeddedAttachement] = source
          attributes[.elementNameAttributeName] = "Fig"
          let attr = NSAttributedString(string: "\n 【図（タップして表示）】 \n", attributes: attributes)
          textStack.last!.renderedAttributedString.append(attr)
        }else {
          let attr = NSAttributedString(string: "\n 【図（データなし）】 \n", attributes: [.anchor: ""])
          textStack.last!.renderedAttributedString.append(attr)
        }
      #endif
      appdxFigNumber += 1
      
    case "AppdxTable" /*, "SupplProvisionAppdxTable"*/:
      attr.addReturnBefore()
      textStack.last!.renderedAttributedString.append(attr)
      let toc = Toc.fig()
      toc.anchor = anchorPath
      toc.title = lastAppdxTableTitle ?? "別 表"
      toc.num = String(appdxTableNumber)
      toc.index = digitString(tocs.count)
      tocs.append(toc)
      lastAppdxTableTitle = nil
    case "AmendProvision", "NewProvision":
      attr.addReturnBefore()
      textStack.last!.renderedAttributedString.append(attr)
//    case "SupplProvisionLabel":
//      if attr.string == "附　則" {
//        // DO NOTHING. LABEL WILL BE ADDED IN SupplProvision
//      }else {
//        addAnchor(attr, anchorPath: anchorPath)
//
//        if attr.length != 0 {
//          attr.addSpace()
//          textStack.last!.renderedAttributedString.append(attr)
//        }
//      }
      
    case "SupplProvisionLabel":
      guard suppleProvisionDictionary != nil else { break }
      supplProvisionNumber += 1

      let husokuTitle = suppleProvisionDictionary?["AmendLawNum"]
      let husokuExtract: Bool = (suppleProvisionDictionary?["Extract"] == "true")
      
      var title = ""
      if husokuTitle != nil {
        title = title + "　\(husokuTitle!)"
      }
      if husokuExtract == true {
        title = title + "　抄"
      }

      let obj = NSMutableAttributedString(string: title)
      addAnchor(attr, anchorPath: anchorPath)
      attr.append(obj)
    
      embedHeaderLink(attr)
      attr.addReturnBefore()
      attr.addReturnBefore()

      
      textStack.last!.renderedAttributedString.append(attr)

      let toc = Toc.husoku()
      toc.anchor = anchorPath
      toc.title = "附則 " + (husokuTitle ?? "")
      toc.num = String(supplProvisionNumber)
      toc.index = digitString(tocs.count)
      tocs.append(toc)
      suppleProvisionDictionary = nil
      
    case "Sentence":
      attr.removeReturn()
      addAnchor(attr, anchorPath: anchorPath)
      textStack.last!.renderedAttributedString.append(attr)
    case "Preamble":
      attr.addReturnBefore()
      textStack.last!.renderedAttributedString.append(attr)
    case "TOC":
      break
  
    case "LawContents":
      parseCompletionHandler?(tocs, attr, nil)
      return

    case "LawNum", "Note", "SupplNote",  "RelatedArticleNum":
      unknownTag = false
      fallthrough
      
    default:
      if ElementDescriptor.segmentElements.contains(didEndElement) { unknownTag = false }
      if unknownTag == true {
        print("***** 🐤 Unknown Tag \(didEndElement)")
      }
      if textStack.count == 0 {
        parseCompletionHandler?(tocs, attr, nil)
        return
      }
      addAnchor(attr, anchorPath: anchorPath)

      textStack.last!.renderedAttributedString.append(attr)
    }
    
    
    lastElementName = didEndElement
  }
  }
  
  func embedHeaderLink(_ attributedString: NSMutableAttributedString) {
    let value = anchorPath
    if value.range(of: "-1") != nil { return }
    if value.isEmpty == false {
      attributedString.addAttribute(.tempLink, value: LawXMLTypes.headerLinkPrefix + value, range: NSMakeRange(0, attributedString.length))
      // print(LawXMLTypes.headerLinkPrefix + value)
    }
  }
  
  func addAnchor(_ attributedString: NSMutableAttributedString, anchorPath: String) {
    if anchorPath.isEmpty == false {
      addAttribute(attributedString, key: .anchor, value: anchorPath)
    }
  }
  
  func addAttribute(_ attributedString: NSMutableAttributedString, key: NSAttributedString.Key, value: Any) {
    guard attributedString.length > 0 else { return }
    var idx = 0
    var range = NSMakeRange(0,0)
    while true {
      let v0 = attributedString.attribute(key, at: idx, longestEffectiveRange: &range, in: NSMakeRange(0,attributedString.length))
      
      if v0 == nil {
        attributedString.addAttribute(key, value: value, range: range)
      }
      
      idx = NSMaxRange(range)
      if idx >= attributedString.length { break }
    }
  }
  
  static func convertLinkToHumanReadable(_ linkString: String) -> String {
    let array = linkString.components(separatedBy: "/")
    var humanReadable = ""
    for obj in array {
      let str = ProcessSentenceXML.humanReadableAnchor(obj)
      humanReadable += str
    }
    humanReadable = humanReadable.replacingOccurrences(of: "条第", with: "条")
    humanReadable = humanReadable.replacingOccurrences(of: "項第", with: "項")
    
    return humanReadable
  }
  
  static func humanReadableAnchor(_ anchor: String) -> String {
    // 条1
    if anchor.utf16.count <= 1 { return "" }
    
    let prefix = anchor[..<anchor.index(anchor.startIndex, offsetBy: 1)]
    var body = String(anchor[anchor.index(anchor.startIndex, offsetBy: 1)... ])
    body = body.replacingOccurrences(of: "_", with: "の")
    
    switch prefix {
    case "編", "章", "節", "款", "目", "条", "項", "号", "欄" :
      // insert suffix before の
      if let range = body.range(of: "の") {
        body.insert(prefix.first!, at: range.lowerBound)
        return "第\(body)"
        
      }else {
        return "第\(body)\(prefix)"
      }
    case "文":
      if body == "main" { return "本文" }
      else if body == "proviso" { return "ただし書" }
      else { return "" }
    case "⑴": return "(\(body))"
    case "⒤":
      guard let number = Int(body) else { return "" }
      let base: NSString = "ⅰⅱⅲⅳⅴⅵⅶⅷⅸⅹⅺⅻ"
      if number > base.length || number < 1 { return "(\(number))" }
      return "(\(base.substring(with: NSMakeRange(number - 1, 1))))"
    case "イ":
      guard let number = Int(body) else { return "" }
      let base: NSString = "イロハヒホヘトチリヌルヲワカヨタレソツネナラムウヰノオクヤマケフコエテアサキユメミシヱヒモセスン"
      if number > base.length || number < 1 { return "" }
      return base.substring(with: NSMakeRange(number - 1, 1))
    case "✢", "✣", "✤":
      return body
    case "表":
      guard let number = Int(body) else { return "表" }
      return "表\(number + 1)"
    case "附":
      return "【附則】"
    case "図": return "図面"
    case "様": return "様式"

    default:
      return ""
      
    }
  }
  
  func convertTableStruct(_ node: XMLElement) -> String {
    //
      let childrenNodes = node.xpath("./*")
      var convertedArray: [String] = Array<String>(repeating: "", count: childrenNodes.count)
      DispatchQueue.concurrentPerform(iterations: childrenNodes.count)  { (i:size_t) in
        let child = childrenNodes[i]
        let convertedTable = convertTableStruct(child)
        convertedArray[i] = convertedTable
      }
      
      
      let children = convertedArray.joined(separator: "")
      guard let tag = node.tagName else { return children }
      if tag == "TableStruct" { return children }
      switch tag {
      case "Table":
        return "<table>" + children + "</table>"
        
      case "TableRow":
        var thisTag = "<tr"
        if  let colspan = node["colspan"] {
          thisTag += " colspan=\"\(colspan)\""
        }
        if let rowspan = node["rowspan"] {
          thisTag += " rowspan=\"\(rowspan)\""
        }
        return thisTag + ">" + children + "</tr>\n"
        
      case "TableColumn":
        var thisTag = "<td"
        var classes = " class=\""
        if  let colspan = node["colspan"] {
          thisTag += " colspan=\"\(colspan)\""
        }
        if let rowspan = node["rowspan"] {
          thisTag += " rowspan=\"\(rowspan)\""
        }
        
        if "solid" == node["BorderBottom"] {
          classes += " bottom_1"
        }
        
        if "solid" == node["BorderTop"] {
          classes += " top_1"
        }
        
        if "solid" == node["BorderLeft"] {
          classes += " left_1"
        }
        
        if "solid" == node["BorderRight"] {
          classes += " right_1"
        }
        
        if "none" == node["BorderBottom"] {
          classes += " bottom_0"
        }
        
        if "none" == node["BorderTop"] {
          classes += " top_0"
        }
        
        if "none" == node["BorderLeft"] {
          classes += " left_0"
        }
        
        if "none" == node["BorderRight"] {
          classes += " right_0"
        }
        
        if "left" == node["Align"] {
          classes += " text-align_left"
        }
        
        if "right" == node["Align"] {
          classes += " text-align_right"
        }
        
        
        if "top" == node["Valign"] {
          classes += " valign_top"
        }
        
        if "bottom" == node["Valign"] {
          classes += " valign_bottom"
        }
        
        classes += "\""
        return thisTag + "\(classes)>" + children + "</td>"
        
        
      case "Sentence":
        if let text = node.text {
          return text + "<br />"
        }
        
      case "ItemSentence":
        if let text = node.text {
          return text + "<br />"
        }
        
      case "Subitem1Sentence":
        if let text = node.text {
          return text + "<br />"
        }
        
      case "Subitem2Sentence":
        if let text = node.text {
          return text + "<br />"
        }
        
      case "Subitem3Sentence":
        if let text = node.text {
          return text + "<br />"
        }
      case "Subitem4Sentence":
        if let text = node.text {
          return text + "<br />"
        }
      case "Subitem5Sentence":
        if let text = node.text {
          return text + "<br />"
        }
      case "Subitem6Sentence":
        if let text = node.text {
          return text + "<br />"
        }
      case "Subitem7Sentence":
        if let text = node.text {
          return text + "<br />"
        }
      case "Fig":
        if let src = node["src"] {
          
          return "<img src=\"__BASE_LOCATION__\(src)\">"
        }else {
          return "[図（データなし）]"
        }
        
      case "ItemTitle":
        let text = node.text ?? ""
        return "<strong>" + text + "　</strong>"
        
      case "Subitem1Title":
        let text = node.text ?? ""
        return "<strong>　" + text + "　</strong>"
        
      case "Subitem2Title":
        let text = node.text ?? ""
        return "<strong>　　" + text + "　</strong>"
        
      case "Subitem3Title":
        let text = node.text ?? ""
        return "<strong>　　　" + text + "　</strong>"
        
      case "Subitem4Title":
        let text = node.text ?? ""
        return "<strong>　　　　" + text + "　</strong>"
        
      case "Subitem5Title":
        let text = node.text ?? ""
        return "<strong>　　　　　" + text + "　</strong>"
        
      case "Subitem6Title":
        let text = node.text ?? ""
        return "<strong>　　　　　　" + text + "　</strong>"
      case "Subitem7Title":
        let text = node.text ?? ""
        return "<strong>　　　　　　　" + text + "　</strong>"
        
      case "RemarksLabel":
        let text = node.text ?? ""
        return "<strong>" + text + "　</strong>"
        
        
      default:
        return "<div class=\"\(tag)\">" + children + "</div>"
      }
      
      return children
    
  }
  
  //MARK:- Extract TOC
  func extractTOC(_ node: XMLElement) -> [Toc] {
    
    var tocs: [Toc] = []
    
    if node.tagName == "TOCPart" {
      let toc = Toc()
      toc.num = node["Num"]
      toc.title = node.at_xpath("PartTitle")?.text
      toc.articleRange = node.at_xpath("ArticleRange")?.text
      
      if let _ = anchor(at: node), let path = anchorPath(element: node, to: []) {
        toc.anchor = path.joined(separator: "/")
      }
      
      tocs.append(toc)
    }
    
    if node.tagName == "TOCChapter" {
      let toc = Toc()
      toc.num = node["Num"]
      toc.title = node.at_xpath("ChapterTitle")?.text
      toc.articleRange = node.at_xpath("ArticleRange")?.text
      
      if let _ = anchor(at: node), let path = anchorPath(element: node, to: []) {
        toc.anchor = path.joined(separator: "/")
      }
      tocs.append(toc)
    }
    
    if node.tagName == "TOCSection" {
      let toc = Toc()
      toc.num = node["Num"]
      toc.title = node.at_xpath("SectionTitle")?.text
      toc.articleRange = node.at_xpath("ArticleRange")?.text
      
      if let _ = anchor(at: node), let path = anchorPath(element: node, to: []) {
        toc.anchor = path.joined(separator: "/")
      }
      tocs.append(toc)
    }
    
    if node.tagName == "TOCSubsection" {
      let toc = Toc()
      toc.num = node["Num"]
      toc.title = node.at_xpath("SubsectionTitle")?.text
      toc.articleRange = node.at_xpath("ArticleRange")?.text
      
      if let _ = anchor(at: node), let path = anchorPath(element: node, to: []) {
        toc.anchor = path.joined(separator: "/")
      }
      tocs.append(toc)
    }
    
    if node.tagName == "TOCDivision" {
      let toc = Toc()
      toc.num = node["Num"]
      toc.title = node.at_xpath("DivisionTitle")?.text
      toc.articleRange = node.at_xpath("ArticleRange")?.text
      
      if let _ = anchor(at: node), let path = anchorPath(element: node, to: []) {
        toc.anchor = path.joined(separator: "/")
      }
      tocs.append(toc)
    }
    
    let children = node.xpath("./*")
    for node in children {
      tocs += extractTOC(node)
    }
    
    var n = 0
    tocs.forEach {
      $0.index = self.digitString(n)
      n += 1
    }
    
    
    
    return tocs
  }
  
  func digitString(_ n: Int) -> String {
    let string = ("0000" + String(n))
    return String(string[string.index(string.startIndex, offsetBy: string.count - 4)...])
  }
  
  func anchor(at element: XMLElement) -> String? {
    
    guard let tagName = element.tagName else { return nil }
    switch tagName {
    case "Division", "TOCDivision":    if let num = element["Num"] { return "目\(num)" }
    case "Subsection", "TOCSubsection":  if let num = element["Num"] { return "款\(num)" }
    case "Section", "TOCSection":     if let num = element["Num"] { return "節\(num)" }
    case "Chapter", "TOCChapter":     if let num = element["Num"] { return "章\(num)" }
    case "Part", "TOCPart":        if let num = element["Num"] { return "編\(num)" }
      //case "AppdxTable":    return ""
      
    default:            return nil
    }
    
    return nil
  }
  
  func anchorPath(element: XMLElement, to path: [String]) -> [String]? {
    var path = path
    let current = anchor(at: element)
    if current != nil {
      path.insert(current!, at: 0)
    }
    
    if element.tagName == "Article" {
      return path
    }
    //    else if element.tagName == "Paragraph" {
    //      return path
    //    }
    
    if let parent = element.parent, parent.tagName != "Law" {
      
      // if let parent = element.parent, parent.toXML?.hasPrefix("<?xml") != true {
      return anchorPath(element: parent, to: path)
    }
    
    return path
  }
  
  //MARK:- ESCAPING TABLE
  
  func escapeTables(_ data: Data) -> (String, [String]) {
    var string = String(data: data, encoding: .utf8)!
    let tableXMLs = string.escapeTableTags()
    return (string, tableXMLs)
  }
  
}

private extension String {
  
  func count(_ regex: String) -> Int {
    var findRange = self.startIndex..<self.endIndex
    var count = 0
    while true {
      if let found = self.range(of: regex, options: [.regularExpression], range: findRange) {
      findRange = found.upperBound..<self.endIndex
        count += 1
      }else {
        break
      }
    }
    return count
  }
  
  mutating func escapeTableTags() -> [String] {
    
    var tableXMLs:[String] = []
    /*
     SPAN class="parenthesis"
     リンクを付加する条件
     B タグ内ではない
     
     最大広さの括弧を検出
     */
    
    
    /*
     substring version 26.344820022583
     unichar version 2.34652501344681
     */
    
    
    
    let openTag = "<Table(>| [^<>/]*>)"
    let closeTag = "</Table>"

    var findRange = self.startIndex..<self.endIndex
    
    func findNearestOpenOrCloseTag(in range: Range<String.Index>) -> (Bool, Range<String.Index>?) {
      
      let openRange = self.range(of: openTag, options: [.regularExpression],  range: range)
      let closeRange = self.range(of: closeTag, options: [],  range: range)
      
      if openRange == nil && closeRange == nil { return (true, nil) }
      if openRange == nil && closeRange != nil { return (false, closeRange) }
      if openRange != nil && closeRange == nil { return (true, openRange) }
      
      if openRange!.lowerBound <= closeRange!.lowerBound { return (true, openRange) }
      else { return (false, closeRange) }
    }
    
    var tableStack: [Range<String.Index>] = []
    while true {

      // 1. get (
      let (isOpen, range) = findNearestOpenOrCloseTag(in: findRange)
      
      // 2. End
      if range == nil { break }
      
      if isOpen {
        tableStack.append(range!)
        findRange = range!.upperBound ..< self.endIndex
        continue
        
      }else {
        
        if tableStack.count == 0 {
          print("*** empty ***") //39800
          
          tableXMLs.forEach { print("\($0.count(openTag)), \($0.count(closeTag))") }
          
          findRange = range!.upperBound ..< self.endIndex
          continue
        }
        
        let openRange = tableStack.popLast()

        if tableStack.count == 0 && openRange != nil {
          // CLOSE
          //   openRange ..< range
          let tableRange = openRange!.lowerBound ..< range!.upperBound
          tableXMLs.append( String(self[tableRange]) )
          self.replaceSubrange(tableRange, with: "<Table />")
          findRange = tableRange.lowerBound ..< self.endIndex

        }else {
          findRange = range!.upperBound ..< self.endIndex

        }
      }
      
    }
    //print("- Got \(tableXMLs.count) tables")
    return tableXMLs
  }
}

extension NSAttributedString {
  
  func serializedData() -> Data? {
    let passthroughAttributes: [NSAttributedString.Key] = [.tempLink, .anchor, .ruby, .rubyTextId, .elementNameAttributeName , .embeddedAttachement, .diffOperation, .diffOperationDeletedString, .reconstructionIndex]
    
    var serializedDictionary: [String: Any] = [:]
    var idx = 0
    var effectiveRange = NSMakeRange(0,0)
    
    while idx < self.length {
      autoreleasepool {
        var plist: [String: Any] = [:]
        var rawAttributes = self.attributes(at: idx, longestEffectiveRange: &effectiveRange, in: NSMakeRange(0,self.length))
        
        rawAttributes = rawAttributes.filter { passthroughAttributes.contains($0.key) }
        rawAttributes.forEach {
          let key = $0.key.rawValue
          plist[key] = $0.value
        }
        
        serializedDictionary[NSStringFromRange(effectiveRange)] = plist
        idx = NSMaxRange(effectiveRange);
      }
    }
    
    serializedDictionary["string"] = self.string
    guard let data = try? JSONSerialization.data(withJSONObject: serializedDictionary, options: [])  else { return nil }
    
    return data //?.deflate()
  }
  
  convenience init?(serializedData: Data) {
    let data = serializedData//(serializedData as NSData).inflate()!
    guard let plist = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else { return nil }
    guard let string = plist!["string"] as? String else { return nil }
    let mstr = NSMutableAttributedString(string: string)
    
    plist!.forEach {
      guard $0.key != "string" else { return }
      guard let value = $0.value as? [String: Any] else { return }
      
      let range = NSRangeFromString($0.key)
      guard NSMaxRange(range) <= mstr.length else { return }
      
      value.forEach {
        let key = NSAttributedString.Key(rawValue: $0.key)
        let value = $0.value
        mstr.addAttribute(key, value: value, range: range)
      }
    }
    
    self.init(attributedString: mstr)
  }
}

extension NSString {
  convenience init?(serializedData: Data) {
    guard let plist = try? JSONSerialization.jsonObject(with: serializedData, options: []) as? [String: Any] else { return nil }
    guard let string = plist!["string"] as? String else { return nil }
    
    self.init(string: string)
  }

}

extension NSMutableAttributedString {
  func removeReturn() {
    while true {
      let range = (self.string as NSString).range(of: "\n")
      if range.location == NSNotFound { break }
      self.replaceCharacters(in: range, with: "")
    }
    while true {
      let range = (self.string as NSString).range(of: " ")
      if range.location == NSNotFound { break }
      self.replaceCharacters(in: range, with: "")
    }
  }
  
  func addSpace() {
    append(NSAttributedString(string: "　", attributes:[.anchor: ""]))
  }
  
  func addSpaceBefore() {
    
    var idx = 0
    let range = (self.string as NSString).range(of: "[^\n]", options: .regularExpression)
    if range.location != NSNotFound { idx = range.location }
    insert(NSAttributedString(string: "　", attributes:[.anchor: ""]), at: idx)
  }
  
  func addReturnBefore() {
    insert(NSAttributedString(string: "\n", attributes:[.anchor: ""]), at: 0)
  }
  
  func addReturn() {
    append(NSAttributedString(string: "\n", attributes:[.anchor: ""]))
  }
  
  func sliced() -> [NSAttributedString] {
    var attributedStringArray = [NSAttributedString]()
    let string = self.string as NSString
    var searchRange = NSMakeRange(0, string.length)
    var index = 0
    while true {
      let range = string.range(of: "\n", range: searchRange)
      if range.location == NSNotFound {
        let subAttr = self.attributedSubstring(from: NSMakeRange(index, string.length - index))
        attributedStringArray.append( subAttr )
        break
      }
      
      var subAttr = self.attributedSubstring(from: NSMakeRange(index, range.location - index))
      if subAttr.length == 0 {
        subAttr = NSAttributedString(string: "\n")
      }
      attributedStringArray.append( subAttr )
      index = NSMaxRange(range)
      searchRange = NSMakeRange( index, string.length - index)
      if searchRange.location >= self.length { break }
    }
    return attributedStringArray
  }
}



