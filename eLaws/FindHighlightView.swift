//
//  FindHighlightView.swift
//  LawXML
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
#if os(iOS)
  import UIKit
#endif

#if os(OSX)
  import Cocoa
#endif


class FindHighlightView: UIView {
  
  static func view(with frame: CGRect) -> FindHighlightView {
    
    let highlight = FindHighlightView(frame: frame)
    #if os(iOS)
      highlight.backgroundColor = UIColor.clear
      highlight.layer.cornerRadius = 5.0
      highlight.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.7994411496).cgColor
      highlight.layer.borderWidth = 3
    #endif
    
    
    #if os(OSX)
      highlight.wantsLayer = true
      highlight.layer?.cornerRadius = 5.0
      highlight.layer?.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.7994411496).cgColor
      highlight.layer?.borderWidth = 5
      
    #endif
    
    
    return highlight
  }
  
  #if os(OSX)
  override var isOpaque: Bool { return true }

  #endif
  
  func highlight(with frame: CGRect) {
    self.frame = frame

    #if os(iOS)
    self.superview?.bringSubviewToFront(self)
    alpha = 1
    transform = CGAffineTransform(scaleX: 2.0, y: 2.0)
    
    UIView.animate(withDuration: 0.1, delay: 0.1, options: UIView.AnimationOptions.curveEaseOut, animations: {
      self.transform = CGAffineTransform.identity
      
    }, completion: { success in })
      
    UIView.animate(withDuration: 0.2, delay: 0.5, options: UIView.AnimationOptions.curveEaseIn, animations: {
        self.transform = CGAffineTransform.identity
        self.alpha = 0
        
      }, completion: { success in })
    #endif
    
    #if os(OSX)

      layer?.zPosition = 100
      self.isHidden = false
      layer?.setNeedsDisplay()
      self.alphaValue = 1
      //      self.layer?.backgroundColor = NSColor.red.cgColor
      self.animator().frame = CGRect(x: frame.origin.x - 15, y: frame.origin.y - 15, width: frame.size.width + 30, height: frame.size.height + 30)

      NSAnimationContext.beginGrouping()
      let ctx = NSAnimationContext.current
      ctx.duration = 0.5

      ctx.completionHandler = {
        self.layer?.setAffineTransform(CGAffineTransform.identity)
        self.isHidden = true
      }
      self.animator().alphaValue = 0
      self.animator().frame = frame

      NSAnimationContext.endGrouping()
      
    #endif
  }
}
