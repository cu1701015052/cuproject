//
//  LinkMaker.swift
//  eLawsDownloader
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class LinkMaker {
  var prefix: String
  var styleSheet: StyleSheet
  var 同法: String? = nil
  var lawNicknames: [String: String] = [:]
  var referredLaws: [(String, String)] = [] // lawNum, lawTitle
  var lawsThatDoesNotExists =  Set<String>()

  init(prefix: String, styleSheet: StyleSheet) {
    self.prefix = prefix
    self.styleSheet = styleSheet
  }
  
  // ANALYZE
  /*
   昭和◯年法律第◯号。以下「医薬品医療機器等法」という。
   
   */
  
  func analyzeShortenedLawTitle(_ string : String, lawName: String) -> String? {
    
    let findRange = string.startIndex..<string.endIndex
    if let matched = string.range(of: "(令和|昭和|大正|平成|明治)[一二三四五六七八九〇十百千年法律第]+号。以下「", options: .regularExpression, range: findRange),
    let lawName = string.range(of: ".*(?=」)", options: .regularExpression, range: matched.upperBound ..< string.endIndex) {
      
      return String(string[lawName])
    }
    return nil
  }
  
  func addLinks(to mattr: NSMutableAttributedString) {
    let regex = ProcessSentenceXML.Regex.regex
    
    // regex1, extract string
    // ->  find article, paragraph, item -> convert to anchor -> combine
    
    var findRange = NSMakeRange(0, mattr.length)
    let processXML = ProcessSentenceXML()
    while true {
      let string = mattr.string as NSString
      let range: NSRange = string.range(of: regex, options: [.regularExpression, .caseInsensitive],  range: findRange)
      if range.length == 0 { break }
      
      let substring = string.substring(with: range)
      guard let anchor = processXML.convertNaturalStringToAnchor(substring) else { break }
      
      if nil == mattr.attribute(.tempLink, at: range.location, effectiveRange: nil) {
        autoreleasepool {

          let (extendedRange, extendedAnchor, lawNum) = extendLinkToLawTitle(to: mattr, range: range, anchor: anchor)

          mattr.addAttribute(.tempLink, value: prefix + extendedAnchor + "/#\(globalLinkSerialNumber)", range: extendedRange)
          
          let color = extendedAnchor == anchor ? styleSheet.backgroundColorScheme.textColor : styleSheet.backgroundColorScheme.tintColor
          mattr.addAttribute(.tempLinkColor, value: color, range: extendedRange)
          
          // EXTRACT SHORTENDED LAW TITLE
          if lawNum != nil {
            let substring = (mattr.string as NSString).substring(with: extendedRange)
            if let shortendedLawname = analyzeShortenedLawTitle(substring, lawName: lawNum!) {
              lawNicknames[shortendedLawname] = lawNum
            }
          }
          
          globalLinkSerialNumber += 1
        }
      }
      let loc = NSMaxRange(range)
      findRange = NSMakeRange(loc, mattr.length - loc)
    }
    
  }
  
  func extendLinkToLawTitle(to mattr: NSMutableAttributedString, range: NSRange, anchor: String) -> (NSRange, String, String?) {
    
    guard range.location != NSNotFound else { return (range, anchor, nil) }
    guard range.location > 1 else { return (range, anchor, nil) }
    guard range.length != 0 else { return (range, anchor, nil) }
    
    let lawList = LawList()
    var location = range.location - 1
    var length = 1
    var skippedLength = 0
    // SKIP （昭和六十三年法律第九十一号）
    let string = mattr.string as NSString
    let substring = string.substring(with: NSMakeRange(location, length))
    if substring == "）" {
      while true {
        location -= 1
        skippedLength += 1
        
        let substring = string.substring(with: NSMakeRange(location, length))
        if substring == "（" {
          skippedLength += 1
          location -= 1
          break
        }
        
        if location < 1 { return (range, anchor, nil) }
        
      }
    }
    
    guard location > 0 else { return (range, anchor, nil) }
    
    // IF LAST STRING IS NOT KANJI, SKIP 『、第三十条』
    
    let lastString = string.substring(with: NSMakeRange(location, length))
    if lastString.range(of: "[。、　（「あ-ん\n]", options: NSString.CompareOptions.regularExpression) != nil { return (range, anchor, nil) }
    
    var lawCandidate: String? = nil
    var lawCandidateTitle: String? = nil
    var lookForHistory: [(String, String)] = Array<(String, String)>()
    
    lookForHistory.append(contentsOf: referredLaws)

    var lawsWithSuffix: [EGovLaw]? = nil
    while true {
      let substring = string.substring(with: NSMakeRange(location, length))
      
      
      // HAS SUFFIX?
      
      // LOOK FOR NICKNAMES
      let keys = lawNicknames.keys
      if let nickname = keys.filter({ $0 == substring }).first {
        lawCandidate = lawNicknames[nickname]

      }else if let nickname = keys.filter({ $0.hasSuffix(substring) }).first {
        lawCandidate = lawNicknames[nickname]
        
      }else if substring == "同法" && 同法 != nil {
        lawCandidate = 同法
        
      }else if substring == "附則"  {
        lawCandidate = nil
        lawCandidateTitle = nil
        break
      }else if substring == "法律"  {
        //ありすぎるからスキップ
      }else if substring.count > 1 {
        let contains = lawsThatDoesNotExists.contains(substring)

        if contains {
          location += 1
          length -= 1
          break
        }

        var lookForHistoryFlag = false
        
        for obj in lookForHistory {
          if obj.1.hasSuffix(substring) { lookForHistoryFlag = true; break }
        }
        
        if lookForHistoryFlag {
          lookForHistory = lookForHistory.filter { $0.1.hasSuffix(substring) }

          lawCandidate = lookForHistory.first?.0

        }else {
          
          if lawsWithSuffix == nil {
            lawsWithSuffix = lawList.lawsWithSuffix(substring)?.sorted { $0.lawTitle.count < $1.lawTitle.count } ?? []
          }else {
            lawsWithSuffix = lawsWithSuffix!.filter({ $0.lawTitle.hasSuffix(substring) })
          }
          
          let count = lawsWithSuffix!.count
          
          if count == 0 {
            lawsThatDoesNotExists.insert(substring)

            //              if lawCandidate != nil {
            //              print("\(substring) ")
            //              }
            
            // NO RESULTS
            location += 1
            length -= 1
            break
          }
          
          if count > 0 {
            let law = lawsWithSuffix!.first!
            lawCandidate = law.lawNum
            lawCandidateTitle = law.lawTitle
          }
        }
      }
      
      if location == 0 { break }
      
      location -= 1
      length += 1
    }

    
    if lawCandidate == nil {
      return (range, anchor, nil)
    }
    
    let extendedRange = NSMakeRange(location, length + skippedLength + range.length)
    let extendedAnchor = "法\(lawCandidate!)/" + anchor
    同法 = lawCandidate!
    if lawCandidateTitle != nil {
      referredLaws.append( (lawCandidate!, lawCandidateTitle!))
      referredLaws = referredLaws.sorted { $0.1.count < $1.1.count }
    }

    return (extendedRange, extendedAnchor, lawCandidate!)
  }
}
