//
//  Types.swift
//  LawXML
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
import CloudKit

#if os(iOS) || os(watchOS)
  import UIKit
#else
  import AppKit
  typealias UIColor = NSColor
#endif

// Error

enum LawXMLError: LocalizedError {
  
  case unknown
  case offline
  case accountMissing
  case userDisabledCloudKit
  case timeOut
  case notFound
  case iCloudOverCellularNotAllowed
  
  case fileError
  case unableToUpload
  case unknownTryAgain

  case selectionRangeTooLong
  case selectionRangeWrong
  case unsupportedLaw
  case xmlParseError
  case downloadedLawIsEmpty
  case attachmentDoesNotExists
  case attemptedInstallingWrongMishiko

  case cannotMakePayments

  var errorDescription: String? {
    
    switch self {
    case .unknown: return NSLocalizedString("ERR:Unknown Error", comment:"")
    case .offline: return NSLocalizedString("ERR:OfflineError", comment:"")
    case .accountMissing: return NSLocalizedString("ERR:Account Missing Error", comment:"")
    case .userDisabledCloudKit: return NSLocalizedString("ERR:UserDisabledCloudKit", comment:"")
    case .timeOut: return NSLocalizedString("ERR:TimeOut Error", comment:"")
    case .notFound: return NSLocalizedString("ERR:NotFound Error", comment:"")
    case .iCloudOverCellularNotAllowed: return NSLocalizedString("ERR:iCloudOverCellularNotAllowed Error", comment:"")
      
    case .fileError: return NSLocalizedString("ERR:File Access Error", comment:"")
    case .unableToUpload: return NSLocalizedString("ERR:Failed to upload", comment:"")
    case .selectionRangeTooLong: return NSLocalizedString("ERR:Selection Range Error", comment:"")
    case .selectionRangeWrong: return NSLocalizedString("ERR:Selection Range Wrong", comment:"")

    case .unsupportedLaw: return NSLocalizedString("ERR:Unsupported Law Error", comment:"")
    case .xmlParseError: return "xmlParseError"

    case .attachmentDoesNotExists: return NSLocalizedString("ERR:attachmentDoesNotExists", comment:"")
    case .attemptedInstallingWrongMishiko: return NSLocalizedString("ERR:attemptedInstallingWrongMishiko", comment:"")
    case .downloadedLawIsEmpty: return NSLocalizedString("ERR:downloadedLawIsEmpty", comment:"")
      
    case .cannotMakePayments: return NSLocalizedString("購入許可がありません", comment:"")
    case .unknownTryAgain: return NSLocalizedString("ERR:UnknownTryAgain Error", comment:"")

    }
  }
}

struct ELawsError: LocalizedError {
  var message: String
  var errorDescription: String? {
    return message
  }
}

enum LawXMLTypes {
  static var internalLinkPrefix = "jpaccyber-u1701015052CUProjectinternal://"
  static var fetchedLinkPrefix = "jpaccyber-u1701015052CUProjectfetched://"
  static var headerLinkPrefix = "jpaccyber-u1701015052CUProjectheader://"
  static var tableStructLinkPrefix = "jpaccyber-u1701015052CUProjectablestruct://"

}


protocol FontDownloaderDelegate: class {
  func fontDownloadingCompleted(_ error: Error?)
}

enum FontType : Int {
  case mincho = 0, gothic
  
  
  func relatedFileNames() -> [String] {
    switch self {
    case .mincho, .gothic: return []
    }
  }
  
  
  func resouceTagName() -> String {
    switch self {
    case .mincho, .gothic: return ""
    }
  }
  
  func thumbnailName() -> String {
    return resouceTagName()
  }
  

  func installedFontPath(for name: String) -> String? {
    let docUrl = FileManager.default.appSupportUrl
    let fontFolder = docUrl.appendingPathComponent("Fonts")
    do {
      let contents = try FileManager.default.contentsOfDirectory(at: fontFolder, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
      
      for url in contents {
        if name == url.deletingPathExtension().lastPathComponent {
          return  url.path
        }
      }
    }catch _ {
      return nil
    }
    return nil
  }
  
  func ctFont(with size: CGFloat) -> CTFont {
    var proportionalMetrics = UserDefaults.standard.bool(forKey: "ProportionalMetrics")
    
    if #available(iOS 12.0, *) {
    }else {
      if UserDefaults.standard.bool(forKey: "VerticalMode") { proportionalMetrics = false }
    }

    let fontDescriptor = CTFontDescriptorCreateWithNameAndSize(fontName() as CFString, size)
    
    if proportionalMetrics {
      
      let descriptor = CTFontDescriptorCreateCopyWithFeature(fontDescriptor, kTextSpacingType as CFNumber, kAltProportionalTextSelector as CFNumber);
      let ctfontToEmbed = CTFontCreateWithFontDescriptor(descriptor, size, nil);
      
      return ctfontToEmbed
    }else {
      let ctfontToEmbed = CTFontCreateWithFontDescriptor(fontDescriptor, size, nil);
      return ctfontToEmbed
    }

  }
  
  func fontName() -> String {
    switch self {
    case .mincho: return "HiraMinProN-W3"
    case .gothic: return "HiraKakuProN-W3"
    }
  }
  
  func name() -> String {
    switch self {
    case .mincho: return "明朝体"
    case .gothic: return "ゴシック体"
    }
  }
  
  // Variations
  
  func ctFontForTitle(with size: CGFloat) -> CTFont {
    var proportionalMetrics = UserDefaults.standard.bool(forKey: "ProportionalMetrics")
    if #available(iOS 12.0, *) {
    }else {
      if UserDefaults.standard.bool(forKey: "VerticalMode") { proportionalMetrics = false }
    }

    switch self {
    case .mincho: fallthrough
    case .gothic: fallthrough
    default: break
    }
    
    
    let fontDescriptor = CTFontDescriptorCreateWithNameAndSize("HiraKakuProN-W6" as CFString, size)
    
    if proportionalMetrics {
      
      let descriptor = CTFontDescriptorCreateCopyWithFeature(fontDescriptor, kTextSpacingType as CFNumber, kAltProportionalTextSelector as CFNumber);
      let ctfontToEmbed = CTFontCreateWithFontDescriptor(descriptor, size, nil);
      
      return ctfontToEmbed
      
    }else {
      let ctfontToEmbed = CTFontCreateWithFontDescriptor(fontDescriptor, size, nil);
      return ctfontToEmbed
    }
  }
  
  func ctFontForCaption(with size: CGFloat) -> CTFont {
    var proportionalMetrics = UserDefaults.standard.bool(forKey: "ProportionalMetrics")
    if #available(iOS 12.0, *) {
    }else {
      if UserDefaults.standard.bool(forKey: "VerticalMode") { proportionalMetrics = false }
    }

    switch self {
    case .mincho: fallthrough
    case .gothic: fallthrough
    default: break
    }
    
    let fontDescriptor = CTFontDescriptorCreateWithNameAndSize("HiraKakuProN-W3" as CFString, size)
    
    if proportionalMetrics {
      
      let descriptor = CTFontDescriptorCreateCopyWithFeature(fontDescriptor, kTextSpacingType as CFNumber, kAltProportionalTextSelector as CFNumber);
      let ctfontToEmbed = CTFontCreateWithFontDescriptor(descriptor, size * 0.98, nil);
      
      return ctfontToEmbed
    }else {
      let ctfontToEmbed = CTFontCreateWithFontDescriptor(fontDescriptor, size * 0.98, nil);
      return ctfontToEmbed
    }

  }
  
  func ctFontForLawTitle(with size: CGFloat) -> CTFont {
    var proportionalMetrics = UserDefaults.standard.bool(forKey: "ProportionalMetrics")
    if #available(iOS 12.0, *) {
    }else {
      if UserDefaults.standard.bool(forKey: "VerticalMode") { proportionalMetrics = false }
    }

    switch self {
    case .mincho: fallthrough
    case .gothic: fallthrough
    default: break
    }
     
    let fontDescriptor = CTFontDescriptorCreateWithNameAndSize("HiraKakuProN-W6" as CFString, size)
    
    if proportionalMetrics {
      
      let descriptor = CTFontDescriptorCreateCopyWithFeature(fontDescriptor, kTextSpacingType as CFNumber, kAltProportionalTextSelector as CFNumber);
      let ctfontToEmbed = CTFontCreateWithFontDescriptor(descriptor, size * 1.5, nil);
      
      return ctfontToEmbed
    }else {
      let ctfontToEmbed = CTFontCreateWithFontDescriptor(fontDescriptor, size * 1.5, nil);
      return ctfontToEmbed
    }

  }
  
  func ctFontForParenthesis(with size: CGFloat, level: Int = 1) -> CTFont {
    var proportionalMetrics = UserDefaults.standard.bool(forKey: "ProportionalMetrics")
    if #available(iOS 12.0, *) {
    }else {
      if UserDefaults.standard.bool(forKey: "VerticalMode") { proportionalMetrics = false }
    }
    
    var factor = CGFloat(pow(0.94, Double(level)))
    
    factor = 0.94 // IGNOR LEVEL

    switch self {
    case .mincho: fallthrough
    case .gothic: fallthrough
    default: break
    }
    
    let fontDescriptor = CTFontDescriptorCreateWithNameAndSize("HiraKakuProN-W3" as CFString, size)
    
    if proportionalMetrics {
      
      let descriptor = CTFontDescriptorCreateCopyWithFeature(fontDescriptor, kTextSpacingType as CFNumber, kAltProportionalTextSelector as CFNumber);
      let ctfontToEmbed = CTFontCreateWithFontDescriptor(descriptor, size * factor, nil);
      
      return ctfontToEmbed
    }else {
      let ctfontToEmbed = CTFontCreateWithFontDescriptor(fontDescriptor, size * factor, nil);
      return ctfontToEmbed
    }
  }
}

enum KanjiType : Int {
  case kanji = 0, zenkaku, hankaku
}


// NOTIFICATION

enum LawNotification {
  static let settingsDidChange = Notification.Name("SettingsDidChange")
  static let splitViewOrientationDidChange = Notification.Name("SplitViewOrientationDidChange")
  static let highlightDidChangeExternally = Notification.Name("HighlightDidChangeExternally")
  static let bookmarkDidChangeExternally = Notification.Name("BookmarkDidChangeExternally")
  static let realmDidUpdateNotification = Notification.Name("RealmDidUpdate")
  static let realmWillUpdateNotification = Notification.Name("RealmWillUpdate")
  static let realmDidUpdateFromExternNotification = Notification.Name("realmDidUpdateFromExternNotification")

}

enum BackgroundColorSchemeType : String {
  case White = "White"
  case Paper = "Paper"
  case Sepia = "Sepia"
  case Grey = "Grey"
  case Dark = "Dark"
  
  init() {
    self = .Paper
  }
  
  init(fromUserDefaults value: String?) {
    
    self.init()
    if value != nil {
      switch value! {
      case "Paper": self = .Paper
      case "White": self = .White
      case "Sepia": self = .Sepia
      case "Grey": self = .Grey
      case "Dark": self = .Dark
      default: self = .Paper
      }
    }
  }
  
  init(rawValue: String ) {
    
    switch rawValue {
    case "Paper": self = .Paper
    case "White": self = .White
    case "Sepia": self = .Sepia
    case "Grey": self = .Grey
    case "Dark": self = .Dark
    default: self = .Paper
    }
  }
  
  var inverted: Bool {
    switch self {
    case .Paper: return false
    case .White: return false
    case .Sepia: return false
    case .Grey: return true
    case .Dark: return true
    }
  }
  
  var backgroundColor: UIColor {
    switch self {
    case .Paper: return UIColor(red: 1, green: 1, blue: 240.0/255.0, alpha: 1)
    case .White: return UIColor(red: 1, green: 1, blue: 1, alpha: 1)
    case .Sepia: return UIColor(red: 247.0/255.0, green: 239.0/255.0, blue: 224.0/255.0, alpha: 1)
    case .Grey: return UIColor(red: 0.29, green: 0.29, blue: 0.29, alpha: 1)
    case .Dark: return UIColor(red: 0, green: 0, blue: 0, alpha: 1)
    }
  }
  
  var secondaryBackgroundColor: UIColor {
    switch self {
    case .Paper: return UIColor(red: 1, green: 1, blue: 1, alpha: 1)
    case .White: return UIColor(red: 1, green: 1, blue: 1, alpha: 1)
    case .Sepia: return UIColor(red: 1, green: 1, blue: 1, alpha: 1)
    case .Grey: return UIColor(red: 0.15, green: 0.15, blue: 0.15, alpha: 1)
    case .Dark: return UIColor(red: 0.25, green: 0.25, blue: 0.25, alpha: 1)
    }
  }
  
  var popoverBackgroundColor: UIColor {
    switch self {
    case .Paper: return secondaryBackgroundColor.withAlphaComponent(0.85)
    case .White: return UIColor(red: 1, green: 1, blue: 1, alpha: 0.85)
    case .Sepia: return secondaryBackgroundColor.withAlphaComponent(0.85)
    case .Grey: return secondaryBackgroundColor.withAlphaComponent(0.6)
    case .Dark: return secondaryBackgroundColor.withAlphaComponent(0.75)
    }
  }
  
  var tableSelectionColor: UIColor {
    switch self {
    case .Paper: return tintColor.withAlphaComponent(0.5)
    case .White: return tintColor.withAlphaComponent(0.5)
    case .Sepia: return tintColor.withAlphaComponent(0.5)
    case .Grey: return tintColor.withAlphaComponent(0.5)
    case .Dark: return tintColor.withAlphaComponent(0.5)
    }
  }
  
  var tabSeparatorColor: UIColor {
    switch self {
    case .Paper: return UIColor(red: 10/255.0, green: 10/255.0, blue: 10/255.0, alpha: 0.1)
    case .White: return UIColor(red: 10/255.0, green: 10/255.0, blue: 10/255.0, alpha: 0.1)
    case .Sepia: return UIColor(red: 10/255.0, green: 10/255.0, blue: 10/255.0, alpha: 0.1)
    case .Grey: return UIColor(red: 20/255.0, green: 20/255.0, blue: 20/255.0, alpha: 0.3)
    case .Dark: return UIColor(red: 30/255.0, green: 30/255.0, blue: 30/255.0, alpha: 1)
    }
  }

  var tableSeparatorColor: UIColor {
    switch self {
    case .Paper: return UIColor(red: 10/255.0, green: 10/255.0, blue: 10/255.0, alpha: 0.2)
    case .White: return UIColor(red: 10/255.0, green: 10/255.0, blue: 10/255.0, alpha: 0.2)
    case .Sepia: return UIColor(red: 10/255.0, green: 10/255.0, blue: 10/255.0, alpha: 0.2)
    case .Grey: return UIColor(red: 20/255.0, green: 20/255.0, blue: 20/255.0, alpha: 0.4)
    case .Dark: return UIColor(red: 40/255.0, green: 40/255.0, blue: 40/255.0, alpha: 1)
    }
  }
  
  var tableBackgroundColor: UIColor {
    switch self {
    case .Paper: return UIColor.white
    case .White: return UIColor.white
    case .Sepia: return UIColor(red: 247.0/255.0, green: 239.0/255.0, blue: 224.0/255.0, alpha: 1)
    case .Grey: return UIColor(red: 0.29, green: 0.29, blue: 0.29, alpha: 1)
    case .Dark: return UIColor(red: 0, green: 0, blue: 0, alpha: 1)
    }
  }
  
  var backgroundColorCSS: String {
    switch self {
    case .Paper: return ""
    case .White: return "body { background-color: white; color: black; }"
    case .Sepia: return "body { background-color: #F7EFE0; color: black; }"
    case .Grey: return "body { background-color: #4c4c4c; color: white; }"
    case .Dark: return "body { background-color: black; color: white; }"
    }
  }
  
  var linkColorCSS: String {
    switch self {
    case .Paper: return ""
    case .White: return ""
    case .Sepia: return ""
    case .Grey: return " a:link { color: #A4C6CE  } " + " a:visited { color: PaleTurquoise  } "
    case .Dark: return " a:link { color: #A4C6CE  } " + " a:visited { color: PaleTurquoise  } "
    }
  }
  
  var blockQuoteColorCSS: String {
    switch self {
    case .Paper: return "#eef2ff"
    case .White: return "#eef2ff"
    case .Sepia: return "#E0E8F7"
    case .Grey: return "#0B2D35"
    case .Dark: return "#253C4C"
    }
  }
  
  var blockQuoteColor: UIColor {
    switch self {
    case .Paper: return RGBA(0xee, 0xf2, 0xff, 1)
    case .White: return RGBA(0xee, 0xf2, 0xff, 1)
    case .Sepia: return #colorLiteral(red: 0.9307039711, green: 0.8692847652, blue: 0.7747186922, alpha: 1)
    case .Grey: return RGBA(0x0B, 0x2D, 0x35, 1)
    case .Dark: return RGBA(0x25, 0x3C, 0x4C, 1)
    }
  }
  
  var blockQuoteBorderCSS: String {
    switch self {
    case .Paper: return "#cccce0"
    case .White: return "#cccce0"
    case .Sepia: return "#677FAA"
    case .Grey: return "#909090"
    case .Dark: return "#909090"
    }
  }
  
  var tintColor: UIColor {
    switch self {
    case .Paper: return RGBA(0.0, 79.0, 120.0, 1.0)
    case .White: return RGBA(0.0, 79.0, 120.0, 1.0)
    case .Sepia: return UIColor(red: 184.0/255.0, green: 126.0/255.0, blue: 55.0/255.0, alpha: 1)
    case .Grey: return #colorLiteral(red: 0, green: 0.6852248907, blue: 0.7926144004, alpha: 1)
    case .Dark: return #colorLiteral(red: 0, green: 0.6947829127, blue: 1, alpha: 1)
    }
  }
  
		#if os(iOS)
  var barStyle: UIBarStyle {
    switch self {
    case .Paper: return .default
    case .White: return .default
    case .Sepia: return .default
    case .Grey: return .default
    case .Dark: return .default
    }
  }
  
  var indicatorStyle: UIScrollView.IndicatorStyle {
    switch self {
    case .Paper: return .black
    case .White: return .black
    case .Sepia: return .black
    case .Grey: return .white
    case .Dark: return .white
    }
  }
  #endif
  
  var textColor: UIColor {
    switch self {
    case .Paper: return .black
    case .White: return .black
    case .Sepia: return .black
    case .Grey: return .white
    case .Dark: return .white
    }
  }
  
  var subTextColor: UIColor {
    switch self {
    case .Paper: return .gray
    case .White: return .gray
    case .Sepia: return .gray
    case .Grey: return .lightGray
    case .Dark: return .lightGray
    }
  }
  
  var subSubTextColor: UIColor {
    switch self {
    case .Paper: return .lightGray
    case .White: return .lightGray
    case .Sepia: return .lightGray
    case .Grey: return .gray
    case .Dark: return .gray
    }
  }
  
  
  var subSubSubTextColor: UIColor {
    switch self {
    case .Paper: return .lightGray
    case .White: return .lightGray
    case .Sepia: return .lightGray
    case .Grey: return .gray
    case .Dark: return .gray
    }
  }
  
}

enum TagColor : Int {
  
  case yellow = 0, green, blue, red, purple, orange, grey, noColor
  
  init(rawValue: Int) {
    switch rawValue {
    case 0: self = .yellow
    case 1: self = .green
    case 2: self = .blue
    case 3: self = .red
    case 4: self = .purple
    case 5: self = .orange
    case 6: self = .grey
    case 7: self = .noColor
      
    default: self = .yellow
    }
  }
  
  func className() -> String {
    switch self {
    case .yellow: return "yellow"
    case .green: return "green"
    case .blue: return "blue"
    case .red: return "red"
    case .purple: return "purple"
    case .orange: return "orange"
    case .grey: return "grey"
    case .noColor: return "no color"
    }
  }
  
  func naturalName() -> String {
    switch self {
    case .yellow: return "Yellow"
    case .green: return "Green"
    case .blue: return "Blue"
    case .red: return "Red"
    case .purple: return "Purple"
    case .orange: return "Orange"
    case .grey: return "Grey"
    case .noColor: return "No Color"
    }
  }

  func color() -> UIColor! {
    switch self {
    case .yellow: return RGBA( 255 , 246 , 10  , 0.8 )
    case .green: return RGBA( 139 , 216 , 92  , 0.8 )
    case .blue: return RGBA( 92 , 174 , 242  , 0.8 )
    case .red: return RGBA( 234.0, 85.0, 70.0, 0.8 )
    case .purple: return RGBA( 193 , 126 , 220  , 0.8 )
    case .orange: return RGBA( 239 , 158 , 64 , 0.8 )
    case .grey: return RGBA( 110.0, 110.0, 110.0, 0.8 )
    case .noColor: return RGBA( 0, 0, 0, 0 )
    }
  }
  
  
  func frameColor() -> UIColor! {
    switch self {
    case .yellow: return RGBA( 244 , 217 , 50  , 1 )
    case .green: return RGBA( 139 , 216 , 92  , 1 )
    case .blue: return RGBA( 92 , 174 , 242  , 1 )
    case .red: return RGBA( 234.0, 85.0, 70.0, 1 )
    case .purple: return RGBA( 193 , 126 , 220  , 1 )
    case .orange: return RGBA( 239 , 158 , 64  , 1 )
    case .grey: return RGBA( 110.0, 110.0, 110.0, 1 )
    case .noColor: return RGBA( 180, 180, 200, 1 )
    }
  }


}

enum HighlightColor : Int {
  
  case yellow = 0, green, blue, red, purple,
  underline, underlineBlue, underlineGreen, underlineYellow, underlinePurple, underlineOrange, orange,
  selection = 99, added = 100, changed = 101, deleted = 102, unmasked = 103, mask = 104
  
  init(rawValue: Int) {
    switch rawValue {
    case 0: self = .yellow
    case 1: self = .green
    case 2: self = .blue
    case 3: self = .red
    case 4: self = .purple
    case 11: self = .orange
    case 5: self = .underline
    case 6: self = .underlineBlue
    case 7: self = .underlineGreen
    case 8: self = .underlineYellow
    case 9: self = .underlinePurple
    case 10: self = .underlineOrange

    case 99: self = .selection
    case 100: self = .added
    case 101: self = .changed
    case 102: self = .deleted
    case 103: self = .mask // Not unmask because unmask is not persistent
    case 104: self = .mask
      
    default: self = .yellow
    }
  }
  
  var isUnderline: Bool {
    if self == .underline || self == .underlineBlue || self == .underlineGreen || self == .underlinePurple || self == .underlineYellow || self == .underlineOrange {
      return true
    }
    return false
  }
  
  func className() -> String {
    switch self {
    case .yellow: return "hilt"
    case .green: return "hilt1"
    case .blue: return "hilt2"
    case .red: return "hilt3"
    case .purple: return "hilt4"
    case .orange: return "hilt11"

    case .underline: return "hilt5"
    case .underlineBlue: return "hilt6"
    case .underlineGreen: return "hilt7"
    case .underlineYellow: return "hilt8"
    case .underlinePurple: return "hilt9"
    case .underlineOrange: return "hilt10"

    case .selection: return ""
    case .added: return ""
    case .changed: return ""
    case .deleted: return ""
    case .unmasked: return ""
    case .mask: return "hilt104"
    }
  }
  
  func naturalName() -> String {
    switch self {
    case .yellow: return "Yellow"
    case .green: return "Green"
    case .blue: return "Blue"
    case .red: return "Red"
    case .purple: return "Purple"
    case .orange: return "Orange"

    case .underline: return "Underline"
    case .underlineBlue: return "Underline Blue"
    case .underlineGreen: return "Underline Green"
    case .underlineYellow: return "Underline Yellow"
    case .underlinePurple: return "Underline Purple"
    case .underlineOrange: return "Underline Orange"

    case .mask: return "Mask"
    case .selection: return "Selection"
    case .added: return "Added"
    case .changed: return "Changed"
    case .deleted: return "Deleted"
    case .unmasked: return "Mask"
    }
  }
  
  #if os(iOS) || os(watchOS)
  
  func color() -> UIColor! {
    switch self {
    case .yellow: return RGBA( 255.0, 220.0, 0.0, 0.6 )
    case .green: return RGBA( 150.0, 237.0, 0.0, 0.6 )
    case .blue: return RGBA( 110.0, 186.0, 255.0, 0.6 )
    case .red: return RGBA( 255.0, 107.0, 155.0, 0.6 )
    case .purple: return RGBA( 187.0, 117.0, 255.0, 0.6 )
    case .orange: return RGBA( 255, 147.0, 0, 0.6 )

    case .underline: return RGBA( 255, 0.0, 0.0, 1.0 )
    case .underlineBlue: return #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 1)
    case .underlineGreen: return #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
    case .underlineYellow: return #colorLiteral(red: 0.9529411793, green: 0.8133985519, blue: 0.07061361374, alpha: 1)
    case .underlinePurple: return #colorLiteral(red: 0.9435058594, green: 0.2527923882, blue: 1, alpha: 1)
    case .underlineOrange: return #colorLiteral(red: 1, green: 0.5419967533, blue: 0, alpha: 1)

    case .mask: return RGBA( 179, 179, 179, 1 )
    case .selection: return #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    case .added: return #colorLiteral(red: 0.2666666806, green: 0.7647058964, blue: 0.4132910344, alpha: 1)
    case .changed: return #colorLiteral(red: 0.9060058594, green: 0.6182157629, blue: 0, alpha: 1)
    case .deleted: return #colorLiteral(red: 0.7733289931, green: 0.1174971704, blue: 0.648392693, alpha: 1)
    case .unmasked: return RGBA( 200.0, 200.0, 200.0, 0.2 )

    }
  }
  #else
  func color() -> UIColor! {
    
    switch self {
    case .yellow: return RGBA( 255.0, 220.0, 0.0, 0.6 )
    case .green: return RGBA( 150.0, 237.0, 0.0, 0.6 )
    case .blue: return RGBA( 110.0, 186.0, 255.0, 0.6 )
    case .red: return RGBA( 255.0, 107.0, 155.0, 0.6 )
    case .purple: return RGBA( 187.0, 117.0, 255.0, 0.6 )
    case .orange: return RGBA( 255, 147.0, 0, 0.6 )
      
    case .underline: return RGBA( 255, 0.0, 0.0, 1.0 )
    case .underlineBlue: return #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 1)
    case .underlineGreen: return #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
    case .underlineYellow: return #colorLiteral(red: 0.9529411793, green: 0.8133985519, blue: 0.07061361374, alpha: 1)
    case .underlinePurple: return #colorLiteral(red: 0.9435058594, green: 0.2527923882, blue: 1, alpha: 1)
    case .underlineOrange: return #colorLiteral(red: 1, green: 0.5419967533, blue: 0, alpha: 1)
      
    case .mask: return RGBA( 179, 179, 179, 1 )
    case .selection: return #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    case .added: return #colorLiteral(red: 0.2666666806, green: 0.7647058964, blue: 0.4132910344, alpha: 1)
    case .changed: return #colorLiteral(red: 0.9060058594, green: 0.6182157629, blue: 0, alpha: 1)
    case .deleted: return #colorLiteral(red: 0.7733289931, green: 0.1174971704, blue: 0.648392693, alpha: 1)
    case .unmasked: return RGBA( 200.0, 200.0, 200.0, 0.2 )
    }
  }
  #endif
}

class FontUtils {
  static var cache = NSCache<NSString, CTFont>()
  static func clearCache() {
    self.cache.removeAllObjects()
  }
  static func font(_ path: String, size: CGFloat, proportionalMetrics: Bool) -> CTFont? {
    let size = round(size)
    let key = NSString(format: "%@ %d", path, Int(size))
    cache.countLimit = 5
    if let font = cache.object(forKey: key) { return font }
    
    guard let url = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, path as CFString, .cfurlposixPathStyle, false) else { return nil }
    guard let dataProvider = CGDataProvider(url: url) else { return nil }
    guard let cgfont = CGFont(dataProvider) else { return nil }
    
    let result = CTFontCreateWithGraphicsFont(cgfont, size, nil, nil);
    
    if( proportionalMetrics ){
      let descriptor = CTFontCopyFontDescriptor(result);
      
      let proportionalFontDescriptor = CTFontDescriptorCreateCopyWithFeature(descriptor, kTextSpacingType as CFNumber, kAltProportionalTextSelector as CFNumber);
      let ctfontToEmbed = CTFontCreateWithFontDescriptor(proportionalFontDescriptor, size, nil);
      cache.setObject(ctfontToEmbed, forKey: key)
      return ctfontToEmbed
      
    }else {
      cache.setObject(result, forKey: key)
      return result
    }
  }
}
