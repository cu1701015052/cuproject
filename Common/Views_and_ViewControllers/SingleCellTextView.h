//
//  NonActivatingTextView.h
//  Speak
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

#if TARGET_OS_IPHONE

#else

#import <Cocoa/Cocoa.h>

#define UIEdgeInsets NSEdgeInsets
#define UIView NSView
#define UIColor NSColor
#define UITextLayoutDirectionRight 2
#define UITextLayoutDirectionLeft 3
#define UITextLayoutDirectionUp 4
#define UITextLayoutDirectionDown 5

#define UITextLayoutDirection NSInteger
#endif

#import "MNFramesetter.h"
#import "IndexedPosition.h"
#import "SelectionKnobLayer.h"


@class SingleTextViewDrawDelegate;

@interface SingleCellTextViewBase: UIView
{
//  MNTextScrollView* scrollView_;
  NSRange selectedTextNSRange_;
  NSRange previousSelectedTextNSRange_;
  NSMutableArray *selectionLayers_;

  MNFramesetter* framesetter_;
  CGSize framesetterSuggestedSize_;

  NSMutableAttributedString* attributedString_;
  NSRange temporaryHighlightRange_;
  NSDictionary * temporaryHighlightAttributes_;
  
  BOOL needsUpdateLayout_;
  SingleTextViewDrawDelegate* drawDelegate_;
  UIColor *selectedTextFillColor_;

#if TARGET_OS_IPHONE
  UITextInputStringTokenizer* tokenizer_;
#endif
  
}
@property (nonatomic, readonly) NSMutableAttributedString* _Nonnull textStorage;
@property(nonatomic) UIEdgeInsets edgeInsets;
@property (nonatomic, readwrite, getter=isVertical) BOOL vertical;
@property(readwrite, readwrite) NSRange selectedRange;

#if TARGET_OS_IPHONE
#else
@property (copy) NSColor * _Nullable backgroundColor;
@property (copy) NSColor * _Nullable tintColor;

#endif

-(void)setTextStorage:(NSMutableAttributedString*_Nonnull)attr;
-(void)setFramesetter:(MNFramesetter*_Nullable)framesetter;
- (void)resetLayout;
-(void)unobscure;
- (IndexedPosition * _Nonnull)closestPositionToPoint:(CGPoint)point;
- (CGRect)enclosingRectForNSRange:(NSRange)range;
-(NSRange)wordRangeAt:(CGPoint)point;

@end

@class SingleCellTextViewSublayer;
@interface SingleCellTextView : SingleCellTextViewBase
{
@private
  NSString* touchHighlightKey;
  NSObject* touchHighlightValue;
  
}
- (instancetype _Nonnull)initWithFrame:(CGRect)frame;          // default initializer;
-(BOOL)showRuledLines ;
-(BOOL)canBecomeFirstResponder;
- (void)selectionDidChange;
//+(CGFloat)contentSize:(NSAttributedString * _Nonnull)attributedString forWidth:(CGFloat)width;
+(MNFramesetter* _Nonnull)contentSize:(NSMutableAttributedString* _Nullable)attributedString forWidth:(CGFloat)width edgeInset: (UIEdgeInsets)edgeInset contentHeight:(CGFloat* _Nullable)contentHeight;
+(UIEdgeInsets)defaultEdgeInsets;
-(void)setTemporaryHighlightRange:(NSRange)range attributes:(NSDictionary*_Nullable)attributes;
-(NSRange)temporaryHighlightRange;

-(void)sizeToFit;
-(void)setTouchHighlightKey:(NSString * _Nullable)key andValue:(NSObject * _Nullable)value;
- (void)drawSublayer:(SingleCellTextViewSublayer *_Nonnull)layer inContext:(CGContextRef _Nonnull )ctx;
#if TARGET_OS_IPHONE
+ (NSMutableArray* _Nonnull)drawPDF:(NSAttributedString* _Nonnull)attributedString vertical:(BOOL)vertical atPath:(NSString* _Nonnull)path inSize:(CGSize)size pdfInfo:(NSDictionary * _Nullable)pdfDict documentScaling:(CGFloat)documentScaling;
#endif
+(void)drawInContext:(MNFramesetter* _Nonnull)framesetter vertical:(BOOL)vertical fromLineIndex:(NSUInteger)lineNumber inRect:(CGRect)contentRect documentAttributes:(NSDictionary* _Nullable)documentAttributes  documentScaling:(CGFloat)documentScaling duplexPageNumber:(NSUInteger)duplexPageNumber containerSize: (CGSize)containerSize customLayout: (BOOL)customLayout layoutContentOrigin: (CGPoint)customLayoutContentOrigin;
+(NSArray<NSString*>* _Nullable)unmaskedUuids;
+(void)clearUnmaskedUuids;

@property (nonatomic, weak) id <NSObject> _Nullable textViewDelegate;

@end



@interface SingleTextViewDrawDelegate : NSObject <CALayerDelegate>
{
  SingleCellTextView __weak * textView_;
}

@property (nonatomic, weak) SingleCellTextViewBase* _Nullable textView;
@end




@interface SingleCellTextViewLayer : CALayer
{
  SingleCellTextViewBase* textView_;
}
@property (nonatomic, retain) SingleCellTextViewBase* _Nullable textView;
@end

@interface SingleCellTextViewSublayer : CALayer
{
  BOOL defer_;
  CGFloat myContentScale;
  CGFloat defaultContentScale;
  
}
@property (nonatomic) BOOL defer;

@end




