//
//  NonActivatingTextView.m
//  Speak
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

#import "SingleCellTextView.h"
#import <CoreText/CoreText.h>
#import "MNCGGeometryForMac.h"
#import "MNCoreTextDefinitions.h"

#ifndef RANGECONTAINSLOCATION
#define RANGECONTAINSLOCATION(r1, r2) \
((r1.location <= r2) && r2<(r1.location+r1.length) )
#endif

#ifndef MAXRANGE
#define MAXRANGE(r) \
r.location+r.length
#endif

#define CGPOINTINTEGRATE(n) CGPointMake( roundf(n.x), roundf(n.y))


#if TARGET_OS_IPHONE
#define CURRENT_CONTEXT UIGraphicsGetCurrentContext()
#else

// OSX
#define CURRENT_CONTEXT (CGContextRef)[[NSGraphicsContext currentContext] graphicsPort]
#endif



#if TARGET_OS_IPHONE
#import "UIImage+KeyboardButton.h"

#else


#define UIFont NSFont
#define UIImage NSImage
#define UIAccessibilityTraits uint64_t
#define UIAccessibilityTraitStaticText 0
#endif

#define TEMPORARY_UNDERLINE_STYLE_ATTRIBUTE @"MNTempUStyle"
#define MASK_STYLE_ATTRIBUTE @"MNMaskStyle"


#define TEMPORARY_UNDERLINE_COLOR_ATTRIBUTE @"MNTempUColor"
#define TEMPORARY_BACKGROUND_COLOR_ATTRIBUTE @"MNTempBColor"
#define TEMPORARY_BOOKMARK_COUNT_ATTRIBUTE @"MNTempBookmarkCount"
#define KAISEI_ATTRIBUTE @"Kaisei"

#define DIFF_ATTRIBUTE @"Diff"

#define OABackgroundColorAttributeName @"OABackgroundColorAttributeName"



#ifndef RGBA

#if TARGET_OS_IPHONE

#define RGBA(r,g,b,a) [UIColor colorWithRed: r/255.0f green: g/255.0f \
blue: b/255.0f alpha: a]

#else

#define RGBA(r,g,b,a) [NSColor colorWithCalibratedRed: r/255.0f green: g/255.0f \
blue: b/255.0f alpha: a]

#endif
#endif

@implementation SingleCellTextViewLayer
@synthesize textView = textView_;

- (void)setNeedsDisplayInRect:(CGRect)theRect
{
  //  IndexedRange *markedTextRange = (IndexedRange*)[self.textView markedTextRange];
  //
  //  if( markedTextRange && ![markedTextRange isEmpty] )
  //    return;
  
  NSArray* sublayers = [self sublayers];
  
  for( CALayer *layer in sublayers )
  {
    @autoreleasepool {
      
      if( [layer isKindOfClass: [SingleCellTextViewSublayer class]] && CGRectIntersectsRect( layer.frame, theRect) )
      {
        [layer setNeedsDisplayInRect: CGRectIntersection( layer.frame, theRect)];
      }
    }
  }
  
  //[super setNeedsDisplayInRect:(CGRect)theRect];
}

- (void)setNeedsDisplay
{
  NSArray* sublayers = [self sublayers];
  
  for( CALayer *layer in sublayers )
  {
    @autoreleasepool {
      
      if( [layer isKindOfClass: [SingleCellTextViewSublayer class]] )
      {
        [layer setNeedsDisplay];
      }
    }
  }
  
  /// ＊これが黒マークテキストの原因
  ///[[self sublayers] makeObjectsPerformSelector:@selector(setNeedsDisplay)];
  /// このかわりに、上のように変更した
  
  
  //[super setNeedsDisplay];
  
}

@end


@implementation SingleCellTextViewSublayer
@synthesize defer = defer_;

- (instancetype)init
{
  self = [super init];
  if (self) {
    defer_ = YES;
#if TARGET_OS_IPHONE
    CGFloat resolution = [[UIScreen mainScreen] scale];
    defaultContentScale = MIN(resolution, [[UIScreen mainScreen] scale]);
#else
    defaultContentScale = NSScreen.mainScreen.backingScaleFactor;
#endif
    myContentScale = 0.05;
    
    self.actions =  [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSNull null], @"onOrderIn",
                          [NSNull null], @"onOrderOut",
                          [NSNull null], @"sublayers",
                          [NSNull null], @"contents",
                          [NSNull null], @"bounds",
                          nil];
  }
  return self;
}
-(void)setDefer:(BOOL)defer
{
  //  NSLog(@"defer %d",defer);
  defer_ = defer;
  if( defer_ == NO ) {
    myContentScale = defaultContentScale;
    [self setContentsScale:myContentScale];
  }else {
    myContentScale = 0.05;
    [self setContentsScale:myContentScale];
    
  }
}
-(CGFloat)contentsScale { return myContentScale; }

@end


@implementation SingleCellTextViewBase
@synthesize textStorage = attributedString_;

+(Class)layerClass
{
  return [SingleCellTextViewLayer class];
}

-(void)setNeedsDisplay
{
#if TARGET_OS_IPHONE
  [self.layer setNeedsDisplay];
#else
  [self.layer.sublayers makeObjectsPerformSelector:@selector(setNeedsDisplay)];
#endif
}
  
-(BOOL)isAccessibilityElement {
  return NO;
}

- (BOOL)hasText
{
  if (self.textStorage.length > 0) {
    return YES;
  }
  return NO;
}

-(UITextPosition *)beginningOfDocument
{
  if( ![self hasText] ) return nil;
  return [IndexedPosition positionWithIndex:0 inAttributedString:attributedString_];
}

-(UITextPosition *)endOfDocument
{
  if( ![self hasText] ) return nil;
  return [IndexedPosition positionWithIndex:attributedString_.length-1 inAttributedString:attributedString_];
}

- (NSInteger)offsetFromPosition:(UITextPosition *)fromPosition toPosition:(UITextPosition *)toPosition
{
  IndexedPosition *pos = (IndexedPosition*)fromPosition;
  IndexedPosition *otherPos = (IndexedPosition*)toPosition;
  
  return otherPos.textIndex - pos.textIndex;
}

- (UITextPosition *)positionFromPosition:(UITextPosition *)position offset:(NSInteger)offset;
{
  // offset is character offset including invisible character
  
  IndexedPosition *pos = (IndexedPosition*)position;
  NSInteger end = (NSInteger)pos.textIndex + offset;
  if( end < 0 ) return nil;
  if( end >  attributedString_.length ) return nil;
  
  IndexedPosition *newPos = [IndexedPosition positionWithIndex:end inAttributedString:attributedString_];
  return newPos;
}

- (NSString *)textInRange:(UITextRange *)range
{
  if( [range isKindOfClass: [IndexedRange class]] )
  {
    IndexedRange* r = (IndexedRange*)range;
    NSRange range = r.range;
    
    if( NSMaxRange(range) > attributedString_.length )
    {
      NSLog(@"** UIKeyboardImpl(ShortcutConvertionSupport) sent illegal range **");
      return @"";
    }
    //    画像の前に文字を入れるとクラッシュ→UIKeyboardImpl(ShortcutConvertionSupport) _shortcutConvertionCandidateForInput:
    //    のバグ iPod touch 4th gen で起こる事がわかっている
    
    NSString* string = [[attributedString_ string] substringWithRange: range];
    return string;
  }
  
  return @"";
}

- (UITextPosition *)positionFromPosition:(UITextPosition *)position inDirection:(UITextLayoutDirection)direction offset:(NSInteger)offset
{
  if( self.isVertical )
  {
    if( direction == UITextLayoutDirectionRight ) direction = UITextLayoutDirectionUp;
    else if( direction == UITextLayoutDirectionLeft ) direction = UITextLayoutDirectionDown;
    else if( direction == UITextLayoutDirectionUp ) direction = UITextLayoutDirectionLeft;
    else if( direction == UITextLayoutDirectionDown ) direction = UITextLayoutDirectionRight;
  }
  
  CTRunStatus status = kCTRunStatusNoStatus;
  
  NSUInteger indexToCheck = [(IndexedPosition*)position textIndex];
  
  MNLine * line = [framesetter_ lineAtIndex: indexToCheck];
  
  if( line )
  {
    NSArray *runs = (NSArray*) CTLineGetGlyphRuns(line.CTLine);
    for( NSInteger runIndex = 0; runIndex < [runs count]; runIndex++ )
    {
      CTRunRef run = (__bridge CTRunRef) [runs objectAtIndex: runIndex];
      if( RANGECONTAINSLOCATION( CTRunGetStringRange(run) , indexToCheck -line.baseLocation ) )
      {
        status = CTRunGetStatus(run);
        
        break;
      }
    }
  }
  
  //  Move cursor by typing arrow keys
  
  if( status == kCTRunStatusRightToLeft )
  {
    if( direction == UITextLayoutDirectionRight )
      return [self positionFromPosition: position offset: -offset];
    
    if( direction == UITextLayoutDirectionLeft )
      return [self positionFromPosition: position offset: +offset];
    
  }else {
    if( direction == UITextLayoutDirectionRight )
      return [self positionFromPosition: position offset:offset];
    
    if( direction == UITextLayoutDirectionLeft )
      return [self positionFromPosition: position offset: -offset];
  }
  
  if( direction == UITextLayoutDirectionUp || direction == UITextLayoutDirectionDown )
  {
    // get the line containing the point
    NSInteger lineNumber = NSNotFound;
    CGFloat lineOffset;
    
    if( !line ) return nil;
    
    lineNumber = line.lineNumber;
    lineOffset = [line offsetForStringIndex: indexToCheck ] + line.origin.x;
    
    [framesetter_ evaluateLines];
    
    // Calc position using closestPositionToPoint: withinRange:
    IndexedPosition* position = nil;
    
    if( lineNumber != NSNotFound )
    {
      MNLine *destLine = line;
      
      if( direction == UITextLayoutDirectionDown )
      {
        while( offset > 0 )
        {
          destLine = [destLine nextLine];
          offset--;
        }
      }
      else
      {
        while( offset > 0 )
        {
          destLine = [destLine previousLine];
          offset--;
        }
      }
      
      if( destLine == nil ) return nil;
      
      IndexedRange *range = [IndexedRange rangeWithNSRange: NSMakeRange(destLine.range.location, destLine.range.length) inAttributedString:attributedString_];
      CGPoint destPoint = CGPointMake(lineOffset, destLine.origin.y);
      
      position = (IndexedPosition*)[self closestPositionToPoint:[self convertPoint:destPoint fromMNFrame:destLine.parentFrame] withinRange: range];
    }
    
    return position;
  }
  
  return nil;
}

- (IndexedPosition *)closestPositionToPoint:(CGPoint)point withinRange:(UITextRange *)range
{
  CFIndex test;
  CGRect lineRect;
  CGFloat offset;
  NSRange nsrange = [(IndexedRange*)range range];
  nsrange.length += 1;
  
  IndexedPosition* pos = [self cursorIndexForPoint: point inRange:nsrange touchedIndex:&test lineRect:&lineRect offset:&offset ];
  
  return pos ;
}


- (UITextRange *)textRangeFromPosition:(UITextPosition *)fromPosition toPosition:(UITextPosition *)toPosition
{
  IndexedPosition *from = (IndexedPosition*)fromPosition;
  IndexedPosition *to = (IndexedPosition*)toPosition;
  NSRange range = NSMakeRange( MIN(from.textIndex, to.textIndex), ABS(to.textIndex - from.textIndex));
  return [IndexedRange rangeWithNSRange: range inAttributedString:attributedString_];
}

-(id)tokenizer
{
#if TARGET_OS_IPHONE
  if( tokenizer_ == nil )
  {
    tokenizer_ = [[UITextInputStringTokenizer alloc] initWithTextInput:(UIResponder <UITextInput> *)self];
  }
  return tokenizer_;
#else
  return nil;
#endif
  
}

-(NSRange)wordRangeAt:(CGPoint)point
{
#if TARGET_OS_IPHONE
  CFIndex test;
  CGRect lineRect;
  CGFloat offset;
  IndexedPosition* pos = [self cursorIndexForPoint: point inRange:NSMakeRange(NSNotFound, 0) touchedIndex:&test lineRect:&lineRect offset:&offset ];
  //NSLog(@"pos %d", pos.textIndex);
  IndexedRange *endRange;
  UITextGranularity granularity = UITextGranularityWord;
  
  
  if ( NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_9_x_Max) {
    granularity = UITextGranularityLine;
  }
  
  endRange = (IndexedRange*)[[self tokenizer] rangeEnclosingPosition: pos
                                                     withGranularity: granularity
                                                         inDirection: UITextStorageDirectionBackward];
  
  if( endRange == nil )
  {
    pos.textIndex -= 1;
    endRange = (IndexedRange*)[[self tokenizer] rangeEnclosingPosition: pos
                                                       withGranularity: granularity
                                                           inDirection: UITextStorageDirectionForward];
  }
  
  //NSLog(@"%f, %f  endRange.range %d", point.x, point.y, endRange.range.length);
  
  return endRange.range;
#else

  return NSMakeRange(NSNotFound,0);

  
#endif
  
}
- (CGRect)enclosingRectForNSRange:(NSRange)range
{
  NSUInteger index = range.location;
  
  CGRect enclosingRect = CGRectZero;
  
  for( MNFrame* frame in framesetter_.frames )
    for ( MNLine* line in frame.lines )
    {
      CFRange lineRange = line.range;
      
      long localIndex = index - lineRange.location;
      if (localIndex >= 0 && localIndex < lineRange.length)
      {
        unsigned long finalIndex = MIN(lineRange.location + lineRange.length,
                                       range.location + range.length);
        CGFloat xStart = [line offsetForStringIndex:index];
        CGFloat xEnd = [line offsetForStringIndex:finalIndex];
        CGPoint origin = line.origin;
        CGFloat ascent = line.ascent, descent = line.descent, leading = line.leading;
        
        CGRect rr = CGRectMake(xStart + origin.x, origin.y - descent - line.lineSpacing + line.paragraphSpacingBefore, xEnd - xStart, leading+ ascent +descent );
        
        if( CGRectEqualToRect(enclosingRect, CGRectZero) )
          enclosingRect = CGRectIntegral(  [self convertRect:rr fromMNFrame: frame] );
        
        else
          enclosingRect = CGRectUnion(enclosingRect, CGRectIntegral(  [self convertRect:rr fromMNFrame: frame] ));
      }
    }
  
  return enclosingRect;
}

-(void)setTextStorage:(NSMutableAttributedString*)attr
{
  if( attributedString_ == attr ) return;
  attributedString_ = attr;
  
  [self checkEndCode];
  
  framesetter_ = nil;
  framesetter_ = [[MNFramesetter alloc] initWithAttributedString:attributedString_];
  framesetter_.containerSize = CGSizeMake( self.frame.size.width - self.edgeInsets.left-self.edgeInsets.right, MAXFLOAT);
  framesetter_.containerOrigin = CGPointMake(self.edgeInsets.left, self.edgeInsets.top);
  
  [self resetLayout];
#if TARGET_OS_IPHONE
  [self setNeedsLayout];
#else
  [self setNeedsLayout:YES];
#endif
}

-(void)setFramesetter:(MNFramesetter*)framesetter
{
  if( framesetter_ == framesetter ) return;
  framesetter_ = framesetter;
  attributedString_ = (NSMutableAttributedString*)framesetter_.textStorage;

#if TARGET_OS_IPHONE
  [self setNeedsLayout];
#else
  [self setNeedsLayout:YES];
#endif
}

-(void)setNeedsUpdateLayout
{
  needsUpdateLayout_ = YES;
}

-(void)setFrame:(CGRect)frame
{
  if( CGRectIsNull(frame) || CGRectIsInfinite(frame) || CGRectIsEmpty(frame)) return;
  if( self.superview )
  {
    CGRect enclosingRect = self.superview.bounds;
    frame.size.height = MAX(frame.size.height, enclosingRect.size.height);
  }
  [super setFrame:frame];
  [self setNeedsDisplay];
}

- (IndexedPosition *)closestPositionToPoint:(CGPoint)point
{
  CFIndex test;
  CGRect lineRect;
  CGFloat offset;
  
  IndexedPosition* pos = [self cursorIndexForPoint: point inRange:NSMakeRange(NSNotFound, 0) touchedIndex:&test lineRect:&lineRect offset:&offset];
  
  return  pos;
}

- (NSComparisonResult)comparePosition:(UITextPosition *)position toPosition:(UITextPosition *)other
{
  IndexedPosition *pos = (IndexedPosition*)position;
  IndexedPosition *otherPos = (IndexedPosition*)other;
  
  if( pos.textIndex < otherPos.textIndex ) return NSOrderedAscending;
  if( pos.textIndex > otherPos.textIndex ) return NSOrderedDescending;
  
  return NSOrderedSame;
}

-(IndexedPosition*)cursorIndexForPoint:(CGPoint)touchPoint inRange:(NSRange)searchRange touchedIndex:(CFIndex*)indexPtr lineRect:(CGRect*)rectPtr offset:(CGFloat*)offsetPtr
{
  
  CFIndex returnIndex = 0;
  UITextStorageDirection affinity = UITextStorageDirectionForward;
  
  for( MNFrame *aFrame in framesetter_.frames )
  {
    CGPoint touchedPointInFrame = [self convertPoint:touchPoint toMNFrame: aFrame];
    
    CGFloat closesedLineDistance = 100.0f;
    BOOL pointIsInsideAtLeastOneRect = NO;
    
    for( MNLine* line in aFrame.lines )
    {
      // If serch range is set and the line range does not intersects with the search range, skip this line
      
      if( searchRange.length > 0 )
      {
        CFRange lineRange = [line stringRange];
        
        if( lineRange.location >= NSMaxRange(searchRange) || lineRange.location + lineRange.length <= searchRange.location ) continue;
      }
      
      // Extend calc area into full width
      
      CGRect typoRect = line.typographicFrame;
      CTRunRef run = [framesetter_ runAtIndex: returnIndex];
      CGRect fullWidthRect = CGRectMake(0, typoRect.origin.y, MAXFLOAT, typoRect.size.height);
      
      BOOL pointInsideRect = CGRectContainsPoint( fullWidthRect, touchedPointInFrame );
      BOOL shouldCalc = NO;
      
      //MNLOG(@"Line %d contains point %d",line.lineNumber, pointInsideRect);
      
      if( !pointInsideRect && pointIsInsideAtLeastOneRect == NO)
      {
        CGFloat distanceBetweenRectAndPoint =
        MIN( fabs(touchedPointInFrame.y - typoRect.origin.y), fabs(touchedPointInFrame.y - CGRectGetMaxY(typoRect)));
        
        if( distanceBetweenRectAndPoint < closesedLineDistance )
        {
          closesedLineDistance = distanceBetweenRectAndPoint;
          shouldCalc= YES;
        }
        
      }else
      {
        pointIsInsideAtLeastOneRect = YES;
      }
      
      if( pointInsideRect || shouldCalc )
      {
        CGPoint pointInLine = CGPointMake( touchedPointInFrame.x - line.origin.x , touchedPointInFrame.y - line.origin.y);
        returnIndex = [line CTLineGetStringIndexForPosition:  pointInLine];
        
        BOOL isReturn = NO;
        NSString* checkCharacter = @"";
        if( attributedString_.length > 0 && returnIndex > 0 )
        {
          checkCharacter = [attributedString_.string substringWithRange:NSMakeRange(returnIndex-1, 1)];
          isReturn = [checkCharacter isEqualToString:@"\n"];
          
          if( isReturn )
          {
            if( CTRunGetStatus(run) == kCTRunStatusRightToLeft )
            {
              if( touchedPointInFrame.x < typoRect.origin.x  /* 20 is margin*/) returnIndex--;
              
            }else
            {
              if(  touchedPointInFrame.x > CGRectGetMaxX(typoRect) + 20 /* 20 is margin*/)
                returnIndex--;
            }
          }
        }
        
        if( indexPtr )
        {
          //get ctrun
          
          if( CTRunGetStatus(run) == kCTRunStatusRightToLeft )
          {
            // Right to left
            CFIndex idx2;
            CFIndex offset = [line offsetForStringIndex:returnIndex];
            
            // カーソル位置より左側をタップした場合、タッチされたインデックスは一つあとのもの
            if( offset + line.origin.x > touchedPointInFrame.x )
            {
              idx2 = returnIndex;
            }
            else
            {
              idx2 = returnIndex-1;
            }
            
            *indexPtr = idx2;
            
            if( rectPtr ) *rectPtr = line.typographicFrame;
            if( offsetPtr ) *offsetPtr = offset;
          }
          else
          {
            // Left to right
            
            CFIndex idx2;
            CFIndex offset = [line offsetForStringIndex:returnIndex];
            
            // カーソル位置より左側をタップした場合、タッチされたインデックスは一つ前のもの
            if( offset + line.origin.x > touchedPointInFrame.x )
            {
              idx2 = returnIndex-1;
            }
            else
            {
              idx2 = returnIndex;
            }
            
            *indexPtr = idx2;
            
            if( rectPtr ) *rectPtr = line.typographicFrame;
            if( offsetPtr ) *offsetPtr = offset;
          }
        }
        
        CFRange lineRange = [line stringRange];
        
        // if at the end of the line and the end character is not return, and
        
        if( returnIndex == MAXRANGE( lineRange ) && !isReturn  )
          affinity = UITextStorageDirectionBackward;
        else
          affinity = UITextStorageDirectionForward;
        
        if( pointInsideRect ) break;
      }
    }
  }
  
  // restrict returnIndex in searchRange
  if( searchRange.length > 0 )
  {
    if( returnIndex < searchRange.location ) returnIndex = searchRange.location;
    if( returnIndex >= MAXRANGE(searchRange) ) returnIndex = MAXRANGE(searchRange)-1;
  }
  
  IndexedPosition* pos = [IndexedPosition positionWithIndex:returnIndex inAttributedString:attributedString_ suggestedAffinity:affinity];
  
  return pos;
}

-(CGPoint)convertPoint:(CGPoint)point toMNFrame:(MNFrame*)frame
{
  // Get Box
  CGRect pathBoundingBox = frame.pathBoundingBox;
  
  // Get Inset from top left corner
  CGFloat marginX = pathBoundingBox.origin.x;
  CGFloat marginY = pathBoundingBox.origin.y;
  
  CGAffineTransform t = CGAffineTransformMakeScale(1, 1);
  t = CGAffineTransformTranslate(t, marginX, marginY);
  
  return CGPointApplyAffineTransform(point, CGAffineTransformInvert(t) );
}
-(CGRect)convertRect:(CGRect)rect fromMNFrame:(MNFrame*)frame
{
  CGPoint origin = CGPointMake(rect.origin.x, rect.origin.y );
  CGPoint point = [self convertPoint:origin fromMNFrame:frame];
  return CGRectMake(point.x, point.y, rect.size.width, rect.size.height);
}

-(CGPoint)convertPoint:(CGPoint)point fromMNFrame:(MNFrame*)frame
{
  // Get Box
  CGRect pathBoundingBox = frame.pathBoundingBox;
  
  // Get Inset from top left corner
  CGFloat marginX = pathBoundingBox.origin.x;
  CGFloat marginY = pathBoundingBox.origin.y;
  
  CGAffineTransform t = CGAffineTransformMakeScale(1, 1);
  t = CGAffineTransformTranslate(t, marginX, marginY);
  
  return CGPointApplyAffineTransform(point, t);
}

- (UITextRange *)characterRangeAtPoint:(CGPoint)point
{
  CFIndex test;
  CGRect lineRect;
  CGFloat offset;
  IndexedPosition* pos = [self cursorIndexForPoint: point inRange:NSMakeRange(NSNotFound, 0) touchedIndex:&test lineRect:&lineRect offset:&offset ];
  
  NSRange nsrange = NSMakeRange(pos.textIndex, 1);
  return [IndexedRange rangeWithNSRange:nsrange inAttributedString:attributedString_] ;
}


-(void)invalidateRange:(NSRange)range
{
  CGRect invalidateRect = self.bounds;
  
  MNLine * line = [framesetter_ lineAtIndex: range.location];
  
  if( line )
  {
    CGRect typeFrameInView = [self convertRect: line.typographicFrame fromMNFrame: line.parentFrame];
    
    invalidateRect.origin.y = typeFrameInView.origin.y;
    invalidateRect.size.height = self.bounds.size.height - typeFrameInView.origin.y;
    
    if( range.length > 0 )
    {
      line = [framesetter_ lineAtIndex: MAXRANGE(range)-1];
      if( line )
      {
        CGRect endTypeFrameInView = [self convertRect: line.typographicFrame fromMNFrame: line.parentFrame];
        
        invalidateRect.size.height = CGRectGetMaxY(endTypeFrameInView) - typeFrameInView.origin.y;
        
      }
    }
  }
  
  [self.layer setNeedsDisplayInRect:invalidateRect];
}

-(void)checkEndCode
{
  //check if text ends with \n#
  
  if( ![attributedString_.string hasSuffix:@"\n "] )
  {
    if( ![attributedString_.string hasSuffix:@" "] )
    {
      NSAttributedString *attr = [[NSAttributedString alloc] initWithString:@"\n "];
      [attributedString_ appendAttributedString: attr];
    }
    else {
      NSAttributedString *attr = [[NSAttributedString alloc] initWithString:@"\n"];
      [attributedString_ insertAttributedString:attr atIndex:attributedString_.length-1];
    }
  }
  
}

-(void)unobscure
{
  NSArray *sublayer = self.layer.sublayers;
  
  CGRect visibleRect = self.bounds;
  
  for( SingleCellTextViewSublayer *layer in sublayer )
  {
    @autoreleasepool {
      
      if( [layer isKindOfClass:[SingleCellTextViewSublayer class]] )
      {
        if( layer.defer && CGRectIntersectsRect( layer.frame, visibleRect)  )
        {
          layer.defer = NO;
          layer.hidden = NO;
          [layer setNeedsDisplay];
          [layer removeAllAnimations];
        }
      }
    }
  }
}


-(void)updateSublayers
{
  
#define SUBLAYER_HEIGHT 100
  NSInteger yy = 0;
  NSArray* sublayers = [self.layer sublayers];
  CGSize sublayerSize = CGSizeMake( self.bounds.size.width, SUBLAYER_HEIGHT);
  
  
  // Remove unused layer
  
  NSMutableArray *layersToDelete = [NSMutableArray array];
  for( CALayer *layer in sublayers )
  {
    @autoreleasepool {
      
      if( [layer isKindOfClass: [SingleCellTextViewSublayer class]] )
      {
        layer.backgroundColor = self.backgroundColor.CGColor;
        
        if( layer.frame.size.width == sublayerSize.width )
          yy++;
        
        
        if( layer.frame.origin.y > self.bounds.size.height || layer.frame.size.width != sublayerSize.width)
        {
          layer.delegate = nil;
          [layersToDelete addObject: layer];
        }
      }
    }
  }
  
  [layersToDelete makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
  
  
  // Add new layer
  
  //double time = [NSDate timeIntervalSinceReferenceDate];
  while( yy*sublayerSize.height < self.bounds.size.height )
  {
    @autoreleasepool {
      
      SingleCellTextViewSublayer *sublayer = [[SingleCellTextViewSublayer alloc] init];
      sublayer.defer = YES;
      sublayer.delegate = drawDelegate_;
      sublayer.frame = CGRectMake(0, yy*sublayerSize.height, sublayerSize.width, sublayerSize.height);
      sublayer.bounds = CGRectMake(0, yy*sublayerSize.height, sublayerSize.width, sublayerSize.height);
//      sublayer.contentsScale = self.layer.contentsScale;
//      sublayer.actions =  [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSNull null], @"onOrderIn",
//                            [NSNull null], @"onOrderOut",
//                            [NSNull null], @"sublayers",
//                            [NSNull null], @"contents",
//                            [NSNull null], @"bounds",
//                            nil];
      
      [self.layer insertSublayer: sublayer atIndex:0];
      
      yy++;
      sublayer.backgroundColor = self.backgroundColor.CGColor;
      
    }
  }
  
  
  //  // DEBUG
  //  for( CALayer* layer in self.layer.sublayers )
  //  {
  //    MNLOG(@"%@ %@",NSStringFromClass([layer class]), NSStringFromCGRect(layer.frame));
  //  //MNLOG(@"self.layer sublayers %@",[self.layer.sublayers description]);
  //  }
  
}

- (void)resetLayout
{
  
  //[self framesetter]; // Instantiate
  
  //MNLOG(@"self.frame %@",NSStringFromCGRect(self.frame));
  
  framesetter_.documentScaling = 1.0;
  framesetter_.containerSize = CGSizeMake( self.frame.size.width - self.edgeInsets.left-self.edgeInsets.right, MAXFLOAT);
  [framesetter_ invalidateLayout];
  
  // Calc rect
  
  CGSize newSize = framesetter_.suggestedContainerSize;
  
  needsUpdateLayout_ = needsUpdateLayout_ || ( !CGSizeEqualToSize(framesetterSuggestedSize_, newSize) || self.frame.size.height != newSize.height +   self.edgeInsets.top + self.edgeInsets.bottom );
  
  framesetterSuggestedSize_ = newSize;
  
  if( needsUpdateLayout_ )
  {
    // reset frame setter
    //CGFloat width = framesetterSuggestedSize_.width  +  self.edgeInset.left + self.edgeInset.right;
    CGFloat height = framesetterSuggestedSize_.height +   self.edgeInsets.top + self.edgeInsets.bottom;
    
    CGRect newFrame =  CGRectMake(0, 0, self.frame.size.width,  height);
    
    self.frame = CGRectIntegral( newFrame );
    
    
    [self updateSublayers];
    
    // reset frame setter
    
    needsUpdateLayout_ = NO;
  }
  
  //[self updateCursor];
  
  [self.layer setNeedsDisplay];
}


-(void)dealloc
{
  [[NSNotificationCenter defaultCenter] removeObserver:self];
  [selectionLayers_ makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
  selectionLayers_ = nil;

  drawDelegate_ = nil;
  attributedString_ = nil;
  framesetter_ = nil;
  
  [(SingleCellTextViewLayer*)self.layer setTextView:nil];
}

-(void)removeFromSuperview
{
  id __strong strongSelf = self;
  
  NSArray *sublayers = self.layer.sublayers;
  
  NSMutableArray *array = [NSMutableArray array];
  for( CALayer* layer in sublayers)
  {
    if( [layer isKindOfClass: [SingleCellTextViewSublayer class]] )
    {
      layer.delegate = nil;
      [array addObject: layer];
    }
  }
  [array makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
  
  drawDelegate_ = nil;
  
  if( [self.layer isKindOfClass:[SingleCellTextViewLayer class]]) {
    [(SingleCellTextViewLayer*)self.layer setTextView:nil];
  }
  [super removeFromSuperview];
  strongSelf = nil;
  
}


@end



@implementation SingleTextViewDrawDelegate
@synthesize textView = textView_;

-(void)dealloc
{
  self.textView = nil;
}

- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx
{
  if( [textView_ textStorage] == nil ) { return; }
  
//  NSLog(@"--- drawLayer layer %x", layer);
  
  @autoreleasepool {
    
    [textView_ drawSublayer:(SingleCellTextViewSublayer*)layer inContext:ctx];

  }
}
@end

@interface IndexedPosition (SingleCellTextViewExt)
@end

@implementation IndexedPosition (SingleCellTextViewExt)
+(IndexedPosition*)positionWithIndex:(NSUInteger)index inAttributedString:(NSAttributedString*)attr suggestedAffinity:(UITextStorageDirection)affinity
{
  IndexedPosition *pos = [[IndexedPosition alloc] init];
  
  // Limit index in content range
  if( index > attr.length )
    index = attr.length;
  
  pos.textIndex = index;
  pos.affinity = affinity;
  
  // adjust affinity
  if( index == 0 ) pos.affinity = UITextStorageDirectionForward;
  //if( index == attr.length-2 ) pos.affinity = UITextStorageDirectionForward;
  
  return pos;
}

+(IndexedPosition*)positionWithIndex:(NSUInteger)index inAttributedString:(NSAttributedString*)attr
{
  IndexedPosition *pos = [[IndexedPosition alloc] init];
  
  // Limit index in content range
  if( index > attr.length  )
    index = attr.length;
  
  pos.textIndex = index;
  
  return pos;
}

@end

@implementation SingleCellTextView
static NSMutableArray <NSString*>* kLastUnmaskedUuids = nil;

+(void)clearUnmaskedUuids
{
  [kLastUnmaskedUuids removeAllObjects];
}

+(NSArray<NSString*>* _Nullable)unmaskedUuids
{
  return kLastUnmaskedUuids;
}
+(UIEdgeInsets)defaultEdgeInsets {
#if TARGET_OS_IPHONE
  
  UIEdgeInsets edgeInset = UIEdgeInsetsMake(0, 14, 0, 14);
#else
  NSEdgeInsets edgeInset = NSEdgeInsetsMake(0, 14, 0, 14);
  
#endif
  return edgeInset;
}

+(MNFramesetter* _Nonnull)contentSize:(NSMutableAttributedString*)attributedString forWidth:(CGFloat)width edgeInset: (UIEdgeInsets)edgeInset contentHeight:(CGFloat*)contentHeight
{

  MNFramesetter* framesetter = [[MNFramesetter alloc] initWithAttributedString:attributedString];
  framesetter.containerSize = CGSizeMake( width - edgeInset.left - edgeInset.right, MAXFLOAT);
  framesetter.containerOrigin = CGPointMake(edgeInset.left, edgeInset.top);
  [framesetter invalidateLayout];
  
  *contentHeight =  edgeInset.top + edgeInset.bottom + framesetter.suggestedContainerSize.height;
  return framesetter;
}

-(BOOL)isFlipped
{
  return YES;
}

-(BOOL)isAccessibilityElement {
  return YES;
}

-(NSString*)accessibilityLabel {
  return self.textStorage.string;
}

-(UIAccessibilityTraits)accessibilityTraits {
  return UIAccessibilityTraitStaticText;
}

-(NSString *)accessibilityValue { return @""; }

-(NSString*)accessibilityHint { return @""; }

- (id)initWithFrame:(CGRect)frame;          // default initializer
{
  self = [super initWithFrame:frame];
  if (self) {
    
#if TARGET_OS_IPHONE
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.edgeInsets = UIEdgeInsetsMake(0, 14, 0, 14);
    self.userInteractionEnabled = NO;
    
    [(SingleCellTextViewLayer*)self.layer setTextView:self];

#else
    self.autoresizingMask = NSViewWidthSizable | NSViewHeightSizable;
    self.edgeInsets = NSEdgeInsetsMake(0, 14, 0, 14);
    self.wantsLayer = YES;
    
    SingleCellTextViewLayer* layer = [SingleCellTextViewLayer layer];

    layer.frame = self.layer.bounds;
    [self.layer addSublayer:layer];
    [layer setTextView:self];
#endif
    

    selectedTextNSRange_ = NSMakeRange(0, 0);
    previousSelectedTextNSRange_ = NSMakeRange(0, 0);

    drawDelegate_ = [[SingleTextViewDrawDelegate alloc] init];
    drawDelegate_.textView = self;

    selectedTextFillColor_ = RGBA(61, 152, 254, 0.4);

    self.textStorage = [[NSMutableAttributedString alloc] init];

    [self unobscure];

    
//    scrollView_ = (id)[[DummyScrollView alloc] init];
//    ((DummyScrollView*)scrollView_).textView = self;
    //self.vertical = true
    //inputStyle_ = [NonActivatingTextView defaultInputStyle];
    //[MNTextView setMaxContentScale: 2.0];
    //plainText_ = NO;
    self.backgroundColor = [UIColor clearColor];
    self.tintColor = [UIColor whiteColor];
    //self.markedTextFillColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    //self.markedTextBorderColor = [UIColor clearColor];
    //self.layer.masksToBounds = YES;
    //self.clipsToBounds = YES;
    //self.layer.contentsScale = MIN(2.0, SCREEN_SCALE);

  }
  return self;
}

-(BOOL)isOpaque
{
  return NO;
}

-(void)setTextStorage:(NSMutableAttributedString*)attr
{
  touchHighlightKey = nil;
  touchHighlightValue = nil;
  
  //[attr appendAttributedString:suffix];
  [super setTextStorage: attr];
  
  [self resetLayout];
  [self unobscure];
  [self sizeToFit];

}


-(void)checkEndCode
{
  //NSMutableDictionary* attributes = [NSMutableDictionary dictionaryWithDictionary: self.inputStyle];
  //[attributes removeObjectForKey: MNCTParagraphStyleAttributeName];
  // [attributes removeObjectForKey: MNCTUnderlineColorAttributeName];
  // [attributes removeObjectForKey: MNCTUnderlineStyleAttributeName];
}

-(void)sizeToFit
{
  CGSize size = [self sizeThatFits: self.bounds.size];
  CGRect frame = [self frame];
  frame.size = size;
  self.frame = frame;
}

-(CGSize)sizeThatFits:(CGSize)size
{
  return CGSizeMake(self.edgeInsets.left + self.edgeInsets.right + framesetterSuggestedSize_.width, self.edgeInsets.top + self.edgeInsets.bottom + framesetterSuggestedSize_.height);
  /*
  for( MNFrame* aFrame in framesetter_.frames )
  {
    CGRect pathBoundingBox = aFrame.pathBoundingBox;
    return CGSizeMake(CGRectGetMaxX(pathBoundingBox), CGRectGetMaxY(pathBoundingBox));

  }
  
  return CGSizeZero;
   */
}

-(NSUndoManager*)undoManager
{
	return nil;
}

-(BOOL)showRuledLines {
	return NO;
}

-(BOOL)canBecomeFirstResponder
{
	return NO;
}

-(void)setSelectedRange:(NSRange)range {
  [self selectionWillChange];
  selectedTextNSRange_ = range;
  [self selectionDidChange];
}

-(NSRange)selectedRange
{
  return selectedTextNSRange_;
}

-(void)setTemporaryHighlightRange:(NSRange)range attributes:(NSDictionary*)attributes {
  temporaryHighlightRange_ = range;
  temporaryHighlightAttributes_ = attributes;
  [self setNeedsDisplay];
  [self unobscure];
}

-(NSRange)temporaryHighlightRange
{
  return temporaryHighlightRange_;
}


-(void)updateCursor
{
  [self updateCursorWithAnimation:NO];
}

-(void)updateCursorWithAnimation:(BOOL)animated
{
  // Frames in view coordinate
  CGRect startFrame = CGRectZero;
  CGRect startAffinityBackwardFrame = CGRectZero;
  CGRect endAffinityBackwardFrame = CGRectZero;
  
  NSMutableArray <NSString * > * selectionLineArray = [NSMutableArray array];
  
  BOOL exteningCursor = NO;
  
  
  for( MNFrame* aFrame in framesetter_.frames )
  {
    for( MNLine *line in aFrame.lines )
    {
      CGRect typoRect = line.typographicFrame;
      CFRange lineRange = line.range;
      
      // Check start
      
      if( RANGECONTAINSLOCATION( lineRange, selectedTextNSRange_.location ) )
      {
        CGFloat offset = [line offsetForStringIndex:selectedTextNSRange_.location];
        CGRect rawRect = CGRectMake( typoRect.origin.x + offset, typoRect.origin.y, 0, typoRect.size.height);
        
        startFrame = [self convertRect:rawRect fromMNFrame: aFrame];
      }
      // Check startAffinityBackwardFrame
      NSUInteger backwardIndex = selectedTextNSRange_.location-1;
      
      if( !exteningCursor )
      {
        if( selectedTextNSRange_.location > 0 && RANGECONTAINSLOCATION( lineRange, backwardIndex) )
        {
          CGFloat offset = [line offsetForStringIndex:backwardIndex+1];
          
          CGRect rawRect = CGRectMake( typoRect.origin.x + offset, typoRect.origin.y, 0, typoRect.size.height);
          startAffinityBackwardFrame = [self convertRect:rawRect fromMNFrame: aFrame];
        }
      }
      
      // Check end
      
      //      NSUInteger maxLocation = NSMaxRange(selectedTextNSRange_);
      //      if( selectedTextNSRange_.length > 0 && RANGECONTAINSLOCATION( lineRange, maxLocation ) )
      //      {
      //        CGFloat offset = [line offsetForStringIndex:NSMaxRange(selectedTextNSRange_)];
      //      }
      
      
      // Check affinity backward end
      
      backwardIndex = NSMaxRange(selectedTextNSRange_)-1;
      
      if( selectedTextNSRange_.length > 0 && RANGECONTAINSLOCATION( lineRange, backwardIndex) )
      {
        CGFloat offset = [line offsetForStringIndex:backwardIndex+1];
        
        CGRect rawRect = CGRectMake( typoRect.origin.x + offset, typoRect.origin.y, 0, typoRect.size.height);
        endAffinityBackwardFrame = [self convertRect:rawRect fromMNFrame: aFrame];
      }
    }
    
    // Selection length is not zero
    
    if( selectedTextNSRange_.length > 0 )
    {
      // Prepare layers
     
      if( selectionLayers_ == nil )
        selectionLayers_ = [[NSMutableArray alloc] init];
      
      
      // Calc selection
      

      
      // setup selection
      for( MNLine *line in aFrame.lines )
      {
        CGRect typoRect = line.typographicFrame;
        CFRange r0 = line.range;
        
        NSRange lineRange = NSMakeRange(r0.location, r0.length);
        
        NSRange intersectionRange = NSIntersectionRange(lineRange, selectedTextNSRange_);
        if( intersectionRange.length > 0 )
        {
          CGFloat offsetX1 = [line offsetForStringIndex:intersectionRange.location];
          
          CGFloat offsetX2 = [line offsetForStringIndex:intersectionRange.location + intersectionRange.length];
          
          CGRect markRect = CGRectMake(offsetX1 + typoRect.origin.x, typoRect.origin.y + line.paragraphSpacingBefore, offsetX2 - offsetX1, typoRect.size.height);
          
          CGRect lineSelection = [self convertRect:markRect fromMNFrame: aFrame];
          
          [selectionLineArray addObject: NSStringFromCGRect(lineSelection)];
        }
      }
    }
  }
  
  if( selectedTextNSRange_.length > 0 )
  {
    for( int hoge = 0; hoge < [selectionLineArray count]; hoge++ )
    {
      CGRect lineRect = CGRectFromString( [selectionLineArray  objectAtIndex:hoge] );
      
      // Add new layer
      if( [selectionLayers_ count] <= hoge )
      {
        CALayer *selectionLayer = [CALayer layer];
        selectionLayer.backgroundColor = selectedTextFillColor_.CGColor;
        selectionLayer.contentsScale = 0.1;
        [selectionLayers_ addObject: selectionLayer];
        [self.layer addSublayer:selectionLayer];
      }
      
      [(CALayer*)[selectionLayers_ objectAtIndex:hoge] setFrame: lineRect];
      [(CALayer*)[selectionLayers_ objectAtIndex:hoge] setHidden:NO];
    }
    
    // Delete unused layer
    if( [selectionLayers_ count] > [selectionLineArray count] && [selectionLineArray count]>0)
    {
      NSArray* layersToDelete = [selectionLayers_ subarrayWithRange:NSMakeRange([selectionLineArray count], [selectionLayers_ count]-[selectionLineArray count])];
      [layersToDelete makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
      [selectionLayers_ removeObjectsInArray: layersToDelete];
    }
    
    if( !animated )
    {
      [selectionLayers_ makeObjectsPerformSelector:@selector(removeAllAnimations)];
      [self.layer removeAllAnimations];
    }
    
    return;
  }
  
  //Standard cursor
  
  for( CALayer *layer in selectionLayers_ )
  {
    layer.hidden = YES;
    [layer removeAllAnimations];
  }
  
  selectionLayers_ = nil;
  
}

- (void)selectionWillChange
{
  previousSelectedTextNSRange_ = selectedTextNSRange_;
  
  [self willChangeValueForKey:@"selectedTextRange"];
}

- (void)selectionDidChange
{
  [self updateCursor];
  
  // limit selectedTextNSRange_
  
  if( selectedTextNSRange_.location >= attributedString_.length )
    selectedTextNSRange_.location = attributedString_.length-1;
  
  if( NSMaxRange(selectedTextNSRange_) > attributedString_.length )
  {
    selectedTextNSRange_.length -= attributedString_.length - NSMaxRange(selectedTextNSRange_);
  }
  
  if( selectedTextNSRange_.length > 4294900000 )
    selectedTextNSRange_.length = self.textStorage.length - selectedTextNSRange_.location -1 ;
  
  [self didChangeValueForKey:@"selectedTextRange"];

//  CGRect invalidateRect = self.bounds;
//
//  [self invalidateRange: previousSelectedTextNSRange_];
//  [self invalidateRange: selectedTextNSRange_];

}

-(void)unobscure
{
  NSArray *sublayer = self.layer.sublayers;
  
  CGRect visibleRect = self.bounds;
  
  for( SingleCellTextViewSublayer *layer in sublayer )
  {
    @autoreleasepool {
      if( [layer isKindOfClass:[SingleCellTextViewSublayer class]] == NO ) continue;
      if( layer.defer && CGRectIntersectsRect( layer.frame, visibleRect) )
      {
        layer.defer = NO;
        
        layer.hidden = NO;
        [layer setNeedsDisplay];
        [layer removeAllAnimations];
      }
    }
  }
}

-(void)setTouchHighlightKey:(NSString*)key andValue:(NSObject*)value {
  BOOL changed = touchHighlightKey != key;
  touchHighlightKey = key;
  touchHighlightValue = value;
  
  if( changed ) {
    [self setNeedsDisplay];
    [self unobscure];
  }
}

+(void)drawBlockquoteBackgroundIn:(CGRect)blockquoteRect highlighted:(BOOL)blockquoteHighlighted backgroundColor:(CGColorRef)blockquoteBackgroundColor in:(CGContextRef)ctx {
  
  if( CGRectIsNull(blockquoteRect) || CGRectIsInfinite(blockquoteRect) || CGRectIsEmpty(blockquoteRect)) return;
  
  CGContextSetBlendMode(ctx, kCGBlendModeOverlay);
  if( blockquoteBackgroundColor != nil ) {
    CGContextSetFillColorWithColor(ctx, blockquoteBackgroundColor);
    CGContextSetStrokeColorWithColor(ctx, blockquoteBackgroundColor);

  }else {
    CGContextSetFillColorWithColor(ctx, [UIColor colorWithWhite:0 alpha:0.1].CGColor);
    CGContextSetStrokeColorWithColor(ctx, [UIColor colorWithWhite:0 alpha:0.1].CGColor);
  }
    
  blockquoteRect.origin.y += 5;
  blockquoteRect.size.height -= 5;
  
  blockquoteRect.origin.x -= 5;
  blockquoteRect.size.width += 10;
  
  
  if(blockquoteRect.size.height > 0 && blockquoteRect.size.width > 0)
  {
    CGMutablePathRef bezierPath = CGPathCreateMutable();
    CGPathAddRoundedRect(bezierPath, nil, blockquoteRect, 3.0, 3.0);
    CGContextAddPath(ctx, bezierPath);
    CGContextFillPath(ctx);
    CFRelease(bezierPath);
    
    if( blockquoteHighlighted )
    {
      CGContextSetBlendMode(ctx, kCGBlendModeNormal);
      CGContextSetFillColorWithColor(ctx, [UIColor colorWithWhite:0 alpha:0.2].CGColor);
      CGMutablePathRef bezierPath = CGPathCreateMutable();
      CGPathAddRoundedRect(bezierPath, nil, blockquoteRect, 3.0, 3.0);
      CGContextAddPath(ctx, bezierPath);
      CGContextFillPath(ctx);
      CFRelease(bezierPath);
    }
  }
}

+ (NSMutableArray*)drawSublayerInContext:(CGContextRef)ctx
                             framesetter:(MNFramesetter*)framesetter_
                              isVertical:(BOOL)isVertical
                       touchHighlightKey:(NSString*)touchHighlightKey
                     touchHighlightValue:(NSObject*)touchHighlightValue
            temporaryHighlightAttributes:(NSDictionary*)temporaryHighlightAttributes
                 temporaryHighlightRange:(NSRange)temporaryHighlightRange
                                pageSize:(CGSize)pageSizeOrZeroSize
           continuePrintingFromFirstPage:(BOOL)continuePrintingFromFirstPage
                          fromLineNumber:(NSUInteger)lineNumber {
  //- (void)drawSublayerInContext:(CGContextRef)ctx{
  
  BOOL paging = !(pageSizeOrZeroSize.width == CGSizeZero.width && pageSizeOrZeroSize.height == CGSizeZero.height);
  CGSize pageSize = pageSizeOrZeroSize;
  NSMutableArray* layoutDictionaries = [NSMutableArray array];
  [layoutDictionaries addObject:[NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:lineNumber] forKey:@"startLineNumber"]];


#if TARGET_OS_IPHONE
  if( paging ) {
    UIGraphicsBeginPDFPage();
  }
#endif
  
//  fromLineIndex:(NSUInteger)lineNumber
  
  /// Draw background
  UIColor *ruledLineColor = RGBA(0,0,0,0.1);
  BOOL drawGuideLines = NO;
  BOOL drawRuledLines = NO;

//  if( drawRuledLines )
//  {
//    const CGFloat *comps = CGColorGetComponents( self.backgroundColor.CGColor );
//    if( !comps || (comps[0] < 0.2 && comps[1] < 0.2 && comps[2] < 0.2 ) )
//    {
//      ruledLineColor = RGBA(255,255,255,0.3);
//    }
//  }
  
  CGContextSaveGState(ctx);
  
  
  // Initialize the text matrix to a known value
  CGAffineTransform textMatrix = CGAffineTransformMakeScale(1, -1);
  if( isVertical ) {
    textMatrix = CGAffineTransformRotate(textMatrix, +M_PI_2);
  }
  CGContextSetTextMatrix(ctx, textMatrix);
  
  // Additional ruled lines
  static CGPoint lastLineRect0, lastLineRect1;
  static CGFloat lastLineHeight = 0;
  
  CGFloat pageOriginYInFrame = 0;
  CGFloat originYInPage = 0;

  MNFrame* aFrame = framesetter_.frames[0]; // Zero
  MNLine *line = nil;

  {
    CGRect pathBoundingBox = aFrame.pathBoundingBox;
#if TARGET_OS_IPHONE
#else
    pathBoundingBox.origin.y -= 2;
#endif
    // Draw suggested box
    
    if( drawGuideLines )
    {
      // Full box
      CGContextSetLineWidth(ctx, 1.0);
      CGContextSetStrokeColorWithColor(ctx, [UIColor grayColor].CGColor);
      CGContextStrokeRect(ctx, pathBoundingBox);
      
      
      CGFloat lengths[] = {3, 2};
      CGContextSetLineDash ( ctx, 0, lengths, 2 );
      CGContextSetStrokeColorWithColor(ctx, [UIColor lightGrayColor].CGColor);
      CGContextStrokeRect(ctx, CGRectMake(pathBoundingBox.origin.x, pathBoundingBox.origin.y + (pathBoundingBox.size.height - framesetter_.suggestedSize.height), framesetter_.suggestedSize.width, framesetter_.suggestedSize.height));
      
      CGContextSetLineDash ( ctx, 0, nil, 0 );
    }
    CGRect blockquoteRect = CGRectNull;
    CGColorRef blockquoteBackgroundColor = nil;
    
    BOOL blockquoteHighlighted = NO;
    
    if( [aFrame.lines count] > lineNumber )
    {
      line = [aFrame.lines objectAtIndex:lineNumber];
      
      CGRect typoRect = line.typographicFrame;
      pageOriginYInFrame = typoRect.origin.y;
    }
    
    

    for( NSUInteger hoge = lineNumber; hoge < [aFrame.lines count]; hoge++ )
    {
      @autoreleasepool {
        
        MNLine *line = [aFrame.lines objectAtIndex:hoge];
        CGContextSaveGState(ctx);
        
        CGRect typoRect = line.typographicFrame;
        CGRect occupieRect = line.occupiedFrame;
        
        CGRect rectInView = [SingleCellTextView convertRect:typoRect fromMNFrame: aFrame];
        rectInView = CGRectInset(rectInView, -10, -10);
        
        //if( CGRectIntersectsRect( rectInView, rect) ) これいる?
        {
          if( drawGuideLines )
          {
            // Baseline
            CGPoint points[2];
            CGFloat lineY = roundf(line.origin.y + line.leading + line.ascent - line.descent ) - 0.5 - pageOriginYInFrame;
            
            //roundf(line.typographicFrame.size.height+line.typographicFrame.origin.y + 2*line.origin.y + pathBoundingBox.origin.y) -0.5;
            points[0] = CGPointMake(pathBoundingBox.origin.x + line.origin.x, pathBoundingBox.origin.y  + lineY);
            points[1] = CGPointMake(pathBoundingBox.origin.x + line.origin.x + typoRect.size.width, pathBoundingBox.origin.y  + lineY);
            
            CGContextSetLineWidth(ctx, 1.0);
            CGContextSetStrokeColorWithColor(ctx, [UIColor redColor].CGColor);
            CGContextStrokeLineSegments(ctx, points, 2);
            
            
            // Draw enclosing box
            CGRect rect = CGRectIntegral( typoRect );
            rect.origin.x += pathBoundingBox.origin.x + 0.5;
            rect.origin.y += pathBoundingBox.origin.y + 0.5;
            CGContextSetStrokeColorWithColor(ctx, [UIColor lightGrayColor].CGColor);
            CGContextStrokeRect(ctx, rect);
            
            CGContextSetStrokeColorWithColor(ctx, [UIColor grayColor].CGColor);
            CGFloat lengths[] = {2, 4};
            CGContextSetLineDash ( ctx, 0, lengths, 2 );
            
            
            rect = CGRectIntegral( occupieRect );
            rect.origin.x += pathBoundingBox.origin.x + 0.5;
            rect.origin.y += pathBoundingBox.origin.y + 0.5;
            CGContextStrokeRect(ctx, rect);
            
            CGContextSetLineDash ( ctx, 0, nil, 0 );
            
          }
          
          else if( drawRuledLines )
          {
            // Draw Baseline
            CGPoint points[2];
            CGFloat lineY = roundf(line.origin.y + line.leading + line.ascent - line.descent ) + 0.5 - pageOriginYInFrame;
            
            points[0] = CGPointMake(pathBoundingBox.origin.x , pathBoundingBox.origin.y  + lineY);
            points[1] = CGPointMake(CGRectGetMaxX(pathBoundingBox), pathBoundingBox.origin.y  + lineY);
            
            //            if( points[0].y <= CGRectGetMaxY(layer.bounds) )
            {
              CGContextSetLineWidth(ctx, 1.0);
              CGContextSetStrokeColorWithColor(ctx, ruledLineColor.CGColor);
              CGContextStrokeLineSegments(ctx, points, 2);
              
              lastLineRect0 = points[0];
              lastLineRect1 = points[1];
              
              lastLineHeight = rectInView.size.height;
              
            }
          }
          
          
          // Draw  marked text
          NSRange lineRange;
          CFRange r0 = [line stringRange];
          lineRange = NSMakeRange(r0.location, r0.length);
          
          //          NSRange intersectionRange = NSIntersectionRange(lineRange, markedTextNSRange_);
          //          if( intersectionRange.length > 0 )
          //          {
          //            CGFloat offsetX1 = [line offsetForStringIndex:   intersectionRange.location];
          //            CGFloat offsetX2 = [line offsetForStringIndex:   intersectionRange.location + intersectionRange.length];
          //
          //            CGRect markRect = CGRectMake(offsetX1 + typoRect.origin.x + pathBoundingBox.origin.x,
          //                                         typoRect.origin.y + pathBoundingBox.origin.y + line.leading,
          //                                         offsetX2 - offsetX1,
          //                                         typoRect.size.height -line.leading);
          //            markRect = CGRectIntegral(markRect);
          //            markRect.origin.x -= 0.5;
          //            markRect.origin.y -= 0.5;
          //
          //
          //            CGContextSetBlendMode(ctx,  kCGBlendModeNormal);
          //            CGContextSetFillColorWithColor(ctx, markedTextFillColor_.CGColor);
          //            CGContextFillRect(ctx, markRect);
          //
          //            CGContextSetLineWidth(ctx, 1.0);
          //            CGContextSetStrokeColorWithColor(ctx, markedTextBorderColor_.CGColor);
          //            CGContextStrokeRect(ctx, markRect);
          //          }
          
          // DRAW BLOCKQUOTE BACKGROUND
          for( NSUInteger idx = line.range.location; idx < MAXRANGE(line.range); )
          {
            @autoreleasepool {
              NSRange effectiveRange;
              
              NSString* controlCode = [framesetter_.textStorage attribute: @"MNControl"
                                                                  atIndex: idx
                                                    longestEffectiveRange: &effectiveRange
                                                                  inRange: NSMakeRange(line.range.location, line.range.length) ];
              
              if( [controlCode hasPrefix:@"blockquote"] )
              {
                CGFloat lineHeight = typoRect.size.height - line.leading;
                
                CGRect markRect = CGRectMake( typoRect.origin.x + pathBoundingBox.origin.x,
                                             typoRect.origin.y + pathBoundingBox.origin.y + line.leading - pageOriginYInFrame,
                                             pathBoundingBox.size.width - typoRect.origin.x,
                                             lineHeight);
                markRect = CGRectIntegral(markRect);
                //            markRect.origin.x -= 0.5;
                //            markRect.origin.y -= 0.5;
                
                if( CGRectEqualToRect(CGRectNull, blockquoteRect) ) blockquoteRect = markRect;
                else blockquoteRect = CGRectUnion(blockquoteRect, markRect);
                
                CGColorRef cgColor = (__bridge CGColorRef)([framesetter_.textStorage attribute: @"BlockQuoteColor"
                                                                                       atIndex: idx
                                                                         longestEffectiveRange: nil
                                                                                       inRange: NSMakeRange(line.range.location, line.range.length) ]);
                if ( cgColor != nil ) {
                  blockquoteBackgroundColor = cgColor;
                }
                
                if( [touchHighlightKey isEqualToString:@"MNControl"] && [touchHighlightValue isEqual:controlCode] )
                {
                  blockquoteHighlighted = YES;
                }
                
                
              }else if( CGRectEqualToRect(CGRectNull, blockquoteRect) == NO ) {
                // DRAW
                
                [self drawBlockquoteBackgroundIn: blockquoteRect
                                     highlighted: blockquoteHighlighted
                                 backgroundColor: blockquoteBackgroundColor
                                              in: ctx];
                blockquoteRect = CGRectNull;
              }
              
              
              idx = MAXRANGE(effectiveRange);
            }
          }
          
          
          // Draw background fill color
          for( NSUInteger idx = line.range.location; idx < MAXRANGE(line.range); )
          {
            @autoreleasepool {
              NSRange effectiveRange;
              
              CGColorRef color = (__bridge CGColorRef)[framesetter_.textStorage attribute: OABackgroundColorAttributeName
                                                                                  atIndex: idx
                                                                    longestEffectiveRange: &effectiveRange
                                                                                  inRange: NSMakeRange(line.range.location, line.range.length) ];
              
              if( color )
              {
                CGFloat offsetX1 = [line offsetForStringIndex: effectiveRange.location];
                CGFloat offsetX2 = [line offsetForStringIndex: effectiveRange.location + effectiveRange.length];
                CGFloat lineHeight = typoRect.size.height - line.leading;
                CGFloat ty = typoRect.origin.y + line.leading + line.paragraphSpacingBefore  + pathBoundingBox.origin.y - pageOriginYInFrame;
                
                CGRect markRect = CGRectMake(offsetX1 + typoRect.origin.x + pathBoundingBox.origin.x,
                                             ty,
                                             offsetX2 - offsetX1,
                                             lineHeight);
                markRect = CGRectIntegral(markRect);
                //            markRect.origin.x -= 0.5;
                //            markRect.origin.y -= 0.5;
                
                
                CGContextSetBlendMode(ctx,  kCGBlendModeNormal);
                CGContextSetFillColorWithColor(ctx, color);
                CGContextFillRect(ctx, markRect);
              }
              
              idx = MAXRANGE(effectiveRange);
            }
          }
          
          // DRAW STYLUS MOVING TEMPORARY HIGHLIGHT
          if( temporaryHighlightAttributes != nil ) {
            NSRange effectiveRange = NSIntersectionRange(NSMakeRange(line.range.location, line.range.length), temporaryHighlightRange);
            if( effectiveRange.length > 0 ) {
              CGColorRef color = (__bridge CGColorRef)temporaryHighlightAttributes[OABackgroundColorAttributeName];
              
              if( color )
              {
                CGFloat offsetX1 = [line offsetForStringIndex: effectiveRange.location];
                CGFloat offsetX2 = [line offsetForStringIndex: effectiveRange.location + effectiveRange.length];
                
                //CGFloat lineHeight = typoRect.size.height - line.leading;
                CGFloat ty = typoRect.origin.y + line.leading + line.paragraphSpacingBefore  + pathBoundingBox.origin.y - pageOriginYInFrame;
                
                CGRect markRect = CGRectMake(offsetX1 + typoRect.origin.x + pathBoundingBox.origin.x,
                                             ty,
                                             offsetX2 - offsetX1,
                                             typoRect.size.height -line.leading);
                markRect = CGRectIntegral(markRect);
                //            markRect.origin.x -= 0.5;
                //            markRect.origin.y -= 0.5;
                
                
                CGContextSetBlendMode(ctx,  kCGBlendModeNormal);
                CGContextSetFillColorWithColor(ctx, color);
                CGContextFillRect(ctx, markRect);
              }
              
              
              NSNumber * num = temporaryHighlightAttributes[TEMPORARY_UNDERLINE_STYLE_ATTRIBUTE];
              CTUnderlineStyle style = (CTUnderlineStyle)[num integerValue];
              if( style != kCTUnderlineStyleNone )  {
                CGColorRef color = (__bridge CGColorRef)temporaryHighlightAttributes[TEMPORARY_UNDERLINE_COLOR_ATTRIBUTE];
                
                
                CGContextSaveGState(ctx);
                
                CGAffineTransform t = CGAffineTransformIdentity;//[self coreTextTransform];
                CGFloat tx = line.origin.x + pathBoundingBox.origin.x;
                CGFloat ty = typoRect.origin.y + line.leading + (line.ascent) +line.paragraphSpacingBefore  + pathBoundingBox.origin.y - pageOriginYInFrame;
                
                ty = roundf(ty);
                t = CGAffineTransformTranslate(t, tx, ty);
                CGContextConcatCTM(ctx, t);
                
                
                CGFloat offset1 = [line offsetForStringIndex:effectiveRange.location];
                CGFloat offset2 = [line offsetForStringIndex:MAXRANGE(effectiveRange)];
                
                CGPoint points[2];
                points[0] = CGPointMake( offset1, +2);
                points[1] = CGPointMake( offset2, +2);
                
                CGContextSetStrokeColorWithColor(ctx, color);
                
                if( style == kCTUnderlineStyleSingle )
                {
                  //                    CGContextSetLineDash (ctx, 0, lengths, 2 );
                  
                  points[0] = CGPointMake( offset1, +2);
                  points[1] = CGPointMake( offset2, +2);
                  
                  CGContextSetLineWidth(ctx, 1.0);
                  CGContextStrokeLineSegments(ctx, points, 2);
                }
                
                if( style == kCTUnderlineStyleThick )
                {
                  //                    CGContextSetLineDash (ctx, 0, lengths, 2 );
                  if( isVertical ) {
                    points[0] = CGPointMake( offset1, -line.ascent - 1 );
                    points[1] = CGPointMake( offset2, -line.ascent - 1 );
                  }else {
                    points[0] = CGPointMake( offset1, +2);
                    points[1] = CGPointMake( offset2, +2);
                  }
                  CGContextSetLineWidth(ctx, 1.5);
                  CGContextStrokeLineSegments(ctx, points, 2);
                }
                
                if( style == kCTUnderlineStyleDouble )
                {
                  //                    CGContextSetLineWidth(ctx, 1.0);
                  
                  points[0] = CGPointMake( offset1, +2);
                  points[1] = CGPointMake( offset2, +2);
                  
                  CGContextStrokeLineSegments(ctx, points, 2);
                  
                  points[0] = CGPointMake( offset1, +4);
                  points[1] = CGPointMake( offset2, +4);
                  
                  CGContextStrokeLineSegments(ctx, points, 2);
                }
                
                CGContextRestoreGState(ctx);
                
              }
            }
          }
          
          // DRAW TOUCH HIGHLIGHT
          if( touchHighlightKey != nil && ![touchHighlightKey isEqualToString:@"MNControl"] )
            for( NSUInteger idx = line.range.location; idx < MAXRANGE(line.range); )
            {
              
              @autoreleasepool {
                NSRange effectiveRange;
                
                NSString* obj = [framesetter_.textStorage attribute: touchHighlightKey
                                                            atIndex: idx
                                              longestEffectiveRange: &effectiveRange
                                                            inRange: NSMakeRange(line.range.location, line.range.length) ];
                
                if( [obj isKindOfClass:[NSString class]] && [touchHighlightValue isEqual: obj] )
                {
                  CGFloat offsetX1 = [line offsetForStringIndex: effectiveRange.location];
                  CGFloat offsetX2 = [line offsetForStringIndex: effectiveRange.location + effectiveRange.length];
                  CGFloat lineHeight = typoRect.size.height - line.leading;
                  CGFloat ty = typoRect.origin.y + line.leading + line.paragraphSpacingBefore  + pathBoundingBox.origin.y - pageOriginYInFrame;
//
                  CGRect markRect = CGRectMake(offsetX1 + typoRect.origin.x + pathBoundingBox.origin.x,
                                               ty,
                                               offsetX2 - offsetX1,
                                               lineHeight);
                  markRect = CGRectIntegral(markRect);
                  //            markRect.origin.x -= 0.5;
                  //            markRect.origin.y -= 0.5;
                  
                  
                  markRect = CGRectIntegral(markRect);
                  //markRect = CGRectInset(markRect, -2, -2);
                  UIColor* color = [UIColor colorWithWhite:0.5  alpha:0.6];
                  CGContextSetBlendMode(ctx,  kCGBlendModeNormal);
                  CGContextSetFillColorWithColor(ctx, color.CGColor);
                  // CGContextFillRect(ctx, markRect);
                  
                  if( markRect.size.width > 0  && markRect.size.height > 0) {
                    CGMutablePathRef bezierPath = CGPathCreateMutable();
                    CGPathAddRoundedRect(bezierPath, nil, markRect, 3.0, 3.0);
                    CGContextAddPath(ctx, bezierPath);
                    CGContextFillPath(ctx);
                    CFRelease(bezierPath);
                  }
                }
                
                idx = MAXRANGE(effectiveRange);
              }
            }
          
          
          // DRAW TEMP BAKGROUND FILL
          for( NSUInteger idx = line.range.location; idx < MAXRANGE(line.range); )
          {
            @autoreleasepool {
              NSRange effectiveRange;
              
              CGColorRef color = (__bridge CGColorRef)[framesetter_.textStorage attribute: TEMPORARY_BACKGROUND_COLOR_ATTRIBUTE
                                                                                  atIndex: idx
                                                                    longestEffectiveRange: &effectiveRange
                                                                                  inRange: NSMakeRange(line.range.location, line.range.length) ];
              
              if( color )
              {
                CGFloat offsetX1 = [line offsetForStringIndex:   effectiveRange.location];
                CGFloat offsetX2 = [line offsetForStringIndex:   effectiveRange.location + effectiveRange.length];
                CGFloat lineHeight = typoRect.size.height - line.leading;
                CGFloat ty = typoRect.origin.y + line.leading + line.paragraphSpacingBefore  + pathBoundingBox.origin.y - pageOriginYInFrame;
                
                CGRect markRect = CGRectMake(offsetX1 + typoRect.origin.x + pathBoundingBox.origin.x,
                                             ty,
                                             offsetX2 - offsetX1,
                                             lineHeight);
                markRect = CGRectIntegral(markRect);
                //            markRect.origin.x -= 0.5;
                //            markRect.origin.y -= 0.5;
                
                if( isVertical ) {
                  CGContextSetShadowWithColor(ctx, CGSizeMake(1,0), 3.0, [UIColor colorWithWhite:0 alpha:0.5].CGColor);
                  
                }else {
                  CGContextSetShadowWithColor(ctx, CGSizeMake(0,1), 3.0, [UIColor colorWithWhite:0 alpha:0.5].CGColor);
                }
                CGContextSetBlendMode(ctx,  kCGBlendModeNormal);
                CGContextSetFillColorWithColor(ctx, color);
                CGContextFillRect(ctx, markRect);
                CGContextSetShadowWithColor(ctx, CGSizeMake(0,0), 0.0, [UIColor clearColor].CGColor);
              }
              
              idx = MAXRANGE(effectiveRange);
            }
          }
          
          // DRAW DIFF FILL
          for( NSUInteger idx = line.range.location; idx < MAXRANGE(line.range); )
          {
            @autoreleasepool {
              NSRange effectiveRange;
              
              NSNumber* diffOperation = (NSNumber*)[framesetter_.textStorage attribute: DIFF_ATTRIBUTE
                                                                               atIndex: idx
                                                                 longestEffectiveRange: &effectiveRange
                                                                               inRange: NSMakeRange(line.range.location, line.range.length) ];
              
              if( diffOperation != nil )
              {
                CGFloat offsetX1 = [line offsetForStringIndex:   effectiveRange.location];
                CGFloat offsetX2 = [line offsetForStringIndex:   effectiveRange.location + effectiveRange.length];
                CGFloat lineHeight = typoRect.size.height - line.leading;
                CGFloat ty = typoRect.origin.y + line.leading + line.paragraphSpacingBefore  + pathBoundingBox.origin.y - pageOriginYInFrame;
                
                CGRect markRect = CGRectMake(offsetX1 + typoRect.origin.x + pathBoundingBox.origin.x,
                                             ty,
                                             offsetX2 - offsetX1,
                                             lineHeight);
                markRect = CGRectIntegral(markRect);
                //            markRect.origin.x -= 0.5;
                //            markRect.origin.y -= 0.5;
                
                
                CGContextSetBlendMode(ctx,  kCGBlendModeNormal);
                if( diffOperation.integerValue == 1 ) {
                  CGColorRef color = RGBA(255,20,50,0.3).CGColor;
                  CGContextSetFillColorWithColor(ctx, color);
                  
                  color = RGBA(255,0,0,1).CGColor;
                  CGContextSetStrokeColorWithColor(ctx, color);
                  
                  CGContextSetLineWidth(ctx, 1.0);
                  
                  CGPoint points[2];
                  points[0] = CGPointMake( markRect.origin.x, CGRectGetMidY(markRect) - 1);
                  points[1] = CGPointMake( CGRectGetMaxX(markRect), CGRectGetMidY(markRect) - 1);
                  CGContextStrokeLineSegments(ctx, points, 2);
                  points[0] = CGPointMake( markRect.origin.x, CGRectGetMidY(markRect) + 1);
                  points[1] = CGPointMake( CGRectGetMaxX(markRect), CGRectGetMidY(markRect) + 1);
                  CGContextStrokeLineSegments(ctx, points, 2);
                  
                }
                
                if( diffOperation.integerValue == 2 ) {
                  CGColorRef color = RGBA(20,60,255,0.2).CGColor;
                  CGContextSetFillColorWithColor(ctx, color);
                  CGContextFillRect(ctx, markRect);
                  
                  color = RGBA(20,60,255,0.5).CGColor;
                  CGContextSetStrokeColorWithColor(ctx, color);
                  CGContextStrokeRect(ctx, markRect);
                  
                }
                
              }
              
              idx = MAXRANGE(effectiveRange);
            }
          }
          
          
          CGContextSaveGState(ctx);
          
          CGAffineTransform t = CGAffineTransformIdentity;//[self coreTextTransform];
          CGFloat tx = line.origin.x + pathBoundingBox.origin.x;
          CGFloat ty = typoRect.origin.y + line.leading + (line.ascent) + line.paragraphSpacingBefore + pathBoundingBox.origin.y - pageOriginYInFrame;
          
          
          //          CGFloat ty = (line.origin.y + line.leading + line.ascent - line.descent - line.lineSpacing + line.paragraphSpacingAfter +line.paragraphSpacingBefore) + pathBoundingBox.origin.y - pageOriginYInFrame;
          
          t = CGAffineTransformTranslate(t, roundf(tx), roundf(ty));
          CGContextConcatCTM(ctx, t);
          
          // Draw Underline
          //if( 0 ) // USE DEFAUT UNDERLINE
          for( NSUInteger idx = line.range.location; idx < MAXRANGE(line.range); )
          {
            @autoreleasepool {
              NSRange effectiveRange;
              
              NSNumber* num = (NSNumber*)[framesetter_.textStorage attribute: MNCTUnderlineStyleAttributeName atIndex:idx
                                                       longestEffectiveRange: &effectiveRange
                                                                     inRange: NSMakeRange(line.range.location, line.range.length) ];
              
              CTUnderlineStyle style = (CTUnderlineStyle)[num integerValue];
              if( style != kCTUnderlineStyleNone )
              {
                for( NSUInteger colorIndex = idx; colorIndex < MAXRANGE(effectiveRange); )
                {
                  NSRange colorRange;
                  
                  CGColorRef color = (__bridge CGColorRef)[framesetter_.textStorage attribute: MNCTUnderlineColorAttributeName
                                                                                      atIndex: colorIndex
                                                                        longestEffectiveRange: &colorRange
                                                                                      inRange: effectiveRange ];
                  
                  if( color == nil ) color = (__bridge CGColorRef)[framesetter_.textStorage attribute:MNCTForegroundColorAttributeName
                                                                                              atIndex:colorIndex effectiveRange:nil];
                  if( color == nil ) color = [UIColor colorWithRed:0 green:0 blue:0 alpha:1].CGColor;
                  
                  CGFloat offset1 = [line offsetForStringIndex:colorRange.location];
                  CGFloat offset2 = [line offsetForStringIndex:MAXRANGE(colorRange)];
                  
                  
                  CGPoint points[2];
                  points[0] = CGPointMake( offset1, +2);
                  points[1] = CGPointMake( offset2, +2);
                  
                  CGContextSetStrokeColorWithColor(ctx, color);
                  
                  if( style == kCTUnderlineStyleSingle )
                  {
                    points[0] = CGPointMake( offset1, +2);
                    points[1] = CGPointMake( offset2, +2);
                    
                    CGContextSetLineWidth(ctx, 1.0);
                    CGContextStrokeLineSegments(ctx, points, 2);
                  }
                  
                  if( style == kCTUnderlineStyleThick )
                  {
                    points[0] = CGPointMake( offset1, +3);
                    points[1] = CGPointMake( offset2, +3);
                    
                    CGContextSetLineWidth(ctx, 2.0);
                    //CGContextStrokeLineSegments(ctx, points, 2);
                  }
                  
                  if( style == kCTUnderlineStyleDouble )
                  {
                    CGContextSetLineWidth(ctx, 1.0);
                    
                    points[0] = CGPointMake( offset1, +2);
                    points[1] = CGPointMake( offset2, +2);
                    
                    CGContextStrokeLineSegments(ctx, points, 2);
                    
                    points[0] = CGPointMake( offset1, +4);
                    points[1] = CGPointMake( offset2, +4);
                    
                    CGContextStrokeLineSegments(ctx, points, 2);
                  }
                  
                  colorIndex = MAXRANGE(colorRange);
                  
                }
              }
              
              idx = MAXRANGE(effectiveRange);
            }
          }
          
          // Draw temporary Underline
          
          
          for( NSUInteger idx = line.range.location; idx < MAXRANGE(line.range); )
          {
            @autoreleasepool {
              NSRange effectiveRange;
              
              NSNumber* num = (NSNumber*)[framesetter_.textStorage attribute: TEMPORARY_UNDERLINE_STYLE_ATTRIBUTE
                                                                     atIndex: idx
                                                       longestEffectiveRange: &effectiveRange
                                                                     inRange: NSMakeRange(line.range.location, line.range.length) ];
              
              CTUnderlineStyle style = (CTUnderlineStyle)[num integerValue];
              if( style != kCTUnderlineStyleNone )
              {
                for( NSUInteger colorIndex = idx; colorIndex < MAXRANGE(effectiveRange); )
                {
                  NSRange colorRange;
                  
                  CGColorRef color = (__bridge CGColorRef)[framesetter_.textStorage attribute: TEMPORARY_UNDERLINE_COLOR_ATTRIBUTE
                                                                                      atIndex: colorIndex
                                                                        longestEffectiveRange: &colorRange
                                                                                      inRange: effectiveRange ];
                  if( color == nil ) color = (__bridge CGColorRef)[framesetter_.textStorage attribute:MNCTForegroundColorAttributeName
                                                                                              atIndex:colorIndex effectiveRange:nil];
                  if( color == nil ) color = [UIColor colorWithRed:0 green:0 blue:0 alpha:1].CGColor;
                  
                  CGFloat offset1 = [line offsetForStringIndex:colorRange.location];
                  CGFloat offset2 = [line offsetForStringIndex:MAXRANGE(colorRange)];
                  
                  CGPoint points[2];
                  points[0] = CGPointMake( offset1, +2);
                  points[1] = CGPointMake( offset2, +2);
                  
                  CGContextSetStrokeColorWithColor(ctx, color);
                  
                  if( style == kCTUnderlineStyleSingle )
                  {
                    //                    CGContextSetLineDash (ctx, 0, lengths, 2 );
                    
                    points[0] = CGPointMake( offset1, +2);
                    points[1] = CGPointMake( offset2, +2);
                    
                    CGContextSetLineWidth(ctx, 1.0);
                    CGContextStrokeLineSegments(ctx, points, 2);
                  }
                  
                  if( style == kCTUnderlineStyleThick )
                  {
                    //                    CGContextSetLineDash (ctx, 0, lengths, 2 );
                    if( isVertical ) {
                      points[0] = CGPointMake( offset1, -line.ascent - 1 );
                      points[1] = CGPointMake( offset2, -line.ascent - 1 );
                    }else {
                      points[0] = CGPointMake( offset1, +2);
                      points[1] = CGPointMake( offset2, +2);
                    }
                    CGContextSetLineWidth(ctx, 1.5);
                    CGContextStrokeLineSegments(ctx, points, 2);
                  }
                  
                  if( style == kCTUnderlineStyleDouble )
                  {
                    //                    CGContextSetLineWidth(ctx, 1.0);
                    
                    points[0] = CGPointMake( offset1, +2);
                    points[1] = CGPointMake( offset2, +2);
                    
                    CGContextStrokeLineSegments(ctx, points, 2);
                    
                    points[0] = CGPointMake( offset1, +4);
                    points[1] = CGPointMake( offset2, +4);
                    
                    CGContextStrokeLineSegments(ctx, points, 2);
                  }
                  
                  colorIndex = MAXRANGE(colorRange);
                }
              }
              
              idx = MAXRANGE(effectiveRange);
            }
          }
          
          // Draw link Underline
          if( paging == NO )
            for( NSUInteger idx = line.range.location; idx < MAXRANGE(line.range); )
            {
              @autoreleasepool {
                
                NSRange effectiveRange;
                
                NSString* URLString = (NSString*)[framesetter_.textStorage attribute: @"MNTempLink"
                                                                             atIndex: idx
                                                               longestEffectiveRange: &effectiveRange
                                                                             inRange: NSMakeRange(line.range.location, line.range.length) ];
                
                UIColor* color = (UIColor*)[framesetter_.textStorage attribute:@"MNTempLinkColor"
                                                                       atIndex:idx effectiveRange:nil];
                if( color == nil ) color = (UIColor*)[framesetter_.textStorage attribute: NSForegroundColorAttributeName
                                                                                 atIndex:idx effectiveRange:nil];
                
                
                if( URLString )
                {
                  CGFloat offset1 = roundf([line offsetForStringIndex:effectiveRange.location]);
                  CGFloat offset2 = roundf([line offsetForStringIndex:MAXRANGE(effectiveRange)]);
                  
                  CGPoint points[2];
                  
                  if( color != nil ) {
                    CGContextSetStrokeColorWithColor(ctx, color.CGColor);
                  }
                  const CGFloat lengths[] = {1,2};
                  CGContextSetLineDash (ctx, 0, lengths, 2 );
                  
                  if( isVertical ) {
                    points[0] = CGPointMake( offset1, -line.ascent - 3.5);
                    points[1] = CGPointMake( offset2, -line.ascent - 3.5);
                    
                  }else {
                    points[0] = CGPointMake( offset1, +3 + 0.5);
                    points[1] = CGPointMake( offset2, +3 + 0.5);
                  }
                  CGContextSetLineWidth(ctx, 1);
                  CGContextStrokeLineSegments(ctx, points, 2);
                }
                
                idx = MAXRANGE(effectiveRange);
              }
            }
          CGContextSetLineDash ( ctx, 0, 0, 0 );
          
          // DRAW RUBY
          for( NSUInteger idx = line.range.location; idx < MAXRANGE(line.range); )
          {
            @autoreleasepool {
              
              NSRange effectiveRange;
              
              NSString* rubyTextId = (NSString*)[framesetter_.textStorage attribute:@"RubyTextId" atIndex:idx effectiveRange:&effectiveRange  ];
              
              if( rubyTextId )
              {
                NSString* rubyText = (NSString*)[framesetter_.textStorage attribute:@"RubyText" atIndex:idx effectiveRange:nil  ];
                
                NSMutableAttributedString* attr = [[NSMutableAttributedString alloc] initWithString:rubyText];
                CGFloat offset1 = [line offsetForStringIndex:effectiveRange.location];
                CGFloat offset2 = [line offsetForStringIndex:MAXRANGE(effectiveRange)];
                CGPoint point = CGPointMake( (offset1 + offset2)/2, typoRect.size.height -line.leading);
                
                [attr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:8] range:NSMakeRange(0, attr.length)];
                
                if( isVertical ) {
                  [attr addAttribute:NSVerticalGlyphFormAttributeName value:@(1) range:NSMakeRange(0, attr.length)];
                }
                [self drawRuby: attr at: point typoRect: typoRect inContext:ctx isVertical: isVertical];
                //[attr release];
              }
              
              idx = MAXRANGE(effectiveRange);
            }
          }
          
          // Draw runs
          
          NSArray* runs = nil;
          @autoreleasepool {
            runs = (NSArray*)CTLineGetGlyphRuns(line.CTLine);
          }
          for( id aRun in runs )
          {
            @autoreleasepool {
              MNRunDrawWithScaling((__bridge CTRunRef)aRun, ctx, CFRangeMake(0,0), NO, NO, 1.0, isVertical);
            }
          }
          
          
          //DRAW MASK
          for( NSUInteger idx = line.range.location; idx < MAXRANGE(line.range); )
          {
            @autoreleasepool {
              NSRange effectiveRange;
              
              CFStringRef mask = (__bridge CFStringRef)[framesetter_.textStorage attribute: MASK_STYLE_ATTRIBUTE
                                                                                   atIndex: idx
                                                                     longestEffectiveRange: &effectiveRange
                                                                                   inRange: NSMakeRange(line.range.location, line.range.length) ];
              
              if( mask )
              {
                // DRAW MASK WHEN NOT UNMASKED
                CGContextRestoreGState(ctx);
                
                CGFloat offsetX1 = [line offsetForStringIndex:   effectiveRange.location];
                CGFloat offsetX2 = [line offsetForStringIndex:   effectiveRange.location + effectiveRange.length];
                CGFloat ty = typoRect.origin.y + line.leading  + line.paragraphSpacingBefore  + pathBoundingBox.origin.y - pageOriginYInFrame;
                
                CGRect markRect = CGRectMake(offsetX1 + typoRect.origin.x + pathBoundingBox.origin.x,
                                             ty,
                                             offsetX2 - offsetX1,
                                             typoRect.size.height -line.leading);
                markRect = CGRectIntegral(markRect);
                
                CGContextSetBlendMode(ctx,  kCGBlendModeNormal);
                if( kLastUnmaskedUuids == nil ) {
                  kLastUnmaskedUuids = [[NSMutableArray alloc] init];
                }
                if( [kLastUnmaskedUuids containsObject: (__bridge NSString*)mask] || [(__bridge NSString*)mask isEqualToString: (NSString*)touchHighlightValue]) {
                  if( touchHighlightValue != nil ) {
                    [kLastUnmaskedUuids addObject: (NSString*)touchHighlightValue];
                  }
                  CGContextSetStrokeColorWithColor(ctx, [UIColor colorWithWhite:0.5 alpha:0.8].CGColor);
                  CGContextSetLineWidth(ctx, 0.5);
                  CGContextStrokeRect(ctx, markRect);
                  
                }else {
                  CGContextSetFillColorWithColor(ctx, [UIColor colorWithWhite:0.7 alpha:1].CGColor);
                  CGContextFillRect(ctx, markRect);
                }
                CGContextSaveGState(ctx);
              }
              
              idx = MAXRANGE(effectiveRange);
            }
          }
          
          
          // DRAW STYLUS MOVING TEMPORARY MASK
          if( temporaryHighlightAttributes != nil ) {
            NSRange effectiveRange = NSIntersectionRange(NSMakeRange(line.range.location, line.range.length), temporaryHighlightRange);
            if( effectiveRange.length > 0 ) {
              CFStringRef mask = (__bridge CFStringRef)temporaryHighlightAttributes[MASK_STYLE_ATTRIBUTE];
              
              if( mask )
              {
                // DRAW MASK WHEN NOT UNMASKED
                CGContextRestoreGState(ctx);
                
                CGFloat offsetX1 = [line offsetForStringIndex:   effectiveRange.location];
                CGFloat offsetX2 = [line offsetForStringIndex:   effectiveRange.location + effectiveRange.length];
                CGFloat ty = typoRect.origin.y + line.leading + line.paragraphSpacingBefore   + pathBoundingBox.origin.y - pageOriginYInFrame;
                
                CGRect markRect = CGRectMake(offsetX1 + typoRect.origin.x + pathBoundingBox.origin.x,
                                             ty,
                                             offsetX2 - offsetX1,
                                             typoRect.size.height -line.leading);
                markRect = CGRectIntegral(markRect);
                markRect.origin.x -= 0.25;
                markRect.origin.y -= 0.25;
                
                CGContextSetBlendMode(ctx,  kCGBlendModeNormal);
                
                
                CGContextSetFillColorWithColor(ctx, [UIColor colorWithWhite:0.7 alpha:1].CGColor);
                CGContextFillRect(ctx, markRect);
                
                CGContextSaveGState(ctx);
              }
              
            }
          }
          
          if( paging ) {
            if( hoge != [aFrame.lines count]-1 )
            {
              MNLine *nextLine = [aFrame.lines objectAtIndex:hoge +1];
              
              originYInPage = nextLine.typographicFrame.origin.y + nextLine.ascent + nextLine.leading  + nextLine.paragraphSpacingBefore - pageOriginYInFrame;
              
              if( originYInPage > pageSize.height  )
              {
                
                //                NSLog(@"begin page originYInPage>size.height = %f,%f",originYInPage,pageSize.height);
                //                NSLog(@"nextLine.typographicFrame.origin.y %f",nextLine.typographicFrame.origin.y);
                //                NSLog(@"nextLine.ascent %f",nextLine.ascent);
                //                NSLog(@"nextLine.leading %f",nextLine.leading);
                //                NSLog(@"pageOriginYInFrame %f",pageOriginYInFrame);
                
                
                // DRAW BLOCK QUOTE
                if( CGRectEqualToRect(blockquoteRect, CGRectNull) == NO ) {
                  CGContextRestoreGState(ctx);
                  
                  [self drawBlockquoteBackgroundIn: blockquoteRect
                                       highlighted: blockquoteHighlighted
                                   backgroundColor: blockquoteBackgroundColor
                                                in: ctx];
                  blockquoteRect = CGRectNull;
                  
                  CGContextSaveGState(ctx);
                  
                }
                
                // New page
                if( continuePrintingFromFirstPage ) {
#if TARGET_OS_IPHONE
                  UIGraphicsBeginPDFPage();
#endif
                  [layoutDictionaries addObject:[NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:hoge+1] forKey:@"startLineNumber"]];
                  
                  pageOriginYInFrame = nextLine.origin.y - nextLine.descent - nextLine.lineSpacing + nextLine.paragraphSpacingAfter + nextLine.paragraphSpacingBefore;
                  
                }else {
                  break;
                }
              }
            }
          }
        }
        CGContextRestoreGState(ctx);
      }// RELEASE POOL
      
      
      
      
      //      CGContextSetLineWidth(ctx, 0.5);
      //      blockquoteRect = CGRectIntegral(blockquoteRect);
      //      blockquoteRect = CGRectInset(blockquoteRect, 0.25, 0.25);
      //      CGContextSetStrokeColorWithColor(ctx, [UIColor lightGrayColor].CGColor);
      //      CGContextStrokeRect(ctx, blockquoteRect);
      
    }//LINE LOOP
    
    // DRAW BLOCK QUOTE
    if( CGRectEqualToRect(blockquoteRect, CGRectNull) == NO ) {
      [self drawBlockquoteBackgroundIn: blockquoteRect
                           highlighted: blockquoteHighlighted
                       backgroundColor: blockquoteBackgroundColor
                                    in: ctx];
      blockquoteRect = CGRectNull;
    }

    
    
    CGContextRestoreGState(ctx);
    CGContextSaveGState(ctx);
  }
  
  CGContextRestoreGState(ctx);
  return layoutDictionaries;
}

- (void)drawSublayer:(SingleCellTextViewSublayer *)layer inContext:(CGContextRef)ctx
{
  if( !ctx )
  {
    NSLog(@"*** context is nil ***");
    return;
  }
  
  // If the layer is obscured, set defer flag on.
  
  if( !CGRectIntersectsRect( layer.frame, self.bounds) )
  {
    layer.defer = YES;
    if( !layer.hidden )
      layer.hidden = YES;
    return;
  }
  
  [SingleCellTextView drawSublayerInContext: ctx
                                framesetter: framesetter_
                                 isVertical: self.isVertical
                          touchHighlightKey: touchHighlightKey
                        touchHighlightValue: touchHighlightValue
               temporaryHighlightAttributes: temporaryHighlightAttributes_
                    temporaryHighlightRange: temporaryHighlightRange_
                                   pageSize: CGSizeZero
              continuePrintingFromFirstPage: NO
                             fromLineNumber: 0];
  
  return;
}


+(void)drawRuby:(NSAttributedString*)attributedString_ at:(CGPoint)point typoRect:(CGRect)typoRect inContext:(CGContextRef)ctx isVertical:(BOOL)isVertical {
  if( isVertical ) return;
  
#define MARGIN 7

  
  CGFloat descent;
  CGFloat ascent;
  CGFloat leading;
  double width;
  CTLineRef line_ = CTLineCreateWithAttributedString((CFAttributedStringRef)attributedString_);
  point.x = roundf(point.x);
  point.y = roundf(point.y);
  
  
  width = CTLineGetTypographicBounds(line_, &ascent, &descent, &leading);
  
  CGContextSaveGState(ctx);
  CGContextSetFillColorWithColor(ctx, [UIColor blackColor].CGColor);
  
  CGAffineTransform transform = CGAffineTransformMakeScale(1, -1);
  if( isVertical ) {
    
    transform = CGAffineTransformRotate(transform, +M_PI_2);
    transform = CGAffineTransformTranslate(transform,  point.x - typoRect.size.height/2 + ascent + descent, -point.y );
    
  }else {
    transform = CGAffineTransformTranslate(transform, point.x - width/2 + 3, point.y + ascent);
  }
  
  CGContextSetTextMatrix(ctx, transform);
  
  CGAffineTransform t;
  
  // Drawing left part
  
  t = CGAffineTransformMakeTranslation( descent - 4, MARGIN );
  
  CGContextConcatCTM(ctx, t);
  
  NSArray* runs = (NSArray*)CTLineGetGlyphRuns(line_);
  for( id run in runs )
  {
    CTRunDraw((CTRunRef)run, ctx, CFRangeMake(0, 0));
  }
  CGContextSetTextMatrix(ctx, CGAffineTransformIdentity);
  CGContextRestoreGState(ctx);
    CFRelease(line_);
    
    CGAffineTransform textMatrix = CGAffineTransformMakeScale(1, -1);
    if( isVertical ) {
        textMatrix = CGAffineTransformRotate(textMatrix, +M_PI_2);
    }
    CGContextSetTextMatrix(ctx, textMatrix);
    
}

+(void)drawBookmarkCount:(NSAttributedString*)attributedString_ at:(CGPoint)point inContext:(CGContextRef)ctx isVertical:(BOOL)isVertical {
#define MARGIN 7
    
    CGFloat descent;
    CGFloat ascent;
    CGFloat leading;
    double width;
    CTLineRef line_ = CTLineCreateWithAttributedString((CFAttributedStringRef)attributedString_);
    point.x = roundf(point.x);
    point.y = roundf(point.y);
    
    
    width = CTLineGetTypographicBounds(line_, &ascent, &descent, &leading);
    
    CGContextSaveGState(ctx);
    
    CGAffineTransform transform = CGAffineTransformMakeScale(1, -1);
    if( isVertical ) {
        transform = CGAffineTransformTranslate(transform, point.x + (descent - 7), -(point.y + 6 + MARGIN));
        transform = CGAffineTransformRotate(transform, +M_PI_2);
    }else {
        transform = CGAffineTransformTranslate(transform, point.x - width/2 + 2 + (descent - 4), -(point.y + ascent + MARGIN));
    }
    CGContextSetTextMatrix(ctx, transform);
    
    NSArray* runs = (NSArray*)CTLineGetGlyphRuns(line_);
    for( id run in runs )
    {
        CTRunDraw((CTRunRef)run, ctx, CFRangeMake(0, 0));
    }
    CGContextSetTextMatrix(ctx, CGAffineTransformIdentity);
    CGContextRestoreGState(ctx);
    CFRelease(line_);
    
    CGAffineTransform textMatrix = CGAffineTransformMakeScale(1, -1);
    if( isVertical ) {
        textMatrix = CGAffineTransformRotate(textMatrix, +M_PI_2);
    }
    CGContextSetTextMatrix(ctx, textMatrix);
    
}

#if TARGET_OS_IPHONE
+ (NSMutableArray* _Nonnull)drawPDF:(NSAttributedString* _Nonnull)attributedString vertical:(BOOL)vertical atPath:(NSString* _Nonnull)path inSize:(CGSize)size pdfInfo:(NSDictionary * _Nullable)pdfDict documentScaling:(CGFloat)documentScaling
{
  NSAssert([NSThread isMainThread] == YES, @"** CALL FROM MAIN THREAD");
  
  CGRect pathBoundingBox = CGRectMake(0, 0, size.width, size.height);
  NSMutableAttributedString* mattr = [attributedString mutableCopy];
  MNFramesetter* framesetter = [[MNFramesetter alloc] initWithAttributedString:mattr];
  framesetter.documentScaling = documentScaling;
  framesetter.containerSize = CGSizeMake( size.width, MAXFLOAT);
  [framesetter invalidateLayout];

  UIGraphicsBeginPDFContextToFile (
                                   path,
                                   pathBoundingBox,
                                   pdfDict
                                   );
  
  CGContextRef ctx = UIGraphicsGetCurrentContext();
  
  NSMutableArray* layoutDictionaries = [self drawSublayerInContext: ctx
                                                       framesetter: framesetter
                                                        isVertical: vertical
                                                 touchHighlightKey: nil
                                               touchHighlightValue: nil
                                      temporaryHighlightAttributes: nil
                                           temporaryHighlightRange: NSMakeRange(NSNotFound, 0)
                                                          pageSize: size
                                     continuePrintingFromFirstPage: YES
                                                    fromLineNumber: 0];
  
  UIGraphicsEndPDFContext();
  
  return layoutDictionaries;
}
#endif

+(void)drawInContext:(MNFramesetter*)framesetter vertical:(BOOL)vertical fromLineIndex:(NSUInteger)lineNumber inRect:(CGRect)contentRect documentAttributes:(NSDictionary*)documentAttributes  documentScaling:(CGFloat)documentScaling duplexPageNumber:(NSUInteger)duplexPageNumber containerSize: (CGSize)containerSize customLayout: (BOOL)customLayout layoutContentOrigin: (CGPoint)customLayoutContentOrigin
{
  @autoreleasepool {

    // TODO:
    for( MNFrame *frameObject in framesetter.frames ) {
      CGRect rect = frameObject.pathBoundingBox; rect.origin = contentRect.origin;
      frameObject.pathBoundingBox = rect;
    }
    
    CGContextRef ctx = CURRENT_CONTEXT;
    
    if( customLayout ) {
      CGContextSaveGState(ctx);
      
      if( vertical ) {
        CGAffineTransform t = CGAffineTransformMakeRotation(0);
        t = CGAffineTransformTranslate(t, customLayoutContentOrigin.x, customLayoutContentOrigin.y);
        CGContextConcatCTM(ctx, t);

      }else {
        
        CGAffineTransform t = CGAffineTransformMakeRotation(-M_PI_2);
        t = CGAffineTransformTranslate(t, -customLayoutContentOrigin.y, customLayoutContentOrigin.x);
        CGContextConcatCTM(ctx, t);
      }
    }
    [self drawSublayerInContext: ctx
                    framesetter: framesetter
                     isVertical: vertical
              touchHighlightKey: nil
            touchHighlightValue: nil
   temporaryHighlightAttributes: nil
        temporaryHighlightRange: NSMakeRange(NSNotFound, 0)
                       pageSize: containerSize
  continuePrintingFromFirstPage: NO
                 fromLineNumber: lineNumber];
    
    
    if( customLayout ) {
      CGContextRestoreGState(ctx);
      
      if( vertical ) {
        CGAffineTransform t = CGAffineTransformIdentity;
        t = CGAffineTransformTranslate(t, -customLayoutContentOrigin.x, -customLayoutContentOrigin.y);
        t = CGAffineTransformRotate(t, 0);
        CGContextConcatCTM(ctx, t);
      }else {
        // なんでリストアされないんだろう
        CGAffineTransform t = CGAffineTransformIdentity;
        t = CGAffineTransformTranslate(t, customLayoutContentOrigin.y, -customLayoutContentOrigin.x);
        t = CGAffineTransformRotate(t, M_PI_2);
        CGContextConcatCTM(ctx, t);
      }
      
    }
  }
}

+(CGRect)convertRect:(CGRect)rect fromMNFrame:(MNFrame*)frame
{
  CGPoint origin = CGPointMake(rect.origin.x, rect.origin.y );
  CGPoint point = [self convertPoint:origin fromMNFrame:frame];
  return CGRectMake(point.x, point.y, rect.size.width, rect.size.height);
}

+(CGPoint)convertPoint:(CGPoint)point fromMNFrame:(MNFrame*)frame
{
  // Get Box
  CGRect pathBoundingBox = frame.pathBoundingBox;
  
  // Get Inset from top left corner
  CGFloat marginX = pathBoundingBox.origin.x;
  CGFloat marginY = pathBoundingBox.origin.y;
  
  CGAffineTransform t = CGAffineTransformMakeScale(1, 1);
  t = CGAffineTransformTranslate(t, marginX, marginY);
  
  return CGPointApplyAffineTransform(point, t);
}


@end

/*
 @interface MFMailComposeViewController:NSObject {

}
@end

@implementation MFMailComposeViewController
+(BOOL)canSendMail
{
	return NO;
}

@end
*/


