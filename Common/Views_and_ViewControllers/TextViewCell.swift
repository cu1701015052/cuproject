//
//  TextViewCell.swift
//  LawXML
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
#if os(iOS) || os(watchOS)
import UIKit
#else
import Cocoa
#endif

class TagView: UIView {
  
  static let radius: CGFloat = 7.0
  var tags: [TagEntity] = [] {
    didSet {
      isHidden = tags.count == 0
      #if os(iOS) || os(watchOS)
      setNeedsDisplay()
      #endif
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    #if os(iOS) || os(watchOS)
    isOpaque = false
    backgroundColor = .clear
    #endif
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func draw(_ rect: CGRect) {
    super.draw(rect)

    let color = UIColor.red.cgColor
    #if os(iOS) || os(watchOS)
    let ctx = UIGraphicsGetCurrentContext()
    #endif
    
    #if os(macOS)
    let ctx = NSGraphicsContext.current?.cgContext
    #endif

    
    
    var x: CGFloat = self.bounds.size.width - TagView.radius - 1
    var y: CGFloat = 8
    
    if tags.count < 4 { y = 8 }
    else if tags.count < 7 { y = 6 }
    else { y = 1 }

    ctx?.setStrokeColor(color)
    ctx?.setFillColor(color)
    

    for tag in tags {
      if tag.color == .noColor { continue }
      let tagRect = CGRect(x: x, y: y, width: TagView.radius, height: TagView.radius)
      
      ctx?.setBlendMode(CGBlendMode.clear)
      ctx?.fillEllipse(in: tagRect.insetBy(dx: -1, dy: -1))

      ctx?.setBlendMode(CGBlendMode.normal)
      ctx?.setFillColor(tag.color.color().cgColor)
      ctx?.setStrokeColor(tag.color.frameColor().cgColor)
      ctx?.fillEllipse(in: tagRect)
      ctx?.strokeEllipse(in: tagRect)
      
      x -= 5
      if x < 1 {
        x = self.bounds.size.width - TagView.radius - 1
        if tags.count < 4 { y += 6 }
        else if tags.count < 7 { y += 6 }
        else { y += 5 }
      
      }
      
      if y > 15 { break }
    }
    
    if tags.count > 9 {
      (0...2).forEach {
        let tagRect = CGRect(x: self.bounds.size.width  - 2 - CGFloat($0 * 4), y: 19, width: 1, height: 1)
        
        ctx?.setFillColor(UIColor.gray.cgColor)
        ctx?.setStrokeColor(UIColor.gray.cgColor)
        ctx?.fillEllipse(in: tagRect)
        ctx?.strokeEllipse(in: tagRect)
      }
    }

//
    
    
    
    
    
//    var tagRect = CGRect(x: 4, y: 2, width: 3, height: 18)
//    ctx?.fill(tagRect)
//
//    color = UIColor.orange.cgColor
//    tagRect = CGRect(x: 8, y: 2, width: 3, height: 18)
//    ctx?.setFillColor(color)
//    ctx?.fill(tagRect)
//
//    color = UIColor.green.cgColor
//    tagRect = CGRect(x: 12, y: 2, width: 3, height: 18)
//    ctx?.setFillColor(color)
//    ctx?.fill(tagRect)
//
//    color = UIColor.purple.cgColor
//    tagRect = CGRect(x: 16, y: 2, width: 3, height: 18)
//    ctx?.setFillColor(color)
//    ctx?.fill(tagRect)
//
//    color = UIColor.blue.cgColor
//    tagRect = CGRect(x: 20, y: 2, width: 3, height: 18)
//    ctx?.setFillColor(color)
//    ctx?.fill(tagRect)
//
//
//
//    tagRect = CGRect(x: 4, y: 7, width: 4, height: 4)
//    ctx?.fill(tagRect)
//
//    color = UIColor.orange.cgColor
//    tagRect = CGRect(x: 9, y: 7, width: 4, height: 4)
//    ctx?.setFillColor(color)
//    ctx?.fill(tagRect)
//
//    color = UIColor.green.cgColor
//    tagRect = CGRect(x: 14, y: 7, width: 4, height: 4)
//    ctx?.setFillColor(color)
//    ctx?.fill(tagRect)
//
//
//    tagRect = CGRect(x: 4, y: 12, width: 4, height: 4)
//    ctx?.fill(tagRect)
//
//    color = UIColor.orange.cgColor
//    tagRect = CGRect(x: 9, y: 12, width: 4, height: 4)
//    ctx?.setFillColor(color)
//    ctx?.fill(tagRect)
//
//    color = UIColor.green.cgColor
//    tagRect = CGRect(x: 14, y: 12, width: 4, height: 4)
//    ctx?.setFillColor(color)
//    ctx?.fill(tagRect)
    
  }

}

class TextViewCell: UIView {
  
  #if os(OSX)
  var backgroundColor: NSColor!
  var tintColor: NSColor!
  
  override var isFlipped: Bool { return true }
  #endif
  
  var indexPath: MyIndexPath!
  
  class func cellHeightFor(attributedString: NSMutableAttributedString, width: CGFloat, styleSheet: StyleSheet) -> (MNFramesetter, CGFloat) {
    
    //    if attributedString.string == "─────────────" { return 2 }
    var contentHeight: CGFloat = 0
    let framesetter = SingleCellTextView.contentSize(attributedString, forWidth: width - styleSheet.leftMargin - styleSheet.rightMargin, edgeInset: NSEdgeInsets.zero, contentHeight: &contentHeight)
    //print("\(attributedString.string) \(height)")
    return (framesetter, contentHeight)
  }
  
  class func estimatedCellHeightFor(length: Int, width: CGFloat, styleSheet: StyleSheet) -> CGFloat {
    if length == 0 { return 20 }
    let k: CGFloat = 0.25 // magic number
    let numOfLettersPerLIne = k * (width - styleSheet.leftMargin - styleSheet.rightMargin) / StyleSheet.fontSize
    
    let numOfLines = ceil( CGFloat(length) / numOfLettersPerLIne )
    // 466 / 20
    let pixPerLine: CGFloat = StyleSheet.fontSize
    return numOfLines * pixPerLine
  }
  
  weak var textView: SingleCellTextView? = nil
  var textStorage: NSMutableAttributedString? {
    set {
      if let textStorage = newValue {
        textView?.setTextStorage(textStorage)
        
//        // LOOK FOR KEY .tempLink
//        var loc = 0
//        var range: NSRange = NSMakeRange(0, 0)
//        var containsHeader = false
//        while NSMaxRange(range) < textStorage.length {
//          if textStorage.attribute(.tempLink, at: loc, effectiveRange: &range) != nil {
//            containsHeader = true
//            break
//          }
//          loc = NSMaxRange(range)
//        }
        //
        //        tagView?.isHidden = !containsHeader
      }
    }
    get {
      return textView?.textStorage
    }
  }
  
  func setFramesetter(_ framesetter: MNFramesetter?) {
    textView?.setFramesetter(framesetter)
    
    //        // LOOK FOR KEY .tempLink
    //        var loc = 0
    //        var range: NSRange = NSMakeRange(0, 0)
    //        var containsHeader = false
    //        while NSMaxRange(range) < textStorage.length {
    //          if textStorage.attribute(.tempLink, at: loc, effectiveRange: &range) != nil {
    //            containsHeader = true
    //            break
    //          }
    //          loc = NSMaxRange(range)
    //        }
    //
    //        tagView?.isHidden = !containsHeader
  
  
}

  
  private weak var tagView: TagView? = nil
  
  func setTags(_ tags: [TagEntity]) {
    tagView?.tags = tags
  }

  var styleSheet: StyleSheet
  
  var findString: String? = nil {
    didSet {
      //if oldValue == findString { return }
      
      if findString != nil {
        
        let tempString = NSMutableString(string: findString!)
        // CONVERT
        if styleSheet.kanjiType == .zenkaku {
          tempString.convertKanjiNumberToArabicNumber(convertToZenkaku: true)
        }else if styleSheet.kanjiType == .hankaku {
          tempString.convertKanjiNumberToArabicNumber(convertToZenkaku: false)
        }
        let convertedFindString = tempString as String
        
        let string = textView!.textStorage.string as NSString
        var range: NSRange = NSMakeRange(0, string.length)
        let findColor: CGColor
        
        if styleSheet.backgroundColorScheme.inverted == true {
          findColor = styleSheet.backgroundColorScheme.tableSelectionColor.cgColor
        }else {
          findColor = UIColor.yellow.cgColor
        }
        
        while true {
          let matchedRange = string.range(of: convertedFindString, options: [.regularExpression], range: range, locale: nil)
          
          if matchedRange.location == NSNotFound { break }
          if matchedRange.length == 0 { break }

          range = NSMakeRange( NSMaxRange(matchedRange), string.length - NSMaxRange(matchedRange))
          
          let attr = [NSAttributedString.Key.tempBackgroundColor: findColor]
          textView?.textStorage.addAttributes(attr, range: matchedRange)
        }
      }
    }
  }
  
  
  init(frame: CGRect, styleSheet: StyleSheet) {
    self.styleSheet = styleSheet
    super.init(frame: frame)
    
    var frame = frame
    frame = CGRect(x: styleSheet.leftMargin, y: 0, width: bounds.size.width - styleSheet.leftMargin - styleSheet.rightMargin, height: bounds.size.height)

    let textView = SingleCellTextView(frame: frame)
    addSubview(textView)
    #if os(OSX)
    autoresizingMask = [.width, .height]
    #else
    autoresizingMask = [.flexibleWidth, .flexibleHeight]
    #endif

    frame = CGRect(x: 0, y: 0, width: 20, height: 20)
    let tagView = TagView(frame: frame)
    
    #if os(OSX)
    autoresizingMask = [.width, .minYMargin]
    #else
    tagView.autoresizingMask = [.flexibleBottomMargin, .flexibleWidth]
    #endif

    
    addSubview(tagView)
    self.tagView = tagView
    self.textView = textView
    
    
    
    //contentView.clipsToBounds = true
    //clipsToBounds = true
    backgroundColor = UIColor.clear
    //    self.layer.shouldRasterize = true
    //    self.layer.rasterizationScale = UIScreen.main.scale
    //    let actions =  ["sublayers" : NSNull(), "onOrderOut" : NSNull(), "backgroundColor" : NSNull(), "contents" : NSNull(), "onOrderIn" : NSNull(), "bounds" : NSNull() ]
    //    self.layer.actions = actions
    //    layer.shouldRasterize = true
    //    layer.rasterizationScale = UIScreen.main.scale
    //    layer.actions = actions
    //    textView.layer.actions = actions
    //    isOpaque = true
  }
  override var isOpaque: Bool {
    get {
      return true
    }
    set {
    }
  }
  var isVertical: Bool {
    set {
      self.textView?.isVertical = newValue
    }
    get {
      return self.textView!.isVertical
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  #if os(iOS)
  override func layoutSubviews() {
    
    super.layoutSubviews()
    
    if let frame = textView?.frame  {
      let newFrame = CGRect(x: styleSheet.leftMargin, y: 0, width: bounds.size.width - styleSheet.leftMargin - styleSheet.rightMargin, height: bounds.size.height)
      
      if newFrame.equalTo(frame) == false {
        textView?.resetLayout()
        textView?.unobscure()
        textView?.sizeToFit()
        textView?.frame = newFrame
      }
    }
  }
  #endif
  
  #if os(OSX)
  
  override func layout() {
    
    super.layout()
    
    textView?.resetLayout()
    textView?.unobscure()
    textView?.sizeToFit()
    
    textView?.frame = CGRect(x: styleSheet.leftMargin, y: 0, width: bounds.size.width - styleSheet.leftMargin - styleSheet.rightMargin, height: bounds.size.height)
  }
  #endif
  
  func characterIndexAtPointInCell(_ point: CGPoint) -> Int {
    let toTextView = self.convert(point, to: textView)
    
    if let indexedPosition = textView?.closestPosition(to: toTextView) {
    return Int(indexedPosition.textIndex)
    }
    return NSNotFound
  }
  
  func wordRange(at point: CGPoint) -> NSRange? {
    guard let textView = textView else { return nil }
    return textView.wordRange(at: point)
  }
  
  func pointForCharacterIndex(_ index: Int) -> CGPoint? {
    guard let rect = rectForCharacterIndex(index) else { return nil }
    guard let textView = textView else { return nil }
    let convertedRect = textView.convert(rect, from: self)
    return CGPoint(x: convertedRect.midX, y: convertedRect.midY )
  }
  
  func rectForCharacterIndex(_ index: Int) -> CGRect? {
    guard let textView = textView else { return nil }
    guard textView.textStorage.length > 0 else { return nil }
    var index = index
    if textView.textStorage.length <= index {
      index = textView.textStorage.length - 1
    }
    
    var rect = textView.enclosingRect(for: NSMakeRange(index,1))
    rect.origin.x += styleSheet.leftMargin
    return rect
  }
  
  func findStringRect(for match: Match) -> CGRect? /* returns offset y */ {
    var indexCount = match.matchIndex
    
    if match.matchPattern == .selectionNotes {
      // LOOK FOR UUID
      if match.userInfo == nil { return nil }
      return rectForAttribute(key: .highlightSerialIdName, value: match.userInfo!, tail: true)
    }
    
    let string = textView!.textStorage.string as NSString

    var range: NSRange = NSMakeRange(0, string.length)
    
    while true {
      var matchedRange:NSRange
      if findString != nil {
        matchedRange = string.range(of: findString!, options: [.regularExpression], range: range, locale: nil)
      }else {
        matchedRange =  match.matchRange
      }
      
      if matchedRange.location == NSNotFound { return nil }
      
      if indexCount == 0 && textView != nil {
        var rect = textView!.enclosingRect(for: matchedRange)
        rect.origin.x += styleSheet.leftMargin
        return rect
      }
      
      range = NSMakeRange( NSMaxRange(matchedRange), string.length - NSMaxRange(matchedRange))
      
      indexCount -= 1
    }
  }
  
  func rectForAttribute(key: NSAttributedString.Key, value: String, tail: Bool = false) -> CGRect? {
    guard let textStorage = textView?.textStorage else { return nil }
    
    var range: NSRange = NSMakeRange(0, textStorage.length)
    var n = 0
    while true {
      let thisValue = textStorage.attribute(key, at: n, longestEffectiveRange: &range, in: NSMakeRange(0, textStorage.length)) as? String
      
      if thisValue == value {
        if tail {
          range.location = NSMaxRange(range) - 1
          range.length = 1
        }
        
        var rect = textView!.enclosingRect(for: range)
        rect.origin.x += styleSheet.leftMargin
        return rect
      }else {
        n = NSMaxRange(range)
        if n >= textStorage.length { return nil }
      }
    }
  }
  
  func rectForAnchor(_ anchor: String) -> CGRect? {
    guard let textStorage = textView?.textStorage else { return nil }
    
    var range: NSRange = NSMakeRange(0, textStorage.length)
    var n = 0
    while true {
      let thisAnchor = textStorage.attribute(.anchor, at: n, longestEffectiveRange: &range, in: NSMakeRange(0, textStorage.length)) as? String
      
      if thisAnchor?.hasPrefix(anchor) == true {
        var rect = textView!.enclosingRect(for: range)
        rect.origin.x += styleSheet.leftMargin
        return rect
      }else {
        n = NSMaxRange(range)
        if n >= textStorage.length { return nil }
      }
    }
  }
  
  var stringLength: Int {
    return textView?.textStorage.length ?? 0
  }
  
  var selectedRange: NSRange? {
    didSet {
      // if oldValue != nil && selectedRange != nil && NSEqualRanges(oldValue!, selectedRange!) == true { return }
      if selectedRange == nil {
        textView?.selectedRange = NSMakeRange(0, 0)
      }else {
        textView?.selectedRange = selectedRange!
      }
    }
  }
  
  func setTemporaryHighlightRange(_ range: NSRange?, attributes: [NSAttributedString.Key: Any]?) {
    // if oldValue != nil && selectedRange != nil && NSEqualRanges(oldValue!, selectedRange!) == true { return }
    
//    if range != nil {
//      if nil != textView?.textStorage.attribute(.blockQuoteBackgroundColor, at: range!.location, effectiveRange: nil)  { return }
//      if nil != textView?.textStorage.attribute(.blockQuoteBackgroundColor, at: NSMaxRange(range!), effectiveRange: nil)  { return }
//    }
    
    if range == nil {
      textView?.setTemporaryHighlight(NSMakeRange(NSNotFound, 0), attributes: nil)
    }else {
      textView?.setTemporaryHighlight(range!, attributes: attributes)
    }
  }
  
  func setTouchHighlightKey(_ key: NSAttributedString.Key?, andValue value: NSObject?) {
    textView?.setTouchHighlightKey(key?.rawValue, andValue: value)
  }
}


