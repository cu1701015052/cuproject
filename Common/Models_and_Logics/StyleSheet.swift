//
//  StyleSheet.swift
//  LawXML
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
#if os(iOS) || os(watchOS)
  import UIKit
#else
  import AppKit
#endif

/*
 case articleCaption -> caption
 case articleTitle -> title
 case paragraphNum -> title
 case itemTitle -> title
 case subitem1Title -> title
 case subitem2Title -> title
 case subitem3Title -> title
 case body -> body
 case chapter -> ?
 */

final class StyleSheet {
  static var fontSize: CGFloat = {
    if UserDefaults.standard.object(forKey: "TextViewCell-fontSize") == nil { return 17 }
    
    var value = CGFloat( UserDefaults.standard.double(forKey: "TextViewCell-fontSize") )
    if value < 4 { value = 4 }
    if value > 50 { value = 50 }
    return value
    
    }() {
    didSet {
      UserDefaults.standard.set(fontSize, forKey:"TextViewCell-fontSize")
    }
  }
  
  var leftMargin: CGFloat = 20
  var rightMargin: CGFloat = 2

  var lawTitle: [NSAttributedString.Key: Any] = [:]
  var caption: [NSAttributedString.Key: Any] = [:]
  var title: [NSAttributedString.Key: Any] = [:]
  var body: [NSAttributedString.Key: Any] = [:]
  var parenthesis: [NSAttributedString.Key: Any] = [:]
  var parenthesisSecondary: [NSAttributedString.Key: Any] = [:]
  var parenthesisTertiary: [NSAttributedString.Key: Any] = [:]

  var blockquote: [NSAttributedString.Key: Any] = [:]
  var quote: [NSAttributedString.Key: Any] = [:]

  var backgroundColorScheme: BackgroundColorSchemeType = .Paper
  
  var addParenthesisAttributes = false
  var kanjiType: KanjiType = .kanji
  //  var linkStyle: Any
  //  func paragraphStyleForHierarchy(_ :Hierarchy) -> NSParagraphStyle
  
  var lawTitleParagraph: NSParagraphStyle?
  var chapterTitleParagraph: NSParagraphStyle?
  var captionParagraph: NSParagraphStyle?
  var primaryParagraph: NSParagraphStyle?
  var secondaryParagraph: NSParagraphStyle?
  var tertiaryParagraph: NSParagraphStyle?
  var subitem2Paragraph: NSParagraphStyle?
  var subitem3Paragraph: NSParagraphStyle?
  var subitem4Paragraph: NSParagraphStyle?
  var subitem5Paragraph: NSParagraphStyle?
  var subitem6Paragraph: NSParagraphStyle?
  var subitem7Paragraph: NSParagraphStyle?

  var preambleParagraph: NSParagraphStyle?
  
  private var blockquoteParagraph: NSParagraphStyle = {
    let blockquoteParagraphStyle = NSMutableParagraphStyle()
    blockquoteParagraphStyle.headIndent = 5
    blockquoteParagraphStyle.firstLineHeadIndent = 5
    blockquoteParagraphStyle.tailIndent = 3
    return blockquoteParagraphStyle
  }()
  
  private static let linePadding: NSAttributedString = {
    #if os(watchOS)
      let attrstr = NSAttributedString(string: "\n", attributes: [.font: UIFont.systemFont(ofSize: 10)])
      return attrstr

    #else
      var font = CTFontCreateWithName("HiraMinProN-W3" as CFString, 1, nil)
      let attrstr = NSAttributedString(string: "\n", attributes: [.font: font])
      
      return attrstr
    #endif
  }()
  
  func blockquote(_ bookmark: Bookmark) -> NSMutableAttributedString {
    let string = (bookmark.notes ?? "") + "\n"
    let attrstr = NSMutableAttributedString(string: string, attributes: blockquote)
    attrstr.insert(StyleSheet.linePadding, at: attrstr.length)
    attrstr.insert(StyleSheet.linePadding, at: 0)
    
    attrstr.addAttribute(.paragraphStyle, value: blockquoteParagraph, range: NSMakeRange(0, attrstr.length))
    
    let blockQuoteID = NSAttributedString.Key.ControlType.blockquote + bookmark.uuid
    let color = backgroundColorScheme.blockQuoteColor.cgColor
    
    attrstr.addAttributes([.blockQuoteBackgroundColor: color, .controlName: blockQuoteID]
      , range: NSMakeRange(0, attrstr.length))

    
    return attrstr
  }
  
  func developAttributedString(_ val: NSAttributedString) -> NSMutableAttributedString {
    var idx = 0
    var range = NSMakeRange(0,0)
    let attr = NSMutableAttributedString(attributedString: val)
    
    //ATTRIBUTES
    while idx < attr.length {
      if let value = attr.attribute(.elementNameAttributeName, at: idx, longestEffectiveRange: &range, in: NSMakeRange(0,attr.length)) as? String {
        var style: [NSAttributedString.Key: Any] = body
        switch value {
        case "LawTitle": style = lawTitle
        case "ArticleCaption": style = caption
        case "ArticleTitle", "ParagraphNum", "ItemTitle", "Subitem1Title", "Subitem2Title", "Subitem3Title", "Subitem4Title", "Subitem5Title", "Subitem6Title", "Subitem7Title", "SupplProvisionLabel", "TOCLabel", "ChapterTitle", "SectionTitle","SubsectionTitle", "PartTitle", "DivisionTitle",  "TableStructTitle", "AppdxTableTitle", "RemarksLabel", "ArithFormulaNum", "NoteStructTitle", "AppdxFigTitle", "FormatStructTitle", "StyleStructTitle", "FigStructTitle", "TOCPreambleLabel", "AppdxNoteTitle", "RelatedArticleNum", "SupplProvisionAppdxStyleTitle", "AppdxStyleTitle", "AppdxFormatTitle", "SupplProvisionAppdxTableTitle"  :
          style = title
          
        default: break
        }
        
        attr.addAttributes(style, range: range)
      }
      
      idx = NSMaxRange(range)
    }
    
    
    // PARAGRAPH STYLE
    idx = 0
    while idx < attr.length {
      if let value = attr.attribute(.elementNameAttributeName, at: idx, longestEffectiveRange: &range, in: NSMakeRange(0,attr.length)) as? String {
        var style: [NSAttributedString.Key: Any] = [:]
        
        switch value {
        case "ArticleCaption": style[.paragraphStyle] = captionParagraph
        case "ParagraphNum", "ArticleTitle": style[.paragraphStyle] = primaryParagraph
        case "ItemTitle": style[.paragraphStyle] = secondaryParagraph
        case "Subitem1Title": style[.paragraphStyle] = tertiaryParagraph
        case "Subitem2Title": style[.paragraphStyle] = subitem2Paragraph
        case "Subitem3Title": style[.paragraphStyle] = subitem3Paragraph
        case "Subitem4Title": style[.paragraphStyle] = subitem4Paragraph
        case "Subitem5Title": style[.paragraphStyle] = subitem5Paragraph
        case "Subitem6Title": style[.paragraphStyle] = subitem6Paragraph
        case "Subitem7Title": style[.paragraphStyle] = subitem7Paragraph
        case "ChapterTitle", "SectionTitle","SubsectionTitle", "PartTitle", "DivisionTitle", "SupplProvisionLabel":  style[.paragraphStyle] = chapterTitleParagraph
        default: break
          
        }
        
        attr.addAttributes(style, range: NSMakeRange(0,attr.length))
        break
      }
      
      idx = NSMaxRange(range)
    }
    
    if attr.length == 1 && attr.string == "\n" { // EMPTY LINE
      
      var style: [NSAttributedString.Key: Any] = [:]
      style[.paragraphStyle] = primaryParagraph
      attr.addAttributes(style, range: NSMakeRange(0,attr.length))
    }
    
    // ADD FETCHED LINKS
    
    if addParenthesisAttributes {
      attr.appendParenthesisStyle(parenthesis, secondary: parenthesisSecondary, tertiary: parenthesisTertiary)
    }
    
    // CONVERT Kanji
    if kanjiType == .zenkaku {
      attr.convertKanjiNumberToArabicNumber(convertToZenkaku: true)
    }else if kanjiType == .hankaku {
      attr.convertKanjiNumberToArabicNumber(convertToZenkaku: false)
    }
    
    return attr
  }
}

