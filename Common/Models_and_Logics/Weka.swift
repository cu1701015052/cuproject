//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation
#if os(iOS) || os(watchOS)
  import UIKit
#else
  import AppKit
#endif

#if os(OSX)
  typealias UIView = NSView
  typealias UIEdgeInsets = NSEdgeInsets
  
  extension NSEdgeInsets {
    static var zero: NSEdgeInsets { return NSEdgeInsetsZero }
  }
#endif

func WLoc( _ title: String, in bundleOrIdentifier: Any? = nil) -> String {
  
  if bundleOrIdentifier == nil {
    return NSLocalizedString(title, comment: "" )
  }
  
  if bundleOrIdentifier is String {
    return NSLocalizedString(title, tableName: nil, bundle: Bundle(identifier: bundleOrIdentifier as! String)!, value: "", comment: "")
  }
    return NSLocalizedString(title, tableName: nil, bundle: bundleOrIdentifier as! Bundle, value: "", comment: "")
}

func WLoc( _ title: String, withArguments args: CVarArg) -> String {
  return NSString(format: NSLocalizedString(title, comment: "" ) as NSString, args) as String
}

#if os(iOS) || os(watchOS)
	
  func RGBA(_ r: CGFloat,_ g: CGFloat,_ b: CGFloat,_ a: CGFloat) -> UIColor  {
    return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
  }
#else
  func RGBA(_ r: CGFloat, _ g: CGFloat, _ b: CGFloat, _ a: CGFloat) -> NSColor  {
    return NSColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
  }
#endif
#if os(iOS)
let DEVICE_IS_IPAD: Bool = ( UI_USER_INTERFACE_IDIOM() == .pad )
#endif

func showError( _ error: Error! ) {
	print("error \(error.localizedDescription)")
}

//func displayErrorAlertIfNecessary( error:NSError? ) {
//	if error == nil { return }
//	
//	if NSThread.isMainThread() == false {
//		print(" (*_*) Please call me on main thread" )
//		return
//	}
//	
//	#if os(iOS)
//		
//		let alert = UIAlertView(title: error!.localizedDescription, message: "", delegate: nil, cancelButtonTitle: "OK")
//		alert.show()
//		
//	#elseif os(OSX)
//		let message = error!.localizedDescription
//		
//		let alert = NSAlert()
//		alert.messageText = "Oops"
//		alert.informativeText = message
//		alert.alertStyle = NSAlertStyle.CriticalAlertStyle
//		alert.addButtonWithTitle(WLoc("OK"))
//		alert.runModal()
//		
//	#endif
//
//}


protocol PresentableError: Error {
    #if os(iOS)
    func displayErrorAlert(in: UIViewController)
    #endif
}


#if os(iOS)
    
    func displayErrorAlertIfNecessary( _ error: Error?, in viewController: UIViewController ) {
        if error == nil { return }
        
        if Thread.isMainThread == false {
            print(" (*_*) Please call me on main thread" )
            return
        }
        
        ShowAlertMessage( error!.localizedDescription, "", viewController )
    }
    
#elseif os(OSX)
    func displayErrorAlertIfNecessary( error: Error? ) {
        if error == nil { return }
        
        if Thread.isMainThread == false {
            print(" (*_*) Please call me on main thread" )
            return
        }
        let message = error!.localizedDescription
        
        let alert = NSAlert()
        alert.messageText = "Oops"
        alert.informativeText = message
        alert.alertStyle = .critical
        alert.addButton(withTitle: WLoc("OK"))
        alert.runModal()
}
#endif

func stringFromUuid_t(_ uuid: uuid_t) -> String {
  return NSString(format:"%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x", uuid.0, uuid.1, uuid.2, uuid.3, uuid.4, uuid.5, uuid.6, uuid.7, uuid.8, uuid.9, uuid.10, uuid.11, uuid.12, uuid.13, uuid.14, uuid.15) as String
}

func shortUUIDString() -> String {
	
	let base64TailBuffer = "="
	let tempUuid = UUID()
	var tempUuidBytes = [UInt8](repeating: 0, count: 16)
	(tempUuid as NSUUID).getBytes(&tempUuidBytes)
	let data = NSData(bytes: &tempUuidBytes, length: 16)
	let base64 = data.base64EncodedString(options: Data.Base64EncodingOptions())
	
	let string = base64.replacingOccurrences(of: "/", with: "_", options: [], range: nil)
	
	return string.replacingOccurrences(of: base64TailBuffer, with: "", options: [], range: nil)
}

func UUIDData() -> Data {
  
  let tempUuid = UUID()
  var tempUuidBytes = [UInt8](repeating: 0, count: 16)
  (tempUuid as NSUUID).getBytes(&tempUuidBytes)
  let data = NSData(bytes: &tempUuidBytes, length: 16)
  
  return data as Data
}

func uuidFromData(_ data: Data) -> UUID! {
  guard data.count == 16 else { return nil }
  var tempUuidBytes = [UInt8](repeating: 0, count: 16)
  for i in 0 ..< 16 {
    tempUuidBytes[i] = data[i]
  }
  return NSUUID(uuidBytes: tempUuidBytes) as UUID
}

func uuid_tFromData(_ data: Data) -> uuid_t! {
  
  guard data.count == 16 else { return nil }
  var tempUuidBytes = [UInt8](repeating: 0, count: 16)
  for i in 0 ..< 16 {
    tempUuidBytes[i] = data[i]
  }
  let uuid: uuid_t = (data[0], data[1], data[2], data[3],
                data[4], data[5], data[6], data[7],
                data[8], data[9], data[10], data[11],
                data[12], data[13], data[14], data[15])
  return uuid
}

func dataFromUuid(_ uuid: uuid_t) -> Data {
  let tempUuid = UUID(uuid: uuid)
  var tempUuidBytes = [UInt8](repeating: 0, count: 16)
  (tempUuid as NSUUID).getBytes(&tempUuidBytes)
  let data = NSData(bytes: &tempUuidBytes, length: 16)
  
  return data as Data
}

func uuidDataFromString(_ string: String) -> Data {
  return dataFromUuid(UUID(uuidString: string)!.uuid)
}

func compareUuids(_ uuidt1: inout uuid_t, _ uuidt2: inout uuid_t) -> Bool {
  if uuidt1.0 != uuidt2.0 { return false }
  if uuidt1.1 != uuidt2.1 { return false }
  if uuidt1.2 != uuidt2.2 { return false }
  if uuidt1.3 != uuidt2.3 { return false }
  if uuidt1.4 != uuidt2.4 { return false }
  if uuidt1.5 != uuidt2.5 { return false }
  if uuidt1.6 != uuidt2.6 { return false }
  if uuidt1.7 != uuidt2.7 { return false }
  if uuidt1.8 != uuidt2.8 { return false }
  if uuidt1.9 != uuidt2.9 { return false }
  if uuidt1.10 != uuidt2.10 { return false }
  if uuidt1.11 != uuidt2.11 { return false }
  if uuidt1.12 != uuidt2.12 { return false }
  if uuidt1.13 != uuidt2.13 { return false }
  if uuidt1.14 != uuidt2.14 { return false }
  if uuidt1.15 != uuidt2.15 { return false }
  
  return true
}


func joinData(_ data: [Data]) -> Data {
  let mdata = NSMutableData()
  data.forEach { mdata.append($0) }
  return mdata as Data
}

func divideData(_ data: Data, count: Int) -> [Data] {
  if data.count == 0 { return [] }
  var dataArray: [Data] = []
  let length = data.count / count
  
  for hoge in 0 ..< count {
    let range = NSMakeRange( hoge * length, length)
    dataArray.append((data as NSData).subdata(with: range))
  }
  return dataArray
}

func appVersionString() -> String {
  let bundle = CFBundleGetMainBundle()
  let versionString = CFBundleGetValueForInfoDictionaryKey(bundle, "CFBundleShortVersionString" as CFString)
  let buildVersionString = CFBundleGetValueForInfoDictionaryKey(bundle, "CFBundleVersion" as CFString)
  
  return "Version \(versionString!) (\(buildVersionString!))";
}

//func displayMessage( title:String ) {
//	
//	#if os(iOS)
//		
//		let alert = UIAlertView(title: title, message: "", delegate: nil, cancelButtonTitle: "OK")
//		alert.show()
//		
//	#elseif os(OSX)
//		
//		let alert = NSAlert()
//		alert.messageText = title
//		alert.informativeText = ""
//		alert.alertStyle = NSAlertStyle.CriticalAlertStyle
//		alert.addButtonWithTitle(WLoc("OK"))
//		alert.runModal()
//		
//	#endif
//	
//}

#if os(OSX)
	
	func loadingImages(tintColor: NSColor) -> [NSImage] {
		
		var images = [NSImage]()
		let size = NSMakeSize(30.0, 30.0);
		let fraction: Float =  0 - 1.0/36.0 * 360.0
		
		for i in 0...36 {
			
			let image = NSImage(size: size)
			image.lockFocus()
			
      let ctx = NSGraphicsContext.current!.cgContext
			
      let point = CGPoint(x: 15, y: 15)
			let radius = CGFloat(12.0)
			let startAngle = CGFloat(Float(i) * fraction)
			let endAngle = CGFloat(fraction * 33) + startAngle
			
			let bezier = NSBezierPath()
			bezier.appendArc( withCenter: point, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
			
			bezier.lineWidth = 2.0
			ctx.setStrokeColor(tintColor.cgColor)
			ctx.setLineCap( CGLineCap.butt)
			bezier.stroke()
			
			image.unlockFocus()
			images.append( image )
		}
		
		return images
	}
	
	func colorizedImage(originalImage: NSImage, withTintColor tintColor: NSColor, alpha: CGFloat ) -> NSImage {
		
		let image = NSImage(size: originalImage.size)
		image.lockFocus()
		
    let rect = CGRect(x: 0, y: 0, width: originalImage.size.width, height: originalImage.size.height);
		let ctx = NSGraphicsContext.current!.cgContext
		
		ctx.setAlpha(alpha);
		ctx.setFillColor(tintColor.cgColor);
		
    __NSRectFill( rect );
		
		ctx.setAlpha(1.0);
		ctx.setBlendMode(CGBlendMode.destinationIn);
		
		var theRect = NSMakeRect(0,0, originalImage.size.width, originalImage.size.height);
		let cgimage = originalImage.cgImage(forProposedRect: &theRect, context: nil ,hints: nil)
    ctx.draw(cgimage!, in: rect)
		
		image.unlockFocus()
		
		return image;
		
	}

typealias UIImage = NSImage
extension NSImage {
 
  func colorizedImage(withTint tintColor: NSColor, alpha: CGFloat, glow: Bool ) -> NSImage {
    
    let image = NSImage(size: self.size)
    image.lockFocus()
    
    let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height);
    let ctx = NSGraphicsContext.current!.cgContext
    
    ctx.setAlpha(alpha);
    ctx.setFillColor(tintColor.cgColor);
    
    __NSRectFill( rect );
    
    ctx.setAlpha(1.0);
    ctx.setBlendMode(CGBlendMode.destinationIn);
    
    var theRect = NSMakeRect(0,0, self.size.width, self.size.height);
    let cgimage = self.cgImage(forProposedRect: &theRect, context: nil ,hints: nil)
    ctx.draw(cgimage!, in: rect)
    
    image.unlockFocus()
    
    return image;
    
  }
}

#endif


#if os(iOS)
	
	func ShowAlertMessage(_ title: String, _ message: String, _ inViewController: UIViewController!) {
		
    let alertController = UIAlertController(title: WLoc(title), message: WLoc(message), preferredStyle: .alert)
		let cancel = UIAlertAction(title: WLoc("OK"), style: .cancel) { (action) -> Void in
		}
		
		alertController.addAction(cancel)
		inViewController?.present(alertController, animated: true, completion: nil)
	}
	
	func loadingImages(_ tintColor: UIColor, size: CGFloat) -> [UIImage] {
		
		var images = [UIImage]()
		let size = CGSize(width: size, height: size);
		
		let fraction: Double = 1.0/36.0 * 2.0 * Double.pi
		
		for i in 0...36 {
			UIGraphicsBeginImageContextWithOptions(size, false, 0.0);
			let ctx = UIGraphicsGetCurrentContext();
			
			let point = CGPoint(x: 15,y: 15)
			let radius = CGFloat(12.0)
			let startAngle = CGFloat(Double(i) * fraction)
			let endAngle = CGFloat(fraction * 33) + startAngle
			let path = UIBezierPath(arcCenter: point, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
			
			path.lineWidth = 2.0
			ctx?.setStrokeColor(tintColor.cgColor)
			path.stroke()
			
			let image = UIGraphicsGetImageFromCurrentImageContext();
			UIGraphicsEndImageContext();
			
			images.append( image! )
		}
		
		return images
	}
	
	func loadingImages(_ tintColor: UIColor) -> [UIImage] {
		return loadingImages(tintColor, size: 30.0)
	}
	
	extension UIImage {
    func colorizedImage(withTint tintColor: UIColor, alpha: CGFloat, glow outerGlow: Bool) -> UIImage {
			UIGraphicsBeginImageContextWithOptions(self.size, false, UIScreen.main.scale)
			let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
			let ctx = UIGraphicsGetCurrentContext()
			var t = CGAffineTransform.identity
			t = t.scaledBy(x: 1, y: -1)
			t = t.translatedBy(x: 0, y: -rect.size.height)
			ctx?.concatenate(t)
			ctx?.setAlpha(alpha)
			ctx?.setFillColor(tintColor.cgColor)
			UIRectFill(rect)
			
			ctx?.setAlpha(1.0)
			ctx?.setBlendMode(.destinationIn)
			ctx?.draw(self.cgImage!, in: rect)
			var theImage = UIGraphicsGetImageFromCurrentImageContext()
			
			if outerGlow {
				ctx?.setShadow(offset: CGSize(width: 0, height: 0), blur: 3.0, color: tintColor.cgColor)
				ctx?.setBlendMode(.normal)
				ctx?.setAlpha(0.5)
				ctx?.draw( (theImage?.cgImage!)!, in: rect)
				theImage = UIGraphicsGetImageFromCurrentImageContext()
			}
			UIGraphicsEndImageContext()
			return theImage!
		}
	}
	
#endif

#if os(iOS)

class ObjCClass : NSObject {

	
	class func showAlertMessage(_ title: String, message: String, inViewController controller: UIViewController!) {
		
		ShowAlertMessage(title, message, controller)
	}
}

#endif


extension URL {
  func setExcludedFromBackup(_ flag: Bool = true) -> Bool {
    
    let success: Bool
    var url = self
    do {
      var values = URLResourceValues()
      values.isExcludedFromBackup = flag
      try url.setResourceValues( values )
      success = true
    } catch _ {
      success = false
    }
    return success
  }
}

