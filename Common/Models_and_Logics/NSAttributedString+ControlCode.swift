//
//  NSAttributedString+ControlCode.swift
//  LawXML
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import Foundation

extension NSAttributedString.Key {
  static let controlName = NSAttributedString.Key(rawValue: "MNControl")
  static let blockSelectionName = NSAttributedString.Key(rawValue: "BlockSelection")
  static let highlightSerialIdName = NSAttributedString.Key(rawValue: "HighlightSerialId")
  static let bookmarkNotesSerialIdName = NSAttributedString.Key(rawValue: "BookmarkNotesSerialId")
  static let blockquoteKajōUuid = NSAttributedString.Key(rawValue: "uuid")
  static let mask = NSAttributedString.Key(rawValue: "MNMaskStyle")
  static let tempBackgroundColor = NSAttributedString.Key(rawValue: "MNTempBColor")
  static let tempLink = NSAttributedString.Key(rawValue: "MNTempLink")
  static let tempLinkColor = NSAttributedString.Key(rawValue: "MNTempLinkColor")
  static let tempBookmarkCount = NSAttributedString.Key(rawValue: "MNTempBookmarkCount")
  static let tempTempLink = NSAttributedString.Key(rawValue: "TempMNTempLink")
  static let tempTempLinkColor = NSAttributedString.Key(rawValue: "TempMNTempLinkColor")
  static let embeddedAttachement = NSAttributedString.Key(rawValue: "MNAttachment")
  static let elementNameAttributeName = NSAttributedString.Key(rawValue: "ElementAttributeName")
  static let blockQuoteBackgroundColor = NSAttributedString.Key(rawValue: "BlockQuoteColor")
  static let kaisei = NSAttributedString.Key(rawValue: "Kaisei")

  static let highlightColor = NSAttributedString.Key(rawValue: "OABackgroundColorAttributeName")
  
  static let anchor = NSAttributedString.Key(rawValue: "ANCHOR")
  static let ji = NSAttributedString.Key(rawValue: "JI")
  static let groupSelection = NSAttributedString.Key(rawValue: "BlockSelection")
  
  static let kanji = NSAttributedString.Key(rawValue: "OriginalKanji")
  static let ruby = NSAttributedString.Key(rawValue: "RubyText")

  static let rubyTextId = NSAttributedString.Key(rawValue: "RubyTextId")
  static let diffOperation = NSAttributedString.Key(rawValue: "Diff")
  static let diffOperationDeletedString = NSAttributedString.Key(rawValue: "DiffDeleted")

  static let reconstructionIndex = NSAttributedString.Key(rawValue: "rix")

  enum ControlType {
    static let inlineNote = "Notes"
    static let blockquote = "blockquote"
  }
  
}
extension NSAttributedString {
  
  func removeControlCode() -> NSMutableAttributedString {
    let mattr = NSMutableAttributedString(attributedString: self)
    
    // REMOVE CONTROL CODES
    for i in (0 ..< mattr.length).reversed() {
      let control = mattr.attribute(.controlName, at: i, effectiveRange: nil)
      if control != nil {
        mattr.deleteCharacters(in: NSMakeRange(i,1))
      }
    }
    
    return mattr
  }
  
  func removeTopSpaces() -> NSMutableAttributedString {
    let mattr = NSMutableAttributedString(attributedString: self)
    
    // REMOVE CONTROL CODES
    for _ in (0 ..< mattr.length) {
      if mattr.length == 0 { break }
      let moji = (mattr.string as NSString).substring(with: NSRange(location: 0, length: 1))
      if moji == " " || moji == "\n" {
        mattr.deleteCharacters(in: NSRange(location: 0, length: 1))
      }else {
        break
      }
    }
    
    return mattr
  }
  
  func actualRangeWithoutControlCharacters(with range: NSRange) -> NSRange {
    let tempHighlightAttrName = NSAttributedString.Key(rawValue: "HighlightRange")
    let mattr = NSMutableAttributedString(attributedString: self)
    
    mattr.addAttributes([tempHighlightAttrName: ""], range: range)
    
    // REMOVE CONTROL CODES
    for i in (0 ..< mattr.length).reversed() {
      let control = mattr.attribute(.controlName, at: i, effectiveRange: nil)
      if control != nil {
        mattr.deleteCharacters(in: NSMakeRange(i,1))
      }
    }
    
    var actualHighlightRange = NSMakeRange(NSNotFound,0)
    var found = false
    for i in 0 ..< mattr.length {
      
      if nil != mattr.attribute(tempHighlightAttrName, at: i, longestEffectiveRange: &actualHighlightRange, in: NSMakeRange(0, mattr.length)) {
        found = true
        break
      }
    }
    if found == false {
      actualHighlightRange = NSMakeRange(NSNotFound,0)
    }
    return actualHighlightRange
  }
  
  func countLocationIncludingControlCode(at index: Int) -> Int {
    guard index < length else { return length - 1 }
    var countedLocation = index
    var n = 0
    var range = NSMakeRange(0, 0)
    while true {
      if let _ = attribute(.controlName, at: n, longestEffectiveRange: &range, in: NSMakeRange(0, index)) {
        countedLocation += range.length
      }
      
      n = NSMaxRange(range)
      if n >= index { break }
    }
    return countedLocation
  }
  
  func highlightSerialIdsInRange(_ range: NSRange) -> [String] {
    var serialIds: [String] = []
    var idx: Int = range.location
    while true {
      if idx >= length { break }
      var longestEffectiveRange: NSRange = NSMakeRange(0,0)
      if let serialId = attribute(.highlightSerialIdName, at: idx, longestEffectiveRange: &longestEffectiveRange, in: range) {
        serialIds.append(serialId as! String)
      }
      
      idx = NSMaxRange(longestEffectiveRange)
      if idx >= NSMaxRange(range) || idx >= length { break }
    }
    return serialIds
  }
  
}
