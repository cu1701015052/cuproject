#import <Foundation/Foundation.h>
//#import <Cocoa/Cocoa.h>
#import <CommonCrypto/CommonDigest.h>

CFStringRef FileMD5HashCreateWithPath(CFStringRef filePath,
                                      size_t chunkSizeForReadingData);
@interface NSData (NSDataExtension)

// Returns range [start, null byte), or (NSNotFound, 0).
- (NSRange) rangeOfNullTerminatedBytesFrom:(int)start;

//	// Canonical Base32 encoding/decoding.
//+ (NSData *) dataWithBase32String:(NSString *)base32;
//- (NSString *) base32String;
//
//	// COBS is an encoding that eliminates 0x00.
//- (NSData *) encodeCOBS;
//- (NSData *) decodeCOBS;

	// ZLIB
- (NSData *) inflate;
- (NSData *) deflate;
- (NSData *)deflate:(NSInteger)compressionLevel;

//	//CRC32
//- (unsigned int)crc32;
//
//	// Hash
//- (NSData*) md5Digest;
//- (NSString*) md5DigestString;
//- (NSData*) sha1Digest;
//- (NSString*) sha1DigestString;
//- (NSData*) ripemd160Digest;
//- (NSString*) ripemd160DigestString;

@end

@interface NSString (MD5)
- (NSString *)_MD5String;
  + (NSString *)MD5StringFromFileAt:(NSURL*)url ;
@end
