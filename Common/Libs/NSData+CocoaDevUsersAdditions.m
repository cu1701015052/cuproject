#import "NSData+CocoaDevUsersAdditions.h"

#include <zlib.h>
#include <CommonCrypto/CommonDigest.h>


@implementation NSData (NSDataExtension)

// Returns range [start, null byte), or (NSNotFound, 0).
- (NSRange) rangeOfNullTerminatedBytesFrom:(int)start
{
	const Byte *pdata = [self bytes];
	int len = (int)[self length];
	if (start < len)
	{
		const Byte *end = memchr (pdata + start, 0x00, len - start);
		if (end != NULL) return NSMakeRange (start, end - (pdata + start));
	}
	return NSMakeRange (NSNotFound, 0);
}


- (NSData *)inflate
{
	if ([self length] == 0) return self;
	
	NSUInteger full_length = [self length];
	NSUInteger half_length = [self length] / 2;
	
	NSMutableData *decompressed = [NSMutableData dataWithLength: full_length + half_length];
	BOOL done = NO;
	int status;
	
	z_stream strm;
	strm.next_in = (Bytef *)[self bytes];
	strm.avail_in = (unsigned)[self length];
	strm.total_out = 0;
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
	
	if (inflateInit (&strm) != Z_OK) return nil;
	while (!done)
	{
		// Make sure we have enough room and reset the lengths.
		if (strm.total_out >= [decompressed length])
			[decompressed increaseLengthBy: half_length];
		strm.next_out = [decompressed mutableBytes] + strm.total_out;
		strm.avail_out = (uInt)( [decompressed length] - strm.total_out );
		
		// Inflate another chunk.
		status = inflate (&strm, Z_SYNC_FLUSH);
		if (status == Z_STREAM_END) done = YES;
		else if (status != Z_OK) break;
	}
	if (inflateEnd (&strm) != Z_OK) return nil;
	
	// Set real length.
	if (done)
	{
		[decompressed setLength: strm.total_out];
		return [NSData dataWithData: decompressed];
	}
	else return nil;
}

- (NSData *)deflate
{
	if ([self length] == 0) return self;
	
	z_stream strm;
	
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
	strm.opaque = Z_NULL;
	strm.total_out = 0;
	strm.next_in=(Bytef *)[self bytes];
	strm.avail_in = (uInt)[self length];
	
	if (deflateInit(&strm, Z_DEFAULT_COMPRESSION) != Z_OK) return nil;
	
	NSMutableData *compressed = [NSMutableData dataWithLength:16384];  // 16K chuncks for expansion
	
	do {
		
		if (strm.total_out >= [compressed length])
			[compressed increaseLengthBy: 16384];
		
		strm.next_out = [compressed mutableBytes] + strm.total_out;
		strm.avail_out = (uInt)( [compressed length] - strm.total_out );
		
		deflate(&strm, Z_FINISH);  
		
	} while (strm.avail_out == 0);
	
	deflateEnd(&strm);
	
	[compressed setLength: strm.total_out];
	return [NSData dataWithData: compressed];
}


- (NSData *)deflate:(NSInteger)compressionLevel
{
	if ([self length] == 0) return self;
	
	z_stream strm;
	
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
	strm.opaque = Z_NULL;
	strm.total_out = 0;
	strm.next_in=(Bytef *)[self bytes];
	strm.avail_in = (uInt)[self length];
	
	if (deflateInit(&strm, compressionLevel) != Z_OK) return nil;
	
	NSMutableData *compressed = [NSMutableData dataWithLength:16384];  // 16K chuncks for expansion
	
	do {
		
		if (strm.total_out >= [compressed length])
			[compressed increaseLengthBy: 16384];
		
		strm.next_out = [compressed mutableBytes] + strm.total_out;
		strm.avail_out = (uInt)( [compressed length] - strm.total_out );
		
		deflate(&strm, Z_FINISH);
		
	} while (strm.avail_out == 0);
	
	deflateEnd(&strm);
	
	[compressed setLength: strm.total_out];
  return [NSData dataWithData: compressed];
}
  
  
  @end



// In bytes
#define FileHashDefaultChunkSizeForReadingData 4096

// Function
CFStringRef FileMD5HashCreateWithPath(CFStringRef filePath,
                                      size_t chunkSizeForReadingData) {
  
  // Declare needed variables
  CFStringRef result = NULL;
  CFReadStreamRef readStream = NULL;
  
  // Get the file URL
  CFURLRef fileURL =
  CFURLCreateWithFileSystemPath(kCFAllocatorDefault,
                                (CFStringRef)filePath,
                                kCFURLPOSIXPathStyle,
                                (Boolean)false);
  if (!fileURL) goto done;
  
  // Create and open the read stream
  readStream = CFReadStreamCreateWithFile(kCFAllocatorDefault,
                                          (CFURLRef)fileURL);
  if (!readStream) goto done;
  bool didSucceed = (bool)CFReadStreamOpen(readStream);
  if (!didSucceed) goto done;
  
  // Initialize the hash object
  CC_MD5_CTX hashObject;
  CC_MD5_Init(&hashObject);
  
  // Make sure chunkSizeForReadingData is valid
  if (!chunkSizeForReadingData) {
    chunkSizeForReadingData = FileHashDefaultChunkSizeForReadingData;
  }
  
  // Feed the data to the hash object
  bool hasMoreData = true;
  while (hasMoreData) {
    uint8_t buffer[chunkSizeForReadingData];
    CFIndex readBytesCount = CFReadStreamRead(readStream,
                                              (UInt8 *)buffer,
                                              (CFIndex)sizeof(buffer));
    if (readBytesCount == -1) break;
    if (readBytesCount == 0) {
      hasMoreData = false;
      continue;
    }
    CC_MD5_Update(&hashObject,
                  (const void *)buffer,
                  (CC_LONG)readBytesCount);
  }
  
  // Check if the read operation succeeded
  didSucceed = !hasMoreData;
  
  // Compute the hash digest
  unsigned char digest[CC_MD5_DIGEST_LENGTH];
  CC_MD5_Final(digest, &hashObject);
  
  // Abort if the read operation failed
  if (!didSucceed) goto done;
  
  // Compute the string result
  char hash[2 * sizeof(digest) + 1];
  for (size_t i = 0; i < sizeof(digest); ++i) {
    snprintf(hash + (2 * i), 3, "%02x", (int)(digest[i]));
  }
  result = CFStringCreateWithCString(kCFAllocatorDefault,
                                     (const char *)hash,
                                     kCFStringEncodingUTF8);
  
done:
  
  if (readStream) {
    CFReadStreamClose(readStream);
    CFRelease(readStream);
  }
  if (fileURL) {
    CFRelease(fileURL);
  }
  return result;
}


@implementation NSString (MD5)
  
- (NSString *)_MD5String {
  NSString* base64;
  
  @autoreleasepool {
    const char *cstr = [self UTF8String];
    unsigned char result[16];
    CC_MD5(cstr, (CC_LONG)strlen(cstr), result);
    NSData* data = [[NSData alloc] initWithBytes:result length:16];
    base64 = [data base64EncodedStringWithOptions:0];
    //NSLog(@"data %@", data);
    
  }
  NSString* str = [base64 stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
  str = [str stringByReplacingOccurrencesOfString:@"=" withString:@""];
  
  
  return str;
}
  
+ (NSString *)MD5StringFromFileAt:(NSURL*)url {
  NSString *filePath = url.path;
  CFStringRef md5hash =
  FileMD5HashCreateWithPath((__bridge CFStringRef)filePath,
                            FileHashDefaultChunkSizeForReadingData);
  NSString* md5 =  (__bridge NSString *)md5hash;
  //NSLog(@"MD5 hash of file at path \"%@\": %@", filePath, md5);
  CFRelease(md5hash);
  
  return md5;
}
  
  @end
